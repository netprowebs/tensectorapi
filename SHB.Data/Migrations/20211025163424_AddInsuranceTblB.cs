﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class AddInsuranceTblB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InsurancePolicyClaim",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    BrokerClaimCode = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: true),
                    InsurancePolicyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PolicyBrokerCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AccidentDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AccidentDetails = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CustomerReportDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BrokerUnderwriterDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CustomerEstimate = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    AdjusterEstimate = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    DocSumbitCount = table.Column<bool>(type: "bit", nullable: false),
                    Verified = table.Column<bool>(type: "bit", nullable: false),
                    VerifiedBy = table.Column<int>(type: "int", nullable: true),
                    VerifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Approved = table.Column<bool>(type: "bit", nullable: false),
                    ApprovedBy = table.Column<int>(type: "int", nullable: true),
                    ApprovedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Posted = table.Column<bool>(type: "bit", nullable: false),
                    PostedBy = table.Column<int>(type: "int", nullable: true),
                    PostedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Void = table.Column<bool>(type: "bit", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InsurancePolicyClaim", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InsurancePolicyClaim_InsurancePolicy_InsurancePolicyId",
                        column: x => x.InsurancePolicyId,
                        principalTable: "InsurancePolicy",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InsurPolicyBrokers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InsurancePolicyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    BrokerCode = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: true),
                    BrokersCompID = table.Column<int>(type: "int", nullable: false),
                    UnderWriterPolicyNo = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: true),
                    Apportionment = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    BrokerCommission = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    BrokerCommissionRate = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    VATRate = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    InsuranceRate = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    VATDue = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    NetDue = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    ActualPolicyBroker = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InsurPolicyBrokers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InsurPolicyBrokers_InsurancePolicy_InsurancePolicyId",
                        column: x => x.InsurancePolicyId,
                        principalTable: "InsurancePolicy",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InsurPolicyUnderwriters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InsurancePolicyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    BrokerCode = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: true),
                    InsuranceCompID = table.Column<int>(type: "int", nullable: false),
                    UnderWriterPolicyNo = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: true),
                    Apportionment = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    InsuranceRate = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    ActualPolicyInsurance = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InsurPolicyUnderwriters", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InsurPolicyUnderwriters_InsurancePolicy_InsurancePolicyId",
                        column: x => x.InsurancePolicyId,
                        principalTable: "InsurancePolicy",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InsurancePolicyClaim_InsurancePolicyId",
                table: "InsurancePolicyClaim",
                column: "InsurancePolicyId");

            migrationBuilder.CreateIndex(
                name: "IX_InsurPolicyBrokers_InsurancePolicyId",
                table: "InsurPolicyBrokers",
                column: "InsurancePolicyId");

            migrationBuilder.CreateIndex(
                name: "IX_InsurPolicyUnderwriters_InsurancePolicyId",
                table: "InsurPolicyUnderwriters",
                column: "InsurancePolicyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InsurancePolicyClaim");

            migrationBuilder.DropTable(
                name: "InsurPolicyBrokers");

            migrationBuilder.DropTable(
                name: "InsurPolicyUnderwriters");
        }
    }
}
