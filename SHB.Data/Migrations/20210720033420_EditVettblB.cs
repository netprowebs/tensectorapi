﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class EditVettblB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CalcType",
                table: "VetServices");

            migrationBuilder.AddColumn<decimal>(
                name: "AdsPrice",
                table: "VetBundles",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdsPrice",
                table: "VetBundles");

            migrationBuilder.AddColumn<int>(
                name: "CalcType",
                table: "VetServices",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
