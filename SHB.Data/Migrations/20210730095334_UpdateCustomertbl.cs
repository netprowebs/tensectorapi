﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class UpdateCustomertbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApprovalDate",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "ApprovedBy",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "ApprovedDate",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "CreditRating",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "CustomerAddress1",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "CustomerEmail",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "CustomerFirstName",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "CustomerLastName",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "CustomerName",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "CustomerPhone",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "CustomerShipForId",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "CustomerShipToId",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "CustomerSpecialInstructions",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "PriceMatrix",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "PriceMatrixCurrent",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "ReferalURL",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "ReferedBy",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "ReferedDate",
                table: "CustomerInformations");

            migrationBuilder.RenameColumn(
                name: "StatementCycleCode",
                table: "CustomerInformations",
                newName: "Otp");

            migrationBuilder.RenameColumn(
                name: "ShipMethodID",
                table: "CustomerInformations",
                newName: "CustomerCode");

            migrationBuilder.RenameColumn(
                name: "CompanyID",
                table: "CustomerInformations",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "Approved",
                table: "CustomerInformations",
                newName: "OtpIsUsed");

            migrationBuilder.RenameColumn(
                name: "AccountStatus",
                table: "CustomerInformations",
                newName: "OtpNoOfTimeUsed");

            migrationBuilder.AddColumn<int>(
                name: "DeviceType",
                table: "CustomerInformations",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "OTPLastUsedDate",
                table: "CustomerInformations",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HostingUrl",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SmsUrl",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TenantUrl",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CustomerInformations_UserId",
                table: "CustomerInformations",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerInformations_AspNetUsers_UserId",
                table: "CustomerInformations",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerInformations_AspNetUsers_UserId",
                table: "CustomerInformations");

            migrationBuilder.DropIndex(
                name: "IX_CustomerInformations_UserId",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "DeviceType",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "OTPLastUsedDate",
                table: "CustomerInformations");

            migrationBuilder.DropColumn(
                name: "HostingUrl",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "SmsUrl",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "TenantUrl",
                table: "CompanyInfo");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "CustomerInformations",
                newName: "CompanyID");

            migrationBuilder.RenameColumn(
                name: "OtpNoOfTimeUsed",
                table: "CustomerInformations",
                newName: "AccountStatus");

            migrationBuilder.RenameColumn(
                name: "OtpIsUsed",
                table: "CustomerInformations",
                newName: "Approved");

            migrationBuilder.RenameColumn(
                name: "Otp",
                table: "CustomerInformations",
                newName: "StatementCycleCode");

            migrationBuilder.RenameColumn(
                name: "CustomerCode",
                table: "CustomerInformations",
                newName: "ShipMethodID");

            migrationBuilder.AddColumn<DateTime>(
                name: "ApprovalDate",
                table: "CustomerInformations",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ApprovedBy",
                table: "CustomerInformations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ApprovedDate",
                table: "CustomerInformations",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreditRating",
                table: "CustomerInformations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerAddress1",
                table: "CustomerInformations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerEmail",
                table: "CustomerInformations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerFirstName",
                table: "CustomerInformations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerLastName",
                table: "CustomerInformations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerName",
                table: "CustomerInformations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerPhone",
                table: "CustomerInformations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerShipForId",
                table: "CustomerInformations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerShipToId",
                table: "CustomerInformations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerSpecialInstructions",
                table: "CustomerInformations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PriceMatrix",
                table: "CustomerInformations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PriceMatrixCurrent",
                table: "CustomerInformations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReferalURL",
                table: "CustomerInformations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReferedBy",
                table: "CustomerInformations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ReferedDate",
                table: "CustomerInformations",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
