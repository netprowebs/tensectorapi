﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class PlatformTypeCol : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PlatformType",
                table: "RequisitionHeaders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PlatformType",
                table: "InventoryTransferHeaders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "TransferDate",
                table: "InventoryTransferHeaders",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PlatformType",
                table: "InventoryReceivedHeaders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "RecieveDate",
                table: "InventoryReceivedHeaders",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PlatformType",
                table: "InventoryLedgers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "RecieveDate",
                table: "InventoryLedgers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PlatformType",
                table: "RequisitionHeaders");

            migrationBuilder.DropColumn(
                name: "PlatformType",
                table: "InventoryTransferHeaders");

            migrationBuilder.DropColumn(
                name: "TransferDate",
                table: "InventoryTransferHeaders");

            migrationBuilder.DropColumn(
                name: "PlatformType",
                table: "InventoryReceivedHeaders");

            migrationBuilder.DropColumn(
                name: "RecieveDate",
                table: "InventoryReceivedHeaders");

            migrationBuilder.DropColumn(
                name: "PlatformType",
                table: "InventoryLedgers");

            migrationBuilder.DropColumn(
                name: "RecieveDate",
                table: "InventoryLedgers");
        }
    }
}
