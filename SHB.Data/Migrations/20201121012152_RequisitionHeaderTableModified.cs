﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class RequisitionHeaderTableModified : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IssuedByIssuedDate",
                table: "RequisitionHeaders");

            migrationBuilder.AddColumn<string>(
                name: "IssuedBy",
                table: "RequisitionHeaders",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "IssuedDate",
                table: "RequisitionHeaders",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IssuedBy",
                table: "RequisitionHeaders");

            migrationBuilder.DropColumn(
                name: "IssuedDate",
                table: "RequisitionHeaders");

            migrationBuilder.AddColumn<DateTime>(
                name: "IssuedByIssuedDate",
                table: "RequisitionHeaders",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
