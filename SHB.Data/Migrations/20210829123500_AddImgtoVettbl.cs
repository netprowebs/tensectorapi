﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class AddImgtoVettbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "VetGeoLink",
                table: "VetteeLists",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VetGuardians",
                table: "VetteeLists",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VetOwnerStatus",
                table: "VetteeLists",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VetPeriodOfstay",
                table: "VetteeLists",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VetPhotoUpload",
                table: "VetteeLists",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VetVideoUpload",
                table: "VetteeLists",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VettersRemarks",
                table: "VetteeLists",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "ImageFiles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FileReference",
                table: "ImageFiles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UploadFormat",
                table: "ImageFiles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "UploadPath",
                table: "ImageFiles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UploadType",
                table: "ImageFiles",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "VetGeoLink",
                table: "VetteeLists");

            migrationBuilder.DropColumn(
                name: "VetGuardians",
                table: "VetteeLists");

            migrationBuilder.DropColumn(
                name: "VetOwnerStatus",
                table: "VetteeLists");

            migrationBuilder.DropColumn(
                name: "VetPeriodOfstay",
                table: "VetteeLists");

            migrationBuilder.DropColumn(
                name: "VetPhotoUpload",
                table: "VetteeLists");

            migrationBuilder.DropColumn(
                name: "VetVideoUpload",
                table: "VetteeLists");

            migrationBuilder.DropColumn(
                name: "VettersRemarks",
                table: "VetteeLists");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "ImageFiles");

            migrationBuilder.DropColumn(
                name: "FileReference",
                table: "ImageFiles");

            migrationBuilder.DropColumn(
                name: "UploadFormat",
                table: "ImageFiles");

            migrationBuilder.DropColumn(
                name: "UploadPath",
                table: "ImageFiles");

            migrationBuilder.DropColumn(
                name: "UploadType",
                table: "ImageFiles");
        }
    }
}
