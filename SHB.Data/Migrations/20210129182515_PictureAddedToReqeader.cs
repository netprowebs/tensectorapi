﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class PictureAddedToReqeader : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Picture",
                table: "RequisitionHeaders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Picture",
                table: "RequisitionHeaders");
        }
    }
}
