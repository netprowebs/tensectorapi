﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class W1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_AspNetUsers_Wallets_WalletId1",
            //    table: "AspNetUsers");

            //migrationBuilder.AlterColumn<int>(
            //    name: "WalletId1",
            //    table: "AspNetUsers",
            //    type: "int",
            //    nullable: false,
            //    defaultValue: 0,
            //    oldClrType: typeof(int),
            //    oldType: "int",
            //    oldNullable: true);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_AspNetUsers_Wallets_WalletId1",
            //    table: "AspNetUsers",
            //    column: "WalletId1",
            //    principalTable: "Wallets",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddColumn<int>(
           name: "WalletId1",
           table: "AspNetUsers",
           type: "int",
           nullable: false,
           defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Wallets_WalletId1",
                table: "AspNetUsers");

            migrationBuilder.AlterColumn<int>(
                name: "WalletId1",
                table: "AspNetUsers",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Wallets_WalletId1",
                table: "AspNetUsers",
                column: "WalletId1",
                principalTable: "Wallets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
