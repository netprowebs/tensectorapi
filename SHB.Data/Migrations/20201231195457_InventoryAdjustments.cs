﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class InventoryAdjustments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InventoryAdjustmentsDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    AdjustmentID = table.Column<Guid>(nullable: false),
                    ItemID = table.Column<string>(nullable: true),
                    WarehouseID = table.Column<string>(nullable: true),
                    WarehouseBinID = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    OriginalQuantity = table.Column<string>(nullable: true),
                    AdjustedQuantity = table.Column<string>(nullable: true),
                    CurrentID = table.Column<string>(nullable: true),
                    CurrentExchangeRate = table.Column<string>(nullable: true),
                    Cost = table.Column<string>(nullable: true),
                    GLAdjustmentPostingAccount = table.Column<string>(nullable: true),
                    ProjectID = table.Column<string>(nullable: true),
                    UnitCost = table.Column<string>(nullable: true),
                    GLAnalysisType1 = table.Column<string>(nullable: true),
                    AssetID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryAdjustmentsDetails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "InventoryAdjustmentsHeaders",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    AdjustmentDate = table.Column<DateTime>(nullable: true),
                    Reason = table.Column<string>(nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    AdjustmentPostToGL = table.Column<string>(nullable: true),
                    SupervisorSignature = table.Column<string>(nullable: true),
                    Total = table.Column<string>(nullable: true),
                    IsCountAdjust = table.Column<bool>(nullable: true),
                    CurrencyID = table.Column<string>(nullable: true),
                    CurrencyExchangeRate = table.Column<string>(nullable: true),
                    Void = table.Column<bool>(nullable: false),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: true),
                    Issued = table.Column<bool>(nullable: false),
                    IssuedBy = table.Column<string>(nullable: true),
                    IssuedDate = table.Column<DateTime>(nullable: true),
                    Approved = table.Column<bool>(nullable: false),
                    ApprovedBy = table.Column<string>(nullable: true),
                    ApprovedDate = table.Column<DateTime>(nullable: false),
                    Posted = table.Column<bool>(nullable: false),
                    PostedBy = table.Column<string>(nullable: true),
                    PostedDate = table.Column<DateTime>(nullable: false),
                    Cancelled = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryAdjustmentsHeaders", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InventoryAdjustmentsDetails");

            migrationBuilder.DropTable(
                name: "InventoryAdjustmentsHeaders");
        }
    }
}
