﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class RequisitionHeadersDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RequisitionHeaders",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    AdjustmentTypeID = table.Column<int>(nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    WarehouseID = table.Column<int>(nullable: true),
                    WarehouseBinID = table.Column<int>(nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    Captured = table.Column<bool>(nullable: false),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false),
                    Issued = table.Column<bool>(nullable: false),
                    IssuedByIssuedDate = table.Column<DateTime>(nullable: false),
                    Approved = table.Column<bool>(nullable: false),
                    ApprovedBy = table.Column<string>(nullable: true),
                    ApprovedDate = table.Column<DateTime>(nullable: false),
                    Posted = table.Column<bool>(nullable: false),
                    PostedBy = table.Column<string>(nullable: true),
                    PostedDate = table.Column<DateTime>(nullable: true),
                    BatchControlNumber = table.Column<string>(nullable: true),
                    BatchControlTotal = table.Column<string>(nullable: true),
                    Signature = table.Column<string>(nullable: true),
                    SignaturePassword = table.Column<string>(nullable: true),
                    SupervisorSignature = table.Column<string>(nullable: true),
                    CustomerName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequisitionHeaders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RequisitionDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    RequisitionID = table.Column<Guid>(nullable: false),
                    RequisitionHeaderId = table.Column<Guid>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    ItemID = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    RequestedQty = table.Column<string>(nullable: true),
                    WarehouseID = table.Column<int>(nullable: true),
                    WarehouseBinID = table.Column<int>(nullable: true),
                    ToWarehouseID = table.Column<int>(nullable: true),
                    ToWarehouseBinID = table.Column<int>(nullable: true),
                    GLExpenseAccount = table.Column<string>(nullable: true),
                    ItemValue = table.Column<decimal>(nullable: false),
                    ItemCost = table.Column<decimal>(nullable: false),
                    CostMethod = table.Column<int>(nullable: false),
                    PONumber = table.Column<string>(nullable: true),
                    ItemUPCCode = table.Column<string>(nullable: true),
                    OrderQty = table.Column<string>(nullable: true),
                    IssuedQty = table.Column<string>(nullable: true),
                    ProjectID = table.Column<string>(nullable: true),
                    GLAnalysisType1 = table.Column<string>(nullable: true),
                    GLAnalysisType2 = table.Column<string>(nullable: true),
                    AssetID = table.Column<string>(nullable: true),
                    TaxGroupID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequisitionDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RequisitionDetails_RequisitionHeaders_RequisitionHeaderId",
                        column: x => x.RequisitionHeaderId,
                        principalTable: "RequisitionHeaders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RequisitionDetails_RequisitionHeaderId",
                table: "RequisitionDetails",
                column: "RequisitionHeaderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RequisitionDetails");

            migrationBuilder.DropTable(
                name: "RequisitionHeaders");
        }
    }
}
