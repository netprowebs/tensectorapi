﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class InventoryTransferDetailTableModified : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ItemValueItemCost",
                table: "InventoryTransferDetails");

            migrationBuilder.AddColumn<decimal>(
                name: "ItemCost",
                table: "InventoryTransferDetails",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ItemValue",
                table: "InventoryTransferDetails",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ItemCost",
                table: "InventoryTransferDetails");

            migrationBuilder.DropColumn(
                name: "ItemValue",
                table: "InventoryTransferDetails");

            migrationBuilder.AddColumn<decimal>(
                name: "ItemValueItemCost",
                table: "InventoryTransferDetails",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
