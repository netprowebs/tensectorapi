﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class AddColVettran : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AssignedTO",
                table: "VettersTransactions",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AssignedTO",
                table: "VettersTransactions");
        }
    }
}
