﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class AlltransportTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BookingDaysRange",
                table: "CompanyInfo",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "CompanyInfo",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsTranspRecieved",
                table: "CompanyInfo",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Complaint",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    ComplaintType = table.Column<int>(nullable: false),
                    PriorityLevel = table.Column<int>(nullable: false),
                    BookingReference = table.Column<string>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    TransDate = table.Column<DateTime>(nullable: false),
                    Responded = table.Column<bool>(nullable: false),
                    RepliedMessage = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Complaint", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Coupon",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    CouponCode = table.Column<string>(nullable: true),
                    CouponValue = table.Column<decimal>(nullable: false),
                    CouponType = table.Column<int>(nullable: false),
                    Validity = table.Column<bool>(nullable: false),
                    DurationType = table.Column<int>(nullable: false),
                    Duration = table.Column<int>(nullable: false),
                    PhoneNumber = table.Column<string>(nullable: true),
                    IsUsed = table.Column<bool>(nullable: false),
                    DateUsed = table.Column<DateTime>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coupon", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Discounts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    BookingType = table.Column<int>(nullable: false),
                    AdultDiscount = table.Column<decimal>(nullable: false),
                    MinorDiscount = table.Column<decimal>(nullable: false),
                    MemberDiscount = table.Column<decimal>(nullable: false),
                    ReturnDiscount = table.Column<decimal>(nullable: false),
                    AppDiscountIos = table.Column<decimal>(nullable: false),
                    AppDiscountAndroid = table.Column<decimal>(nullable: false),
                    AppDiscountWeb = table.Column<decimal>(nullable: false),
                    AppReturnDiscountIos = table.Column<decimal>(nullable: false),
                    AppReturnDiscountAndroid = table.Column<decimal>(nullable: false),
                    AppReturnDiscountWeb = table.Column<decimal>(nullable: false),
                    PromoDiscount = table.Column<decimal>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    CustomerDiscount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Discounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ExcludedSeats",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SeatNumber = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExcludedSeats", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HirePassengers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    PhoneNo = table.Column<string>(nullable: true),
                    NokName = table.Column<string>(nullable: true),
                    NokPhone = table.Column<string>(nullable: true),
                    HireBusId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HirePassengers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MtuReportModel",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    VehicleId = table.Column<string>(nullable: true),
                    RegistrationNumber = table.Column<string>(nullable: true),
                    DriverId = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    Picture = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MtuReportModel", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PaymentResponses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Reference = table.Column<string>(nullable: true),
                    ApprovedAmount = table.Column<int>(nullable: false),
                    AuthorizationCode = table.Column<string>(nullable: true),
                    CardType = table.Column<string>(nullable: true),
                    Last4 = table.Column<string>(nullable: true),
                    Reusable = table.Column<bool>(nullable: false),
                    Bank = table.Column<string>(nullable: true),
                    ExpireMonth = table.Column<string>(nullable: true),
                    ExpireYear = table.Column<string>(nullable: true),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    Channel = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentResponses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Routes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    DispatchFee = table.Column<decimal>(nullable: false),
                    DriverFee = table.Column<decimal>(nullable: false),
                    LoaderFee = table.Column<decimal>(nullable: false),
                    AvailableAtTerminal = table.Column<bool>(nullable: false),
                    AvailableOnline = table.Column<bool>(nullable: false),
                    ParentRouteId = table.Column<int>(nullable: true),
                    ParentRoute = table.Column<string>(nullable: true),
                    DepartureTerminalId = table.Column<int>(nullable: false),
                    DestinationTerminalId = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Routes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Routes_Terminals_DepartureTerminalId",
                        column: x => x.DepartureTerminalId,
                        principalTable: "Terminals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Routes_Terminals_DestinationTerminalId",
                        column: x => x.DestinationTerminalId,
                        principalTable: "Terminals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "SMSProfile",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AppName = table.Column<string>(maxLength: 50, nullable: false),
                    SMSMinQty = table.Column<double>(nullable: true),
                    ConfirmEmail = table.Column<bool>(nullable: true),
                    SmtpAddress = table.Column<string>(maxLength: 50, nullable: true),
                    Port = table.Column<string>(maxLength: 50, nullable: true),
                    Username = table.Column<string>(maxLength: 50, nullable: true),
                    Password = table.Column<string>(maxLength: 50, nullable: true),
                    Profile = table.Column<string>(maxLength: 50, nullable: true),
                    SMSUserName = table.Column<string>(maxLength: 50, nullable: true),
                    SMSPassword = table.Column<string>(maxLength: 50, nullable: true),
                    SMSsubUserName = table.Column<string>(maxLength: 50, nullable: true),
                    IsActive = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SMSProfile", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VehicleAllocationDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    DriverId = table.Column<int>(nullable: true),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    VehicleId = table.Column<int>(nullable: false),
                    VehicleName = table.Column<string>(nullable: true),
                    DestinationTerminal = table.Column<int>(nullable: true),
                    UserEmail = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleAllocationDetail", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VehicleMakes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleMakes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VehicleMileage",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    VehicleRegistrationNumber = table.Column<string>(nullable: true),
                    ServiceLevel = table.Column<int>(nullable: false),
                    CurrentMileage = table.Column<int>(nullable: false),
                    LastServiceDate = table.Column<DateTime>(nullable: true),
                    DateDue = table.Column<DateTime>(nullable: true),
                    IsDue = table.Column<bool>(nullable: false),
                    IsDeactivated = table.Column<bool>(nullable: false),
                    NotificationLevel = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleMileage", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Workshop",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    VehicleStatus = table.Column<int>(nullable: false),
                    VehicleId = table.Column<int>(nullable: false),
                    VehicleLocationId = table.Column<int>(nullable: false),
                    WorkshopLocationId = table.Column<int>(nullable: false),
                    WorkshopStatus = table.Column<bool>(nullable: false),
                    ReleaseDate = table.Column<DateTime>(nullable: true),
                    ReleaseUserId = table.Column<int>(nullable: true),
                    WorkshopNote = table.Column<string>(nullable: true),
                    ReleaseNote = table.Column<string>(nullable: true),
                    ReleaseLocationId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Workshop", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SubRoutes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    NameId = table.Column<int>(nullable: false),
                    RouteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubRoutes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubRoutes_Routes_RouteId",
                        column: x => x.RouteId,
                        principalTable: "Routes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleModels",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    NumberOfSeats = table.Column<int>(nullable: false),
                    VehicleModelTypeCode = table.Column<string>(nullable: true),
                    VehicleMakeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleModels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleModels_VehicleMakes_VehicleMakeId",
                        column: x => x.VehicleMakeId,
                        principalTable: "VehicleMakes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FareCalendars",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    FareType = table.Column<int>(nullable: false),
                    FareAdjustmentType = table.Column<int>(nullable: false),
                    FareParameterType = table.Column<int>(nullable: false),
                    FareValue = table.Column<decimal>(nullable: false),
                    RouteId = table.Column<int>(nullable: true),
                    BookingTypes = table.Column<int>(nullable: true),
                    TerminalId = table.Column<int>(nullable: true),
                    VehicleModelId = table.Column<int>(nullable: true),
                    Monday = table.Column<bool>(nullable: true),
                    Tuesday = table.Column<bool>(nullable: true),
                    Wednesday = table.Column<bool>(nullable: true),
                    Thursday = table.Column<bool>(nullable: true),
                    Friday = table.Column<bool>(nullable: true),
                    Saturday = table.Column<bool>(nullable: true),
                    Sunday = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FareCalendars", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FareCalendars_Routes_RouteId",
                        column: x => x.RouteId,
                        principalTable: "Routes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FareCalendars_Terminals_TerminalId",
                        column: x => x.TerminalId,
                        principalTable: "Terminals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FareCalendars_VehicleModels_VehicleModelId",
                        column: x => x.VehicleModelId,
                        principalTable: "VehicleModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Fares",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DepartureTime = table.Column<string>(nullable: true),
                    TripCode = table.Column<string>(nullable: true),
                    AvailableOnline = table.Column<bool>(nullable: false),
                    ParentRouteDepartureTime = table.Column<string>(nullable: true),
                    RouteId = table.Column<int>(nullable: false),
                    VehicleModelId = table.Column<int>(nullable: true),
                    ParentRouteId = table.Column<int>(nullable: true),
                    ParentTripId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fares", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Fares_Routes_RouteId",
                        column: x => x.RouteId,
                        principalTable: "Routes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Fares_VehicleModels_VehicleModelId",
                        column: x => x.VehicleModelId,
                        principalTable: "VehicleModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Trips",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DepartureTime = table.Column<string>(nullable: true),
                    TripCode = table.Column<string>(nullable: true),
                    AvailableOnline = table.Column<bool>(nullable: false),
                    ParentRouteDepartureTime = table.Column<string>(nullable: true),
                    RouteId = table.Column<int>(nullable: false),
                    VehicleModelId = table.Column<int>(nullable: true),
                    ParentRouteId = table.Column<int>(nullable: true),
                    ParentTripId = table.Column<Guid>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trips", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Trips_Routes_RouteId",
                        column: x => x.RouteId,
                        principalTable: "Routes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Trips_VehicleModels_VehicleModelId",
                        column: x => x.VehicleModelId,
                        principalTable: "VehicleModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Vehicles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    RegistrationNumber = table.Column<string>(nullable: true),
                    ChasisNumber = table.Column<string>(nullable: true),
                    EngineNumber = table.Column<string>(nullable: true),
                    IMEINumber = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    VehicleModelId = table.Column<int>(nullable: false),
                    LocationId = table.Column<int>(nullable: true),
                    IsOperational = table.Column<bool>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: true),
                    PurchaseDate = table.Column<DateTime>(nullable: true),
                    LicenseDate = table.Column<DateTime>(nullable: true),
                    InsuranceDate = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    LicenseExpiration = table.Column<DateTime>(nullable: true),
                    InsuranceExpiration = table.Column<DateTime>(nullable: true),
                    RoadWorthinessDate = table.Column<DateTime>(nullable: true),
                    RoadWorthinessExpiration = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Vehicles_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Vehicles_Terminals_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Terminals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Vehicles_VehicleModels_VehicleModelId",
                        column: x => x.VehicleModelId,
                        principalTable: "VehicleModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PickupPoint",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    PickupTime = table.Column<string>(nullable: true),
                    Latitude = table.Column<string>(nullable: true),
                    Longitude = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    TripId = table.Column<Guid>(nullable: false),
                    TripId1 = table.Column<int>(nullable: true),
                    TerminalId = table.Column<int>(nullable: true),
                    RouteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PickupPoint", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PickupPoint_Routes_RouteId",
                        column: x => x.RouteId,
                        principalTable: "Routes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PickupPoint_Terminals_TerminalId",
                        column: x => x.TerminalId,
                        principalTable: "Terminals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PickupPoint_Trips_TripId1",
                        column: x => x.TripId1,
                        principalTable: "Trips",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VehicleTripRegistrations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    PhysicalBusRegistrationNumber = table.Column<string>(nullable: true),
                    DepartureDate = table.Column<DateTime>(nullable: false),
                    IsVirtualBus = table.Column<bool>(nullable: false),
                    IsBusFull = table.Column<bool>(nullable: false),
                    IsBlownBus = table.Column<bool>(nullable: false),
                    ManifestPrinted = table.Column<bool>(nullable: false),
                    DriverCode = table.Column<string>(nullable: true),
                    OriginalDriverCode = table.Column<string>(nullable: true),
                    JourneyType = table.Column<int>(nullable: false),
                    TripId = table.Column<Guid>(nullable: false),
                    TripId1 = table.Column<int>(nullable: true),
                    VehicleModelId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleTripRegistrations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleTripRegistrations_Trips_TripId1",
                        column: x => x.TripId1,
                        principalTable: "Trips",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleTripRegistrations_VehicleModels_VehicleModelId",
                        column: x => x.VehicleModelId,
                        principalTable: "VehicleModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HireBus",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    DepartureAddress = table.Column<string>(nullable: true),
                    DestinationAddress = table.Column<string>(nullable: true),
                    Amount = table.Column<decimal>(nullable: false),
                    VehicleId = table.Column<int>(nullable: false),
                    DriverCode = table.Column<string>(nullable: true),
                    DepartureDate = table.Column<DateTime>(nullable: false),
                    VehicleTripRegistrationId = table.Column<Guid>(nullable: false),
                    UserLocationId = table.Column<int>(nullable: false),
                    DepartureTerminalId = table.Column<int>(nullable: false),
                    IsManifestPrinted = table.Column<bool>(nullable: true),
                    ArrivalDate = table.Column<DateTime>(nullable: true),
                    ReceivedLocationId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HireBus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HireBus_Vehicles_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HireRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    NextOfKinName = table.Column<string>(nullable: true),
                    NextOfKinPhoneNumber = table.Column<string>(nullable: true),
                    NumberOfBuses = table.Column<int>(nullable: false),
                    RequestDate = table.Column<DateTime>(nullable: false),
                    DepartureDate = table.Column<DateTime>(nullable: false),
                    VehicleId = table.Column<int>(nullable: true),
                    Departure = table.Column<string>(nullable: true),
                    Destination = table.Column<string>(nullable: true),
                    AdditionalRequest = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HireRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HireRequests_Vehicles_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "JourneyManagements",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    ActualTripStartTime = table.Column<DateTime>(nullable: true),
                    TripStartTime = table.Column<DateTime>(nullable: true),
                    TripEndTime = table.Column<DateTime>(nullable: true),
                    TransloadedJourneyId = table.Column<Guid>(nullable: true),
                    JourneyDate = table.Column<DateTime>(nullable: false),
                    Isapproved = table.Column<bool>(nullable: false),
                    ApprovedBy = table.Column<string>(nullable: true),
                    ApprovedDate = table.Column<DateTime>(nullable: false),
                    IsReceived = table.Column<bool>(nullable: false),
                    ReceivedBy = table.Column<string>(nullable: true),
                    ReceivedDate = table.Column<DateTime>(nullable: false),
                    DispatchFee = table.Column<decimal>(nullable: false),
                    CaptainFee = table.Column<decimal>(nullable: false),
                    LoaderFee = table.Column<decimal>(nullable: false),
                    VehicleTripRegistrationId = table.Column<Guid>(nullable: false),
                    JourneyStatus = table.Column<int>(nullable: false),
                    DenialReason = table.Column<string>(nullable: true),
                    JourneyType = table.Column<int>(nullable: false),
                    CaptainTripStatus = table.Column<int>(nullable: false),
                    TransloadedBy = table.Column<string>(nullable: true),
                    TransloadedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JourneyManagements", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JourneyManagements_VehicleTripRegistrations_VehicleTripRegistrationId",
                        column: x => x.VehicleTripRegistrationId,
                        principalTable: "VehicleTripRegistrations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SeatManagements",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    SeatNumber = table.Column<byte>(nullable: false),
                    RemainingSeat = table.Column<int>(nullable: false),
                    BookingReferenceCode = table.Column<string>(nullable: true),
                    MainBookerReferenceCode = table.Column<string>(nullable: true),
                    RescheduleReferenceCode = table.Column<string>(nullable: true),
                    RerouteReferenceCode = table.Column<string>(nullable: true),
                    BookingDate = table.Column<DateTime>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    NextOfKinName = table.Column<string>(nullable: true),
                    Amount = table.Column<decimal>(nullable: true),
                    PartCash = table.Column<decimal>(nullable: true),
                    POSReference = table.Column<string>(nullable: true),
                    RerouteFeeDiff = table.Column<decimal>(nullable: true),
                    UpgradeDowngradeDiff = table.Column<decimal>(nullable: true),
                    Discount = table.Column<decimal>(nullable: false),
                    NextOfKinPhoneNumber = table.Column<string>(nullable: true),
                    PickupPointImage = table.Column<string>(nullable: true),
                    ReschedulePayStackResponse = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    LuggageType = table.Column<string>(nullable: true),
                    Rated = table.Column<bool>(nullable: false),
                    Gender = table.Column<int>(nullable: false),
                    PassengerType = table.Column<int>(nullable: false),
                    PaymentMethod = table.Column<int>(nullable: false),
                    BookingStatus = table.Column<int>(nullable: false),
                    PickupStatus = table.Column<int>(nullable: false),
                    BookingType = table.Column<int>(nullable: false),
                    TravelStatus = table.Column<int>(nullable: false),
                    UpgradeType = table.Column<int>(nullable: false),
                    RescheduleStatus = table.Column<int>(nullable: false),
                    RescheduleMode = table.Column<int>(nullable: false),
                    RerouteStatus = table.Column<int>(nullable: false),
                    RerouteMode = table.Column<int>(nullable: false),
                    RescheduleType = table.Column<int>(nullable: false),
                    RescheduleDate = table.Column<DateTime>(nullable: true),
                    IsPrinted = table.Column<bool>(nullable: false),
                    IsRescheduled = table.Column<bool>(nullable: false),
                    IsRerouted = table.Column<bool>(nullable: false),
                    IsUpgradeDowngrade = table.Column<bool>(nullable: false),
                    IsMainBooker = table.Column<bool>(nullable: false),
                    HasReturn = table.Column<bool>(nullable: false),
                    IsReturn = table.Column<bool>(nullable: false),
                    FromTransload = table.Column<bool>(nullable: false),
                    RouteId = table.Column<int>(nullable: true),
                    IsSub = table.Column<bool>(nullable: false),
                    IsSubReturn = table.Column<bool>(nullable: false),
                    OnlineSubRouteName = table.Column<string>(nullable: true),
                    PickUpPointId = table.Column<int>(nullable: true),
                    NoOfTicket = table.Column<int>(nullable: true),
                    SubRouteId = table.Column<int>(nullable: true),
                    VehicleTripRegistrationId = table.Column<Guid>(nullable: true),
                    VehicleModelId = table.Column<int>(nullable: true),
                    BookingId = table.Column<long>(nullable: true),
                    HasCoupon = table.Column<bool>(nullable: false),
                    CouponCode = table.Column<string>(nullable: true),
                    IsGhanaRoute = table.Column<bool>(nullable: false),
                    PassportType = table.Column<string>(nullable: true),
                    PassportId = table.Column<string>(nullable: true),
                    PlaceOfIssue = table.Column<string>(nullable: true),
                    IssuedDate = table.Column<DateTime>(nullable: true),
                    ExpiredDate = table.Column<DateTime>(nullable: true),
                    Nationality = table.Column<string>(nullable: true),
                    Vat = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeatManagements", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SeatManagements_PickupPoint_PickUpPointId",
                        column: x => x.PickUpPointId,
                        principalTable: "PickupPoint",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SeatManagements_Routes_RouteId",
                        column: x => x.RouteId,
                        principalTable: "Routes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SeatManagements_SubRoutes_SubRouteId",
                        column: x => x.SubRouteId,
                        principalTable: "SubRoutes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SeatManagements_VehicleModels_VehicleModelId",
                        column: x => x.VehicleModelId,
                        principalTable: "VehicleModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SeatManagements_VehicleTripRegistrations_VehicleTripRegistrationId",
                        column: x => x.VehicleTripRegistrationId,
                        principalTable: "VehicleTripRegistrations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FareCalendars_RouteId",
                table: "FareCalendars",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_FareCalendars_TerminalId",
                table: "FareCalendars",
                column: "TerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_FareCalendars_VehicleModelId",
                table: "FareCalendars",
                column: "VehicleModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Fares_RouteId",
                table: "Fares",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_Fares_VehicleModelId",
                table: "Fares",
                column: "VehicleModelId");

            migrationBuilder.CreateIndex(
                name: "IX_HireBus_VehicleId",
                table: "HireBus",
                column: "VehicleId");

            migrationBuilder.CreateIndex(
                name: "IX_HireRequests_VehicleId",
                table: "HireRequests",
                column: "VehicleId");

            migrationBuilder.CreateIndex(
                name: "IX_JourneyManagements_VehicleTripRegistrationId",
                table: "JourneyManagements",
                column: "VehicleTripRegistrationId");

            migrationBuilder.CreateIndex(
                name: "IX_PickupPoint_RouteId",
                table: "PickupPoint",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_PickupPoint_TerminalId",
                table: "PickupPoint",
                column: "TerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_PickupPoint_TripId1",
                table: "PickupPoint",
                column: "TripId1");

            migrationBuilder.CreateIndex(
                name: "IX_Routes_DepartureTerminalId",
                table: "Routes",
                column: "DepartureTerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_Routes_DestinationTerminalId",
                table: "Routes",
                column: "DestinationTerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_SeatManagements_PickUpPointId",
                table: "SeatManagements",
                column: "PickUpPointId");

            migrationBuilder.CreateIndex(
                name: "IX_SeatManagements_RouteId",
                table: "SeatManagements",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_SeatManagements_SubRouteId",
                table: "SeatManagements",
                column: "SubRouteId");

            migrationBuilder.CreateIndex(
                name: "IX_SeatManagements_VehicleModelId",
                table: "SeatManagements",
                column: "VehicleModelId");

            migrationBuilder.CreateIndex(
                name: "IX_SeatManagements_VehicleTripRegistrationId",
                table: "SeatManagements",
                column: "VehicleTripRegistrationId");

            migrationBuilder.CreateIndex(
                name: "IX_SubRoutes_RouteId",
                table: "SubRoutes",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_Trips_RouteId",
                table: "Trips",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_Trips_VehicleModelId",
                table: "Trips",
                column: "VehicleModelId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleModels_VehicleMakeId",
                table: "VehicleModels",
                column: "VehicleMakeId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_EmployeeId",
                table: "Vehicles",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_LocationId",
                table: "Vehicles",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_VehicleModelId",
                table: "Vehicles",
                column: "VehicleModelId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTripRegistrations_TripId1",
                table: "VehicleTripRegistrations",
                column: "TripId1");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleTripRegistrations_VehicleModelId",
                table: "VehicleTripRegistrations",
                column: "VehicleModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Complaint");

            migrationBuilder.DropTable(
                name: "Coupon");

            migrationBuilder.DropTable(
                name: "Discounts");

            migrationBuilder.DropTable(
                name: "ExcludedSeats");

            migrationBuilder.DropTable(
                name: "FareCalendars");

            migrationBuilder.DropTable(
                name: "Fares");

            migrationBuilder.DropTable(
                name: "HireBus");

            migrationBuilder.DropTable(
                name: "HirePassengers");

            migrationBuilder.DropTable(
                name: "HireRequests");

            migrationBuilder.DropTable(
                name: "JourneyManagements");

            migrationBuilder.DropTable(
                name: "MtuReportModel");

            migrationBuilder.DropTable(
                name: "PaymentResponses");

            migrationBuilder.DropTable(
                name: "SeatManagements");

            migrationBuilder.DropTable(
                name: "SMSProfile");

            migrationBuilder.DropTable(
                name: "VehicleAllocationDetail");

            migrationBuilder.DropTable(
                name: "VehicleMileage");

            migrationBuilder.DropTable(
                name: "Workshop");

            migrationBuilder.DropTable(
                name: "Vehicles");

            migrationBuilder.DropTable(
                name: "PickupPoint");

            migrationBuilder.DropTable(
                name: "SubRoutes");

            migrationBuilder.DropTable(
                name: "VehicleTripRegistrations");

            migrationBuilder.DropTable(
                name: "Trips");

            migrationBuilder.DropTable(
                name: "Routes");

            migrationBuilder.DropTable(
                name: "VehicleModels");

            migrationBuilder.DropTable(
                name: "VehicleMakes");

            migrationBuilder.DropColumn(
                name: "BookingDaysRange",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "IsTranspRecieved",
                table: "CompanyInfo");
        }
    }
}
