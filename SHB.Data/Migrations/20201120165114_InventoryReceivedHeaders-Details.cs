﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class InventoryReceivedHeadersDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InventoryReceivedHeaders",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    CompanyId = table.Column<int>(nullable: false),
                    AdjustmentTypeID = table.Column<int>(nullable: true),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    WarehouseID = table.Column<int>(nullable: true),
                    WarehouseBinID = table.Column<int>(nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    Void = table.Column<string>(nullable: true),
                    Captured = table.Column<bool>(nullable: false),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: true),
                    Issued = table.Column<bool>(nullable: false),
                    IssuedBy = table.Column<string>(nullable: true),
                    IssuedDate = table.Column<DateTime>(nullable: true),
                    Approved = table.Column<bool>(nullable: false),
                    ApprovedBy = table.Column<string>(nullable: true),
                    ApprovedDate = table.Column<DateTime>(nullable: false),
                    Posted = table.Column<bool>(nullable: false),
                    PostedBy = table.Column<string>(nullable: true),
                    PostedDate = table.Column<DateTime>(nullable: false),
                    BatchControlNumber = table.Column<string>(nullable: true),
                    BatchControlTotal = table.Column<string>(nullable: true),
                    Signature = table.Column<string>(nullable: true),
                    SignaturePassword = table.Column<string>(nullable: true),
                    SupervisorSignature = table.Column<string>(nullable: true),
                    ManagerSignature = table.Column<string>(nullable: true),
                    Currency = table.Column<int>(nullable: true),
                    CurrencyExchangeRate = table.Column<decimal>(nullable: false),
                    Reference = table.Column<string>(nullable: true),
                    WarehouseCustomerID = table.Column<int>(nullable: true),
                    DeliveryNote = table.Column<string>(nullable: true),
                    SiteNumber = table.Column<string>(nullable: true),
                    VehicleRegistration = table.Column<string>(nullable: true),
                    DriversId = table.Column<int>(nullable: true),
                    FromCompanyID = table.Column<int>(nullable: true),
                    FromWarehouseID = table.Column<int>(nullable: true),
                    FromWarehouseBinID = table.Column<int>(nullable: true),
                    InventoryIssueTransferID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryReceivedHeaders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "InventoryReceivedDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    CompanyId = table.Column<int>(nullable: false),
                    InventoryReceivedID = table.Column<Guid>(nullable: false),
                    InventoryReceivedHeaderId = table.Column<Guid>(nullable: true),
                    ItemID = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    RequestedQty = table.Column<int>(nullable: false),
                    WarehouseID = table.Column<int>(nullable: false),
                    WarehouseBinID = table.Column<int>(nullable: false),
                    ToWarehouseID = table.Column<int>(nullable: false),
                    ToWarehouseBinID = table.Column<int>(nullable: false),
                    GLExpenseAccount = table.Column<int>(nullable: true),
                    ItemValue = table.Column<float>(nullable: false),
                    ItemCost = table.Column<float>(nullable: false),
                    CostMethod = table.Column<int>(nullable: true),
                    ProjectID = table.Column<int>(nullable: true),
                    GLAnalysisType1 = table.Column<int>(nullable: true),
                    GLAnalysisType2 = table.Column<int>(nullable: true),
                    AssetID = table.Column<int>(nullable: true),
                    PONumber = table.Column<string>(nullable: true),
                    ItemUPCCode = table.Column<string>(nullable: true),
                    ReceivedQty = table.Column<float>(nullable: false),
                    InventoryTransferID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryReceivedDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InventoryReceivedDetails_InventoryReceivedHeaders_InventoryReceivedHeaderId",
                        column: x => x.InventoryReceivedHeaderId,
                        principalTable: "InventoryReceivedHeaders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InventoryReceivedDetails_InventoryReceivedHeaderId",
                table: "InventoryReceivedDetails",
                column: "InventoryReceivedHeaderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InventoryReceivedDetails");

            migrationBuilder.DropTable(
                name: "InventoryReceivedHeaders");
        }
    }
}
