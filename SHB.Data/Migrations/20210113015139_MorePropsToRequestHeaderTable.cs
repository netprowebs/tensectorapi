﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class MorePropsToRequestHeaderTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Amount",
                table: "RequisitionHeaders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "RequisitionHeaders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Amount",
                table: "RequisitionHeaders");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "RequisitionHeaders");
        }
    }
}
