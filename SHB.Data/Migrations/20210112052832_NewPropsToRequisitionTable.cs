﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class NewPropsToRequisitionTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Department",
                table: "RequisitionHeaders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location",
                table: "RequisitionHeaders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RetType",
                table: "RequisitionHeaders",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TotalQty",
                table: "RequisitionHeaders",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Department",
                table: "RequisitionHeaders");

            migrationBuilder.DropColumn(
                name: "Location",
                table: "RequisitionHeaders");

            migrationBuilder.DropColumn(
                name: "RetType",
                table: "RequisitionHeaders");

            migrationBuilder.DropColumn(
                name: "TotalQty",
                table: "RequisitionHeaders");
        }
    }
}
