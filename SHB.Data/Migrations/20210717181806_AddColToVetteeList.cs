﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class AddColToVetteeList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "VetAddress",
                table: "VetteeLists",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "VetLatitude",
                table: "VetteeLists",
                type: "real",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<float>(
                name: "VetLongitude",
                table: "VetteeLists",
                type: "real",
                nullable: false,
                defaultValue: 0f);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "VetAddress",
                table: "VetteeLists");

            migrationBuilder.DropColumn(
                name: "VetLatitude",
                table: "VetteeLists");

            migrationBuilder.DropColumn(
                name: "VetLongitude",
                table: "VetteeLists");
        }
    }
}
