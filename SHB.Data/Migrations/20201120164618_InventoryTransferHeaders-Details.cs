﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class InventoryTransferHeadersDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InventoryTransferHeaders",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantID = table.Column<int>(nullable: false),
                    AdjustmentTypeID = table.Column<int>(nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    WarehouseID = table.Column<int>(nullable: true),
                    WarehouseBinID = table.Column<int>(nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    Cleared = table.Column<bool>(nullable: false),
                    EnteredBy = table.Column<string>(nullable: true),
                    Void = table.Column<string>(nullable: true),
                    Captured = table.Column<bool>(nullable: false),
                    Verified = table.Column<bool>(nullable: false),
                    VerifiedBy = table.Column<string>(nullable: true),
                    VerifiedDate = table.Column<DateTime>(nullable: false),
                    Issued = table.Column<bool>(nullable: false),
                    IssuedBy = table.Column<string>(nullable: true),
                    IssuedDate = table.Column<DateTime>(nullable: true),
                    Approved = table.Column<bool>(nullable: false),
                    ApprovedBy = table.Column<string>(nullable: true),
                    ApprovedDate = table.Column<DateTime>(nullable: false),
                    Posted = table.Column<bool>(nullable: false),
                    PostedBy = table.Column<string>(nullable: true),
                    PostedDate = table.Column<DateTime>(nullable: false),
                    BatchControlNumber = table.Column<string>(nullable: true),
                    BatchControlTotal = table.Column<string>(nullable: true),
                    Signature = table.Column<string>(nullable: true),
                    SignaturePassword = table.Column<string>(nullable: true),
                    SupervisorSignature = table.Column<string>(nullable: true),
                    SupervisorPassword = table.Column<string>(nullable: true),
                    ManagerSignature = table.Column<string>(nullable: true),
                    ManagerPassword = table.Column<string>(nullable: true),
                    CurrencyID = table.Column<string>(nullable: true),
                    CurrencyExchangeRate = table.Column<string>(nullable: true),
                    InterTransfer = table.Column<bool>(nullable: false),
                    ToCompanyID = table.Column<int>(nullable: true),
                    LoadingOfficer = table.Column<string>(nullable: true),
                    Rate = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryTransferHeaders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "InventoryTransferDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    InventoryTransferID = table.Column<Guid>(nullable: false),
                    InventoryTransferHeaderId = table.Column<Guid>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    ItemID = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    RequestedQty = table.Column<int>(nullable: true),
                    WarehouseID = table.Column<int>(nullable: true),
                    WarehouseBinID = table.Column<int>(nullable: true),
                    ToWarehouseID = table.Column<int>(nullable: true),
                    ToWarehouseBinID = table.Column<int>(nullable: true),
                    GLExpenseAccount = table.Column<int>(nullable: true),
                    ItemValueItemCost = table.Column<decimal>(nullable: false),
                    CostMethod = table.Column<int>(nullable: false),
                    ProjectID = table.Column<int>(nullable: true),
                    GLAnalysisType = table.Column<string>(nullable: true),
                    AssetID = table.Column<int>(nullable: true),
                    ItemUPCCode = table.Column<string>(nullable: true),
                    ItemName = table.Column<string>(nullable: true),
                    ToCompanyID = table.Column<int>(nullable: true),
                    TruckNumber = table.Column<string>(nullable: true),
                    QtyReceived = table.Column<string>(nullable: true),
                    DateReceived = table.Column<DateTime>(nullable: false),
                    TruckAdvance = table.Column<string>(nullable: true),
                    TruckAdvBalance = table.Column<string>(nullable: true),
                    LoadingTicket = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryTransferDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InventoryTransferDetails_InventoryTransferHeaders_InventoryTransferHeaderId",
                        column: x => x.InventoryTransferHeaderId,
                        principalTable: "InventoryTransferHeaders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InventoryTransferDetails_InventoryTransferHeaderId",
                table: "InventoryTransferDetails",
                column: "InventoryTransferHeaderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InventoryTransferDetails");

            migrationBuilder.DropTable(
                name: "InventoryTransferHeaders");
        }
    }
}
