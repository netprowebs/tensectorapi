﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class NewTransCol : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ReturnDate",
                table: "RequisitionHeaders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReturnNotes",
                table: "RequisitionHeaders",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Void",
                table: "RequisitionHeaders",
                nullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "Void",
                table: "InventoryTransferHeaders",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ReturnDate",
                table: "InventoryTransferHeaders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReturnNotes",
                table: "InventoryTransferHeaders",
                nullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "Void",
                table: "InventoryReceivedHeaders",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ReturnDate",
                table: "InventoryReceivedHeaders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReturnNotes",
                table: "InventoryReceivedHeaders",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RoleActiveType",
                table: "AppMenuItem",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReturnDate",
                table: "RequisitionHeaders");

            migrationBuilder.DropColumn(
                name: "ReturnNotes",
                table: "RequisitionHeaders");

            migrationBuilder.DropColumn(
                name: "Void",
                table: "RequisitionHeaders");

            migrationBuilder.DropColumn(
                name: "ReturnDate",
                table: "InventoryTransferHeaders");

            migrationBuilder.DropColumn(
                name: "ReturnNotes",
                table: "InventoryTransferHeaders");

            migrationBuilder.DropColumn(
                name: "ReturnDate",
                table: "InventoryReceivedHeaders");

            migrationBuilder.DropColumn(
                name: "ReturnNotes",
                table: "InventoryReceivedHeaders");

            migrationBuilder.DropColumn(
                name: "RoleActiveType",
                table: "AppMenuItem");

            migrationBuilder.AlterColumn<string>(
                name: "Void",
                table: "InventoryTransferHeaders",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Void",
                table: "InventoryReceivedHeaders",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(bool),
                oldNullable: true);
        }
    }
}
