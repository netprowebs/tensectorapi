﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class EditVettblD : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "VetteeListID",
                table: "VettersTransactions",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AlterColumn<int>(
                name: "VetServiceID",
                table: "VetteeLists",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VettersTransactions_VetteeListID",
                table: "VettersTransactions",
                column: "VetteeListID");

            migrationBuilder.CreateIndex(
                name: "IX_VetteeLists_VetServiceID",
                table: "VetteeLists",
                column: "VetServiceID");

            migrationBuilder.AddForeignKey(
                name: "FK_VetteeLists_VetServices_VetServiceID",
                table: "VetteeLists",
                column: "VetServiceID",
                principalTable: "VetServices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VettersTransactions_VetteeLists_VetteeListID",
                table: "VettersTransactions",
                column: "VetteeListID",
                principalTable: "VetteeLists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VetteeLists_VetServices_VetServiceID",
                table: "VetteeLists");

            migrationBuilder.DropForeignKey(
                name: "FK_VettersTransactions_VetteeLists_VetteeListID",
                table: "VettersTransactions");

            migrationBuilder.DropIndex(
                name: "IX_VettersTransactions_VetteeListID",
                table: "VettersTransactions");

            migrationBuilder.DropIndex(
                name: "IX_VetteeLists_VetServiceID",
                table: "VetteeLists");

            migrationBuilder.DropColumn(
                name: "VetteeListID",
                table: "VettersTransactions");

            migrationBuilder.AlterColumn<string>(
                name: "VetServiceID",
                table: "VetteeLists",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }
    }
}
