﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class InventoryByWarehousesLedgers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_InventoryByWarehouse",
                table: "InventoryByWarehouse");

            migrationBuilder.RenameTable(
                name: "InventoryByWarehouse",
                newName: "InventoryByWarehouses");

            migrationBuilder.AddPrimaryKey(
                name: "PK_InventoryByWarehouses",
                table: "InventoryByWarehouses",
                columns: new[] { "Id", "CompanyID" });

            migrationBuilder.CreateTable(
                name: "InventoryLedgers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    CompanyID = table.Column<int>(nullable: false),
                    TransDate = table.Column<DateTime>(nullable: false),
                    ItemID = table.Column<int>(nullable: false),
                    TransNumber = table.Column<string>(nullable: true),
                    ILLineNumber = table.Column<string>(nullable: true),
                    TransactionType = table.Column<int>(nullable: false),
                    WarehouseID = table.Column<int>(nullable: false),
                    WarehouseBinID = table.Column<int>(nullable: false),
                    Quantity = table.Column<string>(nullable: true),
                    CostPerUnit = table.Column<float>(nullable: false),
                    TotalCost = table.Column<float>(nullable: false),
                    LIFOCost = table.Column<float>(nullable: false),
                    AverageCost = table.Column<float>(nullable: false),
                    FIFOCost = table.Column<float>(nullable: false),
                    ExpectedCost = table.Column<float>(nullable: false),
                    OtherCost = table.Column<float>(nullable: false),
                    StandardCost = table.Column<float>(nullable: false),
                    BalanceQty = table.Column<string>(nullable: true),
                    BatchNumber = table.Column<string>(nullable: true),
                    AccountName = table.Column<string>(nullable: true),
                    OriginalDocumentNumber = table.Column<string>(nullable: true),
                    ReferenceNumber = table.Column<string>(nullable: true),
                    CurrencyID = table.Column<int>(nullable: false),
                    CurrencyExchangeRate = table.Column<float>(nullable: false),
                    AdjustedBalanceQty = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryLedgers", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InventoryLedgers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_InventoryByWarehouses",
                table: "InventoryByWarehouses");

            migrationBuilder.RenameTable(
                name: "InventoryByWarehouses",
                newName: "InventoryByWarehouse");

            migrationBuilder.AddPrimaryKey(
                name: "PK_InventoryByWarehouse",
                table: "InventoryByWarehouse",
                columns: new[] { "Id", "CompanyID" });
        }
    }
}
