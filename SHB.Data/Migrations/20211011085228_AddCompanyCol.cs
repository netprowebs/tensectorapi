﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class AddCompanyCol : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DOAccessKey",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DOHostUploadURL",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DOSecretKey",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DocDownloadPath",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FlPublicKey",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FlSecretKey",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MJAPIKEYPRIVATE",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MJAPIKEYPUBLIC",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MJSenderEmail",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MJSenderName",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PYPublicKey",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PYSecretKey",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "SmsEnableSSl",
                table: "CompanyInfo",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "SmsPassword",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SmsPort",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SmsServer",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "SmsUseDefaultCred",
                table: "CompanyInfo",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "SmsUserName",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "bucketName",
                table: "CompanyInfo",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DOAccessKey",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "DOHostUploadURL",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "DOSecretKey",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "DocDownloadPath",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "FlPublicKey",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "FlSecretKey",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "MJAPIKEYPRIVATE",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "MJAPIKEYPUBLIC",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "MJSenderEmail",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "MJSenderName",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "PYPublicKey",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "PYSecretKey",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "SmsEnableSSl",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "SmsPassword",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "SmsPort",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "SmsServer",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "SmsUseDefaultCred",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "SmsUserName",
                table: "CompanyInfo");

            migrationBuilder.DropColumn(
                name: "bucketName",
                table: "CompanyInfo");
        }
    }
}
