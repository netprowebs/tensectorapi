﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class AdjtblCol : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WalletId",
                table: "CustomerInformations");

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "Referrals",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "Referrals");

            migrationBuilder.AddColumn<int>(
                name: "WalletId",
                table: "CustomerInformations",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
