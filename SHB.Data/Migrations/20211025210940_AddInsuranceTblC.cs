﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class AddInsuranceTblC : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "NetDue",
                table: "InsurPolicyUnderwriters",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "VATDue",
                table: "InsurPolicyUnderwriters",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "IsCaptured",
                table: "InsurancePolicyClaim",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<Guid>(
                name: "RenewPolicyId",
                table: "InsurancePolicy",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AddColumn<bool>(
                name: "IsCaptured",
                table: "InsurancePolicy",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NetDue",
                table: "InsurPolicyUnderwriters");

            migrationBuilder.DropColumn(
                name: "VATDue",
                table: "InsurPolicyUnderwriters");

            migrationBuilder.DropColumn(
                name: "IsCaptured",
                table: "InsurancePolicyClaim");

            migrationBuilder.DropColumn(
                name: "IsCaptured",
                table: "InsurancePolicy");

            migrationBuilder.AlterColumn<Guid>(
                name: "RenewPolicyId",
                table: "InsurancePolicy",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);
        }
    }
}
