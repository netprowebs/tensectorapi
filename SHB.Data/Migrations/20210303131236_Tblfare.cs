﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class Tblfare : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Fares_Routes_RouteId",
                table: "Fares");

            migrationBuilder.DropForeignKey(
                name: "FK_Fares_VehicleModels_VehicleModelId",
                table: "Fares");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Fares",
                table: "Fares");

            migrationBuilder.DropColumn(
                name: "AvailableOnline",
                table: "Fares");

            migrationBuilder.DropColumn(
                name: "DepartureTime",
                table: "Fares");

            migrationBuilder.DropColumn(
                name: "ParentRouteDepartureTime",
                table: "Fares");

            migrationBuilder.DropColumn(
                name: "ParentRouteId",
                table: "Fares");

            migrationBuilder.DropColumn(
                name: "ParentTripId",
                table: "Fares");

            migrationBuilder.DropColumn(
                name: "TripCode",
                table: "Fares");

            migrationBuilder.RenameTable(
                name: "Fares",
                newName: "Fare");

            migrationBuilder.RenameIndex(
                name: "IX_Fares_VehicleModelId",
                table: "Fare",
                newName: "IX_Fare_VehicleModelId");

            migrationBuilder.RenameIndex(
                name: "IX_Fares_RouteId",
                table: "Fare",
                newName: "IX_Fare_RouteId");

            migrationBuilder.AlterColumn<int>(
                name: "VehicleModelId",
                table: "Fare",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Amount",
                table: "Fare",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<float>(
                name: "ChildrenDiscountPercentage",
                table: "Fare",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "Fare",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "Fare",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "Fare",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "Fare",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Fare",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "Fare",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "Fare",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Fare",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "NonIdAmount",
                table: "Fare",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Fare",
                table: "Fare",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Fare_Routes_RouteId",
                table: "Fare",
                column: "RouteId",
                principalTable: "Routes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Fare_VehicleModels_VehicleModelId",
                table: "Fare",
                column: "VehicleModelId",
                principalTable: "VehicleModels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Fare_Routes_RouteId",
                table: "Fare");

            migrationBuilder.DropForeignKey(
                name: "FK_Fare_VehicleModels_VehicleModelId",
                table: "Fare");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Fare",
                table: "Fare");

            migrationBuilder.DropColumn(
                name: "Amount",
                table: "Fare");

            migrationBuilder.DropColumn(
                name: "ChildrenDiscountPercentage",
                table: "Fare");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "Fare");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "Fare");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "Fare");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "Fare");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Fare");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "Fare");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "Fare");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Fare");

            migrationBuilder.DropColumn(
                name: "NonIdAmount",
                table: "Fare");

            migrationBuilder.RenameTable(
                name: "Fare",
                newName: "Fares");

            migrationBuilder.RenameIndex(
                name: "IX_Fare_VehicleModelId",
                table: "Fares",
                newName: "IX_Fares_VehicleModelId");

            migrationBuilder.RenameIndex(
                name: "IX_Fare_RouteId",
                table: "Fares",
                newName: "IX_Fares_RouteId");

            migrationBuilder.AlterColumn<int>(
                name: "VehicleModelId",
                table: "Fares",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<bool>(
                name: "AvailableOnline",
                table: "Fares",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "DepartureTime",
                table: "Fares",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ParentRouteDepartureTime",
                table: "Fares",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ParentRouteId",
                table: "Fares",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ParentTripId",
                table: "Fares",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TripCode",
                table: "Fares",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Fares",
                table: "Fares",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Fares_Routes_RouteId",
                table: "Fares",
                column: "RouteId",
                principalTable: "Routes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Fares_VehicleModels_VehicleModelId",
                table: "Fares",
                column: "VehicleModelId",
                principalTable: "VehicleModels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
