﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class ItemSetupModelsAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CurrencyExchangeGainOrLossAccount",
                table: "VendorTypes");

            migrationBuilder.AddColumn<string>(
                name: "CurrencyExchange",
                table: "VendorTypes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GainOrLossAccount",
                table: "VendorTypes",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CustomerInformations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    CompanyID = table.Column<int>(nullable: false),
                    AccountStatus = table.Column<int>(nullable: true),
                    CustomerName = table.Column<string>(nullable: true),
                    CustomerAddress1 = table.Column<string>(nullable: true),
                    CustomerAddress2 = table.Column<string>(nullable: true),
                    CustomerAddress3 = table.Column<string>(nullable: true),
                    CustomerCity = table.Column<string>(nullable: true),
                    CustomerState = table.Column<string>(nullable: true),
                    CustomerZip = table.Column<string>(nullable: true),
                    CustomerCountry = table.Column<string>(nullable: true),
                    CustomerPhone = table.Column<string>(nullable: true),
                    CustomerFax = table.Column<string>(nullable: true),
                    CustomerEmail = table.Column<string>(nullable: true),
                    CustomerWebPage = table.Column<string>(nullable: true),
                    CustomerFirstName = table.Column<string>(nullable: true),
                    CustomerLastName = table.Column<string>(nullable: true),
                    CustomerSalutation = table.Column<string>(nullable: true),
                    CustomerTypeID = table.Column<int>(nullable: true),
                    TaxIDNo = table.Column<string>(nullable: true),
                    VATTaxIDNumber = table.Column<string>(nullable: true),
                    VatTaxOtherNumber = table.Column<string>(nullable: true),
                    CurrencyID = table.Column<string>(nullable: true),
                    GLSalesAccount = table.Column<int>(nullable: true),
                    TermsID = table.Column<string>(nullable: true),
                    TermsStart = table.Column<string>(nullable: true),
                    TaxGroupID = table.Column<string>(nullable: true),
                    PriceMatrix = table.Column<string>(nullable: true),
                    PriceMatrixCurrent = table.Column<string>(nullable: true),
                    CreditRating = table.Column<string>(nullable: true),
                    CreditLimit = table.Column<decimal>(nullable: false),
                    CreditComments = table.Column<string>(nullable: true),
                    PaymentDay = table.Column<string>(nullable: true),
                    ApprovalDate = table.Column<DateTime>(nullable: false),
                    CustomerSince = table.Column<DateTime>(nullable: false),
                    SendCreditMemos = table.Column<string>(nullable: true),
                    SendDebitMemos = table.Column<string>(nullable: true),
                    Statements = table.Column<string>(nullable: true),
                    StatementCycleCode = table.Column<string>(nullable: true),
                    CustomerSpecialInstructions = table.Column<string>(nullable: true),
                    CustomerShipToId = table.Column<string>(nullable: true),
                    CustomerShipForId = table.Column<string>(nullable: true),
                    ShipMethodID = table.Column<string>(nullable: true),
                    WarehouseID = table.Column<int>(nullable: false),
                    WarehouseGLAccount = table.Column<string>(nullable: true),
                    Approved = table.Column<bool>(nullable: false),
                    ApprovedBy = table.Column<string>(nullable: true),
                    ApprovedDate = table.Column<DateTime>(nullable: false),
                    ReferedBy = table.Column<string>(nullable: true),
                    ReferedDate = table.Column<DateTime>(nullable: false),
                    ReferalURL = table.Column<string>(nullable: true),
                    AccountBalance = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerInformations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CustomerTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    CompanyID = table.Column<string>(nullable: true),
                    CustomerTypeID = table.Column<string>(nullable: true),
                    CustomerTypeDescription = table.Column<string>(nullable: true),
                    SalesControlAccount = table.Column<string>(nullable: true),
                    COSControlAccount = table.Column<string>(nullable: true),
                    DebtorsControlAccount = table.Column<string>(nullable: true),
                    CurrencyExchange = table.Column<string>(nullable: true),
                    GainOrLossAccount = table.Column<string>(nullable: true),
                    DiscountsAccount = table.Column<string>(nullable: true),
                    DiscountRate = table.Column<string>(nullable: true),
                    BillingType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VendorInformations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    CompanyID = table.Column<int>(nullable: false),
                    AccountStatus = table.Column<int>(nullable: true),
                    VendorName = table.Column<string>(nullable: true),
                    VendorAddress1 = table.Column<string>(nullable: true),
                    VendorAddress2 = table.Column<string>(nullable: true),
                    VendorAddress3 = table.Column<string>(nullable: true),
                    VendorCity = table.Column<string>(nullable: true),
                    VendorState = table.Column<string>(nullable: true),
                    VendorZip = table.Column<string>(nullable: true),
                    VendorCountry = table.Column<string>(nullable: true),
                    VendorPhone = table.Column<string>(nullable: true),
                    VendorFax = table.Column<string>(nullable: true),
                    VendorWebPage = table.Column<string>(nullable: true),
                    VendorTypeID = table.Column<int>(nullable: false),
                    AccountNumber = table.Column<string>(nullable: true),
                    ContactID = table.Column<string>(nullable: true),
                    WarehouseID = table.Column<int>(nullable: true),
                    CurrencyID = table.Column<string>(nullable: true),
                    TermsID = table.Column<string>(nullable: true),
                    TermsStart = table.Column<string>(nullable: true),
                    GLPurchaseAccount = table.Column<string>(nullable: true),
                    TaxIDNo = table.Column<string>(nullable: true),
                    VATTaxIDNumber = table.Column<string>(nullable: true),
                    VatTaxOtherNumber = table.Column<string>(nullable: true),
                    TaxGroupID = table.Column<string>(nullable: true),
                    CreditLimit = table.Column<string>(nullable: true),
                    AvailibleCredit = table.Column<string>(nullable: true),
                    CreditComments = table.Column<string>(nullable: true),
                    CreditRating = table.Column<string>(nullable: true),
                    ApprovalDate = table.Column<string>(nullable: true),
                    CustomerSince = table.Column<string>(nullable: true),
                    FreightPayment = table.Column<string>(nullable: true),
                    CustomerSpecialInstructions = table.Column<string>(nullable: true),
                    SpecialTerms = table.Column<string>(nullable: true),
                    ConvertedFromCustomer = table.Column<string>(nullable: true),
                    VendorRegionID = table.Column<string>(nullable: true),
                    VendorSourceID = table.Column<string>(nullable: true),
                    VendorIndustryID = table.Column<string>(nullable: true),
                    Confirmed = table.Column<bool>(nullable: false),
                    ReferedBy = table.Column<string>(nullable: true),
                    ReferedDate = table.Column<string>(nullable: true),
                    ReferalURL = table.Column<string>(nullable: true),
                    AccountBalance = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VendorInformations", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomerInformations");

            migrationBuilder.DropTable(
                name: "CustomerTypes");

            migrationBuilder.DropTable(
                name: "VendorInformations");

            migrationBuilder.DropColumn(
                name: "CurrencyExchange",
                table: "VendorTypes");

            migrationBuilder.DropColumn(
                name: "GainOrLossAccount",
                table: "VendorTypes");

            migrationBuilder.AddColumn<string>(
                name: "CurrencyExchangeGainOrLossAccount",
                table: "VendorTypes",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
