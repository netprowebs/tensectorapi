﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class MorePropToDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Amount",
                table: "RequisitionHeaders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "RequisitionHeaders",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RequestType",
                table: "RequisitionHeaders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "TransNo",
                table: "RequisitionHeaders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Staff",
                table: "RequisitionDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Vehicle",
                table: "RequisitionDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "InventoryTransferHeaders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Staff",
                table: "InventoryTransferHeaders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TotalQty",
                table: "InventoryTransferHeaders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Vehicle",
                table: "InventoryTransferHeaders",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AdjustmentType",
                table: "InventoryAdjustmentsHeaders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Narratives",
                table: "InventoryAdjustmentsHeaders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Reference",
                table: "InventoryAdjustmentsHeaders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "InventoryAdjustmentsHeaders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Amount",
                table: "RequisitionHeaders");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "RequisitionHeaders");

            migrationBuilder.DropColumn(
                name: "RequestType",
                table: "RequisitionHeaders");

            migrationBuilder.DropColumn(
                name: "TransNo",
                table: "RequisitionHeaders");

            migrationBuilder.DropColumn(
                name: "Staff",
                table: "RequisitionDetails");

            migrationBuilder.DropColumn(
                name: "Vehicle",
                table: "RequisitionDetails");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "InventoryTransferHeaders");

            migrationBuilder.DropColumn(
                name: "Staff",
                table: "InventoryTransferHeaders");

            migrationBuilder.DropColumn(
                name: "TotalQty",
                table: "InventoryTransferHeaders");

            migrationBuilder.DropColumn(
                name: "Vehicle",
                table: "InventoryTransferHeaders");

            migrationBuilder.DropColumn(
                name: "AdjustmentType",
                table: "InventoryAdjustmentsHeaders");

            migrationBuilder.DropColumn(
                name: "Narratives",
                table: "InventoryAdjustmentsHeaders");

            migrationBuilder.DropColumn(
                name: "Reference",
                table: "InventoryAdjustmentsHeaders");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "InventoryAdjustmentsHeaders");
        }
    }
}
