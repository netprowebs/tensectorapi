﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SHB.Data.Migrations
{
    public partial class nothing : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Amount",
                table: "RequisitionHeaders");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "RequisitionHeaders");

            migrationBuilder.DropColumn(
                name: "RecieveDate",
                table: "RequisitionHeaders");

            migrationBuilder.DropColumn(
                name: "RequestType",
                table: "RequisitionHeaders");

            migrationBuilder.DropColumn(
                name: "TransNo",
                table: "RequisitionHeaders");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Amount",
                table: "RequisitionHeaders",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "RequisitionHeaders",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "RecieveDate",
                table: "RequisitionHeaders",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RequestType",
                table: "RequisitionHeaders",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "TransNo",
                table: "RequisitionHeaders",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
