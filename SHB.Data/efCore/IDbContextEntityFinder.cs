﻿using System;
using System.Collections.Generic;
using System.Text;
using SHB.Core.Domain.Entities;
using SHB.Core.Entities;
using SHB.Core.Reflection;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Reflection;
using SHB.Core.Entities.Common;

namespace SHB.Data.efCore
{
    public class EfCoreDbContextEntityFinder
    {
        public static IEnumerable<EntityTypeInfo> GetEntityTypeInfos(Type dbContextType)
        {
            return
                from property in dbContextType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                where
                    ReflectionHelper.IsAssignableToGenericType(property.PropertyType, typeof(DbSet<>)) &&
                    ReflectionHelper.IsAssignableToGenericType(property.PropertyType.GenericTypeArguments[0], typeof(IEntity<>))
                select new EntityTypeInfo(property.PropertyType.GenericTypeArguments[0], property.DeclaringType);
        }
    }
}
