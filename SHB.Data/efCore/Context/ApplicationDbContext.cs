﻿using SHB.Core.Domain.Entities;
using SHB.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Reflection;

namespace SHB.Data.efCore.Context
{
    public class ApplicationDbContext : BaseDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        #region dbsets Admin Setups
        public DbSet<Employee> Employees { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Terminal> Terminals { get; set; }
        public DbSet<Wallet> Wallets { get; set; }
        public DbSet<WalletTransaction> WalletTransactions { get; set; }
        public DbSet<WalletNumber> WalletNumbers { get; set; }
        public DbSet<Referral> Referrals { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<ErrorCode> ErrorCodes { get; set; }
        public DbSet<CompanyInfo> CompanyInfo { get; set; }
        public DbSet<NextNumber> NextNumbers { get; set; }
        public DbSet<AppMenuItem> AppMenuItem { get; set; }
        public DbSet<AppMenuItemAccess> AppMenuItemAccess { get; set; }
        public DbSet<CustomerInformation> CustomerInformations { get; set; }
        public DbSet<CustomerTypes> CustomerTypes { get; set; }
        public DbSet<VendorInformation> VendorInformations { get; set; }
        public DbSet<ImageFile> ImageFiles { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<FeedbackResponse> FeedbackResponses { get; set; }
        #endregion
        #region dbsets for Inventory
        public DbSet<LedgerChartOfAccount> LedgerChartOfAccounts { get; set; }
        public DbSet<InventoryItem> InventoryItems { get; set; }
        public DbSet<ItemCategory> ItemCategories { get; set; }
        public DbSet<ItemType> ItemTypes { get; set; }
        public DbSet<Attributes> Attributes { get; set; }
        public DbSet<ItemFamily> ItemFamilies { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<WarehouseBin> WarehouseBins { get; set; }
        public DbSet<InventoryAdjustmentType> InventoryAdjustmentTypes { get; set; }
        public DbSet<WarehouseBinType> WarehouseBinTypes { get; set; }
        public DbSet<BrandType> BrandTypes { get; set; }
        public DbSet<VendorTypes> VendorTypes { get; set; }
        public DbSet<GeneralTransaction> GeneralTransactions { get; set; }
        public DbSet<InventoryByWarehouse> InventoryByWarehouses { get; set; }
        public DbSet<InventoryLedger> InventoryLedgers { get; set; }
        public DbSet<RequisitionHeader> RequisitionHeaders { get; set; }
        public DbSet<RequisitionDetail> RequisitionDetails { get; set; }
        public DbSet<InventoryTransferHeader> InventoryTransferHeaders { get; set; }
        public DbSet<InventoryTransferDetail> InventoryTransferDetails { get; set; }
        public DbSet<InventoryReceivedHeader> InventoryReceivedHeaders { get; set; }
        public DbSet<InventoryReceivedDetail> InventoryReceivedDetails { get; set; }
        public DbSet<InventoryAdjustmentsHeader> InventoryAdjustmentsHeaders { get; set; }
        public DbSet<InventoryAdjustmentsDetail> InventoryAdjustmentsDetails { get; set; }
        #endregion

        #region dbsets for Transport Mgts
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<VehicleMake> VehicleMakes { get; set; }
        public DbSet<VehicleModel> VehicleModels { get; set; }
        public DbSet<Trip> Trips { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<SubRoute> SubRoutes { get; set; }
        public DbSet<Fare> Fare { get; set; }
        public DbSet<FareCalendar> FareCalendars { get; set; }
        public DbSet<ExcludedSeat> ExcludedSeats { get; set; }
        public DbSet<SeatManagement> SeatManagements { get; set; }
        public DbSet<Discount> Discounts { get; set; }
        public DbSet<VehicleTripRegistration> VehicleTripRegistrations { get; set; }
        public DbSet<JourneyManagement> JourneyManagements { get; set; }
        public DbSet<HireRequest> HireRequests { get; set; }
        public DbSet<HireBus> HireBus { get; set; }
        public DbSet<HirePassenger> HirePassengers { get; set; }
        public DbSet<MtuReportModel> MtuReportModel { get; set; }
        public DbSet<VehicleAllocationDetail> VehicleAllocationDetail { get; set; }
        public DbSet<Complaint> Complaint { get; set; }
        public DbSet<Coupon> Coupon { get; set; }
        public DbSet<PaymentResponse> PaymentResponses { get; set; }
        public DbSet<SMSProfile> SMSProfile { get; set; }
        public DbSet<Workshop> Workshop { get; set; }
        public DbSet<VehicleMileage> VehicleMileage { get; set; }

        #endregion  Transport Mgts

        #region dbsets Vetting Sectors
        public DbSet<VetService> VetServices { get; set; }
        public DbSet<VetteeList> VetteeLists { get; set; }
        public DbSet<VettersTransaction> VettersTransactions { get; set; }
        public DbSet<VetBundles> VetBundles { get; set; }
        #endregion

        #region dbsets Accounting

        public DbSet<InvoiceHeader> InvoiceHeader { get; set; }
        public DbSet<InvoiceDetail> InvoiceDetail { get; set; }

        public DbSet<AccPurchase> AccPurchase { get; set; }
        public DbSet<AccPurchaseDetail> AccPurchaseDetail { get; set; }
        public DbSet<ReceiptHeader> ReceiptHeader { get; set; }
        public DbSet<ReceiptDetail> ReceiptDetail { get; set; }
        public DbSet<JournalHeader> JournalHeader { get; set; }
        public DbSet<JournalDetail> JournalDetail { get; set; }

        #endregion

        #region dbsets Insurance Setups
        //setups
        public DbSet<InsuranceBizClass> InsuranceBizClass { get; set; }
        public DbSet<InsuranceRiskType> InsuranceRiskType { get; set; }
        public DbSet<InsuranceFiles> InsuranceFiles { get; set; }
        //transactions
        public DbSet<InsurancePolicy> InsurancePolicy { get; set; }
        public DbSet<InsurPolicyUnderwriters> InsurPolicyUnderwriters { get; set; }
        public DbSet<InsurPolicyBrokers> InsurPolicyBrokers { get; set; }
        public DbSet<InsurancePolicyClaim> InsurancePolicyClaim { get; set; }

        #endregion
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            modelBuilder.Entity<InventoryByWarehouse>()
            .HasKey(c => new { c.Id, c.CompanyID });
        }

    }
    /// <summary>
    /// Migration only
    /// </summary>
    public class AppDbContextMigrationFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
    {
        public static readonly IConfigurationRoot ConfigBuilder = new ConfigurationBuilder()
                 .SetBasePath(AppContext.BaseDirectory)
                 .AddJsonFile("appsettings.json", false, true).Build();


        public ApplicationDbContext CreateDbContext(string[] args)
        {

            return new ApplicationDbContext(new DbContextOptionsBuilder<ApplicationDbContext>()
                                   .UseSqlServer(ConfigBuilder.GetConnectionString("Database"))
                                   .Options);
        }
    }
}
