﻿using SHB.Core.Extensions;
using SHB.Core.Timing;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using SHB.Business.Messaging.Model;
using System.Net.Http;
using System;
using System.Net.Http.Headers;
using System.Text;

namespace SHB.Business.Messaging.Sms
{
    public class SMSService : ISMSService
    {
        public static IWebClient WebClientSource;

        public SMSService(IWebClient webClient)
        {
            WebClientSource = webClient;
        }

        public  void SendSMSNow(string message, string sender, params string[] recipient)
        {
            bool isSent;

            var model = new SMSLiveModel
            {
                Message = message,
                Sender = sender,
                Recipient = recipient
            };

            //for kirusa
            Task.WaitAll(KonnectKirusaSMS(model).SendSmsAsync());

            //try
            //{
            //    string accountId = "CFQENebSSmp49mTsqO6feg==";
            //    string authKey = "bl3_gOt+bR_bw2qM288gW8wXS8Nw3+6JFz6T_EvCcso=";
            //    List<string> num = new List<string>();
            //    num.Add("234" + "8168816484");
            //    RequestSMS sms = new RequestSMS();
            //    sms.id = Guid.NewGuid().ToString();
            //    sms.to = num;
            //    sms.sender_mask = "LIBMOEXPRES";
            //    sms.body = model.Message;
            //    KonnectAPI konnectAPI = new KonnectAPI(authKey, accountId);
            //    konnectAPI.SendSMS(sms);
            //    //SendSmsb(senderphone, smsmsg);
            //}
            //catch (Exception)
            //{
            //}

 
        }

        public SMSSenderModel OgoSMS(SMSLiveModel model)
        {
            var smsBody = model.Message;
            var recipient = model.Recipient.ArrayToCommaSeparatedString().UrlEncode();

            var url = "http://www.ogosms.com/dynamicapi/?username=LibraMotors&password=Libra123$&sender=LIBMOT.COM&numbers=" + recipient + "&message=" + smsBody;
            var senderModel = new SMSSenderModel
            {
                ApiUrl = url,
                Method = "GET"
            };
            return senderModel;
        }

        public SMSSenderModel KonnectKirusaSMS(SMSLiveModel model)
        {
            var url = "https://konnect.kirusa.com/api/v1/Accounts/CFQENebSSmp49mTsqO6feg==/Messages";

            var senderModel = new SMSSenderModel
            {
                ApiUrl = url,
                Method = "POST",
                Body = JsonConvert.SerializeObject(new
                {
                    //from= "23434565",
                    from = "LIBMOEXPRES",
                    id = $"{Clock.Now.ToFileTimeUtc()}",
                    sender_mask = "LIBMOEXPRES",
                    to = model.Recipient,
                    body = model.Message,
                }),
                Headers = new Dictionary<string, string>()
                {
                    ["Authorization"] = "bl3_gOt+bR_bw2qM288gW8wXS8Nw3+6JFz6T_EvCcso="
                }
            };

            return senderModel;
        }

    

        public class KonnectAPI
        {
            private readonly HttpClient _httpClient;
            Response resp;
            string baseurl;
            /// <summary>
            /// Initialize constructor with authkey and accountid parameters. 
            /// </summary>
            /// <param name="authKey"></param>
            /// <param name="authId"></param>
            public KonnectAPI(string authKey, string accountId)
            {
                baseurl = $"https://konnect.kirusa.com/api/v1/Accounts/{accountId}/";
                _httpClient = new HttpClient();
                _httpClient.BaseAddress = new Uri(baseurl);
                _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", authKey);

            }

            public async Task<bool> SendSMS(RequestSMS request)
            {
                try
                {
                    var uri = baseurl + "Messages";
                    var content = JsonConvert.SerializeObject(request, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                    var buffer = Encoding.UTF8.GetBytes(content);
                    var byteContent = new ByteArrayContent(buffer);
                    byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    var response = await _httpClient.PostAsync(uri, byteContent);
                    var result = response.Content.ReadAsStringAsync().Result;
                    resp = JsonConvert.DeserializeObject<Response>(result);
                    if (resp.status == "ok")
                    {
                        return true;
                    }

                    return false;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString() + $"Error code returned: {resp.error_code} with reason {resp.error_reason}");
                }
                return false;
            }
        }


        public class Response
        {
            public string status { get; set; }
            public string error_code { get; set; }
            public string error_reason { get; set; }
        }


        public class RequestSMS
        {
            // Unique transaction id for the request
            public string id { get; set; }
            //The sender phone number. If not specified, the default sender id is used.
            public string from { get; set; }
            // The destination phone number, multiple phone numbers should be passed as a JSON array separated by comma.
            public List<string> to { get; set; }
            // The string to mask the sender.
            public string sender_mask { get; set; }
            // The full text of the message
            public string body { get; set; }

        }




    }
}