﻿
using SHB.Core.Configuration;

namespace SHB.Business.Messaging.Sms
{
    public abstract class SMSConfigSettings : ISettings
    {
        public string Sender { get; set; }
    }
}