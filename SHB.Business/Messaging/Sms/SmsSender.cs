﻿using System.Threading.Tasks;

namespace SHB.Business.Messaging.Sms
{
    public abstract class SMSSender
    {
        public abstract Task SendSmsAsync();
        public abstract void SendSms();
    }
}