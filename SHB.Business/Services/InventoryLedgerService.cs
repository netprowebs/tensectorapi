﻿using IPagedList;
using Microsoft.EntityFrameworkCore;
using SHB.Core.Domain.Entities;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Business.Services
{
    public interface IInventoryLedgerService
    {

        #region Inventory Ledger
        Task AddInventoryLedger(InventoryLedger model);
        Task<InventoryLedger> GetInventoryLedger(int Id);
        Task<IPagedList<InventoryLedger>> GetAllInventoryLedger(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateInventoryLedger(InventoryLedger model, int Id);
        Task DeleteInventoryLedger(int Id);
        #endregion

    }
    public class InventoryLedgerService:IInventoryLedgerService
    {
        private readonly IRepository<InventoryLedger> _InventoryLedgerRepo;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;

        private readonly IUserService _userSvc;
        public InventoryLedgerService(

             IRepository<InventoryLedger> InventoryLedgerRepo,
            IServiceHelper serviceHelper,
            IUnitOfWork unitOfWork,
            IUserService userSvc
            )
        {
            _InventoryLedgerRepo = InventoryLedgerRepo;
            _serviceHelper = serviceHelper;
            _unitOfWork = unitOfWork;
            _userSvc = userSvc;
        }

        #region Inventory Ledger
        public async Task AddInventoryLedger(InventoryLedger model)
        {
            //model.InventoryLedgerDescription = model.InventoryLedgerDescription.Trim();
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);


            _InventoryLedgerRepo.Insert(new InventoryLedger
            {
                TransDate = model.TransDate,
                ItemID = model.ItemID,
                TransNumber = model.TransNumber,
                ILLineNumber = model.ILLineNumber,
                TransactionType = model.TransactionType,
                WarehouseID = model.WarehouseID,
                WarehouseBinID = model.WarehouseBinID,
                Quantity = model.Quantity,
                CostPerUnit = model.CostPerUnit,
                TotalCost = model.TotalCost,
                LIFOCost = model.LIFOCost,
                AverageCost = model.AverageCost,
                FIFOCost = model.FIFOCost,
                ExpectedCost = model.ExpectedCost,
                OtherCost = model.OtherCost,
                StandardCost = model.StandardCost,
                BalanceQty = model.BalanceQty,
                BatchNumber = model.BatchNumber,
                AccountName = model.AccountName,
                OriginalDocumentNumber = model.OriginalDocumentNumber,
                ReferenceNumber = model.ReferenceNumber,
                CurrencyID = model.CurrencyID,
                CurrencyExchangeRate = model.CurrencyExchangeRate,
                AdjustedBalanceQty = model.AdjustedBalanceQty,

                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                CompanyID = (int)currentUser.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteInventoryLedger(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _InventoryLedgerRepo.DeleteAsync(_InventoryLedgerRepo.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<IPagedList<InventoryLedger>> GetAllInventoryLedger(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var InventoryLedger =
                    from model in _InventoryLedgerRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) //||
                    //model.VehicleRegistration.Contains(searchTerm)
                    orderby model.CreationTime descending

                    select new InventoryLedger
                    {
                        Id = model.Id,
                        TransDate = model.TransDate,
                        ItemID = model.ItemID,
                        TransNumber = model.TransNumber,
                        ILLineNumber = model.ILLineNumber,
                        TransactionType = model.TransactionType,
                        WarehouseID = model.WarehouseID,
                        WarehouseBinID = model.WarehouseBinID,
                        Quantity = model.Quantity,
                        CostPerUnit = model.CostPerUnit,
                        TotalCost = model.TotalCost,
                        LIFOCost = model.LIFOCost,
                        AverageCost = model.AverageCost,
                        FIFOCost = model.FIFOCost,
                        ExpectedCost = model.ExpectedCost,
                        OtherCost = model.OtherCost,
                        StandardCost = model.StandardCost,
                        BalanceQty = model.BalanceQty,
                        BatchNumber = model.BatchNumber,
                        AccountName = model.AccountName,
                        OriginalDocumentNumber = model.OriginalDocumentNumber,
                        ReferenceNumber = model.ReferenceNumber,
                        CurrencyID = model.CurrencyID,
                        CurrencyExchangeRate = model.CurrencyExchangeRate,
                        AdjustedBalanceQty = model.AdjustedBalanceQty,
                    };

                return await InventoryLedger.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task<InventoryLedger> GetInventoryLedger(int Id)
        {
            var model = _InventoryLedgerRepo.FirstOrDefault(x => x.Id == Id);

            return new InventoryLedger
            {
                Id = model.Id,
                TransDate = model.TransDate,
                ItemID = model.ItemID,
                TransNumber = model.TransNumber,
                ILLineNumber = model.ILLineNumber,
                TransactionType = model.TransactionType,
                WarehouseID = model.WarehouseID,
                WarehouseBinID = model.WarehouseBinID,
                Quantity = model.Quantity,
                CostPerUnit = model.CostPerUnit,
                TotalCost = model.TotalCost,
                LIFOCost = model.LIFOCost,
                AverageCost = model.AverageCost,
                FIFOCost = model.FIFOCost,
                ExpectedCost = model.ExpectedCost,
                OtherCost = model.OtherCost,
                StandardCost = model.StandardCost,
                BalanceQty = model.BalanceQty,
                BatchNumber = model.BatchNumber,
                AccountName = model.AccountName,
                OriginalDocumentNumber = model.OriginalDocumentNumber,
                ReferenceNumber = model.ReferenceNumber,
                CurrencyID = model.CurrencyID,
                CurrencyExchangeRate = model.CurrencyExchangeRate,
                AdjustedBalanceQty = model.AdjustedBalanceQty,
            };
        }


        public async Task<bool> UpdateInventoryLedger(InventoryLedger model, int Id)
        {
            var InventoryLedger = _InventoryLedgerRepo.FirstOrDefault(x => x.Id == Id);

            //InventoryLedger.Id = model.Id;
            InventoryLedger.TransDate = model.TransDate;
            InventoryLedger.ItemID = model.ItemID;
            InventoryLedger.TransNumber = model.TransNumber;
            InventoryLedger.ILLineNumber = model.ILLineNumber;
            InventoryLedger.TransactionType = model.TransactionType;
            InventoryLedger.WarehouseID = model.WarehouseID;
            InventoryLedger.WarehouseBinID = model.WarehouseBinID;
            InventoryLedger.Quantity = model.Quantity;
            InventoryLedger.CostPerUnit = model.CostPerUnit;
            InventoryLedger.TotalCost = model.TotalCost;
            InventoryLedger.LIFOCost = model.LIFOCost;
            InventoryLedger.AverageCost = model.AverageCost;
            InventoryLedger.FIFOCost = model.FIFOCost;
            InventoryLedger.ExpectedCost = model.ExpectedCost;
            InventoryLedger.OtherCost = model.OtherCost;
            InventoryLedger.StandardCost = model.StandardCost;
            InventoryLedger.BalanceQty = model.BalanceQty;
            InventoryLedger.BatchNumber = model.BatchNumber;
            InventoryLedger.AccountName = model.AccountName;
            InventoryLedger.OriginalDocumentNumber = model.OriginalDocumentNumber;
            InventoryLedger.ReferenceNumber = model.ReferenceNumber;
            InventoryLedger.CurrencyID = model.CurrencyID;
            InventoryLedger.CurrencyExchangeRate = model.CurrencyExchangeRate;
            InventoryLedger.AdjustedBalanceQty = model.AdjustedBalanceQty;


            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        #endregion
    }
}
