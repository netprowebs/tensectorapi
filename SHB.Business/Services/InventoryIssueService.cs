﻿using IPagedList;
using Microsoft.EntityFrameworkCore;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.Core.Domain.Entities.Enums;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Business.Services
{
    public interface IInventoryIssueService
    {
        //Repushing for Mr. Segun

        #region Requisition Header
        Task AddRequisitionHeader(RequisitionHeaderDTO model);
        Task<RequisitionHeaderDTO> GetRequisitionHeader(Guid Id);
        Task<IPagedList<RequisitionHeaderDTO>> GetAllRequisitionHeader(int pageNumber, int pageSize, string searchTerm);
        Task<IPagedList<RequisitionHeaderDTO>> GetAllRequestHeader(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateRequisitionHeader(RequisitionHeaderDTO model, Guid Id);
        Task DeleteRequisitionHeader(Guid Id);

        Task<bool> ReceiveRequisition(RequisitionHeaderDTO model, Guid Id);
        Task<bool> VerifyRequisition(RequisitionHeaderDTO model, Guid Id);
        Task<bool> ApproveRequisition(RequisitionHeaderDTO model, Guid Id);
        Task<bool> ReturnRequisition(RequisitionHeaderDTO model, Guid Id);
        #endregion

        #region Requisition Detail
        Task AddRequisitionDetail(RequisitionDetailDTO model);
        Task<List<RequisitionDetailDTO>> GetRequisitionDetail(Guid Id);
        Task<IPagedList<RequisitionDetailDTO>> GetAllRequisitionDetail(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateRequisitionDetail(RequisitionDetailDTO model, int Id);
        Task DeleteRequisitionDetail(int Id);
        #endregion


    }
    public class InventoryIssueService : IInventoryIssueService
    {
        private readonly IRepository<RequisitionHeader, Guid> _RequisitionHeaderRepo;
        private readonly IRepository<RequisitionDetail> _RequisitionDetailRepo;
        private readonly IRepository<InventoryByWarehouse, Guid> _InventoryByWarehouseRepo;
        private readonly IRepository<InventoryItem> _InventoryItemRepo;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;

        private readonly IUserService _userSvc;
        public InventoryIssueService(

             IRepository<RequisitionHeader, Guid> RequisitionHeaderRepo,
            IRepository<RequisitionDetail> RequisitionDetailRepo,
            IRepository<InventoryByWarehouse, Guid> InventoryByWarehouseRepo,
            IRepository<InventoryItem> InventoryItemRepo,
            IServiceHelper serviceHelper,
            IUnitOfWork unitOfWork,
            IUserService userSvc
            )
        {
            _RequisitionHeaderRepo = RequisitionHeaderRepo;
            _RequisitionDetailRepo = RequisitionDetailRepo;
            _InventoryByWarehouseRepo = InventoryByWarehouseRepo;
            _InventoryItemRepo = InventoryItemRepo;
            _serviceHelper = serviceHelper;
            _unitOfWork = unitOfWork;
            _userSvc = userSvc;
        }


        #region Inventory Recieved Header
        public async Task AddRequisitionHeader(RequisitionHeaderDTO model)
        {
            //model.RequisitionHeaderDescription = model.RequisitionHeaderDescription.Trim();
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);


            _RequisitionHeaderRepo.Insert(new RequisitionHeader
            {
                Id = model.Id,
                AdjustmentTypeID = model.AdjustmentTypeID,
                TransactionDate = model.TransactionDate,
                WarehouseID = model.WarehouseID,
                WarehouseBinID = model.WarehouseBinID,
                Notes = model.Notes,
                Captured = model.Captured,
                Verified = model.Verified,
                VerifiedBy = model.VerifiedBy,
                VerifiedDate = model.VerifiedDate,
                Issued = model.Issued,
                IssuedBy = model.IssuedBy,
                IssuedDate = model.IssuedDate,
                Approved = model.Approved,
                ApprovedBy = model.ApprovedBy,
                ApprovedDate = model.ApprovedDate,
                Posted = model.Posted,
                PostedBy = model.PostedBy,
                PostedDate = model.PostedDate,
                BatchControlNumber = model.BatchControlNumber,
                BatchControlTotal = model.BatchControlTotal,
                Signature = model.Signature,
                SignaturePassword = model.SignaturePassword,
                SupervisorSignature = model.SupervisorSignature,
                Location = model.Location,
                Department = model.Department,
                TotalQty = model.TotalQty,
                Amount = model.Amount,
                Code = model.Code,
                RequestType = model.RequestType,

                // customer name added
                CustomerName = model.CustomerName,
                CreatorUserId = currentUser.Id,
                CreationTime = DateTime.Now,
                Void = false,
                // CompanyId changed to TenantId
                TenantId = (int)currentUser.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteRequisitionHeader(Guid Id)
        {
            try
            {
                if (Id == null) { return; }
                await _RequisitionHeaderRepo.DeleteAsync(_RequisitionHeaderRepo.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<IPagedList<RequisitionHeaderDTO>> GetAllRequestHeader(int pageNumber, int pageSize, string searchTerm)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            try
            {
                var RequisitionHeader =
                    from model in _RequisitionHeaderRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) && model.TenantId == currentUser.CompanyId && model.AdjustmentTypeID == AdjustmentTypeID.Request //||
                    //model.VehicleRegistration.Contains(searchTerm)
                    orderby model.CreationTime descending

                    select new RequisitionHeaderDTO
                    {
                        Id = model.Id,
                        AdjustmentTypeID = model.AdjustmentTypeID,
                        TransactionDate = model.TransactionDate,
                        WarehouseID = model.WarehouseID,
                        WarehouseBinID = model.WarehouseBinID,
                        Notes = model.Notes,
                        Void = model.Void,
                        Verified = model.Verified,
                        VerifiedBy = model.VerifiedBy,
                        VerifiedDate = model.VerifiedDate,
                        Issued = model.Issued,
                        IssuedBy = model.IssuedBy,
                        IssuedDate = model.IssuedDate,
                        Approved = model.Approved,
                        ApprovedBy = model.ApprovedBy,
                        ApprovedDate = model.ApprovedDate,
                        Posted = model.Posted,
                        PostedBy = model.PostedBy,
                        PostedDate = model.PostedDate,
                        BatchControlNumber = model.BatchControlNumber,
                        BatchControlTotal = model.BatchControlTotal,
                        Signature = model.Signature,
                        SignaturePassword = model.SignaturePassword,
                        SupervisorSignature = model.SupervisorSignature,
                        CustomerName = model.CustomerName,
                        Location = model.Location, //Add Location field to DB
                        Department = model.Department, // Add Department field to DB
                        TotalQty = model.TotalQty,
                        Amount = model.Amount,
                        Code = model.Code,
                        RequestType = model.RequestType,
                        CreatedBy = currentUser.FirstName
                        //WarehouseCustomerID = model.WarehouseCustomerID,
                        //DeliveryNote = model.DeliveryNote,
                        //SiteNumber = model.SiteNumber,
                        //VehicleRegistration = model.VehicleRegistration,
                        //DriversId = model.DriversId,
                        //FromCompanyID = model.FromCompanyID,
                        //FromWarehouseID = model.FromWarehouseID,
                        //FromWarehouseBinID = model.FromWarehouseBinID,
                        //InventoryIssueTransferID = model.InventoryIssueTransferID,
                    };

                return await RequisitionHeader.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<IPagedList<RequisitionHeaderDTO>> GetAllRequisitionHeader(int pageNumber, int pageSize, string searchTerm)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            try
            {
                var RequisitionHeader =
                    from model in _RequisitionHeaderRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) && model.TenantId == currentUser.CompanyId && model.AdjustmentTypeID == AdjustmentTypeID.Issue  //||
                    //model.VehicleRegistration.Contains(searchTerm)
                    orderby model.CreationTime descending

                    select new RequisitionHeaderDTO
                    {
                        Id = model.Id,
                        AdjustmentTypeID = model.AdjustmentTypeID,
                        TransactionDate = model.TransactionDate,
                        WarehouseID = model.WarehouseID,
                        WarehouseBinID = model.WarehouseBinID,
                        Notes = model.Notes,
                        Verified = model.Verified,
                        VerifiedBy = model.VerifiedBy,
                        VerifiedDate = model.VerifiedDate,
                        Issued = model.Issued,
                        IssuedBy = model.IssuedBy,
                        IssuedDate = model.IssuedDate,
                        Approved = model.Approved,
                        ApprovedBy = model.ApprovedBy,
                        ApprovedDate = model.ApprovedDate,
                        Posted = model.Posted,
                        PostedBy = model.PostedBy,
                        PostedDate = model.PostedDate,
                        BatchControlNumber = model.BatchControlNumber,
                        BatchControlTotal = model.BatchControlTotal,
                        Signature = model.Signature,
                        SignaturePassword = model.SignaturePassword,
                        SupervisorSignature = model.SupervisorSignature,
                        CustomerName = model.CustomerName,
                        Location = model.Location, //Add Location field to DB
                        Department = model.Department, // Add Department field to DB
                        TotalQty = model.TotalQty,
                        Amount = model.Amount,
                        Code = model.Code,
                        RequestType = model.RequestType,
                        //Currency = model.Currency,
                        //CurrencyExchangeRate = model.CurrencyExchangeRate,
                        //Reference = model.Reference,
                        //WarehouseCustomerID = model.WarehouseCustomerID,
                        //DeliveryNote = model.DeliveryNote,
                        //SiteNumber = model.SiteNumber,
                        //VehicleRegistration = model.VehicleRegistration,
                        //DriversId = model.DriversId,
                        //FromCompanyID = model.FromCompanyID,
                        //FromWarehouseID = model.FromWarehouseID,
                        //FromWarehouseBinID = model.FromWarehouseBinID,
                        //InventoryIssueTransferID = model.InventoryIssueTransferID,
                    };

                return await RequisitionHeader.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task<RequisitionHeaderDTO> GetRequisitionHeader(Guid Id)
        {
            var model = _RequisitionHeaderRepo.FirstOrDefault(x => x.Id == Id);

            return new RequisitionHeaderDTO
            {
                Id = model.Id,
                AdjustmentTypeID = model.AdjustmentTypeID,
                TransactionDate = model.TransactionDate,
                WarehouseID = model.WarehouseID,
                WarehouseBinID = model.WarehouseBinID,
                Notes = model.Notes,
                Void = model.Void,
                Captured = model.Captured,
                Verified = model.Verified,
                VerifiedBy = model.VerifiedBy,
                VerifiedDate = model.VerifiedDate,
                Issued = model.Issued,
                IssuedBy = model.IssuedBy,
                IssuedDate = model.IssuedDate,
                Approved = model.Approved,
                ApprovedBy = model.ApprovedBy,
                ApprovedDate = model.ApprovedDate,
                Posted = model.Posted,
                PostedBy = model.PostedBy,
                PostedDate = model.PostedDate,
                Code = model.Code,
                Amount = model.Amount,
                BatchControlNumber = model.BatchControlNumber,
                BatchControlTotal = model.BatchControlTotal,
                Signature = model.Signature,
                SignaturePassword = model.SignaturePassword,
                SupervisorSignature = model.SupervisorSignature,
                CustomerName = model.CustomerName,
                Location = model.Location, //Add Location field to DB
                Department = model.Department, // Add Department field to DB
                TotalQty = model.TotalQty,
                RequestType = model.RequestType,
                //Currency = model.Currency,
                //CurrencyExchangeRate = model.CurrencyExchangeRate,
                //Reference = model.Reference,
                //WarehouseCustomerID = model.WarehouseCustomerID,
                //DeliveryNote = model.DeliveryNote,
                //SiteNumber = model.SiteNumber,
                //VehicleRegistration = model.VehicleRegistration,
                //DriversId = model.DriversId,
                //FromCompanyID = model.FromCompanyID,
                //FromWarehouseID = model.FromWarehouseID,
                //FromWarehouseBinID = model.FromWarehouseBinID,
                //InventoryIssueTransferID = model.InventoryIssueTransferID,
            };
        }


        public async Task<bool> UpdateRequisitionHeader(RequisitionHeaderDTO model, Guid Id)
        {
            var RequisitionHeader = _RequisitionHeaderRepo.FirstOrDefault(x => x.Id == Id);

            //RequisitionHeader.Id = model.Id;
            RequisitionHeader.AdjustmentTypeID = model.AdjustmentTypeID;
            RequisitionHeader.TransactionDate = model.TransactionDate;
            RequisitionHeader.WarehouseID = model.WarehouseID;
            RequisitionHeader.WarehouseBinID = model.WarehouseBinID;
            RequisitionHeader.Notes = model.Notes;
            //RequisitionHeader.Void = model.Void;
            RequisitionHeader.Captured = model.Captured;
            RequisitionHeader.Verified = model.Verified;
            RequisitionHeader.VerifiedBy = model.VerifiedBy;
            RequisitionHeader.VerifiedDate = model.VerifiedDate;
            RequisitionHeader.Issued = model.Issued;
            RequisitionHeader.IssuedBy = model.IssuedBy;
            RequisitionHeader.IssuedDate = model.IssuedDate;
            RequisitionHeader.Approved = model.Approved;
            RequisitionHeader.ApprovedBy = model.ApprovedBy;
            RequisitionHeader.ApprovedDate = model.ApprovedDate;
            RequisitionHeader.Posted = model.Posted;
            RequisitionHeader.PostedBy = model.PostedBy;
            RequisitionHeader.PostedDate = model.PostedDate;
            RequisitionHeader.BatchControlNumber = model.BatchControlNumber;
            RequisitionHeader.BatchControlTotal = model.BatchControlTotal;
            RequisitionHeader.Signature = model.Signature;
            RequisitionHeader.SignaturePassword = model.SignaturePassword;
            RequisitionHeader.SupervisorSignature = model.SupervisorSignature;
            RequisitionHeader.CustomerName = model.CustomerName;
            RequisitionHeader.Location = model.Location; //Add Location field to DB
            RequisitionHeader.Department = model.Department; // Add Department field to DB
            RequisitionHeader.TotalQty = model.TotalQty;
            RequisitionHeader.Amount = model.Amount;
            RequisitionHeader.Code = model.Code;
            RequisitionHeader.RequestType = model.RequestType;
            //RequisitionHeader.ManagerSignature = model.ManagerSignature;
            //RequisitionHeader.Currency = model.Currency;
            //RequisitionHeader.CurrencyExchangeRate = model.CurrencyExchangeRate;
            //RequisitionHeader.Reference = model.Reference;
            //RequisitionHeader.WarehouseCustomerID = model.WarehouseCustomerID;
            //RequisitionHeader.DeliveryNote = model.DeliveryNote;
            //RequisitionHeader.SiteNumber = model.SiteNumber;
            //RequisitionHeader.VehicleRegistration = model.VehicleRegistration;
            //RequisitionHeader.DriversId = model.DriversId;
            //RequisitionHeader.FromCompanyID = model.FromCompanyID;
            //RequisitionHeader.FromWarehouseID = model.FromWarehouseID;
            //RequisitionHeader.FromWarehouseBinID = model.FromWarehouseBinID;
            //RequisitionHeader.InventoryIssueTransferID = model.InventoryIssueTransferID;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        public async Task<bool> VerifyRequisition(RequisitionHeaderDTO model, Guid Id)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            var Requisition = _RequisitionHeaderRepo.FirstOrDefault(x => x.Id == Id);

            //var item = _itemRepo.Get(Request.ItemID);

            var AppEmail = "no-reply@libmot.com";
            var VerifyEmail = "netprowebs@gmail.com";

            Requisition.Verified = true;
            Requisition.VerifiedDate = DateTime.Now;
            Requisition.VerifiedBy = currentUser.Id.ToString();
            Requisition.Void = false;

            // Send sms and email containing account details
            //string smsMessage = $"Recieve of {Request.Quantity} unit/s of {items.ItemName} has been verified. Awaiting your approval";
            //string smsMessage = $"Recieve of test quantity has been verified. Awaiting your approval";
            ////_smsSvc.SendSMSNow(smsMessage, recipient: newAdded.ContactName.ToNigeriaMobile());

            //if (currentUserEmail != null)
            //{
            //    var mail = new Mail(AppEmail, "Awaiting Approval!", VerifyEmail)
            //    {
            //        Body = smsMessage
            //    };
            //    _mailSvc.SendMail(mail);
            //}

            await _unitOfWork.SaveChangesAsync();

            return true;
        }


        public async Task<bool> ReceiveRequisition(RequisitionHeaderDTO model, Guid Id)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            var Requisition = _RequisitionHeaderRepo.FirstOrDefault(x => x.Id == Id);
            var AppEmail = "no-reply@libmot.com";
            var ReceiveEmail = "netprowebs@gmail.com";

            Requisition.Issued = true;
            Requisition.IssuedDate = DateTime.Now;
            Requisition.IssuedBy = currentUser.Id.ToString();
            //Requisition.ApprovedBy = $"{currentUser.FirstName} {currentUser.LastName}";
            Requisition.CreatorUserId = currentUser.Id;
            Requisition.Void = false;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }
        public async Task<bool> ApproveRequisition(RequisitionHeaderDTO model, Guid Id)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            var Requisition = _RequisitionHeaderRepo.FirstOrDefault(x => x.Id == Id);
            var AppEmail = "no-reply@libmot.com";
            var VerifyEmail = "netprowebs@gmail.com";

            Requisition.Approved = true;
            Requisition.ApprovedDate = DateTime.Now;
            Requisition.ApprovedBy = currentUser.Id.ToString();
            Requisition.Void = false;

            //Reduce QtyOnHand on InventoryByWarehouse
            var Detail = from m in await GetRequisitionDetail(Id)
                             //where string.IsNullOrWhiteSpace(searchTerm) //||
                         //join c in _InventoryItemRepo.GetAll() on m.ItemID equals c.Id
                        // orderby m.CR descending

                         select new RequisitionDetailDTO
                         {
                             WarehouseID = m.WarehouseID,
                             WarehouseBinID = m.WarehouseBinID,
                             ItemID = m.ItemID,
                             IssuedQty = m.IssuedQty,
                             RequestedQty = m.RequestedQty,
                             //ItemValue = Convert.ToDecimal( c.ItemName)
                         };

            foreach (var item in Detail)
            {
                await _InventoryByWarehouseRepo.DeleteAsync(new InventoryByWarehouse
                {
                    WarehouseID = (int)item.WarehouseID,
                    WarehouseBinID = (int)item.WarehouseBinID,
                    ItemID = Convert.ToInt32(item.ItemID),
                    //QtyOnHand = item.IssuedQty,
                    ItemName = item.ItemName
                });
            }

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        public async Task<bool> ReturnRequisition(RequisitionHeaderDTO model, Guid Id)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            var Requisition = _RequisitionHeaderRepo.FirstOrDefault(x => x.Id == Id);

            var AppEmail = "no-reply@libmot.com";
            var ReturnEmail = "netprowebs@gmail.com";

            if (model.RetType == "2")
            {
                Requisition.Verified = false;
                Requisition.Void = true;
                Requisition.ReturnDate = DateTime.UtcNow;
                Requisition.ReturnNotes = model.ReturnNotes;
            }
            else
            {
                Requisition.Issued = false;
                Requisition.Void = true;
                Requisition.ReturnDate = DateTime.UtcNow;
                Requisition.ReturnNotes = model.ReturnNotes;
            }


            //// Send sms and email conataining account details
            //string smsMessage = $"Return of test quantity has been Return. Awaiting your correction";
            ////_smsSvc.SendSMSNow(smsMessage, recipient: newAdded.ContactName.ToNigeriaMobile());

            //if (currentUserEmail != null)
            //{
            //    var mail = new Mail(AppEmail, "Awaiting Correction!", ReturnEmail)
            //    {
            //        Body = smsMessage
            //    };
            //    _mailSvc.SendMail(mail);
            //}

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        #endregion

        #region Inventory Received Detail


        public async Task AddRequisitionDetail(RequisitionDetailDTO model)
        {
            //model.RequisitionDetailDescription = model.RequisitionDetailDescription.Trim();
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);



            _RequisitionDetailRepo.Insert(new RequisitionDetail
            {
                RequisitionID = model.RequisitionID,
                ItemID = model.ItemID,
                Description = model.Description,
                RequestedQty = model.RequestedQty,
                WarehouseID = model.WarehouseID,
                WarehouseBinID = model.WarehouseBinID,
                ToWarehouseID = model.ToWarehouseID,
                ToWarehouseBinID = model.ToWarehouseBinID,
                GLExpenseAccount = model.GLExpenseAccount,
                ItemValue = model.ItemValue,
                CostMethod = model.CostMethod,
                ProjectID = model.ProjectID,
                ItemCost = model.ItemCost,
                Staff = model.Staff,
                Vehicle = model.Vehicle,
                GLAnalysisType1 = model.GLAnalysisType1,
                GLAnalysisType2 = model.GLAnalysisType2,
                AssetID = model.AssetID,
                PONumber = model.PONumber,
                ItemUPCCode = model.ItemUPCCode,
                // Added according model props
                OrderQty = model.OrderQty,
                IssuedQty = model.IssuedQty,
                TaxGroupID = model.TaxGroupID,
                ItemName = model.ItemName,

                //ReceivedQty = model.ReceivedQty,
                //InventoryTransferID = model.InventoryTransferID,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                //CompanyId = (int)currentUser.CompanyId
                TenantId = (int)currentUser.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteRequisitionDetail(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _RequisitionDetailRepo.DeleteAsync(_RequisitionDetailRepo.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<IPagedList<RequisitionDetailDTO>> GetAllRequisitionDetail(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var RequisitionDetail =
                    from model in _RequisitionDetailRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) ||
                    model.Description.Contains(searchTerm)// ||
                    //Warehouse.WarehouseCode.Contains(searchTerm)
                    orderby model.CreationTime descending

                    select new RequisitionDetailDTO
                    {
                        Id = model.Id,
                        RequisitionID = model.RequisitionID,
                        ItemID = model.ItemID,
                        Description = model.Description,
                        RequestedQty = model.RequestedQty,
                        WarehouseID = model.WarehouseID,
                        WarehouseBinID = model.WarehouseBinID,
                        ToWarehouseID = model.ToWarehouseID,
                        ToWarehouseBinID = model.ToWarehouseBinID,
                        GLExpenseAccount = model.GLExpenseAccount,
                        ItemValue = model.ItemValue,
                        CostMethod = model.CostMethod,
                        ProjectID = model.ProjectID,
                        ItemCost = model.ItemCost,
                        GLAnalysisType1 = model.GLAnalysisType1,
                        GLAnalysisType2 = model.GLAnalysisType2,
                        AssetID = model.AssetID,
                        PONumber = model.PONumber,
                        ItemUPCCode = model.ItemUPCCode,
                        Staff = model.Staff,
                        Vehicle = model.Vehicle,
                        ItemName = model.ItemName,
                        // Added according model props
                        OrderQty = model.OrderQty,
                        IssuedQty = model.IssuedQty,
                        TaxGroupID = model.TaxGroupID,
                        //ReceivedQty = model.ReceivedQty,
                        //InventoryTransferID = model.InventoryTransferID,
                    };

                return await RequisitionDetail.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task<List<RequisitionDetailDTO>> GetRequisitionDetail(Guid Id)
        {
            //var model = _RequisitionDetailRepo.FirstOrDefault(x => x.Id == Id);
            var invreqdetails = from model in _RequisitionDetailRepo.GetAll()
                                where model.RequisitionID == Id

                                select new RequisitionDetailDTO
                                {
                                    Id = model.Id,
                                    //InventoryReceivedID = model.InventoryReceivedID,
                                    ItemID = model.ItemID,
                                    Description = model.Description,
                                    RequestedQty = model.RequestedQty,
                                    WarehouseID = model.WarehouseID,
                                    WarehouseBinID = model.WarehouseBinID,
                                    ToWarehouseID = model.ToWarehouseID,
                                    ToWarehouseBinID = model.ToWarehouseBinID,
                                    GLExpenseAccount = model.GLExpenseAccount,
                                    ItemValue = model.ItemValue,
                                    CostMethod = model.CostMethod,
                                    ProjectID = model.ProjectID,
                                    ItemCost = model.ItemCost,
                                    GLAnalysisType1 = model.GLAnalysisType1,
                                    GLAnalysisType2 = model.GLAnalysisType2,
                                    AssetID = model.AssetID,
                                    PONumber = model.PONumber,
                                    ItemUPCCode = model.ItemUPCCode,
                                    Staff = model.Staff,
                                    Vehicle = model.Vehicle,
                                    // Added according model props
                                    OrderQty = model.OrderQty,
                                    IssuedQty = model.IssuedQty,
                                    TaxGroupID = model.TaxGroupID,
                                    ItemName = model.ItemName,

                                    //ReceivedQty = model.ReceivedQty,
                                    //InventoryTransferID = model.InventoryTransferID,
                                };
            return invreqdetails.ToList();
        }


        public async Task<bool> UpdateRequisitionDetail(RequisitionDetailDTO model, int Id)
        {
            var RequisitionDetail = _RequisitionDetailRepo.FirstOrDefault(x => x.Id == Id);

            //RequisitionDetail.Id = model.Id;
            //RequisitionDetail.InventoryReceivedID = model.InventoryReceivedID;
            RequisitionDetail.ItemID = model.ItemID;
            RequisitionDetail.Description = model.Description;
            RequisitionDetail.RequestedQty = model.RequestedQty;
            RequisitionDetail.WarehouseID = model.WarehouseID;
            RequisitionDetail.WarehouseBinID = model.WarehouseBinID;
            RequisitionDetail.ToWarehouseID = model.ToWarehouseID;
            RequisitionDetail.ToWarehouseBinID = model.ToWarehouseBinID;
            RequisitionDetail.GLExpenseAccount = model.GLExpenseAccount;
            RequisitionDetail.ItemValue = model.ItemValue;
            RequisitionDetail.CostMethod = model.CostMethod;
            RequisitionDetail.ProjectID = model.ProjectID;
            RequisitionDetail.ItemCost = model.ItemCost;
            RequisitionDetail.GLAnalysisType1 = model.GLAnalysisType1;
            RequisitionDetail.GLAnalysisType2 = model.GLAnalysisType2;
            RequisitionDetail.AssetID = model.AssetID;
            RequisitionDetail.PONumber = model.PONumber;
            RequisitionDetail.ItemUPCCode = model.ItemUPCCode;
            RequisitionDetail.Staff = model.Staff;
            RequisitionDetail.Vehicle = model.Vehicle;
            // Added according model props
            RequisitionDetail.OrderQty = model.OrderQty;
            RequisitionDetail.IssuedQty = model.IssuedQty;
            RequisitionDetail.TaxGroupID = model.TaxGroupID;
            RequisitionDetail.ItemName = model.ItemName;

            //RequisitionDetail.ReceivedQty = model.ReceivedQty;
            //RequisitionDetail.InventoryTransferID = model.InventoryTransferID;


            await _unitOfWork.SaveChangesAsync();

            return true;
        }





        #endregion
    }
}
