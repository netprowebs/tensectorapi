﻿using IPagedList;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SHB.Core.Domain.Entities;
using System;

namespace SHB.Business.Services
{
    public interface IVehicleModelService
    {
        Task<IPagedList<VehicleModelDTO>> GetVehicleModels(int page, int size, string query = null);
        Task<VehicleModelDTO> GetVehicleModelById(int vehicleModelId);
        Task AddVehicleModel(VehicleModelDTO vehicleModel);
        Task UpdateVehicleModel(int vehicleModelId, VehicleModelDTO vehicleModel);
        Task RemoveVehicleModel(int vehicleModelId);
    }

    public class VehicleModelService : IVehicleModelService
    {
        private readonly IRepository<VehicleModel> _vehicleModelRepo;
        private readonly IRepository<Route> _routeRepo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceHelper _serviceHelper;

        public VehicleModelService(
            IRepository<VehicleModel> vehicleModelRepo,
            IRepository<Route> routeRepo,
            IUnitOfWork unitOfWork,
            IServiceHelper serviceHelper)
        {
            _vehicleModelRepo = vehicleModelRepo;
            _routeRepo = routeRepo;
            _unitOfWork = unitOfWork;
            _serviceHelper = serviceHelper;
        }

        private async Task<bool> IsValidRoute(int routeId)
        {
            return routeId > 0 &&
                 await _routeRepo.ExistAsync(m => m.Id == routeId);
        }

        public async Task AddVehicleModel(VehicleModelDTO vehicleModelDto)
        {
            //if (!await IsValidRoute(vehicleModelDto.RouteId))
            //{
            //    throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ROUTE_NOT_EXIST);
            //}


            //fareDto.Route.Name = fareDto.Route.Name.Trim();

            if (await _vehicleModelRepo.ExistAsync(v => v.Id == vehicleModelDto.Id))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VEHICLE_MODEL_EXIST);
            }

            _vehicleModelRepo.Insert(new VehicleModel
            {
                Id = vehicleModelDto.Id,
                Name = vehicleModelDto.Name,
                NumberOfSeats = vehicleModelDto.NumberOfSeats,
                VehicleModelTypeCode = vehicleModelDto.VehicleModelTypeCode,
                VehicleMakeId = vehicleModelDto.VehicleMakeId,

            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<VehicleModelDTO> GetVehicleModelById(int vehicleModelId)
        {
            var vehicleModel = await _vehicleModelRepo.GetAsync(vehicleModelId);

            if (vehicleModel == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VEHICLE_MODEL_NOT_EXIST);
            }

            return new VehicleModelDTO
            {
                Id = vehicleModel.Id,
                Name = vehicleModel.Name,
                NumberOfSeats = vehicleModel.NumberOfSeats,
                VehicleModelTypeCode = vehicleModel.VehicleModelTypeCode,
                VehicleMakeId = vehicleModel.VehicleMakeId,

            };
        }

        public Task<IPagedList<VehicleModelDTO>> GetVehicleModels(int page, int size, string query = null)
        {
            var vehicleModels =
                from vehicleModel in _vehicleModelRepo.GetAll()
                //join terminal in _terminalRepo.GetAll() on subRoute.DepartureTerminalId equals terminal.Id
                orderby vehicleModel.Id descending
               // where string.IsNullOrWhiteSpace(query) || trip.Name.Contains(query)
                select new VehicleModelDTO
                {
                    Id = vehicleModel.Id,
                    Name = vehicleModel.Name,
                    NumberOfSeats = vehicleModel.NumberOfSeats,
                    VehicleModelTypeCode = vehicleModel.VehicleModelTypeCode,
                    VehicleMakeId = vehicleModel.VehicleMakeId,
                };

            return vehicleModels.AsNoTracking().ToPagedListAsync(page, size);
        }

        public async Task RemoveVehicleModel(int vehicleModelId)
        {
            var vehicleModel = await _vehicleModelRepo.GetAsync(vehicleModelId);

            if (vehicleModel == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VEHICLE_MODEL_NOT_EXIST);
            }

            _vehicleModelRepo.Delete(vehicleModel);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateVehicleModel(int vehicleModelId, VehicleModelDTO vehicleModel)
        {
            var vehicleModels = await _vehicleModelRepo.GetAsync(vehicleModelId);

            if (vehicleModels == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VEHICLE_MODEL_NOT_EXIST);
            }

            await _unitOfWork.SaveChangesAsync();
        }
    }
}
