﻿using IPagedList;
using Microsoft.EntityFrameworkCore;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Business.Services
{
    public interface IInventoryTransferService
    {

        #region Inventory Transfer Header
        Task AddInventoryTransferHeader(InventoryTransferHeaderDTO model);
        Task<InventoryTransferHeaderDTO> GetInventoryTransferHeader(Guid Id);
        Task<IPagedList<InventoryTransferHeaderDTO>> GetAllInventoryTransferHeader(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateInventoryTransferHeader(InventoryTransferHeaderDTO model, Guid Id);
        Task DeleteInventoryTransferHeader(Guid Id);
        Task<bool> ReceiveTransfer(InventoryTransferHeaderDTO model, Guid Id);
        Task<bool> VerifyTransfer(InventoryTransferHeaderDTO model, Guid Id);
        Task<bool> ApproveTransfer(InventoryTransferHeaderDTO model, Guid Id);
        Task<bool> ReturnTransfer(InventoryTransferHeaderDTO model, Guid Id);
        #endregion

        #region Inventory Transfer Detail
        Task AddInventoryTransferDetail(InventoryTransferDetailDTO model);
        Task<List<InventoryTransferDetailDTO>> GetInventoryTransferDetail(Guid Id);
        Task<IPagedList<InventoryTransferDetailDTO>> GetAllInventoryTransferDetail(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateInventoryTransferDetail(InventoryTransferDetailDTO model, int Id);
        Task DeleteInventoryTransferDetail(int Id);
        #endregion


    }

    public class InventoryTransferService :IInventoryTransferService
    {
        private readonly IRepository<InventoryTransferHeader, Guid> _InventoryTransferHeaderRepo;
        private readonly IRepository<InventoryTransferDetail> _InventoryTransferDetailRepo;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;

        private readonly IUserService _userSvc;
        public InventoryTransferService(

             IRepository<InventoryTransferHeader, Guid> InventoryTransferHeaderRepo,
            IRepository<InventoryTransferDetail> InventoryTransferDetailRepo,
            IServiceHelper serviceHelper,
            IUnitOfWork unitOfWork,
            IUserService userSvc
            )
        {
            _InventoryTransferHeaderRepo = InventoryTransferHeaderRepo;
            _InventoryTransferDetailRepo = InventoryTransferDetailRepo;
            _serviceHelper = serviceHelper;
            _unitOfWork = unitOfWork;
            _userSvc = userSvc;
        }


        #region Inventory Recieved Header
        public async Task AddInventoryTransferHeader(InventoryTransferHeaderDTO model)
        {
            //model.InventoryTransferHeaderDescription = model.InventoryTransferHeaderDescription.Trim();
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);


            _InventoryTransferHeaderRepo.Insert(new InventoryTransferHeader
            {
                Id = model.Id,
                AdjustmentTypeID = model.AdjustmentTypeID,
                TransactionDate = model.TransactionDate,
                WarehouseID = model.WarehouseID,
                WarehouseBinID = model.WarehouseBinID,
                Notes = model.Notes,
                Captured = model.Captured,
                Verified = model.Verified,
                VerifiedBy = model.VerifiedBy,
                VerifiedDate = model.VerifiedDate,
                Issued = model.Issued,
                IssuedBy = model.IssuedBy,
                IssuedDate = model.IssuedDate,
                Approved = model.Approved,
                ApprovedBy = model.ApprovedBy,
                ApprovedDate = model.ApprovedDate,
                Posted = model.Posted,
                PostedBy = model.PostedBy,
                PostedDate = model.PostedDate,
                BatchControlNumber = model.BatchControlNumber,
                BatchControlTotal = model.BatchControlTotal,
                Signature = model.Signature,
                SignaturePassword = model.SignaturePassword,
                SupervisorSignature = model.SupervisorSignature,
                

                // Added according to model props
                Cleared = model.Cleared,
                EnteredBy = model.EnteredBy,
                Void = model.Void,
                ManagerSignature = model.ManagerSignature,
                ManagerPassword = model.ManagerPassword,
                CurrencyID = model.CurrencyID,
                CurrencyExchangeRate = model.CurrencyExchangeRate,
                InterTransfer = model.InterTransfer,
                ToCompanyID = model.ToCompanyID,
                LoadingOfficer = model.LoadingOfficer,
                Rate = model.Rate,
                Code = model.Code,
                Vehicle = model.Vehicle,
                Staff = model.Staff,
                TotalQty = model.TotalQty,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                // CompanyId changed to TenantId
                TenantID = (int)currentUser.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteInventoryTransferHeader(Guid Id)
        {
            try
            {
                if (Id == null) { return; }
                await _InventoryTransferHeaderRepo.DeleteAsync(_InventoryTransferHeaderRepo.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<IPagedList<InventoryTransferHeaderDTO>> GetAllInventoryTransferHeader(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var InventoryTransferHeader =
                    from model in _InventoryTransferHeaderRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) //||
                    //model.VehicleRegistration.Contains(searchTerm)
                    orderby model.CreationTime descending

                    select new InventoryTransferHeaderDTO
                    {
                        Id = model.Id,
                        AdjustmentTypeID = model.AdjustmentTypeID,
                        TransactionDate = model.TransactionDate,
                        WarehouseID = model.WarehouseID,
                        WarehouseBinID = model.WarehouseBinID,
                        Notes = model.Notes,
                        Verified = model.Verified,
                        VerifiedBy = model.VerifiedBy,
                        VerifiedDate = model.VerifiedDate,
                        Issued = model.Issued,
                        IssuedBy = model.IssuedBy,
                        IssuedDate = model.IssuedDate,
                        Approved = model.Approved,
                        ApprovedBy = model.ApprovedBy,
                        ApprovedDate = model.ApprovedDate,
                        Posted = model.Posted,
                        PostedBy = model.PostedBy,
                        PostedDate = model.PostedDate,
                        BatchControlNumber = model.BatchControlNumber,
                        BatchControlTotal = model.BatchControlTotal,
                        Signature = model.Signature,
                        SignaturePassword = model.SignaturePassword,
                        SupervisorSignature = model.SupervisorSignature,

                        // Added according to model props
                        Cleared = model.Cleared,
                        EnteredBy = model.EnteredBy,
                        Void = model.Void,
                        ManagerSignature = model.ManagerSignature,
                        ManagerPassword = model.ManagerPassword,
                        CurrencyID = model.CurrencyID,
                        CurrencyExchangeRate = model.CurrencyExchangeRate,
                        InterTransfer = model.InterTransfer,
                        ToCompanyID = model.ToCompanyID,
                        LoadingOfficer = model.LoadingOfficer,
                        Rate = model.Rate,
                        Code = model.Code,
                        Vehicle = model.Vehicle,
                        Staff = model.Staff,
                        TotalQty = model.TotalQty,
                        //CustomerName = model.CustomerName,
                        //Currency = model.Currency,
                        //CurrencyExchangeRate = model.CurrencyExchangeRate,
                        //Reference = model.Reference,
                        //WarehouseCustomerID = model.WarehouseCustomerID,
                        //DeliveryNote = model.DeliveryNote,
                        //SiteNumber = model.SiteNumber,
                        //VehicleRegistration = model.VehicleRegistration,
                        //DriversId = model.DriversId,
                        //FromCompanyID = model.FromCompanyID,
                        //FromWarehouseID = model.FromWarehouseID,
                        //FromWarehouseBinID = model.FromWarehouseBinID,
                        //InventoryIssueTransferID = model.InventoryIssueTransferID,
                    };

                return await InventoryTransferHeader.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task<InventoryTransferHeaderDTO> GetInventoryTransferHeader(Guid Id)
        {
            var model = _InventoryTransferHeaderRepo.FirstOrDefault(x => x.Id == Id);

            return new InventoryTransferHeaderDTO
            {
                Id = model.Id,
                AdjustmentTypeID = model.AdjustmentTypeID,
                TransactionDate = model.TransactionDate,
                WarehouseID = model.WarehouseID,
                WarehouseBinID = model.WarehouseBinID,
                Notes = model.Notes,
                //Void = model.Void,
                Captured = model.Captured,
                Verified = model.Verified,
                VerifiedBy = model.VerifiedBy,
                VerifiedDate = model.VerifiedDate,
                Issued = model.Issued,
                IssuedBy = model.IssuedBy,
                IssuedDate = model.IssuedDate,
                Approved = model.Approved,
                ApprovedBy = model.ApprovedBy,
                ApprovedDate = model.ApprovedDate,
                Posted = model.Posted,
                PostedBy = model.PostedBy,
                PostedDate = model.PostedDate,
                BatchControlNumber = model.BatchControlNumber,
                BatchControlTotal = model.BatchControlTotal,
                Signature = model.Signature,
                SignaturePassword = model.SignaturePassword,
                SupervisorSignature = model.SupervisorSignature,

                // Added according to model props
                Cleared = model.Cleared,
                EnteredBy = model.EnteredBy,
                Void = model.Void,
                ManagerSignature = model.ManagerSignature,
                ManagerPassword = model.ManagerPassword,
                CurrencyID = model.CurrencyID,
                CurrencyExchangeRate = model.CurrencyExchangeRate,
                InterTransfer = model.InterTransfer,
                ToCompanyID = model.ToCompanyID,
                LoadingOfficer = model.LoadingOfficer,
                Rate = model.Rate,
                Code = model.Code,
                Vehicle = model.Vehicle,
                Staff = model.Staff,
                TotalQty = model.TotalQty,
                //CustomerName = model.CustomerName,
                //Currency = model.Currency,
                //CurrencyExchangeRate = model.CurrencyExchangeRate,
                //Reference = model.Reference,
                //WarehouseCustomerID = model.WarehouseCustomerID,
                //DeliveryNote = model.DeliveryNote,
                //SiteNumber = model.SiteNumber,
                //VehicleRegistration = model.VehicleRegistration,
                //DriversId = model.DriversId,
                //FromCompanyID = model.FromCompanyID,
                //FromWarehouseID = model.FromWarehouseID,
                //FromWarehouseBinID = model.FromWarehouseBinID,
                //InventoryIssueTransferID = model.InventoryIssueTransferID,
            };
        }


        public async Task<bool> UpdateInventoryTransferHeader(InventoryTransferHeaderDTO model, Guid Id)
        {
            var InventoryTransferHeader = _InventoryTransferHeaderRepo.FirstOrDefault(x => x.Id == Id);

            //InventoryTransferHeader.Id = model.Id;
            InventoryTransferHeader.AdjustmentTypeID = model.AdjustmentTypeID;
            InventoryTransferHeader.TransactionDate = model.TransactionDate;
            InventoryTransferHeader.WarehouseID = model.WarehouseID;
            InventoryTransferHeader.WarehouseBinID = model.WarehouseBinID;
            InventoryTransferHeader.Notes = model.Notes;
            //InventoryTransferHeader.Void = model.Void;
            InventoryTransferHeader.Captured = model.Captured;
            InventoryTransferHeader.Verified = model.Verified;
            InventoryTransferHeader.VerifiedBy = model.VerifiedBy;
            InventoryTransferHeader.VerifiedDate = model.VerifiedDate;
            InventoryTransferHeader.Issued = model.Issued;
            InventoryTransferHeader.IssuedBy = model.IssuedBy;
            InventoryTransferHeader.IssuedDate = model.IssuedDate;
            InventoryTransferHeader.Approved = model.Approved;
            InventoryTransferHeader.ApprovedBy = model.ApprovedBy;
            InventoryTransferHeader.ApprovedDate = model.ApprovedDate;
            InventoryTransferHeader.Posted = model.Posted;
            InventoryTransferHeader.PostedBy = model.PostedBy;
            InventoryTransferHeader.PostedDate = model.PostedDate;
            InventoryTransferHeader.BatchControlNumber = model.BatchControlNumber;
            InventoryTransferHeader.BatchControlTotal = model.BatchControlTotal;
            InventoryTransferHeader.Signature = model.Signature;
            InventoryTransferHeader.SignaturePassword = model.SignaturePassword;
            InventoryTransferHeader.SupervisorSignature = model.SupervisorSignature;

            // Added according to model props
            InventoryTransferHeader.Cleared = model.Cleared;
            InventoryTransferHeader.EnteredBy = model.EnteredBy;
            InventoryTransferHeader.Void = model.Void;
            InventoryTransferHeader.ManagerSignature = model.ManagerSignature;
            InventoryTransferHeader.ManagerPassword = model.ManagerPassword;
            InventoryTransferHeader.CurrencyID = model.CurrencyID;
            InventoryTransferHeader.CurrencyExchangeRate = model.CurrencyExchangeRate;
            InventoryTransferHeader.InterTransfer = model.InterTransfer;
            InventoryTransferHeader.ToCompanyID = model.ToCompanyID;
            InventoryTransferHeader.LoadingOfficer = model.LoadingOfficer;
            InventoryTransferHeader.Rate = model.Rate;
            InventoryTransferHeader.Code = model.Code;
            InventoryTransferHeader.Vehicle = model.Vehicle;
            InventoryTransferHeader.Staff = model.Staff;
            InventoryTransferHeader.TotalQty = model.TotalQty;
            //InventoryTransferHeader.CustomerName = model.CustomerName;
            //InventoryTransferHeader.ManagerSignature = model.ManagerSignature;
            //InventoryTransferHeader.Currency = model.Currency;
            //InventoryTransferHeader.CurrencyExchangeRate = model.CurrencyExchangeRate;
            //InventoryTransferHeader.Reference = model.Reference;
            //InventoryTransferHeader.WarehouseCustomerID = model.WarehouseCustomerID;
            //InventoryTransferHeader.DeliveryNote = model.DeliveryNote;
            //InventoryTransferHeader.SiteNumber = model.SiteNumber;
            //InventoryTransferHeader.VehicleRegistration = model.VehicleRegistration;
            //InventoryTransferHeader.DriversId = model.DriversId;
            //InventoryTransferHeader.FromCompanyID = model.FromCompanyID;
            //InventoryTransferHeader.FromWarehouseID = model.FromWarehouseID;
            //InventoryTransferHeader.FromWarehouseBinID = model.FromWarehouseBinID;
            //InventoryTransferHeader.InventoryIssueTransferID = model.InventoryIssueTransferID;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        public async Task<bool> VerifyTransfer(InventoryTransferHeaderDTO model, Guid Id)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            var Transfer = _InventoryTransferHeaderRepo.FirstOrDefault(x => x.Id == Id);

            //var item = _itemRepo.Get(Request.ItemID);

            var AppEmail = "no-reply@libmot.com";
            var VerifyEmail = "netprowebs@gmail.com";

            Transfer.Verified = true;
            Transfer.VerifiedDate = DateTime.Now;
            Transfer.VerifiedBy = currentUser.Id.ToString();
            Transfer.Void = false;

            // Send sms and email containing account details
            //string smsMessage = $"Recieve of {Request.Quantity} unit/s of {items.ItemName} has been verified. Awaiting your approval";
            //string smsMessage = $"Recieve of test quantity has been verified. Awaiting your approval";
            ////_smsSvc.SendSMSNow(smsMessage, recipient: newAdded.ContactName.ToNigeriaMobile());

            //if (currentUserEmail != null)
            //{
            //    var mail = new Mail(AppEmail, "Awaiting Approval!", VerifyEmail)
            //    {
            //        Body = smsMessage
            //    };
            //    _mailSvc.SendMail(mail);
            //}

            await _unitOfWork.SaveChangesAsync();

            return true;
        }


        public async Task<bool> ReceiveTransfer(InventoryTransferHeaderDTO model, Guid Id)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            var Transfer = _InventoryTransferHeaderRepo.FirstOrDefault(x => x.Id == Id);
            var AppEmail = "no-reply@libmot.com";
            var ReceiveEmail = "netprowebs@gmail.com";

            Transfer.Issued = true;
            Transfer.IssuedDate = DateTime.Now;
            Transfer.IssuedBy = currentUser.Id.ToString();
            //Requisition.ApprovedBy = $"{currentUser.FirstName} {currentUser.LastName}";
            Transfer.CreatorUserId = currentUser.Id;
            Transfer.Void = false;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }
        public async Task<bool> ApproveTransfer(InventoryTransferHeaderDTO model, Guid Id)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            var Transfer = _InventoryTransferHeaderRepo.FirstOrDefault(x => x.Id == Id);
            var AppEmail = "no-reply@libmot.com";
            var VerifyEmail = "netprowebs@gmail.com";

            Transfer.Approved = true;
            Transfer.ApprovedDate = DateTime.Now;
            Transfer.ApprovedBy = currentUser.Id.ToString();
            Transfer.Void = false;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        public async Task<bool> ReturnTransfer(InventoryTransferHeaderDTO model, Guid Id)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            var Transfer = _InventoryTransferHeaderRepo.FirstOrDefault(x => x.Id == Id);

            var AppEmail = "no-reply@libmot.com";
            var ReturnEmail = "netprowebs@gmail.com";

            if (model.SiteNumber == "2")
            {
                Transfer.Verified = false;
                Transfer.Void = true;
                Transfer.ReturnDate = DateTime.UtcNow;
                Transfer.ReturnNotes = model.ReturnNotes;
            }
            else
            {
                Transfer.Issued = false;
                Transfer.Void = true;
                Transfer.ReturnDate = DateTime.UtcNow;
                Transfer.ReturnNotes = model.ReturnNotes;
            }


            //// Send sms and email conataining account details
            //string smsMessage = $"Return of test quantity has been Return. Awaiting your correction";
            ////_smsSvc.SendSMSNow(smsMessage, recipient: newAdded.ContactName.ToNigeriaMobile());

            //if (currentUserEmail != null)
            //{
            //    var mail = new Mail(AppEmail, "Awaiting Correction!", ReturnEmail)
            //    {
            //        Body = smsMessage
            //    };
            //    _mailSvc.SendMail(mail);
            //}

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        #endregion

        #region Inventory Transfer Detail
        public async Task AddInventoryTransferDetail(InventoryTransferDetailDTO model)
        {
            //model.InventoryTransferDetailDescription = model.InventoryTransferDetailDescription.Trim();
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);



            _InventoryTransferDetailRepo.Insert(new InventoryTransferDetail
            {
                InventoryTransferID = model.InventoryTransferID,
                ItemID = model.ItemID,
                Description = model.Description,
                RequestedQty = model.RequestedQty,
                WarehouseID = model.WarehouseID,
                WarehouseBinID = model.WarehouseBinID,
                ToWarehouseID = model.ToWarehouseID,
                ToWarehouseBinID = model.ToWarehouseBinID,
                GLExpenseAccount = model.GLExpenseAccount,
                ItemValue = model.ItemValue,
                CostMethod = model.CostMethod,
                ProjectID = model.ProjectID,
                ItemCost = model.ItemCost,
                ItemUPCCode = model.ItemUPCCode,

                // Added according model props
                //InventoryTransferID = model.InventoryTransferID,
                GLAnalysisType = model.GLAnalysisType,
                AssetID = model.AssetID,
                ItemName = model.ItemName,
                ToCompanyID = model.ToCompanyID,
                TruckNumber = model.TruckNumber,
                QtyReceived = model.QtyReceived,
                DateReceived = model.DateReceived,
                TruckAdvance = model.TruckAdvance,
                TruckAdvBalance = model.TruckAdvBalance,
                LoadingTicket = model.LoadingTicket,

                //ReceivedQty = model.ReceivedQty,
                //InventoryTransferID = model.InventoryTransferID,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                //CompanyId = (int)currentUser.CompanyId
                TenantId = (int)currentUser.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteInventoryTransferDetail(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _InventoryTransferDetailRepo.DeleteAsync(_InventoryTransferDetailRepo.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<IPagedList<InventoryTransferDetailDTO>> GetAllInventoryTransferDetail(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var InventoryTransferDetail =
                    from model in _InventoryTransferDetailRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) ||
                    model.Description.Contains(searchTerm)// ||
                    //Warehouse.WarehouseCode.Contains(searchTerm)
                    orderby model.CreationTime descending

                    select new InventoryTransferDetailDTO
                    {
                        Id = model.Id,
                        InventoryTransferID = model.InventoryTransferID,
                        ItemID = model.ItemID,
                        Description = model.Description,
                        RequestedQty = model.RequestedQty,
                        WarehouseID = model.WarehouseID,
                        WarehouseBinID = model.WarehouseBinID,
                        ToWarehouseID = model.ToWarehouseID,
                        ToWarehouseBinID = model.ToWarehouseBinID,
                        GLExpenseAccount = model.GLExpenseAccount,
                        ItemValue = model.ItemValue,
                        CostMethod = model.CostMethod,
                        ProjectID = model.ProjectID,
                        ItemCost = model.ItemCost,
                        AssetID = model.AssetID,
                        ItemUPCCode = model.ItemUPCCode,

                        // Added according model props
                        //InventoryTransferID = model.InventoryTransferID,
                        GLAnalysisType = model.GLAnalysisType,
                        //AssetID = model.AssetID,
                        ItemName = model.ItemName,
                        ToCompanyID = model.ToCompanyID,
                        TruckNumber = model.TruckNumber,
                        QtyReceived = model.QtyReceived,
                        DateReceived = model.DateReceived,
                        TruckAdvance = model.TruckAdvance,
                        TruckAdvBalance = model.TruckAdvBalance,
                        LoadingTicket = model.LoadingTicket,
                        //ReceivedQty = model.ReceivedQty,
                        //InventoryTransferID = model.InventoryTransferID,
                    };

                return await InventoryTransferDetail.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public Task<List<InventoryTransferDetailDTO>> GetInventoryTransferDetail(Guid Id)
        {
            var InventoryTransferDetail = from model in _InventoryTransferDetailRepo.GetAll()
                                where model.InventoryTransferID == Id

                                select new InventoryTransferDetailDTO
                                            {
                                                Id = model.Id,
                                    InventoryTransferID = model.InventoryTransferID,
                                    ItemID = model.ItemID,
                                                Description = model.Description,
                                                RequestedQty = model.RequestedQty,
                                                WarehouseID = model.WarehouseID,
                                                WarehouseBinID = model.WarehouseBinID,
                                                ToWarehouseID = model.ToWarehouseID,
                                                ToWarehouseBinID = model.ToWarehouseBinID,
                                                GLExpenseAccount = model.GLExpenseAccount,
                                                ItemValue = model.ItemValue,
                                                CostMethod = model.CostMethod,
                                                ProjectID = model.ProjectID,
                                                ItemCost = model.ItemCost,
                                                AssetID = model.AssetID,
                                                ItemUPCCode = model.ItemUPCCode,

                                                // Added according model props
                                                //InventoryTransferID = model.InventoryTransferID,
                                                GLAnalysisType = model.GLAnalysisType,
                                                //AssetID = model.AssetID,
                                                ItemName = model.ItemName,
                                                ToCompanyID = model.ToCompanyID,
                                                TruckNumber = model.TruckNumber,
                                                QtyReceived = model.QtyReceived,
                                                DateReceived = model.DateReceived,
                                                TruckAdvance = model.TruckAdvance,
                                                TruckAdvBalance = model.TruckAdvBalance,
                                                LoadingTicket = model.LoadingTicket,

                                                //ReceivedQty = model.ReceivedQty,
                                                //InventoryTransferID = model.InventoryTransferID,
                                            };
            return InventoryTransferDetail.ToListAsync();
        }


        public async Task<bool> UpdateInventoryTransferDetail(InventoryTransferDetailDTO model, int Id)
        {
            var InventoryTransferDetail = _InventoryTransferDetailRepo.FirstOrDefault(x => x.Id == Id);

            //InventoryTransferDetail.Id = model.Id;
            //InventoryTransferDetail.InventoryReceivedID = model.InventoryReceivedID;
            InventoryTransferDetail.ItemID = model.ItemID;
            InventoryTransferDetail.Description = model.Description;
            InventoryTransferDetail.RequestedQty = model.RequestedQty;
            InventoryTransferDetail.WarehouseID = model.WarehouseID;
            InventoryTransferDetail.WarehouseBinID = model.WarehouseBinID;
            InventoryTransferDetail.ToWarehouseID = model.ToWarehouseID;
            InventoryTransferDetail.ToWarehouseBinID = model.ToWarehouseBinID;
            InventoryTransferDetail.GLExpenseAccount = model.GLExpenseAccount;
            InventoryTransferDetail.ItemValue = model.ItemValue;
            InventoryTransferDetail.CostMethod = model.CostMethod;
            InventoryTransferDetail.ProjectID = model.ProjectID;
            InventoryTransferDetail.ItemCost = model.ItemCost;
            InventoryTransferDetail.AssetID = model.AssetID;
            InventoryTransferDetail.ItemUPCCode = model.ItemUPCCode;

            // Added according model props
            //InventoryTransferDetail.InventoryTransferID = model.InventoryTransferID;
            InventoryTransferDetail.GLAnalysisType = model.GLAnalysisType;
            InventoryTransferDetail.ItemName = model.ItemName;
            InventoryTransferDetail.ToCompanyID = model.ToCompanyID;
            InventoryTransferDetail.TruckNumber = model.TruckNumber;
            InventoryTransferDetail.QtyReceived = model.QtyReceived;
            InventoryTransferDetail.DateReceived = model.DateReceived;
            InventoryTransferDetail.TruckAdvance = model.TruckAdvance;
            InventoryTransferDetail.TruckAdvBalance = model.TruckAdvBalance;
            InventoryTransferDetail.LoadingTicket = model.LoadingTicket;


            //InventoryTransferDetail.ReceivedQty = model.ReceivedQty;
            //InventoryTransferDetail.InventoryTransferID = model.InventoryTransferID;


            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        #endregion
    }
}
