﻿using IPagedList;
using Microsoft.EntityFrameworkCore;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Business.Services
{
    public interface IInventoryByWarehouseService
    {

        #region Inventory By Warehouse
        Task AddInventoryByWarehouse(InventoryByWarehouseDTO model);
        Task<InventoryByWarehouseDTO> GetInventoryByWarehouse(Guid Id);
        Task<IPagedList<InventoryByWarehouseDTO>> GetAllInventoryByWarehouse(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateInventoryByWarehouse(InventoryByWarehouseDTO model, Guid Id);
        Task DeleteInventoryByWarehouse(Guid Id);
        #endregion

    }
    public class InventoryByWarehouseService :IInventoryByWarehouseService
    {
        private readonly IRepository<InventoryByWarehouse, Guid> _InventoryByWarehouseRepo;
        private readonly IRepository<InventoryItem> _InventoryItemRepo;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;

        private readonly IUserService _userSvc;
        public InventoryByWarehouseService(

             IRepository<InventoryByWarehouse, Guid> InventoryByWarehouseRepo,
             IRepository<InventoryItem> InventoryItemRepo,
            IServiceHelper serviceHelper,
            IUnitOfWork unitOfWork,
            IUserService userSvc
            )
        {
            _InventoryByWarehouseRepo = InventoryByWarehouseRepo;
            _InventoryItemRepo = InventoryItemRepo;
            _serviceHelper = serviceHelper;
            _unitOfWork = unitOfWork;
            _userSvc = userSvc;
        }

        #region Inventory By Warehouse
        public async Task AddInventoryByWarehouse(InventoryByWarehouseDTO model)
        {
            //model.InventoryByWarehouseDescription = model.InventoryByWarehouseDescription.Trim();
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);


            _InventoryByWarehouseRepo.Insert(new InventoryByWarehouse
            {
                ItemID = model.ItemID,
                WarehouseID = model.WarehouseID,
                WarehouseBinID = model.WarehouseBinID,
                QtyOnHand = model.QtyOnHand,
                QtyCommitted = model.QtyCommitted,
                QtyOnOrder = model.QtyOnOrder,
                QtyOnBackorder = model.QtyOnBackorder,
                CycleCode = model.CycleCode,
                LastCountDate = model.LastCountDate,
                ItemCost = (float)model.ItemCost,

                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                CompanyID = (int)currentUser.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteInventoryByWarehouse(Guid Id)
        {
            try
            {
                if (Id == null) { return; }
                await _InventoryByWarehouseRepo.DeleteAsync(_InventoryByWarehouseRepo.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<IPagedList<InventoryByWarehouseDTO>> GetAllInventoryByWarehouse(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var InventoryByWarehouse =
                    from model in _InventoryByWarehouseRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) //||
                    join c in _InventoryItemRepo.GetAll() on model.ItemID equals c.Id
                    //model.VehicleRegistration.Contains(searchTerm)
                    orderby model.CreationTime descending

                    select new InventoryByWarehouseDTO
                    {
                        Id = model.Id,
                        ItemID = model.ItemID,
                        ItemName =  c.ItemName,
                        WarehouseID = model.WarehouseID,
                        WarehouseBinID = model.WarehouseBinID,
                        QtyOnHand = model.QtyOnHand,
                        QtyCommitted = model.QtyCommitted,
                        QtyOnOrder = model.QtyOnOrder,
                        QtyOnBackorder = model.QtyOnBackorder,
                        CycleCode = model.CycleCode,
                        LastCountDate = model.LastCountDate,
                        ItemCost =(decimal) model.ItemCost,
                    };

                return await InventoryByWarehouse.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task<InventoryByWarehouseDTO> GetInventoryByWarehouse(Guid Id)
        {
            var model = _InventoryByWarehouseRepo.FirstOrDefault(x => x.Id == Id);

            return new InventoryByWarehouseDTO
            {
                Id = model.Id,
                ItemID = model.ItemID,
                WarehouseID = model.WarehouseID,
                WarehouseBinID = model.WarehouseBinID,
                QtyOnHand = model.QtyOnHand,
                QtyCommitted = model.QtyCommitted,
                QtyOnOrder = model.QtyOnOrder,
                QtyOnBackorder = model.QtyOnBackorder,
                CycleCode = model.CycleCode,
                LastCountDate = model.LastCountDate,
                ItemCost = (decimal)model.ItemCost,
            };
        }


        public async Task<bool> UpdateInventoryByWarehouse(InventoryByWarehouseDTO model, Guid Id)
        {
            var InventoryByWarehouse = _InventoryByWarehouseRepo.FirstOrDefault(x => x.Id == Id);

            //InventoryByWarehouse.Id = model.Id;
            InventoryByWarehouse.ItemID = model.ItemID;
            InventoryByWarehouse.WarehouseID = model.WarehouseID;
            InventoryByWarehouse.WarehouseBinID = model.WarehouseBinID;
            InventoryByWarehouse.QtyOnHand = model.QtyOnHand;
            InventoryByWarehouse.QtyCommitted = model.QtyCommitted;
            InventoryByWarehouse.QtyOnOrder = model.QtyOnOrder;
            InventoryByWarehouse.QtyOnBackorder = model.QtyOnBackorder;
            InventoryByWarehouse.CycleCode = model.CycleCode;
            InventoryByWarehouse.LastCountDate = model.LastCountDate;
            InventoryByWarehouse.ItemCost =(float) model.ItemCost;
            

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        #endregion
    }
}
