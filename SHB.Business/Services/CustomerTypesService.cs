﻿using IPagedList;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace SHB.Business.Services
{
    public interface ICustomerTypesService
    {
        #region Customer Information
        Task AddCustomerTypes(CustomerTypesDTO model);
        Task<CustomerTypesDTO> GetCustomerTypes(int Id);
        Task<IPagedList<CustomerTypes>> GetAllCustomerTypes(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateCustomerTypes(CustomerTypesDTO model, int Id);
        Task DeleteCustomerTypes(int Id);
        #endregion
    }
    public class CustomerTypesService : ICustomerTypesService
    {
        private readonly IRepository<CustomerTypes> _customerTypesRepo;

        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;

        private readonly IUserService _userSvc;
        public CustomerTypesService(
             IRepository<CustomerTypes> customerTypesRepo,
            IServiceHelper serviceHelper,
            IUnitOfWork unitOfWork,
            IUserService userSvc
            )
        {
            _customerTypesRepo = customerTypesRepo;
            _serviceHelper = serviceHelper;
            _unitOfWork = unitOfWork;
            _userSvc = userSvc;
        }

        public async Task AddCustomerTypes(CustomerTypesDTO model)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            //For Customer Form
            // CustomerName,lastname,CustomerPhone,Type,DateCreated, Email, State,IsActive,Address
            _customerTypesRepo.Insert(new CustomerTypes
            {
                CustomerTypeDescription = model.CustomerTypeDescription,
                SalesControlAccount = model.SalesControlAccount,
                COSControlAccount = model.COSControlAccount,
                CustomerTypeID = model.CustomerTypeID,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                CompanyID = Convert.ToString(currentUser.CompanyId)
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteCustomerTypes(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _customerTypesRepo.DeleteAsync(_customerTypesRepo.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
            }
        }

        public async Task<IPagedList<CustomerTypes>> GetAllCustomerTypes(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var customerTypes =
                    from model in _customerTypesRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) ||
                    model.CustomerTypeDescription.Contains(searchTerm)
                    orderby model.CreationTime descending
                    select new CustomerTypes
                    {
                        Id = model.Id,
                        CompanyID = model.CompanyID,
                        CustomerTypeID = model.CustomerTypeID,
                        CustomerTypeDescription = model.CustomerTypeDescription,
                        SalesControlAccount = model.SalesControlAccount,
                        COSControlAccount = model.COSControlAccount,
                        DebtorsControlAccount = model.DebtorsControlAccount,
                        CurrencyExchange = model.CurrencyExchange,
                        GainOrLossAccount = model.GainOrLossAccount,
                        DiscountsAccount = model.DiscountsAccount,
                        DiscountRate = model.DiscountRate,
                        BillingType = model.BillingType,

                    };
                return await customerTypes.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public async Task<CustomerTypesDTO> GetCustomerTypes(int Id)
        {
            var model = _customerTypesRepo.FirstOrDefault(x => x.Id == Id);

            return new CustomerTypesDTO
            {
                CustomerTypeDescription = model.CustomerTypeDescription,
                SalesControlAccount = model.SalesControlAccount,
                COSControlAccount = model.COSControlAccount,
                CustomerTypeID = model.CustomerTypeID,
                CompanyID = model.CompanyID,
            };
        }

        public Task<bool> UpdateCustomerTypes(CustomerTypesDTO model, int Id)
        {
            throw new NotImplementedException();
        }
    }
}
