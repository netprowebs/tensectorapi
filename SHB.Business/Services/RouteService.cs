﻿using IPagedList;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SHB.Business.Services
{
    public interface IRouteService
    {
        Task<IPagedList<RouteDTO>> GetRoutes(int page, int size, string query = null);
        Task<RouteDTO> GetRouteById(int routeId);
        Task AddRoute(RouteDTO route);
        Task UpdateRoute(int routeId, RouteDTO route);
        Task RemoveRoute(int routeId);
    }

    public class RouteService : IRouteService
    {
        private readonly IRepository<Route> _routeRepo;
        private readonly IRepository<Terminal> _terminalRepo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceHelper _serviceHelper;
        private readonly IRepository<State> _stateRepo;

        public RouteService(
            IRepository<Route> routeRepo,
            IRepository<Terminal> terminalRepo,
            IUnitOfWork unitOfWork, IRepository<State> stateRepo,
            IServiceHelper serviceHelper)
        {
            _routeRepo = routeRepo;
            _terminalRepo = terminalRepo;
            _unitOfWork = unitOfWork;
            _serviceHelper = serviceHelper;
            _stateRepo = stateRepo;
        }

        private async Task<bool> IsValidTerminal(int terminalId)
        {
            return terminalId > 0 &&
                 await _terminalRepo.ExistAsync(m => m.Id == terminalId);
        }

        public Task<string> GetRouteNameAsync(int departureTerminalId, int destinationTerminalId)
        {
            var routes =
                from departure in _terminalRepo.GetAll()
                join destination in _terminalRepo.GetAll() on destinationTerminalId equals destination.Id
                join fromState in _stateRepo.GetAll() on departure.StateId equals fromState.Id
                join toState in _stateRepo.GetAll() on destination.StateId equals toState.Id
                where departure.Id == departureTerminalId
                select
                departure.Name + " ==> " + destination.Name;

            return routes.SingleOrDefaultAsync();
        }

        public async Task AddRoute(RouteDTO routeDto)
        {
            if (!await IsValidTerminal(routeDto.DepartureTerminalId))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.TERMINAL_NOT_EXIST);
            }

            if (!await IsValidTerminal(routeDto.DestinationTerminalId))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.TERMINAL_NOT_EXIST);
            }

            
            routeDto.Name = await GetRouteNameAsync(routeDto.DepartureTerminalId, routeDto.DestinationTerminalId);

            if (await _routeRepo.ExistAsync(v => v.Name == routeDto.Name))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ROUTE_EXIST);
            }

            _routeRepo.Insert(new Route
            {
                Id = routeDto.Id,
                Name = routeDto.Name,
                DestinationTerminalId = routeDto.DestinationTerminalId,
                DepartureTerminalId = routeDto.DepartureTerminalId,
                Type = routeDto.RouteType,
                DispatchFee = routeDto.DispatchFee,
                DriverFee = routeDto.DriverFee,
                LoaderFee = routeDto.LoaderFee,
                AvailableAtTerminal = routeDto.AvailableAtTerminal,
                AvailableOnline = routeDto.AvailableOnline,
                ParentRouteId = routeDto.ParentRouteId,
                ParentRoute = routeDto.ParentRouteName

            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<RouteDTO> GetRouteById(int routeId)
        {
            var route = await _routeRepo.GetAsync(routeId);

            if (route == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ROUTE_NOT_EXIST);
            }

            return new RouteDTO
            {
                Id = route.Id,
                Name = route.Name,
                DestinationTerminalId = route.DestinationTerminalId,
                DepartureTerminalId = route.DepartureTerminalId,
                RouteType = route.Type,
                DispatchFee = route.DispatchFee,
                DriverFee = route.DriverFee,
                LoaderFee = route.LoaderFee,
                AvailableAtTerminal = route.AvailableAtTerminal,
                AvailableOnline = route.AvailableOnline,
                ParentRouteId = route.ParentRouteId,
                ParentRouteName = route.ParentRoute
            };
        }

        public Task<IPagedList<RouteDTO>> GetRoutes(int page, int size, string query = null)
        {
            var routes =
                from route in _routeRepo.GetAll()
                    //join terminal in _terminalRepo.GetAll() on route.DepartureTerminalId equals terminal.Id
                orderby route.Id descending
                where string.IsNullOrWhiteSpace(query) || route.Name.Contains(query)
                select new RouteDTO
                {
                    Id = route.Id,
                    Name = route.Name,
                    DestinationTerminalId = route.DestinationTerminalId,
                    DepartureTerminalId = route.DepartureTerminalId,
                    RouteType = route.Type,
                    DispatchFee = route.DispatchFee,
                    DriverFee = route.DriverFee,
                    LoaderFee = route.LoaderFee,
                    AvailableAtTerminal = route.AvailableAtTerminal,
                    AvailableOnline = route.AvailableOnline,
                    ParentRouteId = route.ParentRouteId,
                    ParentRouteName = route.ParentRoute
                };

            return routes.AsNoTracking().ToPagedListAsync(page, size);
        }

        public async Task RemoveRoute(int routeId)
        {
            var route = await _routeRepo.GetAsync(routeId);

            if (route == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ROUTE_NOT_EXIST);
            }

            _routeRepo.Delete(route);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateRoute(int routeId, RouteDTO route)
        {
            var routes = await _routeRepo.GetAsync(routeId);

            if (routes == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ROUTE_NOT_EXIST);
            }

            route.Name = await GetRouteNameAsync(route.DepartureTerminalId, route.DestinationTerminalId);

            await _unitOfWork.SaveChangesAsync();
        }
    }
}
