﻿using IPagedList;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SHB.Core.Domain.Entities;
using System;

namespace SHB.Business.Services
{
    public interface ITripService
    {
        Task<IPagedList<TripDTO>> GetTrips(int page, int size, string query = null);
        Task<TripDTO> GetTripById(int tripId);
        Task AddTrip(TripDTO trip);
        Task<bool> UpdateTrip(int tripId, TripDTO trip);
        Task RemoveTrip(int tripId);
    }

    public class TripService : ITripService
    {
        private readonly IRepository<Trip> _tripRepo;
        private readonly IRepository<Route> _routeRepo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceHelper _serviceHelper;

        public TripService(
            IRepository<Trip> tripRepo,
            IRepository<Route> routeRepo,
            IUnitOfWork unitOfWork,
            IServiceHelper serviceHelper)
        {
            _tripRepo = tripRepo;
            _routeRepo = routeRepo;
            _unitOfWork = unitOfWork;
            _serviceHelper = serviceHelper;
        }

        private async Task<bool> IsValidRoute(int routeId)
        {
            return routeId > 0 &&
                 await _routeRepo.ExistAsync(m => m.Id == routeId);
        }

        public async Task AddTrip(TripDTO tripDto)
        {
           
            if ( !await IsValidRoute(tripDto.RouteId))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ROUTE_NOT_EXIST);
            }

            //fareDto.Route.Name = fareDto.Route.Name.Trim();
            if (await _tripRepo.ExistAsync(v => v.Id == tripDto.Id))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.TRIP_EXIST);
            }

            _tripRepo.Insert(new Trip
            {
                DepartureTime = tripDto.DepartureTime,
                TripCode = tripDto.TripCode,
                RouteId = tripDto.RouteId,
                ParentRouteId = tripDto.ParentRouteId,
                ParentRouteDepartureTime = tripDto.ParentRouteDepartureTime,
                ParentTripId = tripDto.ParentTripId,
                VehicleModelId = tripDto.VehicleModelId,
                AvailableOnline = tripDto.AvailableOnline
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<TripDTO> GetTripById(int tripId)
        {
            var trip = await _tripRepo.GetAsync(tripId);

            if (trip == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.TRIP_NOT_EXIST);
            }

            return new TripDTO
            {
                Id = trip.Id,
                DepartureTime = trip.DepartureTime,
                TripCode = trip.TripCode,
                AvailableOnline = trip.AvailableOnline,
                ParentRouteDepartureTime = trip.ParentRouteDepartureTime,
                RouteId = trip.RouteId,
                VehicleModelId = trip.VehicleModelId,
                ParentRouteId = trip.ParentRouteId,
                ParentTripId = trip.ParentTripId

            };
        }

        public Task<IPagedList<TripDTO>> GetTrips(int page, int size, string query = null)
        {
            var trips =
                from trip in _tripRepo.GetAll()
                //join terminal in _terminalRepo.GetAll() on subRoute.DepartureTerminalId equals terminal.Id
                orderby trip.Id descending
               // where string.IsNullOrWhiteSpace(query) || trip.Name.Contains(query)
                select new TripDTO
                {
                    Id = trip.Id,
                    DepartureTime = trip.DepartureTime,
                    TripCode = trip.TripCode,
                    AvailableOnline = trip.AvailableOnline,
                    ParentRouteDepartureTime = trip.ParentRouteDepartureTime,
                    RouteId = trip.RouteId,
                    VehicleModelId = trip.VehicleModelId,
                    ParentRouteId = trip.ParentRouteId,
                    ParentTripId = trip.ParentTripId
                };

            return trips.AsNoTracking().ToPagedListAsync(page, size);
        }

        public async Task RemoveTrip(int tripId)
        {
            var trip = await _tripRepo.GetAsync(tripId);

            if (trip == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.TRIP_NOT_EXIST);
            }

            _tripRepo.Delete(trip);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<bool> UpdateTrip(int tripId, TripDTO trip)
        {
            try
            {
                if (trip == null) { return false; }
                var trips = await _tripRepo.GetAsync(tripId);
                if (trips == null)
                {
                    throw await _serviceHelper.GetExceptionAsync(ErrorConstants.TRIP_NOT_EXIST);
                }

                trips.Id = tripId;
                trips.DepartureTime = trip.DepartureTime;
                trips.TripCode = trip.TripCode;
                trips.AvailableOnline = trip.AvailableOnline;
                trips.ParentRouteDepartureTime = trip.ParentRouteDepartureTime;
                trips.RouteId = trip.RouteId;
                trips.VehicleModelId = trip.VehicleModelId;
                trips.ParentRouteId = trip.ParentRouteId;
                trips.ParentTripId = trip.ParentTripId;

                await _tripRepo.UpdateAsync(trips);
                await _unitOfWork.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

            //if (trips == null)
            //{
            //    throw await _serviceHelper.GetExceptionAsync(ErrorConstants.TRIP_NOT_EXIST);
            //}

        }
        
    }
}
