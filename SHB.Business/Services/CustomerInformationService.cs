﻿using System;
using System.Collections.Generic;
using IPagedList;
using SHB.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using SHB.Data.Repository;
using SHB.Business.Services;
using SHB.Data.UnitOfWork;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Core.Domain.DataTransferObjects;
//using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc;
using SHB.Core.Entities.Enums;
using SHB.Core.Utils;
using SHB.Core.Configuration;
using SHB.Business.Messaging.Email;
using SHB.Business.Messaging.Sms;
using Microsoft.Extensions.Options;
using System.Collections.Specialized;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using SHB.Core.Utilities;

namespace SHB.Business.Services
{
    public interface ICustomerInformationService
    {
        #region Customer Information
        Task<UserDTO> AddCustomerInformation(CustomerInformationDTO model);
        Task<CustomerInformationDTO> GetCustomerInformation(int Id);
        Task<CustomerInformationDTO> GetCustomerByUserId(string UserId);
        Task<IPagedList<CustomerInformationDTO>> GetAllCustomerInformation(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateCustomerInformation(CustomerInformationDTO model, int Id);
        Task DeleteCustomerInformation(int Id);
        #endregion
    }

    public class CustomerInformationService : ICustomerInformationService
    {
        private readonly IRepository<CustomerInformation> _customerInfoRepo;
        private readonly IRepository<WalletNumber> _walletNumberRepo;
        private readonly IWalletService _walletSvc;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMailService _mailSvc;
        private readonly ISMSService _smsSvc;
        private readonly AppConfig _appConfig;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IGuidGenerator _guidGenerator;
        private readonly IReferralService _referralSvc;
        private readonly IRepository<Referral, long> _referr;
        private readonly IUtilityService _utilityService;
        private readonly IRepository<Terminal> _terminalRepo;
        private readonly IRoleService _roleSvc;
        private readonly IRepository<Wallet> _walletrepo;

        private readonly IUserService _userSvc;
        public CustomerInformationService(
             IRepository<CustomerInformation> customerInfoRepo, IRepository<WalletNumber> walletNumberRepo,
            IServiceHelper serviceHelper, IWalletService walletSvc, ISMSService smsSvc, IHostingEnvironment hostingEnvironment,
            IUnitOfWork unitOfWork, IMailService mailSvc, IOptions<AppConfig> appConfig, IRoleService roleSvc,
            IUserService userSvc ,  IGuidGenerator guidGenerator,IReferralService referralSvc, IRepository<Wallet> walletrepo,
            IRepository<Referral, long> referr,IUtilityService utilityService, IRepository<Terminal> terminalRepo
            )
        {
            _customerInfoRepo = customerInfoRepo;
            _walletNumberRepo = walletNumberRepo;
            _walletSvc = walletSvc;
            _serviceHelper = serviceHelper;
            _unitOfWork = unitOfWork;
            _userSvc = userSvc;
            _mailSvc = mailSvc;
            _smsSvc = smsSvc;
            _appConfig = appConfig.Value;
            _hostingEnvironment = hostingEnvironment;
            _guidGenerator = guidGenerator;
            _referralSvc = referralSvc;
            _referr = referr;
            _utilityService = utilityService;
            _terminalRepo = terminalRepo;
            _roleSvc = roleSvc;
            _walletrepo = walletrepo;
        }

        public async Task<UserDTO> AddCustomerInformation(CustomerInformationDTO model)
        {
            if (model is null)
                throw new ArgumentNullException(nameof(model));


            if (string.IsNullOrEmpty(model.TenantId.ToString()))
                throw new LMEGenericException("This transaction does not have a tenant");


            var customer = await _userSvc.FindFirstAsync(c => c.Email == model.CustomerEmail
                                                   //|| c.PhoneNumber.Trim() == model.CustomerPhone 
                                                   && c.UserType == UserType.Customer && c.CompanyId == model.TenantId);
            if (customer is not null)
                throw new LMEGenericException("This account already exist");


            // checks for which device 
            if (model.DeviceType == DeviceType.Android || model.DeviceType == DeviceType.iOS || model.DeviceType == DeviceType.Website)
            {
                model.EmailConfirmed = false;
                model.PhoneNumberConfirmed = false;
            }

            try
            {
                _unitOfWork.BeginTransaction();
                string fileName = "";
                string IdtfileName = "";
                string Nrmfile = "";
                if (!string.IsNullOrEmpty(model.Photo) || !string.IsNullOrEmpty(model.IdentityPhoto))
                {
                    List<ImageFileDTO> imageFileDTO = new List<ImageFileDTO>();

                    if (model.DeviceType == DeviceType.AdminWeb)
                    {
                        foreach (var file in model.PictureList)
                        {
                            if (file.ImgContent == "Photo")
                            {
                                var txt = "-PHT-";
                                var ImageNameWitExt = Path.GetFileNameWithoutExtension(file.fileName);
                                Nrmfile = await InfoForImageB(txt, file.data, ImageNameWitExt, file.fileName, file.ContentType);
                            }
                            else if (file.ImgContent == "IdentityPhoto")
                            {
                                var txt = "-IDTPHT-";
                                var ImageNameWitExt = Path.GetFileNameWithoutExtension(file.fileName);
                                IdtfileName = await InfoForImageB(txt, file.data, ImageNameWitExt, file.fileName, file.ContentType);
                            }
                        }
                    }
                    else if (model.DeviceType == DeviceType.Website || model.DeviceType == DeviceType.Android || model.DeviceType == DeviceType.iOS)
                    {
                        if (!string.IsNullOrEmpty(model.Photo))
                        {
                            var txt = "-PHT-";
                            Nrmfile = await InfoForImage(model, txt);
                        }
                        else if (!string.IsNullOrEmpty(model.IdentityPhoto))
                        {
                            var txt = "-IDTPHT-";
                            IdtfileName = await InfoForImage(model, txt);
                        }
                    }
                }

                // Create wallet for new customer
                var walletNumber = await _walletSvc.GenerateNextValidWalletNumber();
                var userdetail = _userSvc.FindByEmailAsync(model.CustomerEmail);
                _walletNumberRepo.Insert(walletNumber);

                var wallet = new Wallet
                {
                    WalletNumber = walletNumber.WalletPan,
                    CreatorUserId = _serviceHelper.GetCurrentUserId(),
                    Balance = 0.00M,
                    UserType = UserType.Customer,
                    TenantId = model.TenantId,
                    //UserId = Convert.ToInt32(userdetail.Result.Id),
                };

                _walletSvc.Add(wallet);
                await _unitOfWork.SaveChangesAsync();


                customer = new User
                {
                    FirstName = model.CustomerFirstName,
                    LastName = model.CustomerLastName,
                    MiddleName = model.CustomerOtherName,
                    Email = model.CustomerEmail,
                    PhoneNumber = model.CustomerPhone,
                    Address = model.CustomerAddress1,
                    NextOfKinName = model.NextOfKin,
                    NextOfKinPhone = model.NextOfKinPhone,
                    EmailConfirmed = model.EmailConfirmed,
                    PhoneNumberConfirmed = model.PhoneNumberConfirmed,
                    UserName = model.CustomerEmail,
                    ReferralCode = CommonHelper.GenereateRandonAlphaNumeric(),
                    UserType = UserType.Customer,
                    CompanyId = model.TenantId,
                    LoginDeviceType = model.DeviceType,
                    AccountConfirmationCode = _guidGenerator.Create().ToString().Substring(0, 8),
                    IdentificationCode = model.IdentificationCode,
                    IdentificationType = model.IdentificationType,
                    IDIssueDate = model.IDIssueDate,
                    IDExpireDate = model.IDExpireDate,
                    IsFirstTimeLogin = true,
                    LocationId = model.LocationId,
                    IdentityPhoto = IdtfileName,
                    DateOfBirth = model.DateOfBirth.ToString(),
                    Gender = model.Gender,
                    Photo = Nrmfile,
                    WalletId1 = wallet.Id,

                };

                var customerAcctResult = !string.IsNullOrWhiteSpace(model.Password)
                             ? await _userSvc.CreateAsync(customer, model.Password)
                             : await _userSvc.CreateAsync(customer);

                if (customerAcctResult.Succeeded)
                {

                    //Check existing referral
                    if (!string.IsNullOrEmpty(model.ReferralCode))
                    {
                        var existingReferrals = await _userSvc.FindFirstAsync(c => c.ReferralCode == model.ReferralCode && c.CompanyId == model.TenantId);

                        if (existingReferrals == null)
                        {
                            throw new LMEGenericException("Invalid referral code, pls use a valid one or leave the field empty");
                        }
                        //save to referral table details
                        var referrals = new Referral
                        {
                            Email = existingReferrals.Email,
                            ReferralCode = customer.ReferralCode,
                            PhoneNumber = customer.PhoneNumber,
                            UserType = customer.UserType,
                            TenantId = model.TenantId,
                        };

                        _referr.Insert(referrals);

                        //await _unitOfWork.SaveChangesAsync();
                    }

                    //map user to existing row details



                    var PersistedUser = await _userSvc.FindByEmailAsync(customer.Email);

                    if (!string.IsNullOrEmpty(model.RoleId.ToString()))
                    {
                        var dbRole = await _roleSvc.FindByIdAsync(model.RoleId);

                        if (dbRole != null)
                        {
                            await _userSvc.AddToRoleAsync(PersistedUser, dbRole.Name);
                        }
                    }


                    _customerInfoRepo.Insert(new CustomerInformation
                    {
                        CustomerCode = "CU" + _guidGenerator.Create().ToString().Substring(0, 4),
                        CustomerTypeID = model.CustomerTypeID,
                        CustomerState = model.CustomerState,
                        IsActive = true,
                        CreatorUserId = _serviceHelper.GetCurrentUserId(),
                        CreationTime = DateTime.Now,
                        TenantId = model.TenantId,
                        UserId = PersistedUser.Id,
                        CustomerAddress2 = model.CustomerAddress2,
                        CustomerAddress3 = model.CustomerAddress3,
                        CustomerCity = model.CustomerCity,
                        CustomerZip = model.CustomerZip,
                        CustomerCountry = model.CustomerCountry,
                        CustomerFax = model.CustomerFax,
                        CustomerWebPage = model.CustomerWebPage,
                        CustomerSalutation = model.CustomerSalutation,
                        TaxIDNo = model.TaxIDNo,
                        VATTaxIDNumber = model.VATTaxIDNumber,
                        VatTaxOtherNumber = model.VatTaxOtherNumber,
                        CurrencyID = model.CurrencyID,
                        GLSalesAccount = model.GLSalesAccount,
                        TermsID = model.TermsID,
                        TermsStart = model.TermsStart,
                        TaxGroupID = model.TaxGroupID,
                        CreditLimit = model.CreditLimit,
                        CreditComments = model.CreditComments,
                        PaymentDay = model.PaymentDay,
                        CustomerSince = model.CustomerSince,
                        SendCreditMemos = model.SendCreditMemos,
                        SendDebitMemos = model.SendDebitMemos,
                        Statements = model.Statements,
                        WarehouseID = model.WarehouseID,
                        WarehouseGLAccount = model.WarehouseGLAccount,
                        AccountBalance = model.AccountBalance,
                        OtpIsUsed = model.OtpIsUsed,
                        DeviceType = model.DeviceType,

                    });

                    await _unitOfWork.SaveChangesAsync();
                    //Password customer created from front-end, backend create non-password
                    if (!string.IsNullOrWhiteSpace(customer.PasswordHash) && model.DeviceType != DeviceType.AdminWeb)
                    {
                        await SendActivationMessage(customer);
                    }
                    try
                    {
                        //string message = $"Thank You for registering with Seclot.com. Kindly use this Referral Code: {customer.ReferralCode} to invite others ";
                        //_smsSvc.SendSMSNow(message, recipient: customer.PhoneNumber.ToNigeriaMobile());
                    }
                    catch (Exception)
                    {

                    }

                }
                else
                {
                    _unitOfWork.Rollback();

                    throw await _serviceHelper.GetExceptionAsync(customerAcctResult.Errors.FirstOrDefault()?.Description);
                }

                UserDTO usrv = new UserDTO();

                usrv.PhoneNumber = customer.PhoneNumber;
                usrv.Email = customer.Email;
                usrv.FirstName = customer.FirstName;
                usrv.LastName = customer.LastName;
                usrv.UserId = customer.Id;
                usrv.IsActive = customer.EmailConfirmed;
                usrv.AccountIsDeleted = customer.IsDeleted;
                usrv.ReferralCode = customer.ReferralCode;
                usrv.IdentificationCode = model.IdentificationCode;
                usrv.IdentificationType = model.IdentificationType;
                usrv.IDIssueDate = model.IDIssueDate;
                usrv.IDExpireDate = model.IDExpireDate;
                usrv.Gender = model.Gender;

                // checks for which device 
                if (model.DeviceType == DeviceType.AdminWeb)
                {
                    await _userSvc.UserDirectActivation(model.CustomerEmail);

                    await SendAccountEmail(usrv);
                }

                _unitOfWork.Commit();

                return usrv;
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw;
            }
            //For Customer Form
        }
        public async Task<string> InfoForImage(CustomerInformationDTO model, string txt)
        {
            string pic = "";
            string Contype = "";
            string ext = "";
            var ImageNameWitExt = "";
            var ImageName = "";
            string fileName = "";

            List<ImageFileDTO> imageFileDTO = new List<ImageFileDTO>();
            if (!string.IsNullOrEmpty(model.PhotoContent))
            {
                pic = model.PhotoContent.Split(',').Last();
                Contype = model.PhotoContent.Split(new Char[] { ':', ';' })[1];
                ext = "." + model.PhotoContent.Split(new Char[] { '/', ';' })[1];

                ImageNameWitExt = DateTime.UtcNow.ToString("yymmssfff") + txt + CommonHelper.GenereateRandonAlphaNumeric().Substring(0, 3);
                ImageName = ImageNameWitExt + ext;
                ImageFileDTO imgdto = new ImageFileDTO();
                byte[] bytes = Convert.FromBase64String(pic);
                imgdto.data = bytes;
                imgdto.FilenameWithOutExt = ImageNameWitExt;
                imgdto.fileName = ImageName;
                imgdto.ContentType = Contype;
                imgdto.UploadType = UploadType.Image;
                imageFileDTO.Add(imgdto);
                imgdto.PictureList = imageFileDTO;

                fileName = await _utilityService.UplaodBytesToDigOc(imgdto);
            }

            return fileName;
        }


        public async Task<string> InfoForImageB(string txt, byte[] bytes, string ImageNameWitExt, string ImageName, string Contype)
        {     
            string fileName = "";
            List<ImageFileDTO> imageFileDTO = new List<ImageFileDTO>();         
                ImageFileDTO imgdto = new ImageFileDTO();
                imgdto.data = bytes;
                imgdto.FilenameWithOutExt = DateTime.UtcNow.ToString("yymmssfff") + txt + CommonHelper.GenereateRandonAlphaNumeric().Substring(0, 3);
            //imgdto.FilenameWithOutExt = ImageNameWitExt;
                imgdto.fileName = ImageName;
                imgdto.ContentType = Contype;
                imgdto.UploadType = UploadType.Image;
                imageFileDTO.Add(imgdto);
                imgdto.PictureList = imageFileDTO;
                fileName = await _utilityService.UplaodBytesToDigOc(imgdto);
            return fileName;
        }

        private async Task SendActivationMessage(User user)
        {
            EmailSetting model = new EmailSetting();
            model.EmailFrom = _appConfig.seclotEmail;
            model.SenderName = _appConfig.sender_name;
            model.MJ_APIKEY_PRIVATE = _appConfig.MJ_APIKEY_PUBLIC;
            model.MJ_APIKEY_PUBLIC = _appConfig.MJ_APIKEY_PRIVATE;
            model.EmailTo = user.Email;
            model.Subject = "Account Activation";
            model.IsSent = false;
            try
            {
                var replacement = new StringDictionary
                {
                    ["FirstName"] = user.FirstName,
                    ["ActivationCode"] = user.AccountConfirmationCode,
                    ["TenantName"] = "Seclot",
                };

                string pathToHtmlTemplate = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Content", CoreConstants.Url.ActivationCodeEmail);
                string emailmessage = $"Welcome to Seclot.com Please activate your account with this code: {user.AccountConfirmationCode}";
                var mail = new Mail(model.EmailFrom, emailmessage, user.Email)
                {
                    BodyIsFile = true,
                    BodyPath = pathToHtmlTemplate
                };

               await _mailSvc.UseMailJetB(mail, replacement, model);
             

            }
            catch (Exception ex)
            {
            }
        }

        public async Task SendActivationCode(string usernameOrEmail)
        {
            if (string.IsNullOrEmpty(usernameOrEmail))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.USER_ACCOUNT_NOT_EXIST);
            }

            var user = await _userSvc.FindByNameAsync(usernameOrEmail) ?? await _userSvc.FindByEmailAsync(usernameOrEmail);

            if (user == null)
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.USER_ACCOUNT_NOT_EXIST);

            if (user.IsDeleted)
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.USER_ACCOUNT_NOT_EXIST);

            if (user.IsConfirmed())
                throw new LMEGenericException("Your account was activated earlier.");

            if (string.IsNullOrWhiteSpace(user.AccountConfirmationCode))
                throw new LMEGenericException("Activation code is missing. Please contact support.");

            await SendActivationMessage(user);
        }

        private async Task SendAccountEmail(UserDTO user)
        {
            EmailSetting model = new EmailSetting();
            model.EmailFrom = _appConfig.seclotEmail;
            model.SenderName = _appConfig.sender_name;
            model.MJ_APIKEY_PRIVATE = _appConfig.MJ_APIKEY_PUBLIC;
            model.MJ_APIKEY_PUBLIC = _appConfig.MJ_APIKEY_PRIVATE;
            model.EmailTo = user.Email;
            model.Subject = "Account Information";
            model.IsSent = false;

            try
            {
                var replacement = new StringDictionary
                {
                    ["FirstName"] = user.FirstName,
                    ["UserName"] = user.Email,
                    ["DefaultPassword"] = "123456",
                     ["TenantName"] = "Seclot",
                };
                string pathToHtmlTemplate = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Content", CoreConstants.Url.AccountActivationEmail);
                //string Message = $"Hello your login email and password are {user.Email} and 123456 respectively";
                var mail = new Mail(model.EmailFrom, model.Subject, user.Email)
                {
                    BodyIsFile = true,
                    BodyPath = pathToHtmlTemplate
                };
                await _mailSvc.UseMailJetB(mail, replacement, model);
            }
            catch (Exception)
            {
            }
        }

        public async Task DeleteCustomerInformation(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _customerInfoRepo.DeleteAsync(_customerInfoRepo.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
            }
        }

        public async Task<IPagedList<CustomerInformationDTO>> GetAllCustomerInformation(int pageNumber, int pageSize, string query)
        {
            try
            {
                var customers =
                (from customer in _customerInfoRepo.GetAllIncluding(x => x.User)
                 join terminal in _terminalRepo.GetAll() on customer.User.LocationId equals terminal.Id
                into terminals
                 from terminal in terminals.DefaultIfEmpty()
                 let userRole = customer.User == null ? Enumerable.Empty<string>() : _userSvc.GetUserRoles(customer.User).Result
                 where customer.User.CompanyId == _serviceHelper.GetTenantId() && customer.User.UserType == UserType.Customer
                 where string.IsNullOrWhiteSpace(query) ||
                 (
                    customer.CustomerCode.Contains(query) ||
                    customer.User.FirstName.Contains(query) ||
                    customer.User.LastName.Contains(query) ||
                    //(customer.User.FirstName + " " + employee.User.LastName).Contains(query) ||
                    customer.User.PhoneNumber.Contains(query) ||
                    customer.User.Email.Contains(query) ||
                    //string.Join(",", userRole).Contains(query) ||
                    customer.User.NextOfKinName.Contains(query) ||
                    customer.User.NextOfKinPhone.Contains(query)
                 )
                 select new CustomerInformationDTO
                 {
                     Id = customer.Id,
                     CustomerCode  = customer.CustomerCode,
                     CustomerAddress1 = customer.User.Address,
                     CustomerAddress2 = customer.CustomerAddress2,
                    
                     CustomerCity = customer.CustomerCity,
                     CustomerState = customer.CustomerState,
                     CustomerZip = customer.CustomerZip,
                     CustomerCountry = customer.CustomerCountry,
                     CustomerPhone = customer.User.PhoneNumber,
                     CustomerFax = customer.CustomerFax,
                     CustomerEmail = customer.User.Email,
                     CustomerWebPage = customer.CustomerWebPage,
                     CustomerFirstName = customer.User.FirstName,
                     CustomerLastName = customer.User.LastName,
                     CustomerOtherName = customer.User.MiddleName,
                     CustomerSalutation = customer.CustomerSalutation,
                     NextOfKin = customer.User.NextOfKinName,
                     NextOfKinPhone = customer.User.NextOfKinPhone,
                     CustomerAddress3 = customer.CustomerAddress3,
                     CustomerTypeID = customer.CustomerTypeID,
                     TaxIDNo = customer.TaxIDNo,
                     VATTaxIDNumber = customer.VATTaxIDNumber,
                     VatTaxOtherNumber = customer.VatTaxOtherNumber,
                     CurrencyID = customer.CurrencyID,
                     GLSalesAccount = customer.GLSalesAccount,
                     TermsID = customer.TermsID,
                     TermsStart = customer.TermsStart,
                     TaxGroupID = customer.TaxGroupID,
                     CreditLimit = customer.CreditLimit,
                     CreditComments = customer.CreditComments,
                     PaymentDay = customer.PaymentDay,
                     CustomerSince = customer.CustomerSince,
                     SendCreditMemos = customer.SendCreditMemos,
                     SendDebitMemos = customer.SendDebitMemos,
                     Statements = customer.Statements,
                     WarehouseID = customer.WarehouseID,
                     WarehouseGLAccount = customer.WarehouseGLAccount,
                     AccountBalance = customer.AccountBalance,
                     IsActive = customer.IsActive,
                     UserId = customer.User.Id,
                     OtpIsUsed = customer.OtpIsUsed,
                     DeviceType = customer.DeviceType,
                     CreatedTime =customer.CreationTime,
                     DateOfBirth = Convert.ToDateTime(customer.User.DateOfBirth),
                     Gender = customer.User.Gender,                    
                     IdentificationCode = customer.User.IdentificationCode,
                     IdentificationType = customer.User.IdentificationType,
                     IDIssueDate = customer.User.IDIssueDate,
                     IDExpireDate = customer.User.IDExpireDate,
                     LocationId = customer.User.LocationId,
                     LocationName = terminal.Name,

                 }).ToList();

                //foreach (var source in employees.Where(x => x.RoleName == "Operations Manager"))

                //    source.RoleName = "Terminal Manager";

                return customers.ToPagedList(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    
        public async Task<CustomerInformationDTO> GetCustomerInformation(int Id)
        {
        var customer = _customerInfoRepo.GetAllIncluding(x => x.User).FirstOrDefault(x => x.Id == Id);
        return new CustomerInformationDTO
            {
            Id = customer.Id,
            CustomerCode = customer.CustomerCode,
            CustomerAddress1 = customer.User.Address,
            CustomerAddress2 = customer.CustomerAddress2,
            CustomerAddress3 = customer.CustomerAddress3,
            CustomerCity = customer.CustomerCity,
            CustomerState = customer.CustomerState,
            CustomerZip = customer.CustomerZip,
            CustomerCountry = customer.CustomerCountry,
            CustomerPhone = customer.User.PhoneNumber,
            CustomerFax = customer.CustomerFax,
            CustomerEmail = customer.User.Email,
            CustomerWebPage = customer.CustomerWebPage,
            CustomerFirstName = customer.User.FirstName,
            CustomerLastName = customer.User.LastName,
            CustomerOtherName = customer.User.MiddleName,
            CustomerSalutation = customer.CustomerSalutation,
            NextOfKin = customer.User.NextOfKinName,
            NextOfKinPhone = customer.User.NextOfKinPhone,
            CustomerTypeID = customer.CustomerTypeID,
            TaxIDNo = customer.TaxIDNo,
            VATTaxIDNumber = customer.VATTaxIDNumber,
            VatTaxOtherNumber = customer.VatTaxOtherNumber,
            CurrencyID = customer.CurrencyID,
            GLSalesAccount = customer.GLSalesAccount,
            TermsID = customer.TermsID,
            TermsStart = customer.TermsStart,
            TaxGroupID = customer.TaxGroupID,
            CreditLimit = customer.CreditLimit,
            CreditComments = customer.CreditComments,
            PaymentDay = customer.PaymentDay,
            CustomerSince = customer.CustomerSince,
            SendCreditMemos = customer.SendCreditMemos,
            SendDebitMemos = customer.SendDebitMemos,
            Statements = customer.Statements,
            WarehouseID = customer.WarehouseID,
            WarehouseGLAccount = customer.WarehouseGLAccount,
            AccountBalance = customer.AccountBalance,
            IsActive = customer.IsActive,
            UserId = customer.User.Id,
            OtpIsUsed = customer.OtpIsUsed,
            DeviceType = customer.DeviceType,
            IdentificationCode = customer.User.IdentificationCode,
            IdentificationType = customer.User.IdentificationType,
            IDIssueDate = customer.User.IDIssueDate,
            IDExpireDate = customer.User.IDExpireDate,
            Gender = customer.User.Gender,
            LocationId = customer.User.LocationId,
            Photo = customer.User.Photo,
            IdentityPhoto = customer.User.IdentityPhoto,
            DateOfBirth = Convert.ToDateTime(customer.User.DateOfBirth)
        };
        }
        public async Task<CustomerInformationDTO> GetCustomerByUserId(string UserId)
        {
            CustomerInformation customer = null;
            var username = UserId;
            if (username.Contains('@'))
            {
                var getUser = await _userSvc.FindByEmailAsync(UserId);
                customer = _customerInfoRepo.GetAllIncluding(x => x.User).FirstOrDefault(x => x.UserId == getUser.Id);
            }
            else
            {
                var getUser = await _userSvc.FindByIdAsync(UserId);
                customer = _customerInfoRepo.GetAllIncluding(x => x.User).FirstOrDefault(x => x.UserId == getUser.Id);
            }

            return new CustomerInformationDTO
            {
                Id = customer.Id,
                CustomerCode = customer.CustomerCode,
                CustomerAddress1 = customer.User.Address,
                CustomerAddress2 = customer.CustomerAddress2,
                CustomerAddress3 = customer.CustomerAddress3,
                CustomerCity = customer.CustomerCity,
                CustomerState = customer.CustomerState,
                CustomerZip = customer.CustomerZip,
                CustomerCountry = customer.CustomerCountry,
                CustomerPhone = customer.User.PhoneNumber,
                CustomerFax = customer.CustomerFax,
                CustomerEmail = customer.User.Email,
                CustomerWebPage = customer.CustomerWebPage,
                CustomerFirstName = customer.User.FirstName,
                CustomerLastName = customer.User.LastName,
                CustomerOtherName = customer.User.MiddleName,
                CustomerSalutation = customer.CustomerSalutation,
                NextOfKin = customer.User.NextOfKinName,
                NextOfKinPhone = customer.User.NextOfKinPhone,
                CustomerTypeID = customer.CustomerTypeID,
                TaxIDNo = customer.TaxIDNo,
                VATTaxIDNumber = customer.VATTaxIDNumber,
                VatTaxOtherNumber = customer.VatTaxOtherNumber,
                CurrencyID = customer.CurrencyID,
                GLSalesAccount = customer.GLSalesAccount,
                TermsID = customer.TermsID,
                TermsStart = customer.TermsStart,
                TaxGroupID = customer.TaxGroupID,
                CreditLimit = customer.CreditLimit,
                CreditComments = customer.CreditComments,
                PaymentDay = customer.PaymentDay,
                CustomerSince = customer.CustomerSince,
                SendCreditMemos = customer.SendCreditMemos,
                SendDebitMemos = customer.SendDebitMemos,
                Statements = customer.Statements,
                WarehouseID = customer.WarehouseID,
                WarehouseGLAccount = customer.WarehouseGLAccount,
                AccountBalance = customer.AccountBalance,
                IsActive = customer.IsActive,
                UserId = customer.User.Id,
                OtpIsUsed = customer.OtpIsUsed,
                DeviceType = customer.DeviceType,
                IdentificationCode = customer.User.IdentificationCode,
                IdentificationType = customer.User.IdentificationType,
                IDIssueDate = customer.User.IDIssueDate,
                IDExpireDate = customer.User.IDExpireDate,
                Photo = customer.User.Photo,
                IdentityPhoto = customer.User.IdentityPhoto,
                Gender = customer.User.Gender,
                LocationId = customer.User.LocationId,
                DateOfBirth = Convert.ToDateTime(customer.User.DateOfBirth)

            };
        }
        public async Task<bool> UpdateCustomerInformation(CustomerInformationDTO model, int Id)
        {
            CustomerInformation customer = null;
            if (model.UserId != 0)
            {
                Id = model.UserId;
                customer = _customerInfoRepo.GetAllIncluding(x => x.User).FirstOrDefault(x => x.UserId == Id);
            }
            else
            {
                customer = _customerInfoRepo.GetAllIncluding(x => x.User).FirstOrDefault(x => x.Id == Id);
            }

            string fileName = "";
            string IdtfileName = "";
            string Nrmfile = "";
            if (!string.IsNullOrEmpty(model.Photo) || !string.IsNullOrEmpty(model.IdentityPhoto))
            {
                List<ImageFileDTO> imageFileDTO = new List<ImageFileDTO>();

                if (model.DeviceType == DeviceType.AdminWeb)
                {
                    foreach (var file in model.PictureList)
                    {
                        if (model.Photo == "Photo")
                        {
                            var txt = "-PHT-";
                            var ImageNameWitExt = Path.GetFileNameWithoutExtension(file.fileName);
                            Nrmfile = await InfoForImageB(txt, file.data, ImageNameWitExt, file.fileName, file.ContentType);
                        }
                        else { Nrmfile = customer.User.Photo; }
                       if (model.IdentityPhoto  == "IdentityPhoto")
                        {
                            var txt = "-IDTPHT-";
                            var ImageNameWitExt = Path.GetFileNameWithoutExtension(file.fileName);
                            IdtfileName = await InfoForImageB(txt, file.data, ImageNameWitExt, file.fileName, file.ContentType);
                        }
                        else { IdtfileName = customer.User.IdentityPhoto; }
                    }
                }
                else if (model.DeviceType == DeviceType.Website || model.DeviceType == DeviceType.Android || model.DeviceType == DeviceType.iOS)
                {
                    if (!string.IsNullOrEmpty(model.Photo))
                    {
                        var txt = "-PHT-";
                        model.PhotoContent = model.Photo;
                        Nrmfile = await InfoForImage(model, txt);
                    }
                    else { Nrmfile = customer.User.Photo; }
                    if (!string.IsNullOrEmpty(model.IdentityPhoto))
                    {
                        var txt = "-IDTPHT-";
                        model.PhotoContent = model.IdentityPhoto;
                        IdtfileName = await InfoForImage(model, txt);
                    }
                    else { IdtfileName = customer.User.IdentityPhoto; }
                }
            }
            else { IdtfileName = customer.User.IdentityPhoto; Nrmfile = customer.User.Photo; }


            customer.CustomerCode = model.CustomerCode;
            customer.User.Address = model.CustomerAddress1;
            customer.CustomerAddress2 = model.CustomerAddress2;
            customer.CustomerAddress3 = model.CustomerAddress3;
            customer.CustomerCity = model.CustomerCity;
            customer.CustomerState = model.CustomerState;
            customer.CustomerZip = model.CustomerZip;
            customer.CustomerCountry = model.CustomerCountry;
            customer.User.PhoneNumber = model.CustomerPhone;
            customer.CustomerFax = model.CustomerFax;
            customer.CustomerWebPage = model.CustomerWebPage;
            customer.User.FirstName  = model.CustomerFirstName;
            customer.User.LastName = model.CustomerLastName;
            customer.User.MiddleName = model.CustomerOtherName; 
            customer.CustomerSalutation = model.CustomerSalutation;
            customer.User.NextOfKinName = model.NextOfKin;
            customer.User.NextOfKinPhone  = model.NextOfKinPhone;
            customer.CustomerTypeID = model.CustomerTypeID;
            customer.TaxIDNo = model.TaxIDNo;
            customer.VATTaxIDNumber = model.VATTaxIDNumber;
            customer.VatTaxOtherNumber = model.VatTaxOtherNumber;
            customer.CurrencyID = model.CurrencyID;
            customer.GLSalesAccount = model.GLSalesAccount;
            customer.TermsID = model.TermsID;
            customer.TermsStart = model.TermsStart;
            customer.TaxGroupID = model.TaxGroupID;
            customer.CreditLimit = model.CreditLimit;
            customer.CreditComments = model.CreditComments;
            customer.PaymentDay = model.PaymentDay;
            customer.CustomerSince = model.CustomerSince;
            customer.SendCreditMemos = model.SendCreditMemos;
            customer.SendDebitMemos = model.SendDebitMemos;
            customer.Statements = model.Statements;
            customer.WarehouseID = model.WarehouseID;
            customer.WarehouseGLAccount = model.WarehouseGLAccount;
            customer.AccountBalance = model.AccountBalance;
            customer.OtpIsUsed = model.OtpIsUsed;
            customer.DeviceType = model.DeviceType;
            customer.User.IdentificationCode = model.IdentificationCode;
            customer.User.IdentificationType = model.IdentificationType;
            customer.User.IDIssueDate = model.IDIssueDate;
            customer.User.IDExpireDate = model.IDExpireDate;
            customer.User.Gender = model.Gender;
            customer.User.LocationId = model.CustomerCity;
            customer.User.Photo = Nrmfile ;
            customer.User.IdentityPhoto = IdtfileName;
            customer.User.DateOfBirth = model.DateOfBirth.ToString();

            await _unitOfWork.SaveChangesAsync();

            return true;
        }
    }
}
