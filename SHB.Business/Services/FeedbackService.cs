﻿using IPagedList;
using Microsoft.EntityFrameworkCore;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Business.Services
{

    public interface IFeedbackService
    {
        #region FeedbackHeader
        Task AddFeedback(ComplaintDTO model);
        Task<ComplaintDTO> GetFeedback(int Id);
        //Task<IPagedList<ComplaintDTO>> GetAllFeedbacks(int pageNumber, int pageSize, string searchTerm);
        Task<List<ComplaintDTO>> GetAllFeedbacks();
        Task<bool> UpdateFeedbackById(ComplaintDTO model, int Id);
        Task DeleteFeedback(int Id);
        #endregion

        #region FeedbackResponse
        Task<List<FeedBackResponseDTO>> GetAllFeedbackResponses(int id);
        Task AddFeedbackResponse(FeedBackResponseDTO model);
        Task<FeedBackResponseDTO> GetFeedbackResponse(int Id);
        Task<bool> UpdateFeedbackResponseById(FeedBackResponseDTO model, int Id);
        Task DeleteFeedbackResponse(int Id);
        #endregion
    }
    public class FeedBackService : IFeedbackService
    {
        private readonly IRepository<Complaint> _feedbackRepo;
        private readonly IRepository<FeedbackResponse> _feedbackRespRepo;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;

        public FeedBackService(IRepository<Complaint> feedback, IRepository<FeedbackResponse> feedbackRepo,
            IServiceHelper serviceHelper, IUnitOfWork unitOfWork)
        {

            _feedbackRepo = feedback;
            _feedbackRespRepo = feedbackRepo;
            _serviceHelper = serviceHelper;
            _unitOfWork = unitOfWork;
        }

        public async Task AddFeedback(ComplaintDTO model)
        {

            if (await _feedbackRepo.ExistAsync(v => v.Id == model.Id))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.FEEDBACK_EXIST);
            }

            _feedbackRepo.Insert(new Complaint
            {
                Id = model.Id,
                FullName = model.FullName,
                Email = model.Email,
                ComplaintType = model.ComplaintType,
                PriorityLevel = model.PriorityLevel,
                BookingReference = model.BookingReference,
                Message = model.Message,
                TransDate = model.TransDate,
                Responded = model.Responded,
                RepliedMessage = model.RepliedMessage,
                TenantId = model.TenantId
            }); ;

            await _unitOfWork.SaveChangesAsync();
            //throw new NotImplementedException();
        }



        #region FeedbackHeader
        //public async Task<IPagedList<ComplaintDTO>> GetAllFeedbacks(int pageNumber, int pageSize, string searchTerm)
        //{

        //    try
        //    {
        //        var feedback = (from feedbacks in _feedbackRepo.GetAll()

        //                        where string.IsNullOrWhiteSpace(searchTerm) ||
        //                        feedbacks.FullName.Contains(searchTerm)
        //                        //InventoryItems.itemDescription.Contains(searchTerm)
        //                        orderby feedbacks.FullName
        //                        select new ComplaintDTO
        //                        {
        //                            Id = feedbacks.Id,
        //                            FullName = feedbacks.FullName,
        //                            Email = feedbacks.Email,
        //                            ComplaintType = feedbacks.ComplaintType,
        //                            PriorityLevel = feedbacks.PriorityLevel,
        //                            BookingReference = feedbacks.BookingReference,
        //                            Message = feedbacks.Message,
        //                            TransDate = feedbacks.TransDate,
        //                            Responded = feedbacks.Responded,
        //                            RepliedMessage = feedbacks.RepliedMessage
        //                        }


        //                              );
        //        return await feedback.AsTracking().ToPagedListAsync(pageNumber, pageSize);
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //    //throw new NotImplementedException();
        //}


        public async Task<List<ComplaintDTO>> GetAllFeedbacks()
        {
            try
            {
                var feedback = (from feedbacks in _feedbackRepo.GetAll()
                                where feedbacks.TenantId == _serviceHelper.GetTenantId()
                                orderby feedbacks.FullName
                                select new ComplaintDTO
                                {
                                    Id = feedbacks.Id,
                                    FullName = feedbacks.FullName,
                                    Email = feedbacks.Email,
                                    ComplaintType = feedbacks.ComplaintType,
                                    PriorityLevel = feedbacks.PriorityLevel,
                                    BookingReference = feedbacks.BookingReference,
                                    Message = feedbacks.Message,
                                    TransDate = feedbacks.TransDate,
                                    Responded = feedbacks.Responded,
                                    RepliedMessage = feedbacks.RepliedMessage,
                                    TenantId = feedbacks.TenantId
                                } );

                return await feedback.ToListAsync();
            }
            catch (Exception ex)
            {
                return null;
            }
            //throw new NotImplementedException();
        }


        public async Task<ComplaintDTO> GetFeedback(int Id)
        {
            var feedbacks = await _feedbackRepo.GetAsync(Id);

            if (feedbacks == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.FEEDBACK_NOT_EXIST);
            }

            return new ComplaintDTO
            {
                Id = feedbacks.Id,
                FullName = feedbacks.FullName,
                Email = feedbacks.Email,
                ComplaintType = feedbacks.ComplaintType,
                PriorityLevel = feedbacks.PriorityLevel,
                BookingReference = feedbacks.BookingReference,
                Message = feedbacks.Message,
                TransDate = feedbacks.TransDate,
                Responded = feedbacks.Responded,
                RepliedMessage = feedbacks.RepliedMessage

            };
        }


        public async Task<bool> UpdateFeedbackById(ComplaintDTO feedbacks, int Id)
        {
            try
            {
                if (feedbacks == null) { return false; }
                var feedback = await _feedbackRepo.GetAsync(Id);
                if (feedback == null) { return false; }



                feedback.FullName = feedbacks.FullName;
                feedback.Email = feedbacks.Email;
                feedback.ComplaintType = feedbacks.ComplaintType;
                feedback.PriorityLevel = feedbacks.PriorityLevel;
                feedback.BookingReference = feedbacks.BookingReference;
                feedback.Message = feedbacks.Message;
                feedback.TransDate = feedbacks.TransDate;
                feedback.Responded = feedbacks.Responded;
                feedback.RepliedMessage = feedbacks.RepliedMessage;

                //...
                await _feedbackRepo.UpdateAsync(feedback);
                await _unitOfWork.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public async Task DeleteFeedback(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _feedbackRepo.DeleteAsync(_feedbackRepo.Get(Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            //throw new NotImplementedException();
        }
        #endregion Feedback

        #region FeedbackResponse

        public async Task AddFeedbackResponse(FeedBackResponseDTO model)
        {

            if (await _feedbackRespRepo.ExistAsync(v => v.Id == model.Id))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.FEEDBACK_EXIST);
            }

            _feedbackRespRepo.Insert(new FeedbackResponse
            {
                Id = model.Id,
                ComplaintID = model.ComplaintID,
                EmployeeMessage = model.EmployeeMessage,
                ClientMessage = model.ClientMessage,
                EmployeeDate = DateTime.Now,
                ClientDate = DateTime.Now,
                Responded = model.Responded,
                TenantId = _serviceHelper.GetTenantId()
            });

            await _unitOfWork.SaveChangesAsync();
            //throw new NotImplementedException();
        }


        public async Task<List<FeedBackResponseDTO>> GetAllFeedbackResponses(int id)
        {
            try
            {
                var feedback = (from feedbackResponse in _feedbackRespRepo.GetAll()
                                join complist in _feedbackRepo.GetAll() on feedbackResponse.ComplaintID equals complist.Id
                                into complists
                                from complist in complists.DefaultIfEmpty()
                                where feedbackResponse.ComplaintID == id

                                orderby feedbackResponse.Complaint.Message
                                select new FeedBackResponseDTO
                                {
                                    Id = feedbackResponse.Id,
                                    ComplaintID = feedbackResponse.ComplaintID,
                                    EmployeeMessage = feedbackResponse.EmployeeMessage,
                                    ClientMessage = feedbackResponse.ClientMessage,
                                    EmployeeDate = feedbackResponse.EmployeeDate,
                                    ClientDate = feedbackResponse.ClientDate,
                                    Responded = feedbackResponse.Responded,
                                    //for complaintbl
                                    FullName = complist.FullName,
                                    Email = complist.Email,
                                    ComplaintType = complist.ComplaintType,
                                    PriorityLevel = complist.PriorityLevel,
                                    ComplainReference = complist.BookingReference,
                                    ComplainMessage = complist.Message,
                                    ComplainTransDate = complist.TransDate,
                                 
                                    RepliedMessage = complist.RepliedMessage,
                                    CreationTime = feedbackResponse.CreationTime
                                }
                                      );
                return await feedback.AsTracking().ToListAsync();
            }
            catch (Exception ex)
            {
                return null;
            }
            throw new NotImplementedException();
        }



        public async Task<FeedBackResponseDTO> GetFeedbackResponse(int Id)
        {
            var feedbackResponse = await _feedbackRespRepo.GetAsync(Id);

            if (feedbackResponse == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.FEEDBACK_NOT_EXIST);
            }

            return new FeedBackResponseDTO
            {
                Id = feedbackResponse.Id,
                ComplaintID = feedbackResponse.ComplaintID,

                EmployeeMessage = feedbackResponse.EmployeeMessage,
                ClientMessage = feedbackResponse.ClientMessage,
                EmployeeDate = feedbackResponse.EmployeeDate,
                ClientDate = feedbackResponse.ClientDate,
                Responded = feedbackResponse.Responded,
            };
        }



        public async Task<bool> UpdateFeedbackResponseById(FeedBackResponseDTO feedbacks, int Id)
        {
            try
            {
                if (feedbacks == null) { return false; }
                var feedback = await _feedbackRespRepo.GetAsync(Id);
                if (feedback == null) { return false; }


                feedback.ComplaintID = feedbacks.ComplaintID;
                feedback.EmployeeMessage = feedbacks.EmployeeMessage;
                feedback.ClientMessage = feedbacks.ClientMessage;
                feedback.EmployeeDate = feedbacks.EmployeeDate;
                feedback.ClientDate = feedbacks.ClientDate;
                feedback.Responded = feedbacks.Responded;

                await _feedbackRespRepo.UpdateAsync(feedback);
                await _unitOfWork.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public async Task DeleteFeedbackResponse(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _feedbackRespRepo.DeleteAsync(_feedbackRespRepo.Get(Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            throw new NotImplementedException();
        }

        #endregion FeedbackRrsponse



    }
}


