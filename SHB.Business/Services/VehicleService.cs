﻿using IPagedList;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SHB.Core.Domain.Entities;
using System;

namespace SHB.Business.Services
{
    public interface IVehicleService
    {
        Task<IPagedList<VehicleDTO>> GetVehicles(int page, int size, string query = null);
        Task<VehicleDTO> GetVehicleById(int vehicleId);
        Task AddVehicle(VehicleDTO vehicle);
        Task UpdateVehicle(int vehicleId, VehicleDTO vehicle);
        Task RemoveVehicle(int vehicleId);
    }

    public class VehicleService : IVehicleService
    {
        private readonly IRepository<Vehicle> _vehicleRepo;
        private readonly IRepository<Route> _routeRepo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceHelper _serviceHelper;

        public VehicleService(
            IRepository<Vehicle> vehicleRepo,
            IRepository<Route> routeRepo,
            IUnitOfWork unitOfWork,
            IServiceHelper serviceHelper)
        {
            _vehicleRepo = vehicleRepo;
            _routeRepo = routeRepo;
            _unitOfWork = unitOfWork;
            _serviceHelper = serviceHelper;
        }

        private async Task<bool> IsValidRoute(int routeId)
        {
            return routeId > 0 &&
                 await _routeRepo.ExistAsync(m => m.Id == routeId);
        }

        public async Task AddVehicle(VehicleDTO vehicleDto)
        {
            //if (!await IsValidRoute(vehicleDto.RouteId))
            //{
            //    throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ROUTE_NOT_EXIST);
            //}


            //fareDto.Route.Name = fareDto.Route.Name.Trim();

            if (await _vehicleRepo.ExistAsync(v => v.Id == vehicleDto.Id))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VEHICLE_EXIST);
            }

            _vehicleRepo.Insert(new Vehicle
            {
                Id = vehicleDto.Id,
                RegistrationNumber = vehicleDto.RegistrationNumber,
                ChasisNumber = vehicleDto.ChasisNumber,
                EngineNumber = vehicleDto.EngineNumber,
                IMEINumber = vehicleDto.IMEINumber,
                Type = vehicleDto.Type,
                Status = vehicleDto.Status,
                VehicleModelId = vehicleDto.VehicleModelId,
                //VehicleModel = vehicleDto.Id,
                LocationId = vehicleDto.LocationId,
                //Location = vehicleDto.Id,
                IsOperational = vehicleDto.IsOperational,
                //Employee = vehicleDto.Id,
                EmployeeId = vehicleDto.EmployeeId,
                PurchaseDate = vehicleDto.PurchaseDate,
                LicenseDate = vehicleDto.LicenseDate,
                InsuranceDate = vehicleDto.InsuranceDate,
                Description = vehicleDto.Description,
                LicenseExpiration = vehicleDto.LicenseExpiration,
                InsuranceExpiration = vehicleDto.InsuranceExpiration,
                RoadWorthinessDate = vehicleDto.RoadWorthinessDate,
                RoadWorthinessExpiration = vehicleDto.RoadWorthinessExpiration,

            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<VehicleDTO> GetVehicleById(int vehicleId)
        {
            var vehicle = await _vehicleRepo.GetAsync(vehicleId);

            if (vehicle == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VEHICLE_NOT_EXIST);
            }

            return new VehicleDTO
            {
                Id = vehicle.Id,
                RegistrationNumber = vehicle.RegistrationNumber,
                ChasisNumber = vehicle.ChasisNumber,
                EngineNumber = vehicle.EngineNumber,
                IMEINumber = vehicle.IMEINumber,
                Type = vehicle.Type,
                Status = vehicle.Status,
                VehicleModelId = vehicle.VehicleModelId,
                //VehicleModel = vehicle.Id,
                LocationId = vehicle.LocationId,
                //Location = vehicle.Id,
                IsOperational = vehicle.IsOperational,
                //Employee = vehicle.Id,
                EmployeeId = vehicle.EmployeeId,
                PurchaseDate = vehicle.PurchaseDate,
                LicenseDate = vehicle.LicenseDate,
                InsuranceDate = vehicle.InsuranceDate,
                Description = vehicle.Description,
                LicenseExpiration = vehicle.LicenseExpiration,
                InsuranceExpiration = vehicle.InsuranceExpiration,
                RoadWorthinessDate = vehicle.RoadWorthinessDate,
                RoadWorthinessExpiration = vehicle.RoadWorthinessExpiration,

            };
        }

        public Task<IPagedList<VehicleDTO>> GetVehicles(int page, int size, string query = null)
        {
            var vehicles =
                from vehicle in _vehicleRepo.GetAll()
                //join terminal in _terminalRepo.GetAll() on subRoute.DepartureTerminalId equals terminal.Id
                orderby vehicle.Id descending
               // where string.IsNullOrWhiteSpace(query) || trip.Name.Contains(query)
                select new VehicleDTO
                {
                    Id = vehicle.Id,
                    RegistrationNumber = vehicle.RegistrationNumber,
                    ChasisNumber = vehicle.ChasisNumber,
                    EngineNumber = vehicle.EngineNumber,
                    IMEINumber = vehicle.IMEINumber,
                    Type = vehicle.Type,
                    Status = vehicle.Status,
                    VehicleModelId = vehicle.VehicleModelId,
                    //VehicleModel = vehicle.Id,
                    LocationId = vehicle.LocationId,
                    //Location = vehicle.Id,
                    IsOperational = vehicle.IsOperational,
                    //Employee = vehicle.Id,
                    EmployeeId = vehicle.EmployeeId,
                    PurchaseDate = vehicle.PurchaseDate,
                    LicenseDate = vehicle.LicenseDate,
                    InsuranceDate = vehicle.InsuranceDate,
                    Description = vehicle.Description,
                    LicenseExpiration = vehicle.LicenseExpiration,
                    InsuranceExpiration = vehicle.InsuranceExpiration,
                    RoadWorthinessDate = vehicle.RoadWorthinessDate,
                    RoadWorthinessExpiration = vehicle.RoadWorthinessExpiration,
                };

            return vehicles.AsNoTracking().ToPagedListAsync(page, size);
        }

        public async Task RemoveVehicle(int vehicleId)
        {
            var vehicle = await _vehicleRepo.GetAsync(vehicleId);

            if (vehicle == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VEHICLE_NOT_EXIST);
            }

            _vehicleRepo.Delete(vehicle);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateVehicle(int vehicleId, VehicleDTO vehicle)
        {
            var vehicles = await _vehicleRepo.GetAsync(vehicleId);

            if (vehicles == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VEHICLE_NOT_EXIST);
            }

            await _unitOfWork.SaveChangesAsync();
        }
    }
}
