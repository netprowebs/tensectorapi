﻿using System;
using SHB.Core.Entities;
using SHB.Data.Repository;
using System.Threading.Tasks;
using SHB.Core.Domain.Entities;
using SHB.Business.Messaging.Email;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SHB.Core.Exceptions;
using SHB.Core.Utils;
using System.IO;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Configuration;
using Microsoft.Extensions.Options;
using System.Collections.Specialized;
using SHB.Business.Messaging.Sms;
using SHB.Core.Entities.Enums;
using System.Security.Claims;
using SHB.Data.UnitOfWork;

namespace SHB.Business.Services
{
    public interface IUserService
    {
        Task<User> FindByNameAsync(string username);
        Task<User> FindByEmailAsync(string email);
        Task<bool> CheckPasswordAsync(User user, string password);
        Task UpdateAsync(User user);
        Task<IList<string>> GetUserRoles(User user);
        Task<IdentityResult> CreateAsync(User user);
        Task<User> FindFirstAsync(Expression<Func<User, bool>> predicate);
        //Task<string> FindCompanyAsync();
        Task<bool> ExistAsync(Expression<Func<User, bool>> predicate);
        Task<IdentityResult> CreateAsync(User user, string password);
        Task<IdentityResult> AddToRoleAsync(User user, string role);
        string HashPassword(User user, string password);
        Task<IList<User>> GetUsersInRoleAsync(string role);
        Task<string> GenerateEmailConfirmationTokenAsync(User user);
        Task<UserDTO> ActivateAccount(UserAccountDTO model);
        Task<UserProfileDTO> GetProfile(string username);
        Task<bool> ForgotPassword(string usernameOrEmail);
        Task<bool> ResetPassword(PassordResetDTO model);
        Task<bool> ChangePassword(string userName, ChangePassordDTO model);
        Task<IList<string>> GetUserRolesAsync(string username);
        Task<bool> UpdateProfile(string userName, UserProfileDTO model);
        Task<bool> IsInRoleAsync(User user, string role);
        Task<IdentityResult> RemoveFromRolesAsync(User user, IEnumerable<string> roles);
        Task<User> FindByIdAsync(string userId);
        Task<bool> UserDirectActivation(string Email);
        
    }

    public class UserService : IUserService
    {

        private readonly UserManager<User> _userManager;
        private readonly AppConfig appConfig;
        private readonly ISMSService _smsSvc;
        private readonly IServiceHelper _svcHelper;
        private readonly IMailService _mailSvc;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IRepository<Wallet> _walletrepo;
        private readonly IUtilityService _utilityService;
        private readonly IUnitOfWork _unitOfWork;


        public UserService(UserManager<User> userManager, IServiceHelper svcHelper,
            IMailService mailSvc, ISMSService smsSvc, IRepository<Wallet> walletrepo,
            IHostingEnvironment hostingEnvironment, IUtilityService utilityService,
            IOptions<AppConfig> _appConfig,IUnitOfWork unitOfWork)
        {
            _userManager = userManager;
            _svcHelper = svcHelper;
            _mailSvc = mailSvc;
            appConfig = _appConfig.Value;
            _smsSvc = smsSvc;
            _hostingEnvironment = hostingEnvironment;
            _walletrepo = walletrepo;
            _utilityService = utilityService;
            _unitOfWork = unitOfWork;
        }

        protected virtual Task<IdentityResult> CreateAsync(User user)
        {
            return _userManager.CreateAsync(user);
        }

        protected virtual Task<IdentityResult> AddToRoleAsync(User user, string role)
        {
            return _userManager.AddToRoleAsync(user, role);
        }
       
        protected virtual string HashPassword(User user, string password)
        {
            return _userManager.PasswordHasher.HashPassword(user, password);
        }

        protected virtual Task<IdentityResult> CreateAsync(User user, string password)
        {
            return _userManager.CreateAsync(user, password);
        }

        protected virtual Task<User> FindFirstAsync(Expression<Func<User, bool>> filter)
        {
            return _userManager.Users.Where(filter).FirstOrDefaultAsync();
        }

        //public async Task<string> FindCompanyAsync()
        //{
        //    var a = _svcHelper.GetCurrentUserId();
        //    var f = await _userManager.FindByIdAsync(a.ToString());
        //    var bb = await _companyInfo.GetcompanyInfoById((int)f.CompanyId);
        //    //var b = await _companyInfo.GetAll().Where(x => x.Id == f.CompanyId)  .FirstOrDefaultAsync();
        //    var k = bb.Name;
        //    return k;
        //}

        protected virtual Task<bool> ExistAsync(Expression<Func<User, bool>> filter)
        {
            return _userManager.Users.AnyAsync(filter);
        }

        protected virtual Task<User> FindByNameAsync(string username)
        {
            return _userManager.FindByNameAsync(username);
        }

        protected virtual Task<IList<string>> GetUserRoles(User user)
        {
            return _userManager.GetRolesAsync(user);
            //return _userManager.GetRolesAsync(user);
        }
        public async Task<IList<string>> GetUserRolesAsync(string username)
        {
            var user = await _userManager.FindByEmailAsync(username);
            return await _userManager.GetRolesAsync(user);
        }
        protected virtual Task<User> FindByEmailAsync(string email)
        {
            return _userManager.FindByEmailAsync(email);
        }


        protected virtual Task<User> FindByIdAsync(string userId)
        {
            return _userManager.FindByIdAsync(userId);
        }

        protected virtual Task<IList<User>> GetUsersInRoleAsync(string role)
        {
            return _userManager.GetUsersInRoleAsync(role);
        }

        protected virtual Task<string> GenerateEmailConfirmationTokenAsync(User user)
        {
            return _userManager.GenerateEmailConfirmationTokenAsync(user);
        }

        protected virtual Task<bool> CheckPasswordAsync(User user, string password)
        {
            return _userManager.CheckPasswordAsync(user, password);
        }

        protected virtual Task UpdateAsync(User user)
        {
            return _userManager.UpdateAsync(user);
        }

        Task<User> IUserService.FindByNameAsync(string username)
        {
            return FindByNameAsync(username);
        }

        Task<User> IUserService.FindByEmailAsync(string email)
        {
            return FindByEmailAsync(email);
        }

        Task<User> IUserService.FindByIdAsync(string userId)
        {
            return FindByIdAsync(userId);
        }


        Task<bool> IUserService.CheckPasswordAsync(User user, string password)
        {
            return CheckPasswordAsync(user, password);
        }

        Task IUserService.UpdateAsync(User user)
        {
            return UpdateAsync(user);
        }

        Task<IList<string>> IUserService.GetUserRoles(User user)
        {
            return GetUserRoles(user);
        }

        Task<IdentityResult> IUserService.CreateAsync(User user)
        {
            return CreateAsync(user);
        }

        Task<User> IUserService.FindFirstAsync(Expression<Func<User, bool>> filter)
        {
            return FindFirstAsync(filter);
        }

        Task<bool> IUserService.ExistAsync(Expression<Func<User, bool>> predicate)
        {
            return ExistAsync(predicate);
        }

        Task<IdentityResult> IUserService.CreateAsync(User user, string password)
        {
            return CreateAsync(user, password);
        }

        Task<IdentityResult> IUserService.AddToRoleAsync(User user, string role)
        {
            return AddToRoleAsync(user, role);
        }

        string IUserService.HashPassword(User user, string password)
        {
            return HashPassword(user, password);
        }

        Task<IList<User>> IUserService.GetUsersInRoleAsync(string role)
        {
            return GetUsersInRoleAsync(role);
        }

        Task<string> IUserService.GenerateEmailConfirmationTokenAsync(User user)
        {
            return GenerateEmailConfirmationTokenAsync(user);
        }

        
        public async Task<UserDTO> ActivateAccount(UserAccountDTO model)
        {
            if (string.IsNullOrEmpty(model.usernameOrEmail) || string.IsNullOrEmpty(model.activationCode))
            {
                throw await _svcHelper.GetExceptionAsync(ErrorConstants.USER_ACCOUNT_NOT_EXIST);
            }

            var user = await FindByNameAsync(model.usernameOrEmail) ?? await FindByEmailAsync(model.usernameOrEmail);
            await ValidateUser(user);

            if (user.IsConfirmed())
                throw new LMEGenericException("Your account was activated earlier.");

            if (model.activationCode == user.AccountConfirmationCode)
            {
                user.EmailConfirmed = true;
                user.PhoneNumberConfirmed = true;
                await UpdateAsync(user);
            }
            else if (model.activationCode != user.AccountConfirmationCode)
            {
                throw new LMEGenericException("Invalid OTP");
                //await _svcHelper.GetExceptionAsync(ErrorConstants.USER_ACCOUNT_INVALID_OTP);
            }

            return user == null ? null : new UserDTO
            {
                PhoneNumber = user.PhoneNumber,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserId = user.Id,
                Gender = user.Gender,
                IsActive = user.IsConfirmed(),
                AccountIsDeleted = user.IsDeleted,
                deviceType = user.LoginDeviceType,
                WalletId1 = user.WalletId1,   
        };
        }

        public async Task<bool> UserDirectActivation(string ClientEmail)
        {
            try
            {
                if (string.IsNullOrEmpty(ClientEmail))
                {
                    throw await _svcHelper.GetExceptionAsync(ErrorConstants.USER_ACCOUNT_NOT_EXIST);
                }

                var user = await FindByNameAsync(ClientEmail) ?? await FindByEmailAsync(ClientEmail);
              
                await ValidateUser(user);

                if (user.IsConfirmed())
                    throw new LMEGenericException("Your account was activated earlier.");

                user.EmailConfirmed = true;
                user.PhoneNumberConfirmed = true;
               

                await UpdateAsync(user);

                //await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
            }

            return true;
        }

        public async Task<UserProfileDTO> GetProfile(string username)
        {
            var user = await _userManager.FindByNameAsync(username);

            //var userClaimList = await _userManager.GetUsersForClaimAsync(Claim claim);
            //var userClaimListb = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
            //var claimsPrincipal = await _signInManager.CreateUserPrincipalAsync(user);
            //var claims = claimsPrincipal.Claims.ToList();

            return user is null ? null : new UserProfileDTO
            {
                UserId = user.Id,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                WalletId = user.WalletId1,
                Gender = user.Gender,
                NextOfKin = user.NextOfKinName,
                NextOfKinPhone = user.NextOfKinPhone,
                PhoneNumber = user.PhoneNumber,
                Photo = user.Photo,
                ReferralCode = user.ReferralCode,
                Address = user.Address,
                MiddleName = user.MiddleName,
                DateJoined = user.CreationTime.ToString(CoreConstants.DateFormat),
                DateOfBirth = user.DateOfBirth,
                Referrer = user.Referrer,
                userType = user.UserType,
                CompanyId = user.CompanyId,
                IdentificationCode = user.IdentificationCode,
                IdentificationType = user.IdentificationType,
                IDIssueDate = user.IDIssueDate,
                IDExpireDate = user.IDExpireDate,                
                IdentityPhoto = user.IdentityPhoto,
                LocationId = user.LocationId,
                IsFirstTimeLogin = user.IsFirstTimeLogin,

            };
        }

        public async Task<bool> ForgotPassword(string usernameOrEmail)
        {
            if (string.IsNullOrEmpty(usernameOrEmail))
            {
                throw await _svcHelper.GetExceptionAsync(ErrorConstants.USER_ACCOUNT_NOT_EXIST);
            }

            var user = await FindByNameAsync(usernameOrEmail) ?? await FindByEmailAsync(usernameOrEmail);

            if (user == null)
                throw await _svcHelper.GetExceptionAsync(ErrorConstants.USER_ACCOUNT_NOT_EXIST);

            if (user.IsDeleted)
                throw await _svcHelper.GetExceptionAsync(ErrorConstants.USER_ACCOUNT_NOT_EXIST);

            if (user.LockoutEnabled)
                throw await _svcHelper.GetExceptionAsync(ErrorConstants.USER_ACCOUNT_LOCKED);

            user.OTP = CommonHelper.RandomDigits(5);
            await _userManager.UpdateAsync(user);

            //send sms for otp
            try
            {
                string message = $"Password Reset OTP: {user.OTP}.";
                _smsSvc.SendSMSNow(message, recipient: user.PhoneNumber);
            }
            catch (Exception)
            {

            }

            EmailSetting model = new EmailSetting();
            model.EmailFrom = appConfig.seclotEmail;
            model.SenderName = appConfig.sender_name;
            model.MJ_APIKEY_PRIVATE = appConfig.MJ_APIKEY_PUBLIC;
            model.MJ_APIKEY_PUBLIC = appConfig.MJ_APIKEY_PRIVATE;
            model.EmailTo = user.Email;
            model.Subject = "Password Reset OTP";
            model.IsSent = false;

            var replacement = new StringDictionary
            {
                ["FirstName"] = user.FirstName,
                ["Otp"] = user.OTP
            };

            string pathToHtmlTemplate = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Content", CoreConstants.Url.PasswordResetEmail);

            var mail = new Mail(appConfig.AppEmail, model.Subject, user.Email)
            {
                BodyIsFile = true,
                BodyPath = pathToHtmlTemplate
            };

            await _mailSvc.UseMailJetB(mail, replacement, model);

            return await Task.FromResult(true);
        }

        public async Task<bool> ResetPassword(PassordResetDTO model)
        {
            if (string.IsNullOrEmpty(model.UserNameOrEmail))
            {
                throw await _svcHelper.GetExceptionAsync(ErrorConstants.USER_ACCOUNT_NOT_EXIST);
            }

            var user = await FindByNameAsync(model.UserNameOrEmail) ?? await FindByEmailAsync(model.UserNameOrEmail);

            await ValidateUser(user);

            if (user.OTP != model.Code)
            {
                throw new LMEGenericException("Invalid OTP");
                // throw await _svcHelper.GetExceptionAsync(ErrorConstants.USER_ACCOUNT_WRONG_OTP);
            }

            var changeToken = await _userManager.GeneratePasswordResetTokenAsync(user);

            var result = await _userManager.ResetPasswordAsync(user, changeToken, model.NewPassword);

            if (!result.Succeeded)
            {
                throw await _svcHelper.GetExceptionAsync(result.Errors?.FirstOrDefault().Description);
            }

            user.OTP = null;
            await _userManager.UpdateAsync(user);

            return true;
        }

        public async Task<bool> ChangePassword(string usernameOrEmail, ChangePassordDTO model)
        {
            if (string.IsNullOrEmpty(usernameOrEmail))
            {
                throw await _svcHelper.GetExceptionAsync(ErrorConstants.USER_ACCOUNT_NOT_EXIST);
            }

            var user = await FindByNameAsync(usernameOrEmail) ?? await FindByEmailAsync(usernameOrEmail);

            await ValidateUser(user);

            var result = await _userManager.ChangePasswordAsync(user, model.CurrentPassword, model.NewPassword);

            if (!result.Succeeded)
            {
                throw await _svcHelper.GetExceptionAsync(result.Errors?.FirstOrDefault().Description);
            }

            return true;
        }

        public Task<bool> IsInRoleAsync(User user, string role)
        {
            if (string.IsNullOrWhiteSpace(role) || user is null)
                throw new ArgumentNullException();

            return _userManager.IsInRoleAsync(user, role);
        }

        public Task<IdentityResult> RemoveFromRolesAsync(User user, IEnumerable<string> roles)
        {
            if (roles == null || user is null)
                throw new ArgumentNullException();

            return _userManager.RemoveFromRolesAsync(user, roles);
        }

        private async Task ValidateUser(User user)
        {
            if (user == null)
                throw await _svcHelper.GetExceptionAsync(ErrorConstants.USER_ACCOUNT_NOT_EXIST);

            if (user.IsDeleted)
                throw await _svcHelper.GetExceptionAsync(ErrorConstants.USER_ACCOUNT_NOT_EXIST);

            if (user.AccountLocked())
                throw await _svcHelper.GetExceptionAsync(ErrorConstants.USER_ACCOUNT_LOCKED);
        }


        public async Task<string> InfoForImage(UserProfileDTO model, string txt)
        {
            string pic = "";
            string Contype = "";
            string ext = "";
            var ImageNameWitExt = "";
            var ImageName = "";
            string fileName = "";

            List<ImageFileDTO> imageFileDTO = new List<ImageFileDTO>();
            pic = model.PhotoContent.Split(',').Last();
            Contype = model.PhotoContent.Split(new Char[] { ':', ';' })[1];
            ext = "." + model.PhotoContent.Split(new Char[] { '/', ';' })[1];

            ImageNameWitExt = DateTime.UtcNow.ToString("yymmssfff") + txt + CommonHelper.GenereateRandonAlphaNumeric().Substring(0, 3);
            ImageName = ImageNameWitExt + ext;

            ImageFileDTO imgdto = new ImageFileDTO();
            byte[] bytes = Convert.FromBase64String(pic);
            imgdto.data = bytes;
            imgdto.FilenameWithOutExt = ImageNameWitExt;
            imgdto.fileName = ImageName;
            imgdto.ContentType = Contype;
            imgdto.UploadType = UploadType.Image;

            imageFileDTO.Add(imgdto);
            imgdto.PictureList = imageFileDTO;
            fileName = await _utilityService.UplaodBytesToDigOc(imgdto);
            return fileName;
        }


        public async Task<bool> UpdateProfile(string usernameOrEmail, UserProfileDTO model)
        {
            if (string.IsNullOrEmpty(usernameOrEmail))
            {
                throw await _svcHelper.GetExceptionAsync(ErrorConstants.USER_ACCOUNT_NOT_EXIST);
            }

            var user = await FindByNameAsync(usernameOrEmail) ?? await FindByEmailAsync(usernameOrEmail);

            await ValidateUser(user);


            string fileName = "";
            string IdtfileName = "";
            string Nrmfile = "";
            //if (!string.IsNullOrEmpty(model.Photo) || !string.IsNullOrEmpty(model.IdentityPhoto))
            //{ }
            List<ImageFileDTO> imageFileDTO = new List<ImageFileDTO>();

            if (!string.IsNullOrWhiteSpace(model.FirstName) && !string.Equals(user.FirstName, model.FirstName))
            {
                user.FirstName = model.FirstName;
            }

            if (!string.IsNullOrWhiteSpace(model.LastName) && !string.Equals(user.LastName, model.LastName))
            {
                user.LastName = model.LastName;
            }

            if (!string.IsNullOrWhiteSpace(model.PhoneNumber) && !string.Equals(user.PhoneNumber, model.PhoneNumber))
            {
                user.PhoneNumber = model.PhoneNumber;
            }

            if (!string.IsNullOrWhiteSpace(model.MiddleName) && !string.Equals(user.MiddleName, model.MiddleName))
            {
                user.MiddleName = model.MiddleName;
            }

            if (!string.IsNullOrWhiteSpace(model.Address) && !string.Equals(user.Address, model.Address))
            {
                user.Address = model.Address;
            }

            if (!string.IsNullOrWhiteSpace(model.NextOfKin) && !string.Equals(user.NextOfKinName, model.NextOfKin))
            {
                user.NextOfKinName = model.NextOfKin;
            }

            if (!string.IsNullOrWhiteSpace(model.DateOfBirth) && !string.Equals(user.DateOfBirth, model.DateOfBirth))
            {
                user.DateOfBirth = model.DateOfBirth;
            }

            if (!string.IsNullOrWhiteSpace(model.NextOfKinPhone) && !string.Equals(user.NextOfKinPhone, model.NextOfKinPhone))
            {
                user.NextOfKinPhone = model.NextOfKinPhone;
            }

            if (!string.IsNullOrWhiteSpace(model.Title) && !string.Equals(user.Title, model.Title))
            {
                user.Title = model.Title;
            }

            if (!string.IsNullOrWhiteSpace(model.Email) && !string.Equals(user.Email, model.Email) && model.Email.IsValidEmail())
            {
                user.NextOfKinName = model.Email;
            }
            //new col
            if (!string.IsNullOrWhiteSpace(model.IdentificationCode) && !string.Equals(user.IdentificationCode, model.IdentificationCode) )
            {
                user.IdentificationCode = model.IdentificationCode;
            }
            if ((model.IdentificationType != 0) && !string.Equals(user.IdentificationType, model.IdentificationType) )
            {
                user.IdentificationType = model.IdentificationType;
            }
            if ((model.IDIssueDate != default(DateTime)) && !string.Equals(user.IDIssueDate, model.IDIssueDate))
            {
                user.IDIssueDate = model.IDIssueDate;
            }
            if ( model.IDExpireDate != default(DateTime) && !string.Equals(user.IDExpireDate, model.IDExpireDate))
            {
                user.IDExpireDate = model.IDExpireDate;
            }

            if ((model.LocationId != 0) && !string.Equals(user.LocationId, model.LocationId))
            {
                user.LocationId = model.LocationId;
            }

            if (!string.IsNullOrEmpty(model.IdentityPhoto) && !string.Equals(user.IdentityPhoto, model.IdentityPhoto))
            {
                if (model.deviceType == DeviceType.Website || model.deviceType == DeviceType.Android || model.deviceType == DeviceType.iOS)
                {
                    if (!string.IsNullOrEmpty(model.IdentityPhoto))
                    {
                        var txt = "-IDTPHT-";
                        model.PhotoContent = model.IdentityPhoto;
                        IdtfileName = await InfoForImage(model, txt);
                        user.IdentityPhoto = IdtfileName;
                    }
                    else { user.IdentityPhoto = user.IdentityPhoto; }
                }
                
            }

            if (!string.IsNullOrEmpty(model.Photo) && !string.Equals(user.Photo, model.Photo) )
            {
                if (model.deviceType == DeviceType.Website || model.deviceType == DeviceType.Android || model.deviceType == DeviceType.iOS)
                {
                    if (!string.IsNullOrEmpty(model.Photo))
                    {
                        var txt = "-PHT-";
                        model.PhotoContent = model.Photo;
                        Nrmfile = await InfoForImage(model, txt);
                        user.Photo = Nrmfile;
                    }
                    else { user.Photo = user.IdentityPhoto; }
                }
               
            }
            if ((!model.IsFirstTimeLogin) && !string.Equals(user.IsFirstTimeLogin, model.IsFirstTimeLogin))
            {
                user.IsFirstTimeLogin = model.IsFirstTimeLogin;
            }
            var result = await _userManager.UpdateAsync(user);

            return result.Succeeded;
        }
    }
}
