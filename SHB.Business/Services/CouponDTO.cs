﻿using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Business.Services
{
    public class CouponDTO
    {
        public Guid Id { get; set; }

        public string CouponCode { get; set; }
        public decimal CouponValue { get; set; }
        public CouponType CouponType { get; set; }
        public DurationType DurationType { get; set; }
        public int Duration { get; set; }
        public DateTime DateCreated { get; set; }
        public bool Validity { get; set; }
        public string ValidityStatus { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsUsed { get; set; }
        public DateTime? DateUsed { get; set; }
        public string Email { get; set; }
    }

    public class couponCountDTO
    {
        public int CouponCount { get; set; }
        public string EmployeePhone { get; set; }
        public string Email { get; set; }
        public UserType usertype { get; set; }
        public string usertypes => usertype.ToString();
    }
}
