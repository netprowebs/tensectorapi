﻿using IPagedList;
using Microsoft.AspNetCore.Identity;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.Core.Entities.Enums;
using SHB.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SHB.Business.Services
{
    public interface IRoleService
    {
        Task<IList<Claim>> GetClaimsAsync(string name);
        Task<bool> CreateAsync(RoleDTO role);
        Task<bool> CreateAsync(RolesDTO role);
        Task<IPagedList<RoleDTO>> Get(int pageNumber, int pageSize, string query);

        Task<RoleDTO> FindByIdAsync(int roleId);
        Task<Role> FindByName(string name);
        Task<IEnumerable<RoleDTO>> GetClaims(int id);
        Task RemoveRole(int id);
        Task UpdateRole(int id, RolesDTO role);
        Task<RolesDTO> GetRoleById(int id);

        // Task<IPagedList<ClaimDTO>> GetClaims(int pageNumber, int pageSize, string query);
    }

    public class RoleService : IRoleService
    {
        private readonly RoleManager<Role> _roleManager;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceHelper _serviceHelper;
        private readonly IAppMenu _appMenu;

        public RoleService(RoleManager<Role> roleManager, IUnitOfWork unitOfWork, IServiceHelper serviceHelper, IAppMenu appMenu)
        {
            _roleManager = roleManager;
            _unitOfWork = unitOfWork;
            _serviceHelper = serviceHelper;
            _appMenu = appMenu;
        }

        public virtual Task<Role> FindByNameAsync(string name)
        {
            return _roleManager.FindByNameAsync(name);
        }

        public virtual Task<IdentityResult> CreateAsync(Role role)
        {
            return _roleManager.CreateAsync(role);
        }

        public virtual Task<IdentityResult> AddClaimToRoleAsync(Role role, Claim claim)
        {
            return _roleManager.AddClaimAsync(role, claim);
        }

        public virtual async Task<bool> CreateAsync(RoleDTO role)
        {
            var dbrole = FindByNameAsync(role.Name).Result;

            if (dbrole is null)
            {

                dbrole = new Role
                {
                    Name = role.Name,
                    IsActive = role.IsActive,
                    RolesDescription = role.RolesDescription,
                    CompanyInfoId = role.CompanyInfoId
                };

                var createStatus = await CreateAsync(dbrole);

                if (createStatus.Succeeded)
                {
                    var claims = PermissionClaimsProvider.GetClaims();

                    foreach (var item in role.Claims)
                    {
                        var claim = claims.FirstOrDefault(x => x.Value == item);
                        if (claim != null)
                        {
                            await AddClaimToRoleAsync(dbrole, claim);
                        }
                    }
                }

                return createStatus.Succeeded;
            }
            return false;
        }

        public virtual async Task<bool> CreateAsync(RolesDTO role)
        {
            var dbrole = FindByNameAsync(role.Name).Result;

            if (dbrole is null)
            {

                dbrole = new Role
                {
                    Name = role.Name + '-' + role.CompanyInfoId,
                    IsActive = true,
                    RolesDescription = role.Name,
                    CompanyInfoId = role.CompanyInfoId
                };

                var createStatus = await CreateAsync(dbrole);

                //creating claim for roles
                //get all from appmenuitemaccess where defaultsetup is true
                var claimNames = await _appMenu.GetAllMenuItemsAccess();
                var menuItem = await _appMenu.GetAllMenuItems();
                var defaultSetup = claimNames.Where(x => x.SetupDefault == true);


                var mapToMenuItem = defaultSetup.Where(x => x.AppMenuId == 1);

                // insert claims into asproleclaim with for each
                foreach (var item in defaultSetup)
                {
                    var vpermision = item.IsActive ? Permission.Permission : Permission.NotPermitted;
                    await _roleManager.AddClaimAsync(dbrole, new Claim(vpermision.ToString() as string, item.ClaimName.ToString() as string));
                }

                return createStatus.Succeeded;
            }
            return false;
        }

        public virtual async Task<IList<Claim>> GetClaimsAsync(string name)
        {
            var role = await FindByNameAsync(name);

            return role is null ? new List<Claim>() : await _roleManager.GetClaimsAsync(role);
        }

        public async Task<Role> FindByName(string name)
        {
            return await _roleManager.FindByNameAsync(name);
        }

        public Task<IPagedList<RoleDTO>> Get(int pageNumber, int pageSize, string query)
        {
            var roles = from r in _roleManager.Roles
                        where string.IsNullOrWhiteSpace(query) || r.Name.Contains(query)
                        orderby r.CreationTime descending

                        select new RoleDTO
                        {
                            Id = r.Id,
                            Name = r.Name,
                            CompanyInfoId = r.CompanyInfoId,
                            RolesDescription = r.RolesDescription,
                            IsActive = r.IsActive

                        };

            return roles.ToPagedListAsync(pageNumber, pageSize);
        }


        public async Task<RoleDTO> FindByIdAsync(int roleId)
        {
            var result = await _roleManager.FindByIdAsync($"{roleId}");

            return result == null ? null : new RoleDTO { Id = result.Id, Name = result.Name };
        }

        public async Task<IList<ClaimDTO>> GetClaim(int id)
        {
            var role = await FindByIdAsync(id);
            var x = role.Name;
            var d = await FindByNameAsync(x);

            var claim = await _roleManager.GetClaimsAsync(d);

            return (IList<ClaimDTO>)claim;

            //throw new System.NotImplementedException();
        }

        public async Task<IEnumerable<RoleDTO>> GetClaims(int id)
        {
            var role = await FindByIdAsync(id);
            var x = role.Name;
            var d = await FindByNameAsync(x);

            var claim = await _roleManager.GetClaimsAsync(d);

            return (IEnumerable<RoleDTO>)claim;
        }


        public async Task RemoveRole(int id)
        {
            var model = await _roleManager.FindByIdAsync($"{id}");

            _roleManager.DeleteAsync(model);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateRole(int id, RolesDTO roles)
        {
            var model = await _roleManager.FindByIdAsync($"{id}");

            model.Name = roles.Name;
            model.CompanyInfoId = roles.CompanyInfoId;
            model.RolesDescription = roles.RolesDescription;

            await _roleManager.UpdateAsync(model);

            await _unitOfWork.SaveChangesAsync();
        }


        public async Task<RolesDTO> GetRoleById(int id)
        {

            var role = await _roleManager.FindByIdAsync($"{id}");

            return new RolesDTO
            {
                Id = role.Id,
                Name = role.Name,
                RolesDescription = role.RolesDescription,
                CompanyInfoId = role.CompanyInfoId

            };
        }

    }
}
