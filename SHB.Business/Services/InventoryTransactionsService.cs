﻿using System;
using System.Collections.Generic;
using System.Text;
using IPagedList;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.Core.Domain.Entities.Enums;
using SHB.Core.Entities;
using SHB.Core.Entities.Enums;
using SHB.Core.Exceptions;
using SHB.Core.Utils;
using SHB.Data.efCore.Context;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SHB.Business.Messaging.Email;
using SHB.Business.Messaging.Sms;

namespace SHB.Business.Services
{
    public interface IInventoryTransactionsService
    {

        #region Inventory Recieved Header
        Task AddInventoryReceivedHeader(InventoryReceivedHeader model);
        Task<InventoryReceivedHeader> GetInventoryReceivedHeader(Guid Id);
        Task<IPagedList<InventoryReceivedHeader>> GetAllInventoryReceivedHeader(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateInventoryReceivedHeader(InventoryReceivedHeader model, Guid Id);
        Task DeleteInventoryReceivedHeader(Guid Id);
        Task<bool> ReceiveRequest(InventoryReceivedHeader model, Guid Id);
        Task<bool> VerifyRequest(InventoryReceivedHeader model, Guid Id);
        Task<bool> ApproveRequest(InventoryReceivedHeader model, Guid Id);
        Task<bool> ReturnRequest(InventoryReceivedHeader model, Guid Id);

        #endregion

        #region Inventory Received Detail
        Task AddInventoryReceivedDetail(InventoryReceivedDetail model);
        Task<List<InventoryReceivedDetailDTO>> GetInventoryReceivedDetail(Guid Id);
        Task<IPagedList<InventoryReceivedDetail>> GetAllInventoryReceivedDetail(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateInventoryReceivedDetail(InventoryReceivedDetail model, int Id);
        Task DeleteInventoryReceivedDetail(int Id);
        #endregion


    }
    public class InventoryTransactionsService : IInventoryTransactionsService
    {
        private readonly IRepository<InventoryReceivedHeader, Guid> _InventoryReceivedHeaderRepo;
        private readonly IRepository<InventoryReceivedDetail> _InventoryReceivedDetailRepo;
        private readonly IRepository<InventoryByWarehouse, Guid> _InventoryByWarehouseRepo;
        private readonly IRepository<InventoryItem> _InventoryItemRepo;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMailService _mailSvc;
        private readonly ISMSService _smsSvc;

        private readonly IUserService _userSvc;
        public InventoryTransactionsService(

            IRepository<InventoryReceivedHeader, Guid> InventoryReceivedHeaderRepo,
            IRepository<InventoryReceivedDetail> InventoryReceivedDetailRepo,
            IRepository<InventoryByWarehouse, Guid> InventoryByWarehouseRepo,
            IRepository<InventoryItem> InventoryItemRepo,
            IServiceHelper serviceHelper,
            IUnitOfWork unitOfWork,
            IMailService mailSvc,
            ISMSService smsSvc,
            IUserService userSvc
            )
        {
            _InventoryReceivedDetailRepo = InventoryReceivedDetailRepo;
            _InventoryReceivedHeaderRepo = InventoryReceivedHeaderRepo;
            _InventoryByWarehouseRepo = InventoryByWarehouseRepo;
            _InventoryItemRepo = InventoryItemRepo;
            _serviceHelper = serviceHelper;
            _unitOfWork = unitOfWork;
            _mailSvc = mailSvc;
            _smsSvc = smsSvc;
            _userSvc = userSvc;
        }


        #region Inventory Recieved Header
        public async Task AddInventoryReceivedHeader(InventoryReceivedHeader model)
        {
            //model.InventoryReceivedHeaderDescription = model.InventoryReceivedHeaderDescription.Trim();
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);
            //var hid = Guid.NewGuid();

            _InventoryReceivedHeaderRepo.Insert(new InventoryReceivedHeader
            {
                Id = model.Id,
                AdjustmentTypeID = model.AdjustmentTypeID,
                TransactionDate = model.TransactionDate,
                WarehouseID = model.WarehouseID,
                WarehouseBinID = model.WarehouseBinID,
                Notes = model.Notes,
                Void = model.Void,
                Captured = model.Captured,
                Verified = model.Verified,
                VerifiedBy = model.VerifiedBy,
                VerifiedDate = model.VerifiedDate,
                Issued = model.Issued,
                IssuedBy = model.IssuedBy,
                IssuedDate = model.IssuedDate,
                Approved = model.Approved,
                ApprovedBy = model.ApprovedBy,
                ApprovedDate = model.ApprovedDate,
                Posted = model.Posted,
                PostedBy = model.PostedBy,
                PostedDate = model.PostedDate,
                BatchControlNumber = model.BatchControlNumber,
                BatchControlTotal = model.BatchControlTotal,
                Signature = model.Signature,
                SignaturePassword = model.SignaturePassword,
                SupervisorSignature = model.SupervisorSignature,
                ManagerSignature = model.ManagerSignature,
                Currency = model.Currency,
                CurrencyExchangeRate = model.CurrencyExchangeRate,
                Reference = model.Reference,
                WarehouseCustomerID = model.WarehouseCustomerID,
                DeliveryNote = model.DeliveryNote,
                SiteNumber = model.SiteNumber,
                VehicleRegistration = model.VehicleRegistration,
                DriversId = model.DriversId,
                FromCompanyID = model.FromCompanyID,
                FromWarehouseID = model.FromWarehouseID,
                FromWarehouseBinID = model.FromWarehouseBinID,
                InventoryIssueTransferID = model.InventoryIssueTransferID,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                CompanyId = (int)currentUser.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteInventoryReceivedHeader(Guid Id)
        {
            try
            {
                if (Id == null) { return; }
                await _InventoryReceivedHeaderRepo.DeleteAsync(_InventoryReceivedHeaderRepo.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<IPagedList<InventoryReceivedHeader>> GetAllInventoryReceivedHeader(int pageNumber, int pageSize, string searchTerm)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            try
            {
                var InventoryReceivedHeader =
                    from model in _InventoryReceivedHeaderRepo.GetAll()
                    where model.CompanyId == currentUser.CompanyId && string.IsNullOrWhiteSpace(searchTerm) ||
                    model.VehicleRegistration.Contains(searchTerm)
                    orderby model.CreationTime descending

                    select new InventoryReceivedHeader
                    {
                        Id = model.Id,
                        AdjustmentTypeID = model.AdjustmentTypeID,
                        TransactionDate = model.TransactionDate,
                        WarehouseID = model.WarehouseID,
                        WarehouseBinID = model.WarehouseBinID,
                        Notes = model.Notes,
                        Void = model.Void,
                        Captured = model.Captured,
                        Verified = model.Verified,
                        VerifiedBy = model.VerifiedBy,
                        VerifiedDate = model.VerifiedDate,
                        Issued = model.Issued,
                        IssuedBy = model.IssuedBy,
                        IssuedDate = model.IssuedDate,
                        Approved = model.Approved,
                        ApprovedBy = model.ApprovedBy,
                        ApprovedDate = model.ApprovedDate,
                        Posted = model.Posted,
                        PostedBy = model.PostedBy,
                        PostedDate = model.PostedDate,
                        BatchControlNumber = model.BatchControlNumber,
                        BatchControlTotal = model.BatchControlTotal,
                        Signature = model.Signature,
                        SignaturePassword = model.SignaturePassword,
                        SupervisorSignature = model.SupervisorSignature,
                        ManagerSignature = model.ManagerSignature,
                        Currency = model.Currency,
                        CurrencyExchangeRate = model.CurrencyExchangeRate,
                        Reference = model.Reference,
                        WarehouseCustomerID = model.WarehouseCustomerID,
                        DeliveryNote = model.DeliveryNote,
                        SiteNumber = model.SiteNumber,
                        VehicleRegistration = model.VehicleRegistration,
                        DriversId = model.DriversId,
                        FromCompanyID = model.FromCompanyID,
                        FromWarehouseID = model.FromWarehouseID,
                        FromWarehouseBinID = model.FromWarehouseBinID,
                        InventoryIssueTransferID = model.InventoryIssueTransferID,
                    };

                return await InventoryReceivedHeader.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task<InventoryReceivedHeader> GetInventoryReceivedHeader(Guid Id)
        {
            var model = _InventoryReceivedHeaderRepo.FirstOrDefault(x => x.Id == Id);

            return new InventoryReceivedHeader
            {
                Id = model.Id,
                AdjustmentTypeID = model.AdjustmentTypeID,
                TransactionDate = model.TransactionDate,
                WarehouseID = model.WarehouseID,
                WarehouseBinID = model.WarehouseBinID,
                Notes = model.Notes,
                Void = model.Void,
                Captured = model.Captured,
                Verified = model.Verified,
                VerifiedBy = model.VerifiedBy,
                VerifiedDate = model.VerifiedDate,
                Issued = model.Issued,
                IssuedBy = model.IssuedBy,
                IssuedDate = model.IssuedDate,
                Approved = model.Approved,
                ApprovedBy = model.ApprovedBy,
                ApprovedDate = model.ApprovedDate,
                Posted = model.Posted,
                PostedBy = model.PostedBy,
                PostedDate = model.PostedDate,
                BatchControlNumber = model.BatchControlNumber,
                BatchControlTotal = model.BatchControlTotal,
                Signature = model.Signature,
                SignaturePassword = model.SignaturePassword,
                SupervisorSignature = model.SupervisorSignature,
                ManagerSignature = model.ManagerSignature,
                Currency = model.Currency,
                CurrencyExchangeRate = model.CurrencyExchangeRate,
                Reference = model.Reference,
                WarehouseCustomerID = model.WarehouseCustomerID,
                DeliveryNote = model.DeliveryNote,
                SiteNumber = model.SiteNumber,
                VehicleRegistration = model.VehicleRegistration,
                DriversId = model.DriversId,
                FromCompanyID = model.FromCompanyID,
                FromWarehouseID = model.FromWarehouseID,
                FromWarehouseBinID = model.FromWarehouseBinID,
                InventoryIssueTransferID = model.InventoryIssueTransferID,
            };
        }


        public async Task<bool> UpdateInventoryReceivedHeader(InventoryReceivedHeader model, Guid Id)
        {
            var InventoryReceivedHeader = _InventoryReceivedHeaderRepo.FirstOrDefault(x => x.Id == model.Id);
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            if (model.Captured == true)
            {
                model.CreatorUserId = currentUser.Id;
                model.RecieveDate = DateTime.Now;
            }
            if (model.PlatformType == 0)
            {
                model.PlatformType = PlatformType.AdminWeb;
            }

            InventoryReceivedHeader.AdjustmentTypeID = model.AdjustmentTypeID;
            InventoryReceivedHeader.TransactionDate = model.TransactionDate;
            InventoryReceivedHeader.WarehouseID = model.WarehouseID;
            InventoryReceivedHeader.WarehouseBinID = model.WarehouseBinID;
            InventoryReceivedHeader.Notes = model.Notes;
            InventoryReceivedHeader.Void = model.Void;
            InventoryReceivedHeader.Captured = model.Captured;
            InventoryReceivedHeader.Verified = model.Verified;
            InventoryReceivedHeader.VerifiedBy = model.VerifiedBy;
            InventoryReceivedHeader.VerifiedDate = model.VerifiedDate;
            InventoryReceivedHeader.Issued = model.Issued;
            InventoryReceivedHeader.IssuedBy = model.IssuedBy;
            InventoryReceivedHeader.IssuedDate = model.IssuedDate;
            InventoryReceivedHeader.Approved = model.Approved;
            InventoryReceivedHeader.ApprovedBy = model.ApprovedBy;
            InventoryReceivedHeader.ApprovedDate = model.ApprovedDate;
            InventoryReceivedHeader.Posted = model.Posted;
            InventoryReceivedHeader.PostedBy = model.PostedBy;
            InventoryReceivedHeader.PostedDate = model.PostedDate;
            InventoryReceivedHeader.BatchControlNumber = model.BatchControlNumber;
            InventoryReceivedHeader.BatchControlTotal = model.BatchControlTotal;
            InventoryReceivedHeader.Signature = model.Signature;
            InventoryReceivedHeader.SignaturePassword = model.SignaturePassword;
            InventoryReceivedHeader.SupervisorSignature = model.SupervisorSignature;
            InventoryReceivedHeader.ManagerSignature = model.ManagerSignature;
            InventoryReceivedHeader.Currency = model.Currency;
            InventoryReceivedHeader.CurrencyExchangeRate = model.CurrencyExchangeRate;
            InventoryReceivedHeader.Reference = model.Reference;
            InventoryReceivedHeader.WarehouseCustomerID = model.WarehouseCustomerID;
            InventoryReceivedHeader.DeliveryNote = model.DeliveryNote;
            InventoryReceivedHeader.SiteNumber = model.SiteNumber;
            InventoryReceivedHeader.VehicleRegistration = model.VehicleRegistration;
            InventoryReceivedHeader.DriversId = model.DriversId;
            InventoryReceivedHeader.FromCompanyID = model.FromCompanyID;
            InventoryReceivedHeader.FromWarehouseID = model.FromWarehouseID;
            InventoryReceivedHeader.FromWarehouseBinID = model.FromWarehouseBinID;
            InventoryReceivedHeader.InventoryIssueTransferID = model.InventoryIssueTransferID;

            InventoryReceivedHeader.CompanyId = (int)currentUser.CompanyId;
            InventoryReceivedHeader.PlatformType = model.PlatformType;

            await _unitOfWork.SaveChangesAsync();

            //if (model.Captured == true && model.Verified == false && model.Approved == false)
            //{
            //    var AppEmail = "no-reply@libmot.com";
            //    var VerifyEmail = "netprowebs@gmail.com";

            //    string smsMessage = $"Recieve of test quantity has been captured. Awaiting your verification";
            //    //_smsSvc.SendSMSNow(smsMessage, recipient: newAdded.ContactName.ToNigeriaMobile());

            //    if (currentUserEmail != null)
            //    {
            //        var mail = new Mail(AppEmail, "Awaiting Verification!", VerifyEmail)
            //        {
            //            Body = smsMessage
            //        };
            //        _mailSvc.SendMail(mail);
            //    }

            //}

            return true;
        }


        public async Task<bool> VerifyRequest(InventoryReceivedHeader model, Guid Id)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            var Request = _InventoryReceivedHeaderRepo.FirstOrDefault(x => x.Id == Id);

            //var item = _itemRepo.Get(Request.ItemID);

            var AppEmail = "no-reply@libmot.com";
            var VerifyEmail = "netprowebs@gmail.com";

            Request.Verified = true;
            Request.VerifiedDate = DateTime.Now;
            Request.VerifiedBy = currentUser.Id.ToString();
            Request.Void = false;

            // Send sms and email containing account details
            //string smsMessage = $"Recieve of {Request.Quantity} unit/s of {items.ItemName} has been verified. Awaiting your approval";
            //string smsMessage = $"Recieve of test quantity has been verified. Awaiting your approval";
            ////_smsSvc.SendSMSNow(smsMessage, recipient: newAdded.ContactName.ToNigeriaMobile());

            //if (currentUserEmail != null)
            //{
            //    var mail = new Mail(AppEmail, "Awaiting Approval!", VerifyEmail)
            //    {
            //        Body = smsMessage
            //    };
            //    _mailSvc.SendMail(mail);
            //}

            await _unitOfWork.SaveChangesAsync();

            return true;
        }


        public async Task<bool> ReceiveRequest(InventoryReceivedHeader model, Guid Id)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            var Request = _InventoryReceivedHeaderRepo.FirstOrDefault(x => x.Id == Id);
            var AppEmail = "no-reply@libmot.com";
            var ReceiveEmail = "netprowebs@gmail.com";

            Request.Captured = true;
            Request.RecieveDate = DateTime.Now;
            //Request.ApprovedBy = $"{currentUser.FirstName} {currentUser.LastName}";
            Request.CreatorUserId = currentUser.Id;
            Request.Void = false;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }
        public async Task<bool> ApproveRequest(InventoryReceivedHeader model, Guid Id)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            var Request = _InventoryReceivedHeaderRepo.FirstOrDefault(x => x.Id == Id);
            
            var AppEmail = "no-reply@libmot.com";
            var VerifyEmail = "netprowebs@gmail.com";

            Request.Approved = true;
            Request.ApprovedDate = DateTime.Now;
            Request.ApprovedBy = currentUser.Id.ToString();
            Request.Void = false;

            //var Detail = await GetInventoryReceivedDetail(Id);

            var Detail = from m in await GetInventoryReceivedDetail(Id)
                             //where string.IsNullOrWhiteSpace(searchTerm) //||
                         join c in _InventoryItemRepo.GetAll() on m.ItemID equals c.Id
                         orderby m.CreationTime descending

                         select new InventoryReceivedDetailDTO
                         {
                             WarehouseID = m.WarehouseID,
                             WarehouseBinID = m.WarehouseBinID,
                             ItemID = m.ItemID,
                             ReceivedQty =m.ReceivedQty,
                             RequestedQty = m.RequestedQty,
                             ItemName =  c.ItemName
                         };

            foreach (var item in Detail)
            {
                _InventoryByWarehouseRepo.Insert(new InventoryByWarehouse
                {
                    WarehouseID = (int)item.WarehouseID,
                    WarehouseBinID = (int)item.WarehouseBinID,
                    ItemID = (int)item.ItemID,
                    QtyOnHand = Convert.ToString(item.ReceivedQty),
                    ItemName = item.ItemName
                });
            }
            
               
            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        public async Task<bool> ReturnRequest(InventoryReceivedHeader model, Guid Id)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            var Request = _InventoryReceivedHeaderRepo.FirstOrDefault(x => x.Id == Id);

            var AppEmail = "no-reply@libmot.com";
            var ReturnEmail = "netprowebs@gmail.com";

            if (model.SiteNumber == "2")
            {
                Request.Verified = false;
                Request.Void = true;
                Request.ReturnDate = DateTime.UtcNow;
                Request.ReturnNotes = model.ReturnNotes;
            }
            else
            {
                Request.Captured = false;
                Request.Void = true;
                Request.ReturnDate = DateTime.UtcNow;
                Request.ReturnNotes = model.ReturnNotes;
            }


            //// Send sms and email conataining account details
            //string smsMessage = $"Return of test quantity has been Return. Awaiting your correction";
            ////_smsSvc.SendSMSNow(smsMessage, recipient: newAdded.ContactName.ToNigeriaMobile());

            //if (currentUserEmail != null)
            //{
            //    var mail = new Mail(AppEmail, "Awaiting Correction!", ReturnEmail)
            //    {
            //        Body = smsMessage
            //    };
            //    _mailSvc.SendMail(mail);
            //}

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        #endregion

        #region Inventory Received Detail
        public async Task AddInventoryReceivedDetail(InventoryReceivedDetail model)
        {
            //model.InventoryReceivedDetailDescription = model.InventoryReceivedDetailDescription.Trim();
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);



            _InventoryReceivedDetailRepo.Insert(new InventoryReceivedDetail
            {
                InventoryReceivedID = model.InventoryReceivedID,
                ItemID = model.ItemID,
                Description = model.Description,
                RequestedQty = model.RequestedQty,
                WarehouseID = model.WarehouseID,
                WarehouseBinID = model.WarehouseBinID,
                ToWarehouseID = model.ToWarehouseID,
                ToWarehouseBinID = model.ToWarehouseBinID,
                GLExpenseAccount = model.GLExpenseAccount,
                ItemValue = model.ItemValue,
                CostMethod = model.CostMethod,
                ProjectID = model.ProjectID,
                ItemCost = model.ItemCost,
                GLAnalysisType1 = model.GLAnalysisType1,
                GLAnalysisType2 = model.GLAnalysisType2,
                AssetID = model.AssetID,
                PONumber = model.PONumber,
                ItemUPCCode = model.ItemUPCCode,
                ReceivedQty = model.ReceivedQty,
                InventoryTransferID = model.InventoryTransferID,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                CompanyId = (int)currentUser.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteInventoryReceivedDetail(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _InventoryReceivedDetailRepo.DeleteAsync(_InventoryReceivedDetailRepo.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<IPagedList<InventoryReceivedDetail>> GetAllInventoryReceivedDetail(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var InventoryReceivedDetail =
                    from model in _InventoryReceivedDetailRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) ||
                    model.Description.Contains(searchTerm)// ||
                    //Warehouse.WarehouseCode.Contains(searchTerm)
                    orderby model.CreationTime descending

                    select new InventoryReceivedDetail
                    {
                        Id = model.Id,
                        InventoryReceivedID = model.InventoryReceivedID,
                        ItemID = model.ItemID,
                        Description = model.Description,
                        RequestedQty = model.RequestedQty,
                        WarehouseID = model.WarehouseID,
                        WarehouseBinID = model.WarehouseBinID,
                        ToWarehouseID = model.ToWarehouseID,
                        ToWarehouseBinID = model.ToWarehouseBinID,
                        GLExpenseAccount = model.GLExpenseAccount,
                        ItemValue = model.ItemValue,
                        CostMethod = model.CostMethod,
                        ProjectID = model.ProjectID,
                        ItemCost = model.ItemCost,
                        GLAnalysisType1 = model.GLAnalysisType1,
                        GLAnalysisType2 = model.GLAnalysisType2,
                        AssetID = model.AssetID,
                        PONumber = model.PONumber,
                        ItemUPCCode = model.ItemUPCCode,
                        ReceivedQty = model.ReceivedQty,
                        InventoryTransferID = model.InventoryTransferID,
                    };

                return await InventoryReceivedDetail.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Task<List<InventoryReceivedDetailDTO>> GetInventoryReceivedDetail(Guid Id)
        {
            //var model = _InventoryReceivedDetailRepo.GetAll(x => x.InventoryReceivedID == Id);
            var invrecdetails = from model in _InventoryReceivedDetailRepo.GetAll()
                                where model.InventoryReceivedID == Id

                                select new InventoryReceivedDetailDTO
                                {
                                    Id = model.Id,
                                    InventoryReceivedID = model.InventoryReceivedID,
                                    ItemID = model.ItemID,
                                    Description = model.Description,
                                    RequestedQty = model.RequestedQty,
                                    WarehouseID = model.WarehouseID,
                                    WarehouseBinID = model.WarehouseBinID,
                                    ToWarehouseID = model.ToWarehouseID,
                                    ToWarehouseBinID = model.ToWarehouseBinID,
                                    GLExpenseAccount = model.GLExpenseAccount,
                                    ItemValue = model.ItemValue,
                                    CostMethod = model.CostMethod,
                                    ProjectID = model.ProjectID,
                                    ItemCost = model.ItemCost,
                                    GLAnalysisType1 = model.GLAnalysisType1,
                                    GLAnalysisType2 = model.GLAnalysisType2,
                                    AssetID = model.AssetID,
                                    PONumber = model.PONumber,
                                    ItemUPCCode = model.ItemUPCCode,
                                    ReceivedQty = model.ReceivedQty,
                                    InventoryTransferID = model.InventoryTransferID,
                                };
            return invrecdetails.ToListAsync();
        }

        public async Task<bool> UpdateInventoryReceivedDetail(InventoryReceivedDetail model, int Id)
        {
            var InventoryReceivedDetail = _InventoryReceivedDetailRepo.FirstOrDefault(x => x.Id == Id);
            //InventoryReceivedDetail.Id = model.Id;
            InventoryReceivedDetail.InventoryReceivedID = model.InventoryReceivedID;
            InventoryReceivedDetail.ItemID = model.ItemID;
            InventoryReceivedDetail.Description = model.Description;
            InventoryReceivedDetail.RequestedQty = model.RequestedQty;
            InventoryReceivedDetail.WarehouseID = model.WarehouseID;
            InventoryReceivedDetail.WarehouseBinID = model.WarehouseBinID;
            InventoryReceivedDetail.ToWarehouseID = model.ToWarehouseID;
            InventoryReceivedDetail.ToWarehouseBinID = model.ToWarehouseBinID;
            InventoryReceivedDetail.GLExpenseAccount = model.GLExpenseAccount;
            InventoryReceivedDetail.ItemValue = model.ItemValue;
            InventoryReceivedDetail.CostMethod = model.CostMethod;
            InventoryReceivedDetail.ProjectID = model.ProjectID;
            InventoryReceivedDetail.ItemCost = model.ItemCost;
            InventoryReceivedDetail.GLAnalysisType1 = model.GLAnalysisType1;
            InventoryReceivedDetail.GLAnalysisType2 = model.GLAnalysisType2;
            InventoryReceivedDetail.AssetID = model.AssetID;
            InventoryReceivedDetail.PONumber = model.PONumber;
            InventoryReceivedDetail.ItemUPCCode = model.ItemUPCCode;
            InventoryReceivedDetail.ReceivedQty = model.ReceivedQty;
            InventoryReceivedDetail.InventoryTransferID = model.InventoryTransferID;


            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        #endregion

    }
}