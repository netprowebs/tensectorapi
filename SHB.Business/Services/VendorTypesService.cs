﻿using System;
using IPagedList;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using SHB.Data.Repository;
using SHB.Core.Domain.Entities;
using SHB.Data.UnitOfWork;
using SHB.Core.Domain.DataTransferObjects;

namespace SHB.Business.Services
{
    public interface IVendorTypesService
    {

        #region Vendor Types
        Task AddVendorTypes(VendorTypesDTO model);
        Task<VendorTypesDTO> GetVendorType(int Id);
        Task<IPagedList<VendorTypes>> GetAllVendorTypes(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateVendorType(VendorTypesDTO model, int Id);
        Task DeleteVendorType(int Id);
        #endregion

        #region Brand Types
        Task AddBrandType(BrandType model);
        Task<BrandType> GetBrandType(int Id);
        Task<IPagedList<BrandType>> GetAllBrandType(int pageNumber, int pageSize, string searchTerm);

        Task<bool> UpdateBrandType(BrandType model, int Id);
        Task DeleteBrandType(int Id);
        #endregion


    }
    public class VendorTypesService : IVendorTypesService
    {
        private readonly IRepository<VendorTypes> _vendorTypesRepo;
        private readonly IRepository<BrandType> _brandTypeRepo;

        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;

        private readonly IUserService _userSvc;
        public VendorTypesService(

             IRepository<VendorTypes> vendorTypesRepo,
             IRepository<BrandType> brandTypeRepo,

            IServiceHelper serviceHelper,
            IUnitOfWork unitOfWork,

            IUserService userSvc
            )
        {

            _vendorTypesRepo = vendorTypesRepo;
            _brandTypeRepo = brandTypeRepo;
            _serviceHelper = serviceHelper;
            _unitOfWork = unitOfWork;
            _userSvc = userSvc;
        }

        #region Vendor Types
        public async Task AddVendorTypes(VendorTypesDTO model)
        {
            model.Name = model.Name.Trim();
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);



            _vendorTypesRepo.Insert(new VendorTypes
            {
                Name = model.Name,
                VendorTypeDescription = model.VendorTypeDescription,
                CreditorsControlAccount = model.CreditorsControlAccount,
                CurrencyExchange = model.CurrencyExchange,
                GainOrLossAccount = model.GainOrLossAccount,
                DiscountsAccount = model.DiscountsAccount,
                DiscountRate = model.DiscountRate,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                CompanyId = (int)currentUser.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteVendorType(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _vendorTypesRepo.DeleteAsync(_vendorTypesRepo.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<IPagedList<VendorTypes>> GetAllVendorTypes(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var vendorType =
                    from model in _vendorTypesRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) ||
                    model.VendorTypeDescription.Contains(searchTerm)// ||
                    //Warehouse.WarehouseCode.Contains(searchTerm)
                    orderby model.CreationTime descending

                    select new VendorTypes
                    {
                        Id = model.Id,
                        Name = model.Name,
                        VendorTypeDescription = model.VendorTypeDescription,
                        CreditorsControlAccount = model.CreditorsControlAccount,
                        CurrencyExchange = model.CurrencyExchange,
                        GainOrLossAccount = model.GainOrLossAccount,
                        DiscountsAccount = model.DiscountsAccount,
                        DiscountRate = model.DiscountRate,
                    };

                return await vendorType.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task<VendorTypesDTO> GetVendorType(int Id)
        {
            var model = _vendorTypesRepo.FirstOrDefault(x => x.Id == Id);

            return new VendorTypesDTO
            {
                ID = model.Id,
                Name = model.Name,
                VendorTypeDescription = model.VendorTypeDescription,
                CreditorsControlAccount = model.CreditorsControlAccount,
                CurrencyExchange = model.CurrencyExchange,
                GainOrLossAccount = model.GainOrLossAccount,
                DiscountsAccount = model.DiscountsAccount,
                DiscountRate = model.DiscountRate,
            };
        }


        public async Task<bool> UpdateVendorType(VendorTypesDTO model, int Id)
        {
            var vendorType = _vendorTypesRepo.FirstOrDefault(x => x.Id == Id);

            //vendorType.Id = model.Id;
            vendorType.Name = model.Name;
            vendorType.VendorTypeDescription = model.VendorTypeDescription;
            vendorType.CreditorsControlAccount = model.CreditorsControlAccount;
            vendorType.CurrencyExchange = model.CurrencyExchange;
            vendorType.GainOrLossAccount = model.GainOrLossAccount;
            vendorType.DiscountsAccount = model.DiscountsAccount;
            vendorType.DiscountRate = model.DiscountRate;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }
        #endregion


        #region Brand Types
        public async Task AddBrandType(BrandType model)
        {
            model.Name = model.Name.Trim();
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);



            _brandTypeRepo.Insert(new BrandType
            {
                Name = model.Name,
                Description = model.Description,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                CompanyId = (int)currentUser.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteBrandType(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _brandTypeRepo.DeleteAsync(_brandTypeRepo.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<IPagedList<BrandType>> GetAllBrandType(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var brandType =
                    from model in _brandTypeRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) ||
                    model.Name.Contains(searchTerm)// ||
                    //Warehouse.WarehouseCode.Contains(searchTerm)
                    orderby model.CreationTime descending

                    select new BrandType
                    {
                        Id = model.Id,
                        Name = model.Name,
                        Description = model.Description
                    };

                return await brandType.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task<BrandType> GetBrandType(int Id)
        {
            var model = _brandTypeRepo.FirstOrDefault(x => x.Id == Id);

            return new BrandType
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description
            };
        }


        public async Task<bool> UpdateBrandType(BrandType model, int Id)
        {
            var brandType = _brandTypeRepo.FirstOrDefault(x => x.Id == Id);

            brandType.Name = model.Name;
            brandType.Description = model.Description;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }
        #endregion
    }
}