﻿using SHB.Core.Domain.DataTransferObjects;
using SHB.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Business.Services
{
    public interface IClaimService
    {
        Task<IEnumerable<ClaimDTO>> GetClaims();
        Task<ClaimDTO> GetClaimById(int claimId);
        Task AddClaim(ClaimDTO claim);
        Task UpdateClaim(int claimId, ClaimDTO claim);
        Task RemoveClaim(int claimId);
    }
    public class ClaimService : IClaimService
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceHelper _serviceHelper;

        public ClaimService(

            IUnitOfWork unitOfWork,
            IServiceHelper serviceHelper)
        {

            _unitOfWork = unitOfWork;
            _serviceHelper = serviceHelper;
        }

        public Task AddClaim(ClaimDTO claim)
        {
            throw new NotImplementedException();
        }

        public Task<ClaimDTO> GetClaimById(int claimId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ClaimDTO>> GetClaims()
        {
            throw new NotImplementedException();
        }

        public Task RemoveClaim(int claimId)
        {
            throw new NotImplementedException();
        }

        public Task UpdateClaim(int claimId, ClaimDTO claim)
        {
            throw new NotImplementedException();
        }
    }
}
