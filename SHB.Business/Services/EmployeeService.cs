﻿using IPagedList;
using SHB.Core;
using SHB.Core.Configuration;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Core.Utilities;
using SHB.Core.Utils;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using SHB.Business.Messaging.Sms;
using SHB.Business.Messaging.Email;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LME.Core.Domain.DataTransferObjects;
using SHB.Core.Entities.Enums;

namespace SHB.Business.Services
{
    public interface IEmployeeService
    {
        Employee FirstOrDefault(Expression<Func<Employee, bool>> filter);
        IQueryable<Employee> GetAll();
        Task<List<EmployeeDTO>> GetTerminalEmployees(int terminalId);
        Task<EmployeeDTO> GetEmployeesByemailAsync(string email);
        Task<User> GetUserInfo(string email);        
        Task AddEmployee(EmployeeDTO employee);
        Task<IPagedList<EmployeeDTO>> GetEmployees(int pageNumber, int pageSize, string query);
        Task<int?> GetAssignedTerminal(string email);
        Task<EmployeeDTO> GetOperationManager(string email);
        Task UpdateEmployeeOtp(int employeeId, string otp);
        Task<bool> Verifyotp(string otp);
        Task<EmployeeDTO> GetEmployee(int id);
        Task UpdateEmployee(int id, EmployeeDTO model);
        Task<bool> DeactivateOrActiveAccount(int Id);
        string GetNumbers(string phoneNumber);
        string GetUserId(string phoneNumber);
        Task<bool> SendMailNSms(EmailSetting model);
    }

    public class EmployeeService : IEmployeeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceHelper _serviceHelper;
        private readonly IRepository<Employee> _repo;
        private readonly IRepository<Terminal> _terminalRepo;
        private readonly IRepository<Wallet> _walletRepo;
        //private readonly IRepository<Department> _departmentRepo;
        private readonly IUserService _userSvc;
        private readonly IRoleService _roleSvc;
        private readonly IReferralService _referralSvc;
        private readonly ISMSService _smsSvc;
        private readonly IMailService _mailSvc;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IGuidGenerator _guidGenerator;
        private readonly AppConfig appConfig;
        private readonly UserManager<User> _userManager;
        private readonly IWalletService _walletSvc;
        private readonly IRepository<WalletNumber> _walletNumberRepo;
        private readonly IRepository<CustomerInformation> _customerInfoRepo;
        private readonly IUtilityService _utilityService;

        private readonly IRepository<CompanyInfo> _companyinfo;

        public EmployeeService(IUnitOfWork unitOfWork,
            IRepository<Employee> employeeRepo,
            UserManager<User> userManager,
            IRepository<Terminal> terminalRepo,
            IRepository<Wallet> walletRepo,
            //IRepository<Department> departmentRepo,
            IServiceHelper serviceHelper,
            IUserService userSvc,
            IRoleService roleSvc,
            IReferralService referralSvc,
            ISMSService smsSvc,
            IMailService mailSvc,
            IHostingEnvironment hostingEnvironment,
            IGuidGenerator guidGenerator,
            IOptions<AppConfig> _appConfig,
            IRepository<CompanyInfo> companyinfo,
            IWalletService walletSvc,
            IRepository<WalletNumber> walletNumberRepo,
            IRepository<CustomerInformation> customerInfoRepo,
            IUtilityService utilityService
            )
        {
            _unitOfWork = unitOfWork;
            _repo = employeeRepo;
            _terminalRepo = terminalRepo;
            _walletRepo = walletRepo;
            //_departmentRepo = departmentRepo;
            _serviceHelper = serviceHelper;
            _userSvc = userSvc;
            _referralSvc = referralSvc;
            _smsSvc = smsSvc;
            _mailSvc = mailSvc;
            appConfig = _appConfig.Value;
            _hostingEnvironment = hostingEnvironment;
            _guidGenerator = guidGenerator;
            _roleSvc = roleSvc;
            _userManager = userManager;
            _companyinfo = companyinfo;
            _walletSvc = walletSvc;
            _walletNumberRepo = walletNumberRepo;
            _customerInfoRepo = customerInfoRepo;
            _utilityService = utilityService;
        }

        private async Task<bool> IsValidTerminal(int? id)
        {
            return await _terminalRepo.ExistAsync(x => x.Id == id);
        }

        //private async Task<bool> IsValidDepartment(int? id)
        //{
        //    return await _departmentRepo.ExistAsync(x => x.Id == id);
        //}

        private async Task<bool> EmployeeExist(string code)
        {
            return await _repo.ExistAsync(x => x.EmployeeCode == code);
        }
        public string GetNumbers(string phoneNumber)
        {
            var userEmail = _repo.GetAll().Where(x => x.User.PhoneNumber.Trim() == phoneNumber.Trim()).Select(x => x.User.Email).FirstOrDefault();
            return userEmail;
        }

        public string GetUserId(string phoneNumber)
        {
            return _repo.GetAll().Where(x => x.User.PhoneNumber == phoneNumber ).Select(x => x.UserId).FirstOrDefault().ToString();
        }
        public async Task AddEmployee(EmployeeDTO employee)
        {
            if (employee == null)
            {
                throw await _serviceHelper.GetExceptionAsync("invalid parameter");
            }

            if (employee.TerminalId != null && !await IsValidTerminal(employee.TerminalId))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.TERMINAL_NOT_EXIST);
            }

            //if (employee.DepartmentId != null && !await IsValidDepartment(employee.DepartmentId))
            //{
            //    throw await _serviceHelper.GetExceptionAsync(ErrorConstants.DEPARTMENT_NOT_EXIST);
            //}

            employee.EmployeeCode = employee.EmployeeCode.Trim();

            if (await _repo.ExistAsync(v => v.EmployeeCode == employee.EmployeeCode))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.EMPLOYEE_EXIST);
            }

            try
            {
                _unitOfWork.BeginTransaction();

                // Create wallet for new customer
                var walletNumber = await _walletSvc.GenerateNextValidWalletNumber();
                var PersistedUser = await _userSvc.FindByEmailAsync(employee.Email);

                _walletNumberRepo.Insert(walletNumber);

                var wallet = new Wallet
                {
                    WalletNumber = walletNumber.WalletPan,
                    CreatorUserId = _serviceHelper.GetCurrentUserId(),
                    Balance = 0.00M,
                    UserType = UserType.Employee,
                    TenantId = _serviceHelper.GetTenantId(),
                    //UserId = Convert.ToInt32(PersistedUser.Id),
                };

                _walletSvc.Add(wallet);

                await _unitOfWork.SaveChangesAsync();
                // for picture upload and insertion
                string Nrmfile = "";
                if (!string.IsNullOrEmpty(employee.EmployeePhoto) )
                {
                    List<ImageFileDTO> imageFileDTO = new List<ImageFileDTO>();

                    if (employee.DeviceType == DeviceType.AdminWeb)
                    {
                        foreach (var file in employee.PictureList)
                        {
                                var txt = "-EPHT-";
                                var ImageNameWitExt = Path.GetFileNameWithoutExtension(file.fileName);
                                Nrmfile = await InfoForImageB(txt, file.data, ImageNameWitExt, file.fileName, file.ContentType);
                        }
                    }
                    else if (employee.DeviceType == DeviceType.Website || employee.DeviceType == DeviceType.Android || employee.DeviceType == DeviceType.iOS)
                    {
                        var txt = "-EPHT-";
                        Nrmfile = await InfoForImage(employee, txt);
                    }
                }


                var user = new User
                {
                    FirstName = employee.FirstName,
                    LastName = employee.LastName,
                    MiddleName = employee.MiddleName,
                    Gender = employee.Gender,
                    Email = employee.Email,
                    PhoneNumber = employee.PhoneNumber,
                    Address = employee.Address,
                    NextOfKinName = employee.NextOfKin,
                    NextOfKinPhone = employee.NextOfKinPhone,
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true,
                    UserName = employee.Email,
                    ReferralCode = CommonHelper.GenereateRandonAlphaNumeric(),
                    CompanyId = _serviceHelper.GetTenantId(),
                    IdentificationCode = employee.IdentificationCode,
                    IdentificationType = employee.IdentificationType,
                    IDIssueDate = employee.IDIssueDate,
                    IDExpireDate = employee.IDExpireDate,
                    LocationId = employee.TerminalId,
                    UserType = UserType.Employee,
                    IsFirstTimeLogin = true,
                    //WalletId = wallet.Id,
                    WalletId1 = wallet.Id,
                    Photo = Nrmfile
                };

                var creationStatus = await _userSvc.CreateAsync(user, "123456");

                if (creationStatus.Succeeded)
                {
                    //map user to existing row details
                    var dbRole = await _roleSvc.FindByIdAsync(employee.RoleId);

                    if (dbRole != null)
                    {
                        await _userSvc.AddToRoleAsync(user, dbRole.Name);
                    }

                    _repo.Insert(new Employee
                    {
                        UserId = user.Id,
                        EmployeeCode = employee.EmployeeCode,
                        DateOfEmployment = employee.DateOfEmployment,
                        DepartmentId = employee.DepartmentId,
                        TerminalId = employee.TerminalId,
                        CreatorUserId = _serviceHelper.GetCurrentUserId(),
                        IsActive = true
                    });

                    //PersistedUser.WalletId = wallet.Id;
                    await _unitOfWork.SaveChangesAsync();
                    // checks for which device 
                    //if (employee.DeviceType == DeviceType.AdminWeb)
                    //{
                    //    await _userSvc.UserDirectActivation(employee.Email);
                    //}
                    await SendAccountEmail(user);
                    
                }
                else
                {
                    _unitOfWork.Rollback();

                    throw await _serviceHelper
                        .GetExceptionAsync(creationStatus.Errors.FirstOrDefault()?.Description);
                }
                _unitOfWork.Commit();
            }
            catch (Exception ex)
            {

                _unitOfWork.Rollback();
                throw;
            }
        }


        public async Task<string> InfoForImage(EmployeeDTO model, string txt)
        {
            string pic = "";
            string Contype = "";
            string ext = "";
            var ImageNameWitExt = "";
            var ImageName = "";
            string fileName = "";

            List<ImageFileDTO> imageFileDTO = new List<ImageFileDTO>();
            if (!string.IsNullOrEmpty(model.EmployeePhoto))
            {
                pic = model.EmployeePhoto.Split(',').Last();
                Contype = model.EmployeePhoto.Split(new Char[] { ':', ';' })[1];
                ext = "." + model.EmployeePhoto.Split(new Char[] { '/', ';' })[1];

                ImageNameWitExt = DateTime.UtcNow.ToString("yymmssfff") + txt + CommonHelper.GenereateRandonAlphaNumeric().Substring(0, 3);
                ImageName = ImageNameWitExt + ext;
                ImageFileDTO imgdto = new ImageFileDTO();
                byte[] bytes = Convert.FromBase64String(pic);
                imgdto.data = bytes;
                imgdto.FilenameWithOutExt = ImageNameWitExt;
                imgdto.fileName = ImageName;
                imgdto.ContentType = Contype;
                imgdto.UploadType = UploadType.Image;
                imageFileDTO.Add(imgdto);
                imgdto.PictureList = imageFileDTO;

                fileName = await _utilityService.UplaodBytesToDigOc(imgdto);
            }

            return fileName;
        }


        public async Task<string> InfoForImageB(string txt, byte[] bytes, string ImageNameWitExt, string ImageName, string Contype)
        {
            string fileName = "";
            List<ImageFileDTO> imageFileDTO = new List<ImageFileDTO>();
            ImageFileDTO imgdto = new ImageFileDTO();
            imgdto.data = bytes;
            imgdto.FilenameWithOutExt = DateTime.UtcNow.ToString("yymmssfff") + txt + CommonHelper.GenereateRandonAlphaNumeric().Substring(0, 3);
            //imgdto.FilenameWithOutExt = ImageNameWitExt;
            imgdto.fileName = ImageName;
            imgdto.ContentType = Contype;
            imgdto.UploadType = UploadType.Image;
            imageFileDTO.Add(imgdto);
            imgdto.PictureList = imageFileDTO;
            fileName = await _utilityService.UplaodBytesToDigOc(imgdto);
            return fileName;
        }

        private async Task SendAccountEmail(User user)
        {
            try
            {
                var replacement = new StringDictionary
                {
                    ["FirstName"] = user.FirstName,
                    ["UserName"] = user.UserName,
                    ["DefaultPassword"] = "123456"
                };

                var mail = new Mail(appConfig.AppEmail, "Seclot.com: New staff account information", user.Email)
                {
                    BodyIsFile = true,
                    BodyPath = Path.Combine(_hostingEnvironment.ContentRootPath, CoreConstants.Url.AccountActivationEmail)
                };

                await _mailSvc.SendMailAsync(mail, replacement);
            }
            catch (Exception)
            {
            }
        }
        public async Task<bool> SendMailNSms(EmailSetting model)
        {
            model.EmailFrom = model.EmailFrom;

            model.SenderName = model.EmailFrom;
            model.MJ_APIKEY_PRIVATE = appConfig.MJ_APIKEY_PUBLIC;
            model.MJ_APIKEY_PUBLIC = appConfig.MJ_APIKEY_PRIVATE;
            model.EmailTo = model.EmailFrom;
            model.Subject = model.EmailFrom;
            model.IsSent = false;

            try
            {

                if (model.SenderType == 1) 
                {

                    //Send sms and email containing account details
                    string smsMessage = $"Recieve of test quantity has been verified. Awaiting your approval";

                 _smsSvc.SendSMSNow(model.Body, "LIBMOEXPRES", recipient: model.ToPhoneContact.ToNigeriaMobile());

                    model.IsSent = true;
                }
                else if (model.SenderType == 2)
                {
                    var replacement = new StringDictionary
                    {
                        ["FirstName"] = "Oluwasegun",
                        ["UserName"] = "Ayodele",
                        ["DefaultPassword"] = "123456"
                    };
                    string pathToHtmlTemplate = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Content", CoreConstants.Url.AccountActivationEmail);
                    string templateContent = File.ReadAllText(pathToHtmlTemplate);


                    var mail = new Mail("noreply@vet.seclot.com", "Seclot.com: New staff account information", "shextem2008@gmail.com")
                    {
                        BodyIsFile = true,
                        BodyPath = pathToHtmlTemplate
                        //BodyPath = Path.Combine(_hostingEnvironment.ContentRootPath, CoreConstants.Url.AccountActivationEmail)
                    };

                    model.IsSent = await _mailSvc.UseMailJetB(mail, replacement, model);
                    //model.IsSent = await _mailSvc.UseMailJet(mail, replacement, model);
                    //await _mailSvc.SendMailAsync(mail, replacement);
                    if (model.IsSent);
                    // context.Add(model);
                }

                return model.IsSent;
            }
            catch (Exception ex)
            {
                throw;

            }
            return model.IsSent;

        }

      


        public Employee FirstOrDefault(Expression<Func<Employee, bool>> filter)
        {
            return _repo.FirstOrDefault(filter);
        }

        public IQueryable<Employee> GetAll()
        {
            return _repo.GetAll();
        }

        public Task<EmployeeDTO> GetEmployeesByemailAsync(string email)
        {
            //var uuu = _userManager.Users.Include(u => u.Wallet).ThenInclude(ur => ur.Role).ToList();

            var employees =
                from employee in _repo.GetAllIncluding(x => x.User)

                    //join department in _departmentRepo.GetAll() on employee.DepartmentId equals department.Id
                    //into departments
                    //from department in departments.DefaultIfEmpty()

                //join wallet in _walletRepo.GetAll() on employee.User.WalletId equals wallet.Id
                //into wallets
                //from wallet in wallets.DefaultIfEmpty()

                join terminal in _terminalRepo.GetAll() on employee.TerminalId equals terminal.Id
                into terminals
                from terminal in terminals.DefaultIfEmpty()

                join company in _companyinfo.GetAll() on employee.User.CompanyId equals company.Id
                into companys
                from company in companys.DefaultIfEmpty()

                where employee.User.Email == email 

                select new EmployeeDTO
                {
                    Id = employee.Id,
                    DateOfEmployment = employee.DateOfEmployment,
                    FirstName = employee.User.FirstName,
                    LastName = employee.User.LastName,
                    MiddleName = employee.User.MiddleName,
                    Gender = employee.User.Gender,
                    Email = employee.User.Email,
                    PhoneNumber = employee.User.PhoneNumber,
                    Address = employee.User.Address,
                    EmployeePhoto = employee.User.Photo,
                    NextOfKin = employee.User.NextOfKinName,
                    NextOfKinPhone = employee.User.NextOfKinPhone,
                    //DepartmentName = department.Name,
                    //DepartmentId = department.Id,
                    TerminalId = terminal.Id,
                    TerminalName = terminal.Name,
                    Otp = employee.Otp,
                    OtpIsUsed = employee.OtpIsUsed,
                    EmployeeCode = employee.EmployeeCode,
                    Company = company.Name
                };

            return employees.AsNoTracking().FirstOrDefaultAsync();
        }


        public Task<User> GetUserInfo(string email)
        {
            var userv = _userSvc.FindByEmailAsync(email);
  
                  new User
                  {
                     Id = userv.Result.Id,
                     CreationTime = userv.Result.CreationTime,
                     FirstName = userv.Result.FirstName,
                     LastName = userv.Result.LastName,
                     Gender = userv.Result.Gender,
                     Email = userv.Result.Email,
                     PhoneNumber = userv.Result.PhoneNumber,
                     Address = userv.Result.Address,
                     Photo = userv.Result.Photo,
                     NextOfKinName = userv.Result.NextOfKinName,
                     NextOfKinPhone = userv.Result.NextOfKinPhone,
                     LocationId = userv.Result.LocationId,
                     CompanyId = userv.Result.CompanyId,
                     //WalletId = userv.Result.WalletId,
                      IdentificationCode = userv.Result.IdentificationCode,
                      IdentificationType = userv.Result.IdentificationType,
                      IDIssueDate = userv.Result.IDIssueDate,
                      IDExpireDate = userv.Result.IDExpireDate,
                      IdentityPhoto = userv.Result.IdentityPhoto,
                      UserType = userv.Result.UserType,
                  };

            return userv;
        }


        public Task<List<EmployeeDTO>> GetTerminalEmployees(int terminalId)
        {
            var employees =
                from employee in _repo.GetAllIncluding(x => x.User)
                join terminal in _terminalRepo.GetAll() on employee.TerminalId equals terminal.Id
                where terminal.Id == terminalId

                select new EmployeeDTO
                {
                    Id = employee.Id,
                    FirstName = employee.User.FirstName,
                    LastName = employee.User.LastName,
                    Email = employee.User.Email,
                    Otp = employee.Otp,
                    OtpIsUsed = employee.OtpIsUsed,
                    PhoneNumber = employee.User.PhoneNumber
                };

            return employees.AsNoTracking().ToListAsync();
        }
        public async Task<EmployeeDTO> GetOperationManager(string email)
        {


            var terminal = await GetAssignedTerminal(email);
            if (terminal != null)
            {
                var terminalEmployees = await GetAllEmployeeByTerminalId(terminal.GetValueOrDefault());
                foreach (var employee in terminalEmployees)
                {
                    IList<string> userRoles = new List<string>();

                    try
                    {
                        userRoles = await _userSvc.GetUserRolesAsync(employee.Email);
                    }
                    catch
                    {

                    }


                    foreach (var role in userRoles)
                    {
                        if (role == "Operations Manager")
                        {
                            return employee;
                        }
                    }
                }
            }

            return new EmployeeDTO();



        }

        public async Task<bool> Verifyotp(string otp)
        {


            var email = await _userSvc.FindByNameAsync(_serviceHelper.GetCurrentUserEmail());
            var operationManager = await GetOperationManager(email.Email);
            if (operationManager.Otp == otp && !operationManager.OtpIsUsed)
            {
                await UpdateUsedEmployeeOtp(operationManager.Id);
                return true;
            }
            return false;


        }
        public async Task<List<EmployeeDTO>> GetAllEmployeeByTerminalId(int terminalId)
        {

            var employee = await GetTerminalEmployees(terminalId);

            if (employee == null)
            {
                //throw await _helper.GetExceptionAsync(ErrorConstants.EMPLOYEE_NOT_EXIST);
                return new List<EmployeeDTO>();
            }

            return employee;

        }

        public async Task UpdateEmployeeOtp(int employeeId, string otp)
        {
            var employees = await _repo.GetAsync(employeeId);

            if (employees == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.EMPLOYEE_NOT_EXIST);
            }

            employees.Otp = otp;
            employees.OtpIsUsed = false;
            DateTime today = DateTime.Now.Date;
            var otpMaxperday = appConfig.OTPMaxperday != null ? int.Parse(appConfig.OTPMaxperday) : 2;


            if (employees.OTPLastUsedDate == today && employees.OtpNoOfTimeUsed == otpMaxperday)
            {
                throw await _serviceHelper.GetExceptionAsync("You have exceeded the maximum number of OTP for a day");
            }
            else
            {
                //if (employees.OTPLastUsedDate == null)
                //{
                //    employees.OTPLastUsedDate = today;
                //    employees.OtpNoOfTimeUsed = 1;
                //}
                //else if (employees.OTPLastUsedDate != today)
                //{
                //    employees.OTPLastUsedDate = today;
                //    employees.OtpNoOfTimeUsed = 1;
                //}
                //else if (employees.OTPLastUsedDate == today)
                //{
                //    employees.OtpNoOfTimeUsed = employees.OtpNoOfTimeUsed.GetValueOrDefault() + 1;
                //}


            }

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateUsedEmployeeOtp(int employeeId)
        {
            var employees = await _repo.GetAsync(employeeId);

            if (employees == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.EMPLOYEE_NOT_EXIST);
            }

            employees.OtpIsUsed = true;
            DateTime today = DateTime.Now.Date;
            var otpMaxperday = appConfig.OTPMaxperday != null ? int.Parse(appConfig.OTPMaxperday) : 2;

            if (employees.OTPLastUsedDate == today && employees.OtpNoOfTimeUsed == otpMaxperday)
            {
                throw await _serviceHelper.GetExceptionAsync("You have exceeded the maximum number of OTP for a day");
            }
            else
            {
                if (employees.OTPLastUsedDate == null)
                {
                    employees.OTPLastUsedDate = today;
                    employees.OtpNoOfTimeUsed = 1;
                }
                else if (employees.OTPLastUsedDate != today)
                {
                    employees.OTPLastUsedDate = today;
                    employees.OtpNoOfTimeUsed = 1;
                }
                else if (employees.OTPLastUsedDate == today)
                {
                    employees.OtpNoOfTimeUsed = employees.OtpNoOfTimeUsed.GetValueOrDefault() + 1;
                }
            }

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IPagedList<EmployeeDTO>> GetEmployees(int pageNumber, int pageSize, string query)
        {

            try
            {
                
                List<string> Emp = new List<string>();
                var employees =
                (from employee in _repo.GetAllIncluding(x => x.User)

                 //join department in _departmentRepo.GetAll() on employee.DepartmentId equals department.Id
                 //into departments
                 //from department in departments.DefaultIfEmpty()

                 join terminal in _terminalRepo.GetAll() on employee.TerminalId equals terminal.Id
                 into terminals
                 from terminal in terminals.DefaultIfEmpty()

                 let userRole = employee.User == null ? Emp : _userSvc.GetUserRoles(employee.User).Result

                 where string.IsNullOrWhiteSpace(query) ||
                 (
                    employee.EmployeeCode.Contains(query) ||
                    employee.User.FirstName.Contains(query) ||
                     employee.User.LastName.Contains(query) ||
                     //(employee.User.FirstName + " " + employee.User.LastName).Contains(query) ||
                     employee.User.PhoneNumber.Contains(query) ||
                     employee.User.Email.Contains(query) ||
                     employee.Terminal.Name.Contains(query) ||
                     //string.Join(",", userRole).Contains(query) ||
                     employee.User.NextOfKinName.Contains(query) ||
                     employee.User.NextOfKinPhone.Contains(query)
                 ) && employee.User.CompanyId == _serviceHelper.GetTenantId() && employee.User.UserType == UserType.Employee 
                 
                 select new EmployeeDTO
                 {
                     Id = employee.Id,
                     DateOfEmployment = employee.DateOfEmployment,
                     FirstName = employee.User.FirstName,
                     LastName = employee.User.LastName,
                     MiddleName = employee.User.MiddleName,
                     Gender = employee.User.Gender,
                     Email = employee.User.Email,
                     PhoneNumber = employee.User.PhoneNumber,
                     Address = employee.User.Address,
                     EmployeePhoto = employee.User.Photo,
                     NextOfKin = employee.User.NextOfKinName,
                     NextOfKinPhone = employee.User.NextOfKinPhone,
                     //DepartmentName = department.Name,
                     //DepartmentId = department.Id,
                     TerminalId = terminal.Id,
                     TerminalName = terminal.Name,
                     EmployeeCode = employee.EmployeeCode,
                     RoleName = string.Join(",", userRole),
                     LockoutEnabled = employee.User.LockoutEnabled,
                     UserId = employee.UserId.ToString(),
                     IsActive = employee.IsActive,
                 }).ToList();



                return employees.ToPagedList(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<EmployeeDTO> GetEmployee(int id)
        {
            var employee = _repo.GetAllIncluding(x => x.User).FirstOrDefault(x => x.Id == id);

            var employeeDto = new EmployeeDTO
            {
                Id = employee.Id,
                DateOfEmployment = employee.DateOfEmployment,
                FirstName = employee.User.FirstName,
                LastName = employee.User.LastName,
                MiddleName = employee.User.MiddleName,
                Gender = employee.User.Gender,
                Email = employee.User.Email,
                PhoneNumber = employee.User.PhoneNumber,
                Address = employee.User.Address,
                EmployeePhoto = employee.User.Photo,
                NextOfKin = employee.User.NextOfKinName,
                NextOfKinPhone = employee.User.NextOfKinPhone,
                DepartmentId = employee.DepartmentId,
                TerminalId = employee.TerminalId,
                EmployeeCode = employee.EmployeeCode,
                LockoutEnabled = employee.User.LockoutEnabled,
                UserId = employee.UserId.ToString(),
                IsActive = employee.IsActive,
            };

            var userRole = _userSvc.GetUserRoles(employee.User).Result.FirstOrDefault();

            if (!string.IsNullOrEmpty(userRole))
            {
                var role = await _roleSvc.FindByName(userRole);
                employeeDto.RoleId = role.Id;
                employeeDto.RoleName = role.Name;
            }

            return employeeDto;
        }


        public async Task<int?> GetAssignedTerminal(string email)
        {
            var employee = await GetEmployeesByemailAsync(email);
            return employee?.TerminalId;
        }

        public async Task UpdateEmployee(int id, EmployeeDTO model)
        {
            var employee = _repo.Get(id);

            var user = await _userSvc.FindFirstAsync(x => x.Id == employee.UserId);

            if (user is null)
            {
                throw new EntityNotFoundException(typeof(User), id);
            }

            employee.EmployeeCode = model.EmployeeCode;
            employee.DepartmentId = model.DepartmentId;
            employee.TerminalId = model.TerminalId;

            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.MiddleName = model.MiddleName;
            user.Gender = model.Gender;
            user.Email = model.Email;
            user.PhoneNumber = model.PhoneNumber;
            user.Address = model.Address;
            user.NextOfKinName = model.NextOfKin;
            user.NextOfKinPhone = model.NextOfKinPhone;


            if (model.RoleId != 0)
            {
                var newRole = await _roleSvc.FindByIdAsync(model.RoleId);
                if (newRole != null && !await _userSvc.IsInRoleAsync(user, newRole.Name))
                {
                    await _userSvc.RemoveFromRolesAsync(user, await _userSvc.GetUserRoles(user));
                    await _userSvc.AddToRoleAsync(user, newRole.Name);
                }
            }

            await _userSvc.UpdateAsync(user);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<bool> DeactivateOrActiveAccount(int Id)
        {
            var getuser = _repo.Get(Id);

            var result = getuser.UserId;

            var user = await _userManager.FindByIdAsync(result.ToString());
            if (user.LockoutEnabled == false)
            {
                user.LockoutEnabled = true;
            }
            else if (user.LockoutEnabled == true)
            {
                user.LockoutEnabled = false;
            };
            await _userManager.UpdateAsync(user);
            return true;
        }

        //public Task<List<AllUserDTO>> GetAllUsers()
        //{
        //    var tenant = _serviceHelper.GetTenantId();
            
        //    var allusers = 
        //            (from allusers in _repo.GetAllIncluding(x => x.User)
                     
        //             select new AllUserDTO
        //             {
        //                 Id = allusers.Id,
        //                 DateOfEmployment = allusers.DateOfEmployment,
        //                 FirstName = allusers.User.FirstName,
        //                 LastName = allusers.User.LastName,
        //                 MiddleName = allusers.User.MiddleName,
        //                 Gender = allusers.User.Gender,
        //                 Email = allusers.User.Email,
        //                 PhoneNumber = allusers.User.PhoneNumber,
        //                 Address = allusers.User.Address,
        //                 EmployeePhoto = allusers.User.Photo,
        //                 NextOfKin = allusers.User.NextOfKinName,
        //                 NextOfKinPhone = allusers.User.NextOfKinPhone,
                
             
        //                 Code = allusers.EmployeeCode,
        //             //RoleName = string.Join(",", userRole),
        //             LockoutEnabled = allusers.User.LockoutEnabled,
        //                 UserId = allusers.UserId.ToString()
        //             }).ToList();


        //    return Task.FromResult(allusers.ToList());
        //}


    }
}
