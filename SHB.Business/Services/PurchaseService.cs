﻿using IPagedList;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Business.Services
{
    public interface IPurchaseService
    {
        #region PurchaseHeader
        Task AddPurchaseHeader(AccPurchase purchaseHeader);
        Task<AccPurchase> GetPurchaseHeaderById(Guid purchaseHeaderID);

        Task<bool> UpdatePurchaseHeader(Guid purchaseHeaderId, PurchaseHeaderDTO purchaseHeaderDto);
        Task RemovePurchaseHeader(Guid purchaseHeaderId);
        Task<IPagedList<AccPurchase>> GetAllPurchaseHeader(int pageNumber, int pageSize, string searchTerm);

        #endregion

        #region PurchaseDetail
        Task AddPurchaseDetail(PurchaseDetailDTO model);
        Task<PurchaseDetailDTO> GetPurchaseDetail(int Id);
        Task<IPagedList<PurchaseDetailDTO>> GetAllPurchaseDetail(int pageNumber, int pageSize, string searchTerm);
        Task<IPagedList<AccPurchaseDetail>> GetAllPurchaseDetailByUser(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdatePurchaseDetail(PurchaseDetailDTO model, int Id);
        Task DeletePurchaseDetail(int Id);

        #endregion

    }
    public class PurchaseService : IPurchaseService
    {
        private readonly IRepository<AccPurchase, Guid> _repositoryPurchaseHeader;
        private readonly IRepository<AccPurchaseDetail> _repositoryPurchaseDetail;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUserService _userSvc;
        public PurchaseService(IRepository<AccPurchase, Guid> repositoryPurchaseHeader, IRepository<AccPurchaseDetail> repositoryPurchaseDetail, IUnitOfWork unitOfWork,
            IServiceHelper serviceHelper, IUserService userSvc)
        {
            _repositoryPurchaseHeader = repositoryPurchaseHeader;
            _repositoryPurchaseDetail = repositoryPurchaseDetail;
            _unitOfWork = unitOfWork;
            _serviceHelper = serviceHelper;
            _userSvc = userSvc;
        }

        #region PurchaseHeader
        public async Task AddPurchaseHeader(AccPurchase purchaseHeader)
        {
            if (await _repositoryPurchaseHeader.ExistAsync(v => v.TenantId == purchaseHeader.TenantId))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.PurchaseHeader_NOT_EXIST);
            }

            _repositoryPurchaseHeader.Insert(new AccPurchase
            {
                Id = purchaseHeader.Id,
                TenantId = purchaseHeader.TenantId,
                DocNumber = purchaseHeader.DocNumber,
                DocType = purchaseHeader.DocType,
                PaymentDate = purchaseHeader.PaymentDate,
                ContractNumber = purchaseHeader.ContractNumber,
                VendorInvoiceNumber = purchaseHeader.VendorInvoiceNumber,
                VendorID = purchaseHeader.VendorID,
                Reference = purchaseHeader.Reference,
                HDiscountAmount = purchaseHeader.HDiscountAmount,
                TaxableSubTotal = purchaseHeader.TaxableSubTotal,
                TotalAddCost = purchaseHeader.TotalAddCost,
                TotalSubAmount = purchaseHeader.TotalSubAmount,
                TotalWeight = purchaseHeader.TotalWeight,
                TrackingNumber = purchaseHeader.TrackingNumber,
                VehicleId = purchaseHeader.VehicleId,
                BankID = purchaseHeader.BankID,
                LocationId = purchaseHeader.LocationId,
                UserId = purchaseHeader.UserId,
                WarehouseID = purchaseHeader.WarehouseID,
                Captured = purchaseHeader.Captured,
                PurchaseDate = purchaseHeader.PurchaseDate,
                Verified = purchaseHeader.Verified,
                VerifiedBy = purchaseHeader.VerifiedBy,
                VerifiedDate = purchaseHeader.VerifiedDate,
                Approved = purchaseHeader.Approved,
                ApprovedBy = purchaseHeader.ApprovedBy,
                ApprovedDate = purchaseHeader.ApprovedDate,
                Posted = purchaseHeader.Posted,
                PostedBy = purchaseHeader.PostedBy,
                PostedDate = purchaseHeader.PostedDate,
                IsReverse = purchaseHeader.IsReverse,
                Void = purchaseHeader.Void,
            });

            await _unitOfWork.SaveChangesAsync();
        }



        public async Task<AccPurchase> GetPurchaseHeaderById(Guid purchaseHeaderID)
        {
            var purchaseHeader = _repositoryPurchaseHeader.FirstOrDefault(x => x.Id == purchaseHeaderID);

            if (purchaseHeader == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.PurchaseHeader_EXIST);
            }

            return new AccPurchase
            {
                Id = purchaseHeader.Id,
                TenantId = purchaseHeader.TenantId,
                DocNumber = purchaseHeader.DocNumber,
                DocType = purchaseHeader.DocType,
                PaymentDate = purchaseHeader.PaymentDate,
                ContractNumber = purchaseHeader.ContractNumber,
                VendorInvoiceNumber = purchaseHeader.VendorInvoiceNumber,
                VendorID = purchaseHeader.VendorID,
                Reference = purchaseHeader.Reference,
                HDiscountAmount = purchaseHeader.HDiscountAmount,
                TaxableSubTotal = purchaseHeader.TaxableSubTotal,
                TotalAddCost = purchaseHeader.TotalAddCost,
                TotalSubAmount = purchaseHeader.TotalSubAmount,
                TotalWeight = purchaseHeader.TotalWeight,
                TrackingNumber = purchaseHeader.TrackingNumber,
                VehicleId = purchaseHeader.VehicleId,
                BankID = purchaseHeader.BankID,
                LocationId = purchaseHeader.LocationId,
                UserId = purchaseHeader.UserId,
                WarehouseID = purchaseHeader.WarehouseID,
                Captured = purchaseHeader.Captured,
                PurchaseDate = purchaseHeader.PurchaseDate,
                Verified = purchaseHeader.Verified,
                VerifiedBy = purchaseHeader.VerifiedBy,
                VerifiedDate = purchaseHeader.VerifiedDate,
                Approved = purchaseHeader.Approved,
                ApprovedBy = purchaseHeader.ApprovedBy,
                ApprovedDate = purchaseHeader.ApprovedDate,
                Posted = purchaseHeader.Posted,
                PostedBy = purchaseHeader.PostedBy,
                PostedDate = purchaseHeader.PostedDate,
                IsReverse = purchaseHeader.IsReverse,
                Void = purchaseHeader.Void,

            };
        }

        public Task<IPagedList<AccPurchase>> GetAllPurchaseHeader(int pageNumber, int pageSize, string searchTerm)
        {

            var tenant = _serviceHelper.GetTenantId();
            var purchaseHeaders =
                from purchaseHeader in _repositoryPurchaseHeader.GetAll()
                orderby purchaseHeader.PurchaseDate descending
                select new AccPurchase
                {
                    Id = purchaseHeader.Id,
                    TenantId = purchaseHeader.TenantId,
                    DocNumber = purchaseHeader.DocNumber,
                    DocType = purchaseHeader.DocType,
                    PaymentDate = purchaseHeader.PaymentDate,
                    ContractNumber = purchaseHeader.ContractNumber,
                    VendorInvoiceNumber = purchaseHeader.VendorInvoiceNumber,
                    VendorID = purchaseHeader.VendorID,
                    Reference = purchaseHeader.Reference,
                    HDiscountAmount = purchaseHeader.HDiscountAmount,
                    TaxableSubTotal = purchaseHeader.TaxableSubTotal,
                    TotalAddCost = purchaseHeader.TotalAddCost,
                    TotalSubAmount = purchaseHeader.TotalSubAmount,
                    TotalWeight = purchaseHeader.TotalWeight,
                    TrackingNumber = purchaseHeader.TrackingNumber,
                    VehicleId = purchaseHeader.VehicleId,
                    BankID = purchaseHeader.BankID,
                    LocationId = purchaseHeader.LocationId,
                    UserId = purchaseHeader.UserId,
                    WarehouseID = purchaseHeader.WarehouseID,
                    Captured = purchaseHeader.Captured,
                    PurchaseDate = purchaseHeader.PurchaseDate,
                    Verified = purchaseHeader.Verified,
                    VerifiedBy = purchaseHeader.VerifiedBy,
                    VerifiedDate = purchaseHeader.VerifiedDate,
                    Approved = purchaseHeader.Approved,
                    ApprovedBy = purchaseHeader.ApprovedBy,
                    ApprovedDate = purchaseHeader.ApprovedDate,
                    Posted = purchaseHeader.Posted,
                    PostedBy = purchaseHeader.PostedBy,
                    PostedDate = purchaseHeader.PostedDate,
                    IsReverse = purchaseHeader.IsReverse,
                    Void = purchaseHeader.Void,

                };

            return purchaseHeaders.AsNoTracking().ToPagedListAsync(pageNumber, pageSize);
        }

        public async Task RemovePurchaseHeader(Guid purchaseHeaderId)
        {
            var purchaseHeader = await _repositoryPurchaseHeader.GetAsync(purchaseHeaderId);

            if (purchaseHeader == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.PurchaseHeader_NOT_EXIST);
            }

            _repositoryPurchaseHeader.Delete(purchaseHeader);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<bool> UpdatePurchaseHeader(Guid purchaseHeaderId, PurchaseHeaderDTO purchaseHeaderDto)
        {
            var purchaseHeader = await _repositoryPurchaseHeader.GetAsync(purchaseHeaderId);

            if (purchaseHeader is null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.PurchaseHeader_NOT_EXIST);
            }

            purchaseHeader.TenantId = purchaseHeaderDto.TenantId;
            purchaseHeader.DocNumber = purchaseHeaderDto.DocNumber;
            purchaseHeader.DocType = purchaseHeaderDto.DocType;
            purchaseHeader.PaymentDate = purchaseHeaderDto.PaymentDate;
            purchaseHeader.ContractNumber = purchaseHeaderDto.ContractNumber;
            purchaseHeader.VendorInvoiceNumber = purchaseHeaderDto.VendorInvoiceNumber;
            purchaseHeader.VendorID = purchaseHeaderDto.VendorID;
            purchaseHeader.Reference = purchaseHeaderDto.Reference;
            purchaseHeader.HDiscountAmount = purchaseHeaderDto.HDiscountAmount;
            purchaseHeader.TaxableSubTotal = purchaseHeaderDto.TaxableSubTotal;
            purchaseHeader.TotalAddCost = purchaseHeaderDto.TotalAddCost;
            purchaseHeader.TotalSubAmount = purchaseHeaderDto.TotalSubAmount;
            purchaseHeader.TotalWeight = purchaseHeaderDto.TotalWeight;
            purchaseHeader.TrackingNumber = purchaseHeaderDto.TrackingNumber;
            purchaseHeader.VehicleId = purchaseHeaderDto.VehicleId;
            purchaseHeader.BankID = purchaseHeaderDto.BankID;
            purchaseHeader.LocationId = purchaseHeaderDto.LocationId;
            purchaseHeader.UserId = purchaseHeaderDto.UserId;
            purchaseHeader.WarehouseID = purchaseHeaderDto.WarehouseID;
            purchaseHeader.Captured = purchaseHeaderDto.Captured;
            purchaseHeader.PurchaseDate = purchaseHeaderDto.PurchaseDate;
            purchaseHeader.Verified = purchaseHeaderDto.Verified;
            purchaseHeader.VerifiedBy = purchaseHeaderDto.VerifiedBy;
            purchaseHeader.VerifiedDate = purchaseHeaderDto.VerifiedDate;
            purchaseHeader.Approved = purchaseHeaderDto.Approved;
            purchaseHeader.ApprovedBy = purchaseHeaderDto.ApprovedBy;
            purchaseHeader.ApprovedDate = purchaseHeaderDto.ApprovedDate;
            purchaseHeader.Posted = purchaseHeaderDto.Posted;
            purchaseHeader.PostedBy = purchaseHeaderDto.PostedBy;
            purchaseHeader.PostedDate = purchaseHeaderDto.PostedDate;
            purchaseHeader.IsReverse = purchaseHeaderDto.IsReverse;
            purchaseHeader.Void = purchaseHeaderDto.Void;


            await _unitOfWork.SaveChangesAsync();
            return true;
        }


        #endregion

        #region PurchaseDetail

        public async Task AddPurchaseDetail(PurchaseDetailDTO model)
        {

            _repositoryPurchaseDetail.Insert(new AccPurchaseDetail
            {
                PurchaseHeaderId = model.PurchaseHeaderId,
                ItemID = model.ItemID,
                ItemUPCCode = model.ItemUPCCode,
                WarehouseID = model.WarehouseID,
                OrderQty = model.OrderQty,
                ItemUnitPrice = model.ItemUnitPrice,
                ItemUnitCost = model.ItemUnitCost,
                WarehouseBinID = model.WarehouseBinID,
                Description = model.Description,
                SerialNumber = model.SerialNumber,
                ItemUOM = model.ItemUOM,
                ItemWeight = model.ItemWeight,
                CurrencyID = model.CurrencyID,
                CurrencyExcRate = model.CurrencyExcRate,
                TaxGroupID = model.TaxGroupID,
                TaxAmount = model.TaxAmount,
                TaxInclusive = model.TaxInclusive,
                GLSalesAccount = model.GLSalesAccount,
                GLCOGAccount = model.GLCOGAccount,
                DiscountAmount = model.DiscountAmount,
                TenantId = model.TenantId,
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<PurchaseDetailDTO> GetPurchaseDetail(int Id)
        {
            var model = _repositoryPurchaseDetail.FirstOrDefault(x => x.Id == Id);
            return new PurchaseDetailDTO
            {
                Id = model.Id,
                PurchaseHeaderId = model.PurchaseHeaderId,
                ItemID = model.ItemID,
                ItemUPCCode = model.ItemUPCCode,
                WarehouseID = model.WarehouseID,
                OrderQty = model.OrderQty,
                ItemUnitPrice = model.ItemUnitPrice,
                ItemUnitCost = model.ItemUnitCost,
                WarehouseBinID = model.WarehouseBinID,
                Description = model.Description,
                SerialNumber = model.SerialNumber,
                ItemUOM = model.ItemUOM,
                ItemWeight = model.ItemWeight,
                CurrencyID = model.CurrencyID,
                CurrencyExcRate = model.CurrencyExcRate,
                TaxGroupID = model.TaxGroupID,
                TaxAmount = model.TaxAmount,
                TaxInclusive = model.TaxInclusive,
                GLSalesAccount = model.GLSalesAccount,
                GLCOGAccount = model.GLCOGAccount,
                DiscountAmount = model.DiscountAmount,
                TenantId = model.TenantId,
            };
        }

        public async Task<IPagedList<PurchaseDetailDTO>> GetAllPurchaseDetail(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var PurchaseDetail =
                    from purchaseDetail in _repositoryPurchaseDetail.GetAll()

                        //join purchaseHeader in _repositoryPurchaseHeader.GetAll() on purchaseDetail.PurchaseId equals purchaseHeader.Id
                        //into purchaseHeaders
                        //from purchaseHeader in purchaseHeaders.DefaultIfEmpty()

                    where string.IsNullOrWhiteSpace(searchTerm) ||
                    purchaseDetail.Description.Contains(searchTerm)// ||
                    //Warehouse.WarehouseCode.Contains(searchTerm)
                    orderby purchaseDetail.CreationTime descending

                    select new PurchaseDetailDTO
                    {
                        Id = purchaseDetail.Id,
                        PurchaseHeaderId = purchaseDetail.PurchaseHeaderId,
                        ItemID = purchaseDetail.ItemID,
                        ItemUPCCode = purchaseDetail.ItemUPCCode,
                        WarehouseID = purchaseDetail.WarehouseID,
                        OrderQty = purchaseDetail.OrderQty,
                        ItemUnitPrice = purchaseDetail.ItemUnitPrice,
                        ItemUnitCost = purchaseDetail.ItemUnitCost,
                        WarehouseBinID = purchaseDetail.WarehouseBinID,
                        Description = purchaseDetail.Description,
                        SerialNumber = purchaseDetail.SerialNumber,
                        ItemUOM = purchaseDetail.ItemUOM,
                        ItemWeight = purchaseDetail.ItemWeight,
                        CurrencyID = purchaseDetail.CurrencyID,
                        CurrencyExcRate = purchaseDetail.CurrencyExcRate,
                        TaxGroupID = purchaseDetail.TaxGroupID,
                        TaxAmount = purchaseDetail.TaxAmount,
                        TaxInclusive = purchaseDetail.TaxInclusive,
                        GLSalesAccount = purchaseDetail.GLSalesAccount,
                        GLCOGAccount = purchaseDetail.GLCOGAccount,
                        DiscountAmount = purchaseDetail.DiscountAmount,
                        TenantId = purchaseDetail.TenantId,

                    };

                return await PurchaseDetail.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<IPagedList<AccPurchaseDetail>> GetAllPurchaseDetailByUser(int pageNumber, int pageSize, string searchTerm)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            try
            {
                var InventoryReceivedHeader =
                    from model in _repositoryPurchaseDetail.GetAll()
                    where model.CreatorUserId == currentUser.Id && string.IsNullOrWhiteSpace(searchTerm)
                    orderby model.CreationTime descending

                    select new AccPurchaseDetail
                    {
                        Id = model.Id,
                        PurchaseHeaderId = model.PurchaseHeaderId,
                        ItemID = model.ItemID,
                        ItemUPCCode = model.ItemUPCCode,
                        WarehouseID = model.WarehouseID,
                        OrderQty = model.OrderQty,
                        ItemUnitPrice = model.ItemUnitPrice,
                        ItemUnitCost = model.ItemUnitCost,
                        WarehouseBinID = model.WarehouseBinID,
                        Description = model.Description,
                        SerialNumber = model.SerialNumber,
                        ItemUOM = model.ItemUOM,
                        ItemWeight = model.ItemWeight,
                        CurrencyID = model.CurrencyID,
                        CurrencyExcRate = model.CurrencyExcRate,
                        TaxGroupID = model.TaxGroupID,
                        TaxAmount = model.TaxAmount,
                        TaxInclusive = model.TaxInclusive,
                        GLSalesAccount = model.GLSalesAccount,
                        GLCOGAccount = model.GLCOGAccount,
                        DiscountAmount = model.DiscountAmount,
                        TenantId = model.TenantId,
                    };

                return await InventoryReceivedHeader.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<bool> UpdatePurchaseDetail(PurchaseDetailDTO model, int Id)
        {
            
            var DetailData = _repositoryPurchaseDetail.GetAll().Where(x => x.Id == Id).FirstOrDefault();
            DetailData.ItemID = model.ItemID;
            DetailData.ItemUPCCode = model.ItemUPCCode;
            DetailData.WarehouseID = model.WarehouseID;
            DetailData.OrderQty = model.OrderQty;
            DetailData.ItemUnitPrice = model.ItemUnitPrice;
            DetailData.ItemUnitCost = model.ItemUnitCost;
            DetailData.WarehouseBinID = model.WarehouseBinID;
            DetailData.Description = model.Description;
            DetailData.SerialNumber = model.SerialNumber;
            DetailData.ItemUOM = model.ItemUOM;
            DetailData.ItemWeight = model.ItemWeight;
            DetailData.CurrencyID = model.CurrencyID;
            DetailData.CurrencyExcRate = model.CurrencyExcRate;
            DetailData.TaxGroupID = model.TaxGroupID;
            DetailData.TaxAmount = model.TaxAmount;
            DetailData.TaxInclusive = model.TaxInclusive;
            DetailData.GLSalesAccount = model.GLSalesAccount;
            DetailData.GLCOGAccount = model.GLCOGAccount;
            DetailData.DiscountAmount = model.DiscountAmount;


            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        public async Task DeletePurchaseDetail(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _repositoryPurchaseDetail.DeleteAsync(_repositoryPurchaseDetail.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }

        #endregion
    }
}