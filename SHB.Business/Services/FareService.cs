﻿using IPagedList;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SHB.Core.Domain.Entities;
using System;

namespace SHB.Business.Services
{
    public interface IFareService
    {
        Task<IPagedList<FareDTO>> GetFares(int page, int size, string query = null);
        Task<FareDTO> GetFareById(int fareId);
        Task AddFare(FareDTO fare);
        Task UpdateFare(int fareId, FareDTO fare);
        Task RemoveFare(int FareId);
    }

    public class FareService : IFareService
    {
        private readonly IRepository<Fare> _fareRepo;
        private readonly IRepository<Route> _routeRepo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceHelper _serviceHelper;

        public FareService(
            IRepository<Fare> fareRepo,
            IRepository<Route> routeRepo,
            IUnitOfWork unitOfWork,
            IServiceHelper serviceHelper)
        {
            _fareRepo = fareRepo;
            _routeRepo = routeRepo;
            _unitOfWork = unitOfWork;
            _serviceHelper = serviceHelper;
        }

        private async Task<bool> IsValidRoute(int routeId)
        {
            return routeId > 0 &&
                 await _routeRepo.ExistAsync(m => m.Id == routeId);
        }

        public async Task AddFare(FareDTO fareDto)
        {
            if (!await IsValidRoute(fareDto.RouteId))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ROUTE_NOT_EXIST);
            }


            //fareDto.Route.Name = fareDto.Route.Name.Trim();

            if (await _fareRepo.ExistAsync(v => v.Id == fareDto.Id))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.FARE_EXIST);
            }

            _fareRepo.Insert(new Fare
            {
                Id = fareDto.Id,
                Name = fareDto.Name,
                Amount = fareDto.Amount,
                ChildrenDiscountPercentage = fareDto.ChildrenDiscountPercentage,
                NonIdAmount = fareDto.NonIdAmount,
                RouteId = fareDto.RouteId,
                VehicleModelId = fareDto.VehicleModelId,

            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<FareDTO> GetFareById(int fareId)
        {
            var fare = await _fareRepo.GetAsync(fareId);

            if (fare == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.FARE_NOT_EXIST);
            }

            return new FareDTO
            {
                Id = fare.Id,
                Name = fare.Name,
                Amount = fare.Amount,
                ChildrenDiscountPercentage = fare.ChildrenDiscountPercentage,
                NonIdAmount = fare.NonIdAmount,
                RouteId = fare.RouteId,
                VehicleModelId = fare.VehicleModelId

            };
        }

        public Task<IPagedList<FareDTO>> GetFares(int page, int size, string query = null)
        {
            var fares =
                from fare in _fareRepo.GetAll()
                //join terminal in _terminalRepo.GetAll() on subRoute.DepartureTerminalId equals terminal.Id
                orderby fare.Id descending
               // where string.IsNullOrWhiteSpace(query) || fare.Name.Contains(query)
                select new FareDTO
                {
                    Id = fare.Id,
                    Name = fare.Name,
                    Amount = fare.Amount,
                    ChildrenDiscountPercentage = fare.ChildrenDiscountPercentage,
                    NonIdAmount = fare.NonIdAmount,
                    RouteId = fare.RouteId,
                    VehicleModelId = fare.VehicleModelId
                };

            return fares.AsNoTracking().ToPagedListAsync(page, size);
        }

        public async Task RemoveFare(int fareId)
        {
            var fare = await _fareRepo.GetAsync(fareId);

            if (fare == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.FARE_NOT_EXIST);
            }

            _fareRepo.Delete(fare);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateFare(int fareId, FareDTO fare)
        {
            var fares = await _fareRepo.GetAsync(fareId);

            if (fares == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.FARE_NOT_EXIST);
            }

            await _unitOfWork.SaveChangesAsync();
        }
    }
}
