﻿using IPagedList;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Business.Services
{
    public interface IReceiptService
    {
        #region ReceiptHeader

        Task AddReceiptHeader(ReceiptHeaderDTO receipt);
        Task<ReceiptHeaderDTO> GetReceiptHeaderById(Guid id);
        Task<IPagedList<ReceiptDetailDTO>> GetReceiptDetailByUserId(int UserId, int page, int size, string query = null);

        IQueryable<ReceiptHeader> GetAllReceiptHeader();
        Task UpdateReceiptHeader(Guid id, ReceiptHeaderDTO receipt);
        Task RemoveReceiptHeader(Guid id);
        Task<IPagedList<ReceiptHeaderDTO>> GetReceiptHeader(int page, int size, string searchTerm);

        #endregion

        #region ReceiptDetail

        Task<ReceiptDetailDTO> GetReceiptDetailById(int reciptId);
        IQueryable<ReceiptDetail> GetAllReceiptDetail();
        Task UpdateReceiptDetail(int receiptId, ReceiptDetailDTO receiptDto);
        Task<IPagedList<ReceiptDetailDTO>> GetReceiptDetail(int page, int size, string query = null);
        Task AddReceiptDetail(ReceiptDetailDTO reciptDtail);

        #endregion
    }
    public class ReceiptService : IReceiptService
    {
        private readonly IRepository<ReceiptHeader, Guid> _repositoryHeaderRepo;
        private readonly IRepository<ReceiptDetail> _repositoryDetail;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceHelper _serviceHelper;
        IRepository<CustomerInformation> _customerInfoRepo;
       /// private readonly IRepository<ReceiptDetail> _receiptserviceRepo;
        private readonly IHostingEnvironment _appEnvironment;
        private readonly IUserService _userSvc;

        public ReceiptService(IRepository<ReceiptHeader, Guid> repositoryHeaderRepo, IRepository<ReceiptDetail> repositoryDetail, IUnitOfWork unitOfWork,
            IServiceHelper serviceHelper, IRepository<CustomerInformation> customerInfoRepo, IUserService userSvc,
            IHostingEnvironment hostingEnvironment)
        {
            _repositoryHeaderRepo = repositoryHeaderRepo;
            _repositoryDetail = repositoryDetail;
            _unitOfWork = unitOfWork;
            _userSvc = userSvc;
            _serviceHelper = serviceHelper;
            _customerInfoRepo = customerInfoRepo;
           // _receiptserviceRepo = receiptserviceRepo;
            _appEnvironment = hostingEnvironment;
        }



        #region ReceiptHeader
        public async Task AddReceiptHeader(ReceiptHeaderDTO receipt)
        {
            if (await _repositoryHeaderRepo.ExistAsync(r => r.Id == receipt.Id))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.RECEIPT_NOT_EXIST);
            }

            _repositoryHeaderRepo.Insert(new ReceiptHeader
            {
                TenantId = receipt.TenantId,
                ReceiptNumber = receipt.ReceiptNumber,
                ReceiptType = receipt.ReceiptType,
                DueDate = receipt.DueDate,
                Reference = receipt.Reference,
                ChequeNumber = receipt.ChequeNumber,
                CustomerID = receipt.CustomerID,
                LodgementDate = receipt.LodgementDate,
                BankID = receipt.BankID,
                TotalAmount = receipt.TotalAmount,
                LocationId = receipt.LocationId,
                UserId = receipt.UserId,
                Captured = receipt.Captured,
                TransactionDate = receipt.TransactionDate,
                Verified = receipt.Verified,
                VerifiedBy = receipt.VerifiedBy,
                VerifiedDate = receipt.VerifiedDate,
                Approved = receipt.Approved,
                ApprovedBy = receipt.ApprovedBy,
                ApprovedDate = receipt.ApprovedDate,
                Posted = receipt.Posted,
                PostedBy = receipt.PostedBy,
                PostedDate = receipt.PostedDate,
                IsReverse = receipt.IsReverse,
                Void = receipt.Void

            });

            await _unitOfWork.SaveChangesAsync();
        }


        public async Task<string> GetRefCode(int clientId)
        {
            bool isUnique = false;
            string otp = "";
            while (isUnique == false)
            {
                var CusDetail = _customerInfoRepo.GetAllIncluding(x => x.User).FirstOrDefault(x => x.UserId == clientId);

                string ALIAS = CusDetail.User.FirstName?.Substring(0, 3).ToUpper();
                otp = ALIAS + Guid.NewGuid().ToString().Remove(8).ToUpper();

                var exists = await _repositoryHeaderRepo.ExistAsync(a => a.Reference == otp);


                isUnique = !exists;
            }
            return otp;
        }

        public IQueryable<ReceiptHeader> GetAllReceiptHeader()
        {
            return _repositoryHeaderRepo.GetAll();
        }

        public Task<IPagedList<ReceiptDetailDTO>> GetReceiptDetailByUserId(int userId, int page, int size, string query = null)
        {
            var tenant = _serviceHelper.GetTenantId();

            var receipts =
               from receiptdetail in _repositoryDetail.GetAll().Where(x => x.TenantId == tenant && x.CreatorUserId == userId)
               join receiptHd in _repositoryHeaderRepo.GetAll() on receiptdetail.ReceiptId equals receiptHd.Id
            into receiptservices  from receiptservice in receiptservices.DefaultIfEmpty()

               orderby receiptdetail.CreationTime descending
               select new ReceiptDetailDTO
               {
                   Id = receiptdetail.Id,
                   TenantId = receiptdetail.TenantId,
                   AccountID = receiptdetail.AccountID,
                   Narratives = receiptdetail.Narratives,
                   DocumentNo = receiptdetail.DocumentNo,
                   Amount = receiptdetail.Amount,
                   CurrencyID = receiptdetail.CurrencyID,
                   AssetId = receiptdetail.AssetId,
                   ProjectId = receiptdetail.ProjectId,

               };
            return receipts.AsNoTracking().ToPagedListAsync(page, size);
        }

        public async Task<IPagedList<ReceiptHeaderDTO>> GetReceiptHeader(int page, int size, string searchTerm)
        {

            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            try
            {
                var receiptHeader =
                    from model in _repositoryHeaderRepo.GetAll()
                    where model.CreatorUserId == currentUser.Id && string.IsNullOrWhiteSpace(searchTerm) ||
                    model.ReceiptNumber.Contains(searchTerm)
                    orderby model.CreationTime descending

                    select new ReceiptHeaderDTO
                    {
                        Id = model.Id,
                        TenantId = model.TenantId,
                        ReceiptNumber = model.ReceiptNumber,
                        ReceiptType = model.ReceiptType,
                        DueDate = model.DueDate,
                        Reference = model.Reference,
                        ChequeNumber = model.ChequeNumber,
                        CustomerID = model.CustomerID,
                        LodgementDate = model.LodgementDate,
                        BankID = model.BankID,
                        TotalAmount = model.TotalAmount,
                        LocationId = model.LocationId,
                        UserId = model.UserId,
                        Captured = model.Captured,
                        TransactionDate = model.TransactionDate,
                        Verified = model.Verified,
                        VerifiedBy = model.VerifiedBy,
                        VerifiedDate = model.VerifiedDate,
                        Approved = model.Approved,
                        ApprovedBy = model.ApprovedBy,
                        ApprovedDate = model.ApprovedDate,
                        Posted = model.Posted,
                        PostedBy = model.PostedBy,
                        PostedDate = model.PostedDate,
                        IsReverse = model.IsReverse,
                        Void = model.Void

                    };

                return await receiptHeader.AsTracking().ToPagedListAsync(page, size);
            }
            catch (Exception ex)
            {
                return null;
            }

        } 

        public async Task<ReceiptHeaderDTO> GetReceiptHeaderById(Guid receiptId)
        {
            var receipt = await _repositoryHeaderRepo.GetAsync(receiptId);

            if(receipt == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.RECEIPT_EXIST);
            }

            return new ReceiptHeaderDTO
            {
                Id = receipt.Id,
                TenantId = receipt.TenantId,
                ReceiptNumber = receipt.ReceiptNumber,
                ReceiptType = receipt.ReceiptType,
                DueDate = receipt.DueDate,
                Reference = receipt.Reference,
                ChequeNumber = receipt.ChequeNumber,
                CustomerID = receipt.CustomerID,
                LodgementDate = receipt.LodgementDate,
                BankID = receipt.BankID,
                TotalAmount = receipt.TotalAmount,
                LocationId = receipt.LocationId,
                UserId = receipt.UserId,
                Captured = receipt.Captured,
                TransactionDate = receipt.TransactionDate,
                Verified = receipt.Verified,
                VerifiedBy = receipt.VerifiedBy,
                VerifiedDate = receipt.VerifiedDate,
                Approved = receipt.Approved,
                ApprovedBy = receipt.ApprovedBy,
                ApprovedDate = receipt.ApprovedDate,
                Posted = receipt.Posted,
                PostedBy = receipt.PostedBy,
                PostedDate = receipt.PostedDate,
                IsReverse = receipt.IsReverse,
                Void = receipt.Void
            };
        }

        public async Task UpdateReceiptHeader(Guid id, ReceiptHeaderDTO receiptDto)
        {
            var receiptRep = await _repositoryHeaderRepo.GetAsync(id);

            if (receiptRep is null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.RECEIPT_NOT_EXIST);
            }

                    receiptRep.TenantId = receiptDto.TenantId;
                    receiptRep.ReceiptNumber = receiptDto.ReceiptNumber;
                    receiptRep.ReceiptType = receiptDto.ReceiptType;
                    receiptRep.DueDate = receiptDto.DueDate;
                    receiptRep.Reference = receiptDto.Reference;
                    receiptRep.ChequeNumber = receiptDto.ChequeNumber;
                    receiptRep.CustomerID = receiptDto.CustomerID;
                    receiptRep.LodgementDate = receiptDto.LodgementDate;
                    receiptRep.BankID = receiptDto.BankID;
                    receiptRep.TotalAmount = receiptDto.TotalAmount;
                    receiptRep.LocationId = receiptDto.LocationId;
                    receiptRep.UserId = receiptDto.UserId;
                    receiptRep.Captured = receiptDto.Captured;
                    receiptRep.TransactionDate = receiptDto.TransactionDate;
                    receiptRep.Verified = receiptDto.Verified;
                    receiptRep.VerifiedBy = receiptDto.VerifiedBy;
                    receiptRep.VerifiedDate = receiptDto.VerifiedDate;
                    receiptRep.Approved = receiptDto.Approved;
                    receiptRep.ApprovedBy = receiptDto.ApprovedBy;
                    receiptRep.ApprovedDate = receiptDto.ApprovedDate;
                    receiptRep.Posted = receiptDto.Posted;
                    receiptRep.PostedBy = receiptDto.PostedBy;
                    receiptRep.PostedDate = receiptDto.PostedDate;
                    receiptRep.IsReverse = receiptDto.IsReverse;
                    receiptRep.Void = receiptDto.Void;

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task RemoveReceiptHeader(Guid id)
        {
            var receipt = await _repositoryHeaderRepo.GetAsync(id);

            if (receipt == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.RECEIPT_NOT_EXIST);
            }

            _repositoryHeaderRepo.Delete(receipt);

            await _unitOfWork.SaveChangesAsync();
        }

        #endregion

        #region ReceiptDetail
        public async Task AddReceiptDetail(ReceiptDetailDTO receiptId)
        {
            if (await _repositoryDetail.ExistAsync(r => r.ReceiptId == receiptId.ReceiptHeaderId))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.RECEIPT_NOT_EXIST);
            }

            _repositoryDetail.Insert(new ReceiptDetail
            {
                ReceiptId = receiptId.ReceiptHeaderId,
                TransType = receiptId.TransType,
                AccountID = receiptId.AccountID,
                Narratives = receiptId.Narratives,
                DocumentNo = receiptId.DocumentNo,
                Amount = receiptId.Amount,
                CurrencyID = receiptId.CurrencyID,
                ExchangeRate = receiptId.ExchangeRate,
                ProjectId = receiptId.ProjectId,
                AssetId = receiptId.AssetId,
                TenantId = receiptId.TenantId

            });

            await _unitOfWork.SaveChangesAsync();
        }

        public IQueryable<ReceiptDetail> GetAllReceiptDetail()
        {
            return _repositoryDetail.GetAll();
        }

        public Task<IPagedList<ReceiptDetailDTO>> GetReceiptDetail(int page, int size, string searchTerm)
        {
            var tenant = _serviceHelper.GetTenantId();

            var receipts =
               from receipt in _repositoryDetail.GetAll()

               join receiptheader in _repositoryHeaderRepo.GetAll() on receipt.TenantId equals receiptheader.TenantId
               into receiptdetails
               from receiptdetail in receiptdetails.DefaultIfEmpty()

               
               

               where receiptdetail.TenantId == tenant

               && string.IsNullOrWhiteSpace(searchTerm) || receipt.Narratives.Contains(searchTerm) ||
               receipt.DocumentNo.Contains(searchTerm) || receipt.ReceiptHeader.ReceiptNumber.Contains(searchTerm)
               orderby receipt.Id descending

               select new ReceiptDetailDTO
               {
                   Id = receipt.Id,
                   ReceiptHeaderId = receipt.ReceiptId,              
                   TransType = receipt.TransType,
                   AccountID = receipt.AccountID,
                   Narratives = receipt.Narratives,
                   DocumentNo = receipt.DocumentNo,
                   Amount = receipt.Amount,
                   CurrencyID = receipt.CurrencyID,
                   ExchangeRate = receipt.ExchangeRate,
                   ProjectId = receipt.ProjectId,
                   AssetId = receipt.AssetId,
                   TenantId = receipt.TenantId
               };
            return receipts.AsNoTracking().ToPagedListAsync(page, size);
        }

        public async Task<ReceiptDetailDTO> GetReceiptDetailById(int receiptId)
        {
            var receipt = await _repositoryDetail.GetAsync(receiptId);

            if (receipt == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.RECEIPT_EXIST);
            }

            return new ReceiptDetailDTO
            {
                Id = receipt.Id,
                ReceiptHeaderId = receipt.ReceiptId,
                TransType = receipt.TransType,
                AccountID = receipt.AccountID,
                Narratives = receipt.Narratives,
                DocumentNo = receipt.DocumentNo,
                Amount = receipt.Amount,
                CurrencyID = receipt.CurrencyID,
                ExchangeRate = receipt.ExchangeRate,
                ProjectId = receipt.ProjectId,
                AssetId = receipt.AssetId,
                TenantId = receipt.TenantId
            };
        }

        public async Task UpdateReceiptDetail(int receiptId, ReceiptDetailDTO receiptDto)
        {
            var receipt = await _repositoryDetail.GetAsync(receiptId);

            if (receipt is null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.RECEIPT_NOT_EXIST);
            }
            receipt.TransType = receiptDto.TransType;
            receipt.AccountID = receiptDto.AccountID;
            receipt.Narratives = receiptDto.Narratives;
            receipt.DocumentNo = receiptDto.DocumentNo;
            receipt.Amount = receiptDto.Amount;
            receipt.CurrencyID = receiptDto.CurrencyID;
            receipt.ExchangeRate = receiptDto.ExchangeRate;
            receipt.ProjectId = receiptDto.ProjectId;
            receipt.AssetId = receiptDto.AssetId;


            await _unitOfWork.SaveChangesAsync();
        }

        #endregion

    }
}
