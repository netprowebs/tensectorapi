﻿using IPagedList;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SHB.Core.Domain.Entities;
using System;

namespace SHB.Business.Services
{
    public interface ISubRouteService
    {
        Task<IPagedList<SubRouteDTO>> GetSubRoutes(int page, int size, string query = null);
        Task<SubRouteDTO> GetSubRouteById(int subRouteId);
        Task AddSubRoute(SubRouteDTO subRoute);
        Task UpdateSubRoute(int subRouteId, SubRouteDTO subRoute);
        Task RemoveSubRoute(int subRouteId);
    }

    public class SubRouteService : ISubRouteService
    {
        private readonly IRepository<SubRoute> _subRouteRepo;
        private readonly IRepository<Route> _routeRepo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceHelper _serviceHelper;

        public SubRouteService(
            IRepository<SubRoute> subRouteRepo,
            IRepository<Route> routeRepo,
            IUnitOfWork unitOfWork,
            IServiceHelper serviceHelper)
        {
            _subRouteRepo = subRouteRepo;
            _routeRepo = routeRepo;
            _unitOfWork = unitOfWork;
            _serviceHelper = serviceHelper;
        }

        private async Task<bool> IsValidRoute(int routeId)
        {
            return routeId > 0 &&
                 await _routeRepo.ExistAsync(m => m.Id == routeId);
        }

        public async Task AddSubRoute(SubRouteDTO subRouteDto)
        {
            if (!await IsValidRoute(subRouteDto.RouteId))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ROUTE_NOT_EXIST);
            }


            //subRouteDto.Name = subRouteDto.Name.Trim();

            if (await _subRouteRepo.ExistAsync(v => v.Name == subRouteDto.Name))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.SUBROUTE_EXIST);
            }

            _subRouteRepo.Insert(new SubRoute
            {
                Id = subRouteDto.Id,
                Name = subRouteDto.Name,
                NameId = subRouteDto.NameId,
                RouteId = subRouteDto.RouteId,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,

            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<SubRouteDTO> GetSubRouteById(int subRouteId)
        {
            var subRoute = await _subRouteRepo.GetAsync(subRouteId);

            if (subRoute == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.SUBROUTE_NOT_EXIST);
            }

            return new SubRouteDTO
            {
                Id = subRoute.Id,
                Name = subRoute.Name,
                NameId = subRoute.NameId,
                RouteId = subRoute.RouteId,
                
            };
        }

        public Task<IPagedList<SubRouteDTO>> GetSubRoutes(int page, int size, string query = null)
        {
            var subRoutes =
                from subRoute in _subRouteRepo.GetAll()
                //join terminal in _terminalRepo.GetAll() on subRoute.DepartureTerminalId equals terminal.Id
                orderby subRoute.Id descending
                where string.IsNullOrWhiteSpace(query) || subRoute.Name.Contains(query)
                select new SubRouteDTO
                {
                    Id = subRoute.Id,
                    Name = subRoute.Name,
                    NameId = subRoute.NameId,
                    RouteId = subRoute.RouteId,
                };

            return subRoutes.AsNoTracking().ToPagedListAsync(page, size);
        }

        public async Task RemoveSubRoute(int subRouteId)
        {
            var subRoute = await _subRouteRepo.GetAsync(subRouteId);

            if (subRoute == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.SUBROUTE_NOT_EXIST);
            }

            _subRouteRepo.Delete(subRoute);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateSubRoute(int subRouteId, SubRouteDTO subRoute)
        {
            var subRoutes = await _subRouteRepo.GetAsync(subRouteId);

            if (subRoutes == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.SUBROUTE_NOT_EXIST);
            }

            subRoutes.Name = subRoute.Name.Trim();

            await _unitOfWork.SaveChangesAsync();
        }
    }
}
