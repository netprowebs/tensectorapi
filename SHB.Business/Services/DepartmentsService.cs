﻿using IPagedList;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Core.Utils;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using SHB.Core.Domain.Entities;
using System;

namespace SHB.Business.Services
{
    public interface IDepartmentsService
    {

        #region Departments
        Task AddDepartment(Department model);
        Task<Department> GetDepartment(int Id);
        Task<IPagedList<Department>> GetAllDepartments(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateDepartment(Department model, int Id);
        Task DeleteDepartment(int Id);
        #endregion



    }
    public class DepartmentsService : IDepartmentsService
    {


        private readonly IRepository<Department> _DepartmentsRepo;

        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;

        private readonly IUserService _userSvc;
        public DepartmentsService(

             IRepository<Department> DepartmentsRepo,
             IRepository<BrandType> brandTypeRepo,

            IServiceHelper serviceHelper,
            IUnitOfWork unitOfWork,

            IUserService userSvc
            )
        {

            _DepartmentsRepo = DepartmentsRepo;
            _serviceHelper = serviceHelper;
            _unitOfWork = unitOfWork;
            _userSvc = userSvc;
        }

        #region Departments
        public async Task AddDepartment(Department model)
        {
            model.Name = model.Name.Trim();
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);



            _DepartmentsRepo.Insert(new Department
            {
                Name = model.Name,
                Description = model.Description,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                CompanyId = (int)currentUser.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteDepartment(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _DepartmentsRepo.DeleteAsync(_DepartmentsRepo.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<IPagedList<Department>> GetAllDepartments(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var departments =
                    from model in _DepartmentsRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) ||
                    model.Name.Contains(searchTerm)// ||
                    //Warehouse.WarehouseCode.Contains(searchTerm)
                    orderby model.CreationTime descending

                    select new Department
                    {
                        Id = model.Id,
                        Name = model.Name,
                        Description = model.Description
                    };

                return await departments.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task<Department> GetDepartment(int Id)
        {
            var model = _DepartmentsRepo.FirstOrDefault(x => x.Id == Id);

            return new Department
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description
            };
        }


        public async Task<bool> UpdateDepartment(Department model, int Id)
        {
            var departments = _DepartmentsRepo.FirstOrDefault(x => x.Id == Id);


            departments.Name = model.Name;
            departments.Description = model.Description;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }
        #endregion

    }
}
