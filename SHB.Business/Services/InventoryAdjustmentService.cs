﻿using IPagedList;
using Microsoft.EntityFrameworkCore;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Business.Services
{
    public interface IInventoryAdjustmentService
    {

        #region Adjustment Header
        Task AddAdjustmentHeader(InventoryAdjustmentsHeaderDTO model);
        Task<InventoryAdjustmentsHeaderDTO> GetAdjustmentHeader(Guid Id);
        Task<IPagedList<InventoryAdjustmentsHeaderDTO>> GetAllAdjustmentHeader(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateAdjustmentHeader(InventoryAdjustmentsHeaderDTO model, Guid Id);
        Task DeleteAdjustmentHeader(Guid Id);
        Task<bool> ReceiveAdjustment(InventoryAdjustmentsHeaderDTO model, Guid Id);
        Task<bool> VerifyAdjustment(InventoryAdjustmentsHeaderDTO model, Guid Id);
        Task<bool> ApproveAdjustment(InventoryAdjustmentsHeaderDTO model, Guid Id);
        Task<bool> ReturnAdjustment(InventoryAdjustmentsHeaderDTO model, Guid Id);
        #endregion

        #region Adjustment Detail
        Task AddAdjustmentDetail(InventoryAdjustmentsDetailDTO model);
        Task<List<InventoryAdjustmentsDetailDTO>> GetAdjustmentDetail(Guid Id);
        Task<IPagedList<InventoryAdjustmentsDetailDTO>> GetAllAdjustmentDetail(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateAdjustmentDetail(InventoryAdjustmentsDetailDTO model, int Id);
        Task DeleteAdjustmentDetail(int Id);
        #endregion


    }
    public class InventoryAdjustmentService : IInventoryAdjustmentService
    {
        private readonly IRepository<InventoryAdjustmentsHeader, Guid> _InventoryAdjustmentsHeaderRepo;
        private readonly IRepository<InventoryAdjustmentsDetail> _InventoryAdjustmentsDetailRepo;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;

        private readonly IUserService _userSvc;
        public InventoryAdjustmentService(

             IRepository<InventoryAdjustmentsHeader, Guid> InventoryAdjustmentsHeaderRepo,
            IRepository<InventoryAdjustmentsDetail> InventoryAdjustmentsDetailRepo,
            IServiceHelper serviceHelper,
            IUnitOfWork unitOfWork,
            IUserService userSvc
            )
        {
            _InventoryAdjustmentsHeaderRepo = InventoryAdjustmentsHeaderRepo;
            _InventoryAdjustmentsDetailRepo = InventoryAdjustmentsDetailRepo;
            _serviceHelper = serviceHelper;
            _unitOfWork = unitOfWork;
            _userSvc = userSvc;
        }


        #region Inventory Recieved Header
        public async Task AddAdjustmentHeader(InventoryAdjustmentsHeaderDTO model)
        {
            //model.AdjustmentHeaderDescription = model.AdjustmentHeaderDescription.Trim();
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);


            _InventoryAdjustmentsHeaderRepo.Insert(new InventoryAdjustmentsHeader
            {
                Id = model.Id,
                Code = model.Code,
                AdjustmentDate = model.AdjustmentDate,
                Reason = model.Reason,
                Notes = model.Notes,
                AdjustmentPostToGL = model.AdjustmentPostToGL,
                SupervisorSignature = model.SupervisorSignature,
                Total = model.Total,
                IsCountAdjust = model.IsCountAdjust,
                CurrencyID = model.CurrencyID,
                CurrencyExchangeRate = model.CurrencyExchangeRate,
                Void = model.Void,
                Verified = model.Verified,
                VerifiedBy = model.VerifiedBy,
                VerifiedDate = model.VerifiedDate,
                Issued = model.Issued,
                IssuedBy = model.IssuedBy,
                IssuedDate = model.IssuedDate,
                Approved = model.Approved,
                ApprovedBy = model.ApprovedBy,
                ApprovedDate = model.ApprovedDate,
                Posted = model.Posted,
                PostedBy = model.PostedBy,
                PostedDate = model.PostedDate,
                Cancelled = model.Cancelled,
                AdjustmentType = model.AdjustmentType,
                Status = model.Status,
                Reference = model.Reference,
                Narratives = model.Narratives,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                // CompanyId changed to TenantId
                TenantId = Convert.ToString(currentUser.CompanyId),
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteAdjustmentHeader(Guid Id)
        {
            try
            {
                if (Id == null) { return; }
                await _InventoryAdjustmentsHeaderRepo.DeleteAsync(_InventoryAdjustmentsHeaderRepo.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<IPagedList<InventoryAdjustmentsHeaderDTO>> GetAllAdjustmentHeader(int pageNumber, int pageSize, string searchTerm)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);
            try
            {
                var AdjustmentHeader =
                    from model in _InventoryAdjustmentsHeaderRepo.GetAll()
                    where model.TenantId == Convert.ToString(currentUser.CompanyId) && string.IsNullOrWhiteSpace(searchTerm) //||
                    //model.VehicleRegistration.Contains(searchTerm)
                    orderby model.CreationTime descending

                    select new InventoryAdjustmentsHeaderDTO
                    {
                        Id = model.Id,
                        Code = model.Code,
                        AdjustmentDate = model.AdjustmentDate,
                        Reason = model.Reason,
                        Notes = model.Notes,
                        AdjustmentPostToGL = model.AdjustmentPostToGL,
                        SupervisorSignature = model.SupervisorSignature,
                        Total = model.Total,
                        IsCountAdjust = model.IsCountAdjust,
                        CurrencyID = model.CurrencyID,
                        CurrencyExchangeRate = model.CurrencyExchangeRate,
                        Void = model.Void,
                        Verified = model.Verified,
                        VerifiedBy = model.VerifiedBy,
                        VerifiedDate = model.VerifiedDate,
                        Issued = model.Issued,
                        IssuedBy = model.IssuedBy,
                        IssuedDate = model.IssuedDate,
                        Approved = model.Approved,
                        ApprovedBy = model.ApprovedBy,
                        ApprovedDate = model.ApprovedDate,
                        Posted = model.Posted,
                        PostedBy = model.PostedBy,
                        PostedDate = model.PostedDate,
                        Cancelled = model.Cancelled,
                        AdjustmentType = model.AdjustmentType,
                        Status = model.Status,
                        Reference = model.Reference,
                        Narratives = model.Narratives,
                    };

                return await AdjustmentHeader.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task<InventoryAdjustmentsHeaderDTO> GetAdjustmentHeader(Guid Id)
        {
            var model = _InventoryAdjustmentsHeaderRepo.FirstOrDefault(x => x.Id == Id);

            return new InventoryAdjustmentsHeaderDTO
            {
                Id = model.Id,
                Code = model.Code,
                AdjustmentDate = model.AdjustmentDate,
                Reason = model.Reason,
                Notes = model.Notes,
                AdjustmentPostToGL = model.AdjustmentPostToGL,
                SupervisorSignature = model.SupervisorSignature,
                Total = model.Total,
                IsCountAdjust = model.IsCountAdjust,
                CurrencyID = model.CurrencyID,
                CurrencyExchangeRate = model.CurrencyExchangeRate,
                Void = model.Void,
                Verified = model.Verified,
                VerifiedBy = model.VerifiedBy,
                VerifiedDate = model.VerifiedDate,
                Issued = model.Issued,
                IssuedBy = model.IssuedBy,
                IssuedDate = model.IssuedDate,
                Approved = model.Approved,
                ApprovedBy = model.ApprovedBy,
                ApprovedDate = model.ApprovedDate,
                Posted = model.Posted,
                PostedBy = model.PostedBy,
                PostedDate = model.PostedDate,
                Cancelled = model.Cancelled,
                AdjustmentType = model.AdjustmentType,
                Status = model.Status,
                Reference = model.Reference,
                Narratives = model.Narratives,
            };
        }


        public async Task<bool> UpdateAdjustmentHeader(InventoryAdjustmentsHeaderDTO model, Guid Id)
        {
            var AdjustmentHeader = _InventoryAdjustmentsHeaderRepo.FirstOrDefault(x => x.Id == Id);

            //AdjustmentHeader.Id = model.Id;
            AdjustmentHeader.Code = model.Code;
            AdjustmentHeader.AdjustmentDate = model.AdjustmentDate;
            AdjustmentHeader.Reason = model.Reason;
            AdjustmentHeader.Notes = model.Notes;
            AdjustmentHeader.AdjustmentPostToGL = model.AdjustmentPostToGL;
            AdjustmentHeader.SupervisorSignature = model.SupervisorSignature;
            AdjustmentHeader.Total = model.Total;
            AdjustmentHeader.IsCountAdjust = model.IsCountAdjust;
            AdjustmentHeader.CurrencyID = model.CurrencyID;
            AdjustmentHeader.CurrencyExchangeRate = model.CurrencyExchangeRate;
            AdjustmentHeader.Void = model.Void;
            AdjustmentHeader.Verified = model.Verified;
            AdjustmentHeader.VerifiedBy = model.VerifiedBy;
            AdjustmentHeader.VerifiedDate = model.VerifiedDate;
            AdjustmentHeader.Issued = model.Issued;
            AdjustmentHeader.IssuedBy = model.IssuedBy;
            AdjustmentHeader.IssuedDate = model.IssuedDate;
            AdjustmentHeader.Approved = model.Approved;
            AdjustmentHeader.ApprovedBy = model.ApprovedBy;
            AdjustmentHeader.ApprovedDate = model.ApprovedDate;
            AdjustmentHeader.Posted = model.Posted;
            AdjustmentHeader.PostedBy = model.PostedBy;
            AdjustmentHeader.PostedDate = model.PostedDate;
            AdjustmentHeader.Cancelled = model.Cancelled;
            AdjustmentHeader.AdjustmentType = model.AdjustmentType;
            AdjustmentHeader.Status = model.Status;
            AdjustmentHeader.Reference = model.Reference;
            AdjustmentHeader.Narratives = model.Narratives;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }


        public async Task<bool> VerifyAdjustment(InventoryAdjustmentsHeaderDTO model, Guid Id)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            var Adjustment = _InventoryAdjustmentsHeaderRepo.FirstOrDefault(x => x.Id == Id);

            //var item = _itemRepo.Get(Request.ItemID);

            var AppEmail = "no-reply@libmot.com";
            var VerifyEmail = "netprowebs@gmail.com";

            Adjustment.Verified = true;
            Adjustment.VerifiedDate = DateTime.Now;
            Adjustment.VerifiedBy = currentUser.Id.ToString();
            Adjustment.Void = false;

            // Send sms and email containing account details
            //string smsMessage = $"Recieve of {Request.Quantity} unit/s of {items.ItemName} has been verified. Awaiting your approval";
            //string smsMessage = $"Recieve of test quantity has been verified. Awaiting your approval";
            ////_smsSvc.SendSMSNow(smsMessage, recipient: newAdded.ContactName.ToNigeriaMobile());

            //if (currentUserEmail != null)
            //{
            //    var mail = new Mail(AppEmail, "Awaiting Approval!", VerifyEmail)
            //    {
            //        Body = smsMessage
            //    };
            //    _mailSvc.SendMail(mail);
            //}

            await _unitOfWork.SaveChangesAsync();

            return true;
        }


        public async Task<bool> ReceiveAdjustment(InventoryAdjustmentsHeaderDTO model, Guid Id)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            var Adjustment = _InventoryAdjustmentsHeaderRepo.FirstOrDefault(x => x.Id == Id);
            var AppEmail = "no-reply@libmot.com";
            var ReceiveEmail = "netprowebs@gmail.com";

            Adjustment.Issued = true;
            Adjustment.IssuedDate = DateTime.Now;
            Adjustment.IssuedBy = currentUser.Id.ToString();
            //Adjustment.ApprovedBy = $"{currentUser.FirstName} {currentUser.LastName}";
            Adjustment.CreatorUserId = currentUser.Id;
            Adjustment.Void = false;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }
        public async Task<bool> ApproveAdjustment(InventoryAdjustmentsHeaderDTO model, Guid Id)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            var Adjustment = _InventoryAdjustmentsHeaderRepo.FirstOrDefault(x => x.Id == Id);
            var AppEmail = "no-reply@libmot.com";
            var VerifyEmail = "netprowebs@gmail.com";

            Adjustment.Approved = true;
            Adjustment.ApprovedDate = DateTime.Now;
            Adjustment.ApprovedBy = currentUser.Id.ToString();
            Adjustment.Void = false;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        public async Task<bool> ReturnAdjustment(InventoryAdjustmentsHeaderDTO model, Guid Id)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            var Adjustment = _InventoryAdjustmentsHeaderRepo.FirstOrDefault(x => x.Id == Id);

            var AppEmail = "no-reply@libmot.com";
            var ReturnEmail = "netprowebs@gmail.com";

            if (model.SiteNumber == "2")
            {
                Adjustment.Verified = false;
                Adjustment.Void = true;
                Adjustment.AdjustmentDate = DateTime.UtcNow;
                Adjustment.Reason = model.Reason;
            }
            else
            {
                Adjustment.Issued = false;
                Adjustment.Void = true;
                Adjustment.AdjustmentDate = DateTime.UtcNow;
                Adjustment.Reason = model.Reason;
            }


            //// Send sms and email conataining account details
            //string smsMessage = $"Return of test quantity has been Return. Awaiting your correction";
            ////_smsSvc.SendSMSNow(smsMessage, recipient: newAdded.ContactName.ToNigeriaMobile());

            //if (currentUserEmail != null)
            //{
            //    var mail = new Mail(AppEmail, "Awaiting Correction!", ReturnEmail)
            //    {
            //        Body = smsMessage
            //    };
            //    _mailSvc.SendMail(mail);
            //}

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        #endregion

        #region Inventory Adjustment Detail
        public async Task AddAdjustmentDetail(InventoryAdjustmentsDetailDTO model)
        {
            //model.AdjustmentDetailDescription = model.AdjustmentDetailDescription.Trim();
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);



            _InventoryAdjustmentsDetailRepo.Insert(new InventoryAdjustmentsDetail
            {
                AdjustmentID = model.AdjustmentID,
                ItemID = model.ItemID,
                WarehouseID = model.WarehouseID,
                WarehouseBinID = model.WarehouseBinID,
                Description = model.Description,
                OriginalQuantity = model.OriginalQuantity,
                AdjustedQuantity = model.AdjustedQuantity,
                CurrentID = model.CurrentID,
                CurrentExchangeRate = model.CurrentExchangeRate,
                Cost = model.Cost,
                GLAdjustmentPostingAccount = model.GLAdjustmentPostingAccount,
                ProjectID = model.ProjectID,
                UnitCost = model.UnitCost,
                GLAnalysisType1 = model.GLAnalysisType1,
                AssetID = model.AssetID,

                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                //CompanyId = (int)currentUser.CompanyId
                //Waiting for confirmation
                //TenantId = Convert.ToString( currentUser.CompanyId)
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteAdjustmentDetail(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _InventoryAdjustmentsDetailRepo.DeleteAsync(_InventoryAdjustmentsDetailRepo.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<IPagedList<InventoryAdjustmentsDetailDTO>> GetAllAdjustmentDetail(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var AdjustmentDetail =
                    from model in _InventoryAdjustmentsDetailRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) ||
                    model.Description.Contains(searchTerm)// ||
                    //Warehouse.WarehouseCode.Contains(searchTerm)
                    orderby model.CreationTime descending

                    select new InventoryAdjustmentsDetailDTO
                    {
                        Id = model.Id,
                        AdjustmentID = model.AdjustmentID,
                        ItemID = model.ItemID,
                        WarehouseID = model.WarehouseID,
                        WarehouseBinID = model.WarehouseBinID,
                        Description = model.Description,
                        OriginalQuantity = model.OriginalQuantity,
                        AdjustedQuantity = model.AdjustedQuantity,
                        CurrentID = model.CurrentID,
                        CurrentExchangeRate = model.CurrentExchangeRate,
                        Cost = model.Cost,
                        GLAdjustmentPostingAccount = model.GLAdjustmentPostingAccount,
                        ProjectID = model.ProjectID,
                        UnitCost = model.UnitCost,
                        GLAnalysisType1 = model.GLAnalysisType1,
                        AssetID = model.AssetID,
                    };

                return await AdjustmentDetail.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public  Task<List<InventoryAdjustmentsDetailDTO>> GetAdjustmentDetail(Guid Id)
        {
            //var model = _InventoryAdjustmentsDetailRepo.FirstOrDefault(x => x.Id == Id);

            var invadjdetails = from model in _InventoryAdjustmentsDetailRepo.GetAll()
                                where model.AdjustmentID == Id

                                select new InventoryAdjustmentsDetailDTO
                                {
                                    Id = model.Id,
                                    AdjustmentID = model.AdjustmentID,
                                    ItemID = model.ItemID,
                                    WarehouseID = model.WarehouseID,
                                    WarehouseBinID = model.WarehouseBinID,
                                    Description = model.Description,
                                    OriginalQuantity = model.OriginalQuantity,
                                    AdjustedQuantity = model.AdjustedQuantity,
                                    CurrentID = model.CurrentID,
                                    CurrentExchangeRate = model.CurrentExchangeRate,
                                    Cost = model.Cost,
                                    GLAdjustmentPostingAccount = model.GLAdjustmentPostingAccount,
                                    ProjectID = model.ProjectID,
                                    UnitCost = model.UnitCost,
                                    GLAnalysisType1 = model.GLAnalysisType1,
                                    AssetID = model.AssetID,
                                };
            return invadjdetails.ToListAsync();
        }


        public async Task<bool> UpdateAdjustmentDetail(InventoryAdjustmentsDetailDTO model, int Id)
        {
            var AdjustmentDetail = _InventoryAdjustmentsDetailRepo.FirstOrDefault(x => x.Id == Id);

            //AdjustmentDetail.Id = model.Id;
            //AdjustmentDetail.InventoryReceivedID = model.InventoryReceivedID;
            AdjustmentDetail.ItemID = model.ItemID;
            AdjustmentDetail.WarehouseID = model.WarehouseID;
            AdjustmentDetail.WarehouseBinID = model.WarehouseBinID;
            AdjustmentDetail.Description = model.Description;
            AdjustmentDetail.OriginalQuantity = model.OriginalQuantity;
            AdjustmentDetail.AdjustedQuantity = model.AdjustedQuantity;
            AdjustmentDetail.CurrentID = model.CurrentID;
            AdjustmentDetail.CurrentExchangeRate = model.CurrentExchangeRate;
            AdjustmentDetail.Cost = model.Cost;
            AdjustmentDetail.GLAdjustmentPostingAccount = model.GLAdjustmentPostingAccount;
            AdjustmentDetail.ProjectID = model.ProjectID;
            AdjustmentDetail.UnitCost = model.UnitCost;
            AdjustmentDetail.GLAnalysisType1 = model.GLAnalysisType1;
            AdjustmentDetail.AssetID = model.AssetID;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        #endregion
    }
}
