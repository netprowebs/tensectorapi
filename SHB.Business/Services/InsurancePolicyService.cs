﻿using IPagedList;
using Microsoft.EntityFrameworkCore;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Business.Services
{
    public interface IInsurancePolicyService
    {

        #region InsurancePolicy

        #endregion

        #region InsurPolicyUnderwriters

        #endregion

        #region InsurPolicyBrokers

        #endregion

        #region InsurPolicyBrokers

        #endregion


    }

    public class InsurancePolicyService : IInsurancePolicyService
    {

        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;

        private readonly IUserService _userSvc;
        public InsurancePolicyService(
            IServiceHelper serviceHelper,
            IUnitOfWork unitOfWork,
            IUserService userSvc
            )
        {
            _serviceHelper = serviceHelper;
            _unitOfWork = unitOfWork;
            _userSvc = userSvc;
        }


        #region InsurancePolicy

        #endregion

        #region InsurPolicyUnderwriters

        #endregion

        #region InsurPolicyBrokers

        #endregion
    }
}
