﻿using IPagedList;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Core.Utils;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using SHB.Core.Domain.Entities;

namespace SHB.Business.Services
{
    public interface IVetSetupsService
    {
        #region Vetbundles
        Task<IPagedList<VetBundlesDTO>> GetVetBundles(int page, int size, string search = null);
        Task<VetBundlesDTO> GetVetBundlesById(int vetBundlesId);
        Task AddVetBundles(VetBundlesDTO vetBundles);
        Task UpdateVetBundles(int vetBundlesId, VetBundlesDTO vetBundles);
        Task RemoveVetBundles(int vetBundlesId);

        #endregion

        #region VetServicesType
        Task<IPagedList<VetServiceDTO>> GetVetService(int page, int size, string search = null);
        Task<VetServiceDTO> GetVetServiceById(int VetServiceId);
        Task AddVetService(VetServiceDTO VetService);
        Task UpdateVetService(int VetServiceId, VetServiceDTO VetService);
        Task RemoveVetService(int VetServiceId);

        #endregion

    }

    public class VetSetupsService : IVetSetupsService
    {
        private readonly IRepository<VetBundles> _vetbundlesRepo;
        private readonly IRepository<VetService> _vetserviceRepo;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;

        public VetSetupsService(
            IRepository<VetBundles> vetbundlesRepo,
              IRepository<VetService> vetserviceRepo,
            IServiceHelper serviceHelper,
            IUnitOfWork unitOfWork)
        {
            _vetbundlesRepo = vetbundlesRepo;
            _serviceHelper = serviceHelper;
            _vetserviceRepo = vetserviceRepo;
            _unitOfWork = unitOfWork;
        }

        #region VetBundles
        public Task<IPagedList<VetBundlesDTO>> GetVetBundles(int page, int size, string search)
        {
            var bundles = from bundle in _vetbundlesRepo.GetAll()
                          where string.IsNullOrWhiteSpace(search) || bundle.Code.Contains(search)
                          orderby bundle.CreationTime descending
                          select new VetBundlesDTO
                          {
                              Id = bundle.Id,
                              Code = bundle.Code,
                              UnitCounts = bundle.UnitCounts,
                              Description = bundle.Description,
                              AdsPrice = bundle.AdsPrice,
                              Amount = bundle.Amount,
                              IsMax = bundle.IsMax,
                              DateCreated = bundle.CreationTime.ToString(CoreConstants.DateFormat)
                          };

            return bundles.AsNoTracking().ToPagedListAsync(page, size);
        }

        public async Task<VetBundlesDTO> GetVetBundlesById(int bundleId)
        {
            var vetbundle = await _vetbundlesRepo.GetAsync(bundleId);

            if (vetbundle is null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VETBUNDLE_NOT_EXIST);
            }

            return new VetBundlesDTO
            {
                Code = vetbundle.Code,
                Id = vetbundle.Id,
                UnitCounts = vetbundle.UnitCounts,
                Description = vetbundle.Description,
                AdsPrice = vetbundle.AdsPrice,
                Amount = vetbundle.Amount,
                IsMax = vetbundle.IsMax,
                DateCreated = vetbundle.CreationTime.ToString(CoreConstants.DateFormat),

            };
        }

        public async Task AddVetBundles(VetBundlesDTO vetBundlesDto)
        {
            vetBundlesDto.Code = vetBundlesDto.Code.Trim();

            var vetBundlesName = vetBundlesDto.Code.ToLower();

            if (await _vetbundlesRepo.ExistAsync(v => v.Code.ToLower() == vetBundlesName))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VETBUNDLE_NOT_EXIST);
            }

            _vetbundlesRepo.Insert(new VetBundles
            {
                Code = vetBundlesDto.Code,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                TenantId = _serviceHelper.GetTenantId(),
                UnitCounts = vetBundlesDto.UnitCounts,
                Description = vetBundlesDto.Description,
                AdsPrice = vetBundlesDto.AdsPrice,
                Amount = vetBundlesDto.Amount,
                IsMax = vetBundlesDto.IsMax,

            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task RemoveVetBundles(int vetBundlesId)
        {
            var vetBundles = await _vetbundlesRepo.GetAsync(vetBundlesId);

            if (vetBundles is null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VETBUNDLE_NOT_EXIST);
            }

            _vetbundlesRepo.Delete(vetBundles);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateVetBundles(int vetBundlesId, VetBundlesDTO vetBundlesDto)
        {
            var vetBundles = await _vetbundlesRepo.GetAsync(vetBundlesId);

            if (vetBundles is null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VETBUNDLE_NOT_EXIST);
            }

            vetBundles.Code = vetBundlesDto.Code.Trim();
            vetBundles.UnitCounts = vetBundlesDto.UnitCounts;
            vetBundles.Description = vetBundlesDto.Description;
            vetBundles.AdsPrice = vetBundlesDto.AdsPrice;
            vetBundles.Amount = vetBundlesDto.Amount;
            vetBundles.IsMax = vetBundlesDto.IsMax;

            await _unitOfWork.SaveChangesAsync();
        }
        #endregion

        #region VetServicesType
        public Task<IPagedList<VetServiceDTO>> GetVetService(int page, int size, string search)
        {
            var bundles = from service in _vetserviceRepo.GetAll()
                          where string.IsNullOrWhiteSpace(search) || service.Code.Contains(search)
                          orderby service.CreationTime descending
                          select new VetServiceDTO
                          {
                              Id = service.Id,
                              Code = service.Code,
                              TenantId = service.TenantId,
                              ToVetDaysCount = service.ToVetDaysCount,
                              Name = service.Name,
                              Description = service.Description,
                              CostInAmount = service.CostInAmount,
                              CostInUnit = service.CostInUnit,
                              DateCreated = service.CreationTime.ToString(CoreConstants.DateFormat)
                          };

            return bundles.AsNoTracking().ToPagedListAsync(page, size);
        }

        public async Task<VetServiceDTO> GetVetServiceById(int serviceId)
        {
            var vetservice = await _vetserviceRepo.GetAsync(serviceId);

            if (vetservice is null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VETSERVICE_NOT_EXIST);
            }

            return new VetServiceDTO
            {
                Id = vetservice.Id,
                Code = vetservice.Code,
                TenantId = vetservice.TenantId,
                ToVetDaysCount = vetservice.ToVetDaysCount,
                Name = vetservice.Name,
                Description = vetservice.Description,
                CostInAmount = vetservice.CostInAmount,
                CostInUnit = vetservice.CostInUnit,
                DateCreated = vetservice.CreationTime.ToString(CoreConstants.DateFormat),

            };
        }

        public async Task AddVetService(VetServiceDTO vetServiceDto)
        {
            vetServiceDto.Code = vetServiceDto.Code.Trim();

            var vetServiceName = vetServiceDto.Code.ToLower();

            if (await _vetserviceRepo.ExistAsync(v => v.Code.ToLower() == vetServiceName))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VETSERVICE_NOT_EXIST);
            }

            _vetserviceRepo.Insert(new VetService
            {
                Code = vetServiceDto.Code,
                TenantId = _serviceHelper.GetTenantId(),
                ToVetDaysCount = vetServiceDto.ToVetDaysCount,
                Name = vetServiceDto.Name,
                Description = vetServiceDto.Description,
                CostInAmount = vetServiceDto.CostInAmount,
                CostInUnit = vetServiceDto.CostInUnit,
                CreationTime = new System.DateTime(),
                CreatorUserId = _serviceHelper.GetCurrentUserId()
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task RemoveVetService(int vetServiceId)
        {
            var vetService = await _vetserviceRepo.GetAsync(vetServiceId);

            if (vetService is null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VETSERVICE_NOT_EXIST);
            }

            _vetserviceRepo.Delete(vetService);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateVetService(int vetServiceId, VetServiceDTO vetServiceDto)
        {
            var vetService = await _vetserviceRepo.GetAsync(vetServiceId);

            if (vetService is null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VETSERVICE_NOT_EXIST);
            }

            vetService.Code = vetServiceDto.Code.Trim();
            vetService.ToVetDaysCount = vetServiceDto.ToVetDaysCount;
            vetService.Name = vetServiceDto.Name;
            vetService.Description = vetServiceDto.Description;
            vetService.CostInAmount = vetServiceDto.CostInAmount;
            vetService.CostInUnit = vetServiceDto.CostInUnit;

            await _unitOfWork.SaveChangesAsync();
        }
        #endregion


        //#region V

        //#endregion
    }
}
