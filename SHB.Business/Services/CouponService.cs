﻿using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Entities;
//using SHB.Core.Entities.Enums;
using SHB.Core.Exceptions;
using SHB.Data.efCore.Context;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using SHB.Core.Timing;
using SHB.Core.Domain.Entities;
using SHB.Core.Utils;
//using SHB.Data.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using SHB.Data.UnitOfWork.Extensions;
using SHB.Data.efCore.Extensions;
using SHB.Core.Entities.Enums;

namespace SHB.Business.Services
{
    public interface ICouponService
    {
        Task<CouponDTO> GetValidCouponByPhone(string couponCode, string phone);
        Task<List<CouponDTO>> GetCouponsByPhone(string phoneNumber);
        Task<List<couponCountDTO>> GetEmployeeCouponCount(string phoneNumber);
    }

    public class CouponService : ICouponService
    {
        private readonly IRepository<Coupon, Guid> _repository;
        //private readonly IRepository<SeatManagement, long> _seatMgtRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceHelper _serviceHelper;
        private ApplicationDbContext _context;
        public CouponService(IRepository<Coupon, Guid> repository,
            //IRepository<SeatManagement, long> seatMgtRepository,
            IUnitOfWork unitOfWork,
            IServiceHelper serviceHelper, ApplicationDbContext context)
        {
            _repository = repository;
            //_seatMgtRepository = seatMgtRepository;
            _unitOfWork = unitOfWork;
            _serviceHelper = serviceHelper;
            _context = context;
        }

        public IQueryable<Coupon> GetAll()
        {
            return _repository.GetAll();
        }


        public async Task<CouponDTO> GetValidCouponByPhone(string couponCode, string phone)
        {
            var coupon = await GetCouponByCodeAsync(couponCode);

            if (coupon == null)
            {
                throw await _serviceHelper.GetExceptionAsync("Coupon does not exist");
            }
            coupon = await GetValidCouponByCodeAsync(couponCode);
            if (coupon == null)
            {
                throw await _serviceHelper.GetExceptionAsync("Coupon has been used");
            }

            var couponexpiryDate = await GetCouponExpiryDate(couponCode);

            if (DateTime.Now > couponexpiryDate)
            {
                throw await _serviceHelper.GetExceptionAsync("Coupon has Expired");
            }

            //var couponused = await GetCouponUsedAsync(couponCode);
            //if (couponused != null)
            //{
            //    throw await _serviceHelper.GetExceptionAsync("Coupon already used by Someone");
            //}

            //var couponuseByGuest = await GetCouponUsedByPhoneAsync(couponCode, phone);
            //if (couponuseByGuest != null)
            //{
            //    throw await _serviceHelper.GetExceptionAsync("Coupon already used by Guest");
            //}
            return coupon;
        }

        //public Task<SeatManagementDTO> GetCouponUsedByPhoneAsync(string couponCode, string phone)
        //{
        //    var coupons =
        //         from seat in _seatMgtRepository.GetAll()
        //         where seat.HasCoupon && seat.CouponCode == couponCode && seat.PhoneNumber == phone
        //         select new SeatManagementDTO
        //         {
        //             Id = seat.Id,
        //             BookingReferenceCode = seat.BookingReferenceCode,
        //             DateCreated = seat.CreationTime,
        //             BookingStatus = seat.BookingStatus
        //         };

        //    return coupons.AsNoTracking().FirstOrDefaultAsync();
        //}
        //public Task<SeatManagementDTO> GetCouponUsedAsync(string couponCode)
        //{
        //    var coupons =
        //         from seat in _seatMgtRepository.GetAll()
        //         where seat.HasCoupon && seat.CouponCode == couponCode
        //         select new SeatManagementDTO
        //         {
        //             Id = seat.Id,
        //             BookingReferenceCode = seat.BookingReferenceCode,
        //             DateCreated = seat.CreationTime,
        //             BookingStatus = seat.BookingStatus
        //         };

        //    return coupons.AsNoTracking().FirstOrDefaultAsync();
        //}
        private async Task<DateTime> GetCouponExpiryDate(string couponCode)
        {
            DateTime Expiry = DateTime.Now;
            var coupon = await GetCouponByCodeAsync(couponCode);

            if (coupon == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.COUPON_NOT_EXIST);
            }

            if (coupon.DurationType == DurationType.Second)
            {
                Expiry = coupon.DateCreated.AddSeconds(coupon.Duration);
            }
            else if (coupon.DurationType == DurationType.Minute)
            {
                Expiry = coupon.DateCreated.AddMinutes(coupon.Duration);
            }
            else if (coupon.DurationType == DurationType.Hour)
            {
                Expiry = coupon.DateCreated.AddHours(coupon.Duration);
            }
            else if (coupon.DurationType == DurationType.Day)
            {
                Expiry = coupon.DateCreated.AddDays(coupon.Duration);
            }
            else if (coupon.DurationType == DurationType.Month)
            {
                Expiry = coupon.DateCreated.AddMonths(coupon.Duration);
            }
            else if (coupon.DurationType == DurationType.Year)
            {
                Expiry = coupon.DateCreated.AddYears(coupon.Duration);
            }

            return Expiry;
        }

        private Task<CouponDTO> GetValidCouponByCodeAsync(string couponCode)
        {
            var coupons =
                from coupon in GetAll()
                where coupon.CouponCode == couponCode && coupon.Validity == true
                select new CouponDTO
                {
                    Id = coupon.Id,
                    CouponType = coupon.CouponType,
                    CouponCode = coupon.CouponCode,
                    Validity = coupon.Validity,
                    CouponValue = coupon.CouponValue,
                    DateCreated = coupon.CreationTime,
                    Duration = coupon.Duration,
                    DurationType = coupon.DurationType,
                    IsUsed = coupon.IsUsed,
                    DateUsed = coupon.DateUsed
                };

            return coupons.AsNoTracking().FirstOrDefaultAsync();
        }

        private Task<CouponDTO> GetCouponByCodeAsync(string couponCode)
        {
            var coupons =
                 from coupon in GetAll()
                 where coupon.CouponCode == couponCode
                 select new CouponDTO
                 {
                     Id = coupon.Id,
                     CouponType = coupon.CouponType,
                     CouponCode = coupon.CouponCode,
                     Validity = coupon.Validity,
                     CouponValue = coupon.CouponValue,
                     DateCreated = coupon.CreationTime,
                     Duration = coupon.Duration,
                     DurationType = coupon.DurationType,
                     IsUsed = coupon.IsUsed,
                     DateUsed = coupon.DateUsed
                 };

            return coupons.AsNoTracking().FirstOrDefaultAsync();
        }

        public Task<List<CouponDTO>> GetCouponsByPhone(string phoneNumber)
        {
            var getcoupons = from coupon in GetAll()
                             where coupon.PhoneNumber == phoneNumber
                             select new CouponDTO
                             {
                                 PhoneNumber = coupon.PhoneNumber,
                                 DateCreated = coupon.CreationTime,
                                 Validity = coupon.Validity,
                                 ValidityStatus = coupon.Validity.ToString(),
                                 CouponCode = coupon.CouponCode,
                                 CouponType = coupon.CouponType,
                                 CouponValue = coupon.CouponValue,
                                 Id = coupon.Id,
                                 DurationType = coupon.DurationType,
                                 Duration = coupon.Duration,
                                 IsUsed = coupon.IsUsed,
                                 DateUsed = coupon.DateUsed
                             };

            return getcoupons.AsNoTracking().ToListAsync();
        }
        public async Task<List<couponCountDTO>> GetEmployeeCouponCount(string phoneNumber)
        {
            var employeeCoupon = await _unitOfWork
            .GetDbContext<ApplicationDbContext>()
            .Database.ExecuteSqlToObject<couponCountDTO>(@"Exec Sp_EmployeeCouponCount", phoneNumber);

            await _unitOfWork.SaveChangesAsync();

            return employeeCoupon.AsEnumerable().ToList();


        }


    }
}
