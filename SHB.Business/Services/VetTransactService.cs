﻿using IPagedList;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Core.Utils;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using SHB.Core.Domain.Entities;
using System;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing;
using Microsoft.AspNetCore.Hosting;
using System.Collections.Generic;
using SHB.Core.Entities.Enums;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;

namespace SHB.Business.Services
{
    public interface IVetTransactService
    {
        #region Vettee
        Task AddVetteeList(VetteeListDTO vettee);
        Task<VetteeListDTO> GetVetteeListById(Guid vetteeID);
        Task<IPagedList<VetteeListDTO>> GetVetByClient(int ClientId,int page, int size, string query = null);
        IQueryable<VetteeList> GetAll();
        Task UpdateVetteeList(Guid vetteeId, VetteeListDTO vetteeDto);
        Task UpdateVettersForm(Guid vetteeId, VetteeListDTO vetteeDto);       
        Task RemoveVetteeList(Guid vetteeId);
        Task<IPagedList<VetteeListDTO>> GetVetteeList(int page, int size, string query = null);
        Task<bool> UploadVetteeFiles(IFormFile file);
        #endregion

        #region VettersTransactions
        Task<VettersTransactionDTO> GetVetTransactionById(int vetterID);
        IQueryable<VettersTransaction> GetAllVetTransaction();
        Task UpdateVetTransaction(int vetterId, VettersTransactionDTO vetterDto);
        Task<IPagedList<VettersTransactionDTO>> GetVetTransactionList(int page, int size, string query = null);
        Task<IPagedList<VettersTransactionDTO>> AllVetAssigned(int UserId);       
        Task<List<VettersDistinctDTO>> GetDistinctVettrans();       
        Task AddVettrans(VettersTransactionDTO vettrans);
        Task<bool> VetterActionTo(ActionDto model);
        
        #endregion

    }

    public class VetTransactService : IVetTransactService
    {
        private readonly IRepository<VetteeList, Guid> _repository;
        private readonly IRepository<VettersTransaction> _repositoryVetters;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceHelper _serviceHelper;
        IRepository<CustomerInformation> _customerInfoRepo;
        private readonly IRepository<VetService> _vetserviceRepo;
        private readonly IRepository<ImageFile> _imgRepo;
        private readonly IHostingEnvironment _appEnvironment;
        private readonly IEmployeeService _employeeService ;
        private readonly IUtilityService _utilityService;
        private readonly IRepository<NextNumber> _nextnoRepo;
        private readonly INextNumberService _nextNumberService;

        public VetTransactService(IRepository<VetteeList, Guid> repository, IRepository<VettersTransaction> repositoryvetters, IUnitOfWork unitOfWork,
            IServiceHelper serviceHelper, IRepository<CustomerInformation> customerInfoRepo, IRepository<VetService> vetserviceRepo,
            IHostingEnvironment hostingEnvironment,  IEmployeeService employeeService, IRepository<ImageFile> imgRepo,
            IUtilityService utilityService, IRepository<NextNumber> nextnoRepo, INextNumberService nextNumberService)
        {
            _repository = repository;
            _repositoryVetters = repositoryvetters;
            _unitOfWork = unitOfWork;
            _serviceHelper = serviceHelper;
            _customerInfoRepo = customerInfoRepo;
            _vetserviceRepo = vetserviceRepo;
            _appEnvironment = hostingEnvironment;    
            _employeeService = employeeService;
            _imgRepo = imgRepo;
            _utilityService = utilityService;
            _nextnoRepo = nextnoRepo;
            _nextNumberService = nextNumberService;
        }


        public async Task<bool> UploadVetteeFiles(IFormFile file)
        {
            if (file?.Length > 0)
            {
                // convert to a stream
                var stream = file.OpenReadStream();
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                //ExcelPackage.LicenseContext = LicenseContext.Commercial;
                try
                {
                    using (var package = new ExcelPackage(stream))
                    {
                        var worksheet = package.Workbook.Worksheets.First();
                        var rowCount = worksheet.Dimension.Rows;

                        for (var row = 2; row <= rowCount; row++)
                        {
                            try
                            {
                                var VetteeLastName = worksheet.Cells[row, 1].Value?.ToString();
                                var VetteeFirstName = worksheet.Cells[row, 2].Value?.ToString();
                                var VetteeOtherName = worksheet.Cells[row, 3].Value?.ToString();
                                var VetService = worksheet.Cells[row, 4].Value?.ToString();
                                var PhoneNumber = "0"+ worksheet.Cells[row, 5].Value?.ToString();
                                var Email = worksheet.Cells[row, 6].Value?.ToString();
                                var Address = worksheet.Cells[row, 7].Value?.ToString();
                                var ClientEmail = worksheet.Cells[row, 8].Value?.ToString();
                                var Picture = worksheet.Cells[row, 9].Value?.ToString();
                                var UnitAmount = 0;

                                var CusDetail = _customerInfoRepo.GetAllIncluding(x => x.User).FirstOrDefault(x => x.User.Email == ClientEmail);
                                //var CusDetailId = (int)_serviceHelper.GetCurrentUserId();
                                VetService VetServe = null;
                                string refCodes = "";
                                if (CusDetail != null) {
                                     refCodes = await GetRefCode(CusDetail.User.Id);
                                    // Get unitunit cost value by refcode
                                    if (!string.IsNullOrEmpty(VetService))
                                    {
                                        VetServe = _vetserviceRepo.GetAll().FirstOrDefault(x => x.Description == VetService);
                                        UnitAmount = VetServe.CostInUnit;

                                        var vettees = new VetteeListDTO()
                                        {
                                            TenantId = _serviceHelper.GetTenantId(),
                                            ClientId = CusDetail.User.Id,
                                            ReferenceCode = refCodes,
                                            VetteeLastName = VetteeLastName,
                                            VetteeFirstName = VetteeFirstName,
                                            VetteeOtherName = VetteeOtherName,
                                            VetServiceID = VetServe.Id,
                                            PhoneNumber = PhoneNumber,
                                            Email = Email,
                                            Address = Address,
                                            Picture = Picture,
                                            Amount = UnitAmount,
                                            LoginDeviceType = DeviceType.Website
                                        };

                                        await AddVetteeList(vettees);
                                    }
                                }else
                                {

                                }

                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }

                    return true;
                    //return View("Index", users);
                }
                catch (Exception ex)
                {
                    return false;
                    Console.WriteLine(ex.Message);
                }
            }
            return true;

        }
        public async Task AddVetteeList(VetteeListDTO vettee)
        {
            if (await _repository.ExistAsync(v => v.Id == vettee.Id))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VETSERVICE_NOT_EXIST);
            }

            string refCodes = await GetRefCode(vettee.ClientId);
            var CusDetail = _customerInfoRepo.GetAllIncluding(x => x.User).FirstOrDefault(x => x.UserId == vettee.ClientId);
            VetService VetServe = null;
            // Get unitunit cost value by refcode
            if ( !string.IsNullOrEmpty(vettee.VetServiceID.ToString())) {
                VetServe = _vetserviceRepo.GetAll().FirstOrDefault(x => x.Id == vettee.VetServiceID);
                vettee.UnitUsed = VetServe.CostInUnit;
            }
            else
            {
              throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VETSERVICE_NOT_EXIST);
            }
            
            // Check for Picture 
            string fileName = "";
            if ( !string.IsNullOrEmpty(vettee.Picture) )
            {
                List<ImageFileDTO> imageFileDTO = new List<ImageFileDTO>();
                string pic = vettee.Picture.Split(',').Last();
                string Contype = vettee.Picture.Split(new Char[] { ':', ';' })[1];
                string ext = "." + vettee.Picture.Split(new Char[] { '/', ';' })[1];

                var ImageNameWitExt  = DateTime.UtcNow.ToString("yymmssfff") + refCodes;
                var ImageName = ImageNameWitExt + ext;

                ImageFileDTO imgdto = new ImageFileDTO();
                byte[] bytes = Convert.FromBase64String(pic);
                imgdto.data = bytes;
                imgdto.FilenameWithOutExt = ImageNameWitExt;
                imgdto.fileName = ImageName;
                imgdto.ContentType = Contype;
                imgdto.UploadType = UploadType.Image;

                imageFileDTO.Add(imgdto);
                imgdto.PictureList = imageFileDTO;
                fileName = await _utilityService.UplaodBytesToDigOc(imgdto);

            }
           

                var vinsert = _repository.Insert(new VetteeList
                {
                    TenantId = vettee.TenantId,
                    ClientId = vettee.ClientId,
                    ReferenceCode = refCodes,
                    VetteeLastName = vettee.VetteeLastName,
                    VetteeFirstName = vettee.VetteeFirstName,
                    VetteeOtherName = vettee.VetteeOtherName,
                    VetServiceID = vettee.VetServiceID,
                    PhoneNumber = vettee.PhoneNumber,
                    Email = vettee.Email,
                    Address = vettee.Address,
                    Picture = fileName,
                    Amount = vettee.Amount,
                    Vat = vettee.Vat,
                    UnitUsed = vettee.UnitUsed,
                    VetteeStatus = vettee.VetteeStatus,
                    LoginDeviceType = vettee.LoginDeviceType,
                    PaymentMethod = vettee.PaymentMethod,
                    PaymentStatus = vettee.PaymentStatus,
                    VetAddress = vettee.VetAddress,
                    VetLatitude = vettee.VetLatitude,
                    VetLongitude = vettee.VetLongitude,
                    CouponCode = vettee.CouponCode,
                    VetStartdate = vettee.VetStartdate,
                    VetEnddate = vettee.VetEnddate,
                    Latitude = vettee.Latitude,
                    Longitude = vettee.Longitude,
                    VetGeoLink = vettee.VetGeoLink,
                    VetOwnerStatus = vettee.VetOwnerStatus,
                    VetPeriodOfstay = vettee.VetPeriodOfstay,
                    VetGuardians = vettee.VetGuardians,
                    VettersRemarks = vettee.VettersRemarks,
                    VetCity = vettee.VetCity,
                    Landmark = vettee.Landmark

                });

            await _unitOfWork.SaveChangesAsync();


            //check if the vet has been created for the Tenant 
            var tenantVet = _repository.GetAll().Where(v => v.Email == CusDetail.User.Email).ToList();

            if (tenantVet.Count == 0 )
            {
                string vrefCodes = await GetRefCode(vettee.ClientId);
                //Insert in vettees list for tenant
                _repository.Insert(new VetteeList
                {
                    TenantId = _serviceHelper.GetTenantId(),
                    ClientId = vettee.ClientId,
                    ReferenceCode = vrefCodes,
                    VetteeLastName = CusDetail.User.LastName,
                    VetteeFirstName = CusDetail.User.FirstName,
                    VetteeOtherName = CusDetail.User.MiddleName,
                    VetServiceID = 4,
                    PhoneNumber = CusDetail.User.PhoneNumber,
                    Email = CusDetail.User.Email,
                    Address = CusDetail.User.Address,
                    Picture = CusDetail.User.Photo,
                    Amount = vettee.Amount,
                    Vat = vettee.Vat,
                    UnitUsed = vettee.UnitUsed,
                    VetteeStatus = VetStatus.Pending,
                    LoginDeviceType = vettee.LoginDeviceType,
                    PaymentMethod = vettee.PaymentMethod,
                    PaymentStatus = PaymentStatus.Pending,
                    VetAddress = vettee.VetAddress,
                    VetLatitude = vettee.VetLatitude,
                    VetLongitude = vettee.VetLongitude,
                    CouponCode = vettee.CouponCode,
                    VetStartdate = vettee.VetStartdate,
                    VetEnddate = vettee.VetEnddate,
                    Latitude = vettee.Latitude,
                    Longitude = vettee.Longitude,
                    VetGeoLink = vettee.VetGeoLink,
                    VetOwnerStatus = vettee.VetOwnerStatus,
                    VetPeriodOfstay = vettee.VetPeriodOfstay,
                    VetGuardians = vettee.VetGuardians,
                    VettersRemarks = vettee.VettersRemarks,
                    //VetVideoUpload = vettee.VetVideoUpload,
                    VetPhotoUpload = CusDetail.User.Photo,

                });

                //var VId = vinsert.Id;

                //Insert in vetter list for tenant
                //_repositoryVetters.Insert(new VettersTransaction
                //{
                //    TenantId = _serviceHelper.GetTenantId(),
                //    VetteeListID = VId,
                //    ReferenceCode = refCodes,
                //    CreatorUserId = _serviceHelper.GetCurrentUserId(),
                //    LoginDeviceType = vettee.LoginDeviceType,
                //});

            }

            await _unitOfWork.SaveChangesAsync();

        }


   

        public async Task<string> GetRefCode(int clientId)
        {
            bool isUnique = false;

            var otp = "null";
            while (isUnique == false)
            {
                //var CusDetail = _customerInfoRepo.GetAllIncluding(x => x.User).FirstOrDefault(x => x.UserId == clientId);           
                //string ALIAS = CusDetail.User.FirstName?.Substring(0, 3).ToUpper();
                 otp = _nextNumberService.GetTenantNextNumber("NextVetNumber").Result.ToString();

                var exists = await _repository.ExistAsync(a => a.ReferenceCode == otp);
                isUnique = !exists;
            }
            return otp;
        }

        public Task<IPagedList<VetteeListDTO>> GetVetteeList(int page, int size, string query = null)
        {

            var tenant = _serviceHelper.GetTenantId();
            var vettees =
                from vettee in _repository.GetAll().Where(x => x.TenantId == tenant)
                join vetService in _vetserviceRepo.GetAll() on vettee.VetServiceID equals vetService.Id
                into vetServices from vetService in vetServices.DefaultIfEmpty()

                orderby vettee.Id descending
                select new VetteeListDTO
                {
                    Id = vettee.Id,
                    TenantId = vettee.TenantId,
                    ClientId = vettee.ClientId,
                    ReferenceCode = vettee.ReferenceCode,
                    VetteeLastName = vettee.VetteeLastName,
                    VetteeFirstName = vettee.VetteeFirstName,
                    VetteeOtherName = vettee.VetteeOtherName,
                    VetServiceID = vettee.VetServiceID,
                    VetService = vetService.Name,
                    PhoneNumber = vettee.PhoneNumber,
                    Email = vettee.Email,
                    Address = vettee.Address,
                    VetLatitude = vettee.Latitude,
                    VetLongitude = vettee.Longitude,
                    Picture = vettee.Picture,
                    Amount = vettee.Amount,
                    Vat = vettee.Vat,
                    UnitUsed = vettee.UnitUsed,
                    VetteeStatus = vettee.VetteeStatus,
                    LoginDeviceType = vettee.LoginDeviceType,
                    PaymentMethod = vettee.PaymentMethod,
                    PaymentStatus = vettee.PaymentStatus,
                    VetAddress = vettee.VetAddress,
                    CouponCode = vettee.CouponCode,
                    VetStartdate = vettee.VetStartdate,
                    VetEnddate = vettee.VetEnddate,
                    Latitude = vettee.Latitude,
                    Longitude = vettee.Longitude,
                    CreatedTime = vettee.CreationTime,
                    CreatedUserId = _serviceHelper.GetCurrentUserId(),
                    VetGeoLink = vettee.VetGeoLink,
                    VetOwnerStatus = vettee.VetOwnerStatus,
                    VetPeriodOfstay = vettee.VetPeriodOfstay,
                    VetGuardians = vettee.VetGuardians,
                    VettersRemarks = vettee.VettersRemarks,
                    VetVideoUpload = vettee.VetVideoUpload,
                    VetPhotoUpload = vettee.VetPhotoUpload,
                    Landmark = vettee.Landmark
                };

            return vettees.AsNoTracking().ToPagedListAsync(page, size);
        }

        public IQueryable<VetteeList> GetAll()
        {
            return _repository.GetAll();
        }

        public async Task<VetteeListDTO> GetVetteeListById(Guid vetteeID)
        {
            var vettee = await _repository.GetAsync(vetteeID);

            if (vettee == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VETTEELIST_EXIST);
            }

            return new VetteeListDTO
            {
                Id = vettee.Id,
                TenantId = vettee.TenantId,
                ClientId = vettee.ClientId,
                ReferenceCode = vettee.ReferenceCode,
                VetteeLastName = vettee.VetteeLastName,
                VetteeFirstName = vettee.VetteeFirstName,
                VetteeOtherName = vettee.VetteeOtherName,
                VetServiceID = vettee.VetServiceID,
                PhoneNumber = vettee.PhoneNumber,
                Email = vettee.Email,
                Address = vettee.Address,
                Picture = GetImagebyRef(vettee.ReferenceCode),
                Amount = vettee.Amount,
                Vat = vettee.Vat,
                UnitUsed = vettee.UnitUsed,
                VetteeStatus = vettee.VetteeStatus,
                LoginDeviceType = vettee.LoginDeviceType,
                PaymentMethod = vettee.PaymentMethod,
                PaymentStatus = vettee.PaymentStatus,
                VetAddress = vettee.VetAddress,
                VetLatitude = vettee.VetLatitude,
                VetLongitude = vettee.VetLongitude,
                CouponCode = vettee.CouponCode,
                VetStartdate = vettee.VetStartdate,
                VetEnddate = vettee.VetEnddate,
                Latitude = vettee.Latitude,
                Longitude = vettee.Longitude,
                VetGeoLink = vettee.VetGeoLink,
                VetOwnerStatus = vettee.VetOwnerStatus,
                VetPeriodOfstay = vettee.VetPeriodOfstay,
                VetGuardians = vettee.VetGuardians,
                VettersRemarks = vettee.VettersRemarks,
                VetVideoUpload = vettee.VetVideoUpload,
                VetPhotoUpload = vettee.VetPhotoUpload,
                VetCity = vettee.VetCity,
                Landmark = vettee.Landmark

            };
        }


        public string GetImagebyRef(string refcode)
        {
            var imgUrl = "";

            var getimagefile = _imgRepo.GetAll()?.Where(x => x.FileReference == refcode).FirstOrDefault();
            if (getimagefile != null) {
                imgUrl = getimagefile.UploadPath;
            }

            return imgUrl ;
        }

        public Task<IPagedList<VetteeListDTO>> GetVetByClient(int clientId,int page, int size, string query = null)
        {
            var tenant = _serviceHelper.GetTenantId();
            //var Picture = GetImagebyRef("TAY20C78382");
            var vettees =
                from vettee in _repository.GetAll().Where(x => x.TenantId == tenant && x.ClientId == clientId)
        
                join vetService in _vetserviceRepo.GetAll() on vettee.VetServiceID equals vetService.Id
                into vetServices from vetService in vetServices.DefaultIfEmpty()
                join Customer in _customerInfoRepo.GetAllIncluding(x => x.User) on vettee.ClientId equals Customer.UserId
                into Customers from Customer in Customers.DefaultIfEmpty()

                orderby vettee.Id descending
                select new VetteeListDTO
                {
                    Id = vettee.Id,
                    TenantId = vettee.TenantId,
                    ClientId = vettee.ClientId,
                    ClientEmail = Customer.User.Email,
                    ClientName = Customer.User.LastName + " " + Customer.User.FirstName,
                    ReferenceCode = vettee.ReferenceCode,
                    VetteeLastName = vettee.VetteeLastName,
                    VetteeFirstName = vettee.VetteeFirstName,
                    VetteeOtherName = vettee.VetteeOtherName,
                    VetServiceID = vettee.VetServiceID,
                    VetService = vetService.Name,
                    PhoneNumber = vettee.PhoneNumber,
                    Email = vettee.Email,
                    Address = vettee.Address,
                    VetLatitude = vettee.Latitude,
                    VetLongitude = vettee.Longitude,
                    Picture = vettee.Picture,
                    Amount = vettee.Amount,
                    Vat = vettee.Vat,
                    UnitUsed = vettee.UnitUsed,
                    VetteeStatus = vettee.VetteeStatus,
                    LoginDeviceType = vettee.LoginDeviceType,
                    PaymentMethod = vettee.PaymentMethod,
                    PaymentStatus = vettee.PaymentStatus,
                    VetAddress = vettee.VetAddress,
                    CouponCode = vettee.CouponCode,
                    VetStartdate = vettee.VetStartdate,
                    VetEnddate = vettee.VetEnddate,
                    Latitude = vettee.Latitude,
                    Longitude = vettee.Longitude,
                    CreatedTime = vettee.CreationTime,
                    CreatedUserId = vettee.CreatorUserId,
                    VetGeoLink = vettee.VetGeoLink,
                    VetOwnerStatus = vettee.VetOwnerStatus,
                    VetPeriodOfstay = vettee.VetPeriodOfstay,
                    VetGuardians = vettee.VetGuardians,
                    VettersRemarks = vettee.VettersRemarks,
                    VetCity = vettee.VetCity,
                    VetVideoUpload = vettee.VetVideoUpload,
                    VetPhotoUpload = vettee.VetPhotoUpload,
                    Landmark = vettee.Landmark


                };

            return vettees.AsNoTracking().ToPagedListAsync(page, size);
        }

        public async Task UpdateVetteeList(Guid vetteeId, VetteeListDTO vetteeDto)
        {
            var vettee = await _repository.GetAsync(vetteeId);

            if (vettee is null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VETTEELIST_NOT_EXIST);
            }

            vettee.TenantId = vetteeDto.TenantId;
            vettee.ClientId = vetteeDto.ClientId;
            vettee.ReferenceCode = vetteeDto.ReferenceCode;
            vettee.VetteeLastName = vetteeDto.VetteeLastName;
            vettee.VetteeFirstName = vetteeDto.VetteeFirstName;
            vettee.VetteeOtherName = vetteeDto.VetteeOtherName;
            vettee.VetServiceID = vetteeDto.VetServiceID;
            vettee.PhoneNumber = vetteeDto.PhoneNumber;
            vettee.Email = vetteeDto.Email;
            vettee.Address = vetteeDto.Address;
            vettee.Picture = vetteeDto.Picture;
            vettee.Amount = vetteeDto.Amount;
            vettee.Vat = vetteeDto.Vat;
            vettee.UnitUsed = vetteeDto.UnitUsed;
            vettee.VetteeStatus = vetteeDto.VetteeStatus;
            vettee.LoginDeviceType = vetteeDto.LoginDeviceType;
            vettee.PaymentMethod = vetteeDto.PaymentMethod;
            vettee.PaymentStatus = vetteeDto.PaymentStatus;
            vettee.VetAddress = vetteeDto.VetAddress;
            vettee.VetLatitude = vetteeDto.VetLatitude;
            vettee.VetLongitude = vetteeDto.VetLongitude;
            vettee.VetStartdate = vetteeDto.VetStartdate;
            vettee.VetEnddate = vetteeDto.VetEnddate;
            vettee.Latitude = vetteeDto.Latitude;
            vettee.Longitude = vetteeDto.Longitude;
            vettee.VetGeoLink = vetteeDto.VetGeoLink;
            vettee.VetOwnerStatus = vetteeDto.VetOwnerStatus;
            vettee.VetPeriodOfstay = vetteeDto.VetPeriodOfstay;
            vettee.VetGuardians = vetteeDto.VetGuardians;
            vettee.VettersRemarks = vetteeDto.VettersRemarks;
            //vettee.VetVideoUpload = vetteeDto.VetVideoUpload;
            //vettee.VetPhotoUpload = vetteeDto.VetPhotoUpload;

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateVettersForm(Guid vetteeId, VetteeListDTO vetteeDto)
        {
            var vettee = await _repository.GetAsync(vetteeId);

            if (vettee is null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VETTEELIST_NOT_EXIST);
            }
            string Nrmfile = "";
            string Vidfile = "";
            if (!string.IsNullOrEmpty(vetteeDto.VetPhotoUpload) || !string.IsNullOrEmpty(vetteeDto.VetVideoUpload))
            {
                List<ImageFileDTO> imageFileDTO = new List<ImageFileDTO>();

                if (vetteeDto.LoginDeviceType == DeviceType.AdminWeb)
                {
                    foreach (var file in vetteeDto.PictureList)
                    {
                        if (vetteeDto.VetPhotoUpload == "Photo" && !file.ContentType.Contains("video"))
                        {
                            var txt = "-PHT-";
                            var ImageNameWitExt = Path.GetFileNameWithoutExtension(file.fileName);
                            Nrmfile = await InfoForImageB(txt, file.data, ImageNameWitExt, file.fileName, file.ContentType);
                        }
                        else { Nrmfile = vettee.VetPhotoUpload; }

                        if (vetteeDto.VetVideoUpload == "Video" && file.ContentType.Contains("video"))
                        {
                            var txt = "-VID-";
                            var ImageNameWitExt = Path.GetFileNameWithoutExtension(file.fileName);
                            Vidfile = await InfoForImageB(txt, file.data, ImageNameWitExt, file.fileName, file.ContentType);
                        }
                        else { Vidfile = vettee.VetVideoUpload; }

                    }
                }
                else if (vetteeDto.LoginDeviceType == DeviceType.Website || vetteeDto.LoginDeviceType == DeviceType.Android || vetteeDto.LoginDeviceType == DeviceType.iOS)
                {
                    if (!string.IsNullOrEmpty(vetteeDto.VetPhotoUpload))
                    {
                        var txt = "-PHT-";
                        vetteeDto.PhotoContent = vetteeDto.VetPhotoUpload;
                        Nrmfile = await InfoForImage(vetteeDto, txt);
                    }
                    else { Nrmfile = vettee.VetPhotoUpload; }
                    if (!string.IsNullOrEmpty(vetteeDto.VetVideoUpload))
                    {
                        var txt = "-VID-";
                        vetteeDto.PhotoContent = vetteeDto.VetVideoUpload;
                        Vidfile = await InfoForImage(vetteeDto, txt);
                    }
                    else { Vidfile = vettee.VetVideoUpload; }
                }
            }
            else { Nrmfile = vettee.VetPhotoUpload; Vidfile = vettee.VetVideoUpload; }



            vettee.VetAddress = vetteeDto.VetAddress;
            vettee.VetLatitude = vetteeDto.VetLatitude;
            vettee.VetLongitude = vetteeDto.VetLongitude;
            vettee.VetGeoLink = vetteeDto.VetGeoLink;
            vettee.VetOwnerStatus = vetteeDto.VetOwnerStatus;
            vettee.VetPeriodOfstay = vetteeDto.VetPeriodOfstay;
            vettee.VetGuardians = vetteeDto.VetGuardians;
            vettee.VettersRemarks = vetteeDto.VettersRemarks;
            vettee.VetVideoUpload = Vidfile;
            vettee.VetPhotoUpload = Nrmfile;

            await _unitOfWork.SaveChangesAsync();
        }



        public async Task<string> InfoForImage(VetteeListDTO model, string txt)
        {
            string pic = "";
            string Contype = "";
            string ext = "";
            var ImageNameWitExt = "";
            var ImageName = "";
            string fileName = "";

            List<ImageFileDTO> imageFileDTO = new List<ImageFileDTO>();
            if (!string.IsNullOrEmpty(model.PhotoContent))
            {
                pic = model.PhotoContent.Split(',').Last();
                Contype = model.PhotoContent.Split(new Char[] { ':', ';' })[1];
                ext = "." + model.PhotoContent.Split(new Char[] { '/', ';' })[1];

                ImageNameWitExt = DateTime.UtcNow.ToString("yymmssfff") + txt + CommonHelper.GenereateRandonAlphaNumeric().Substring(0, 3);
                ImageName = ImageNameWitExt + ext;
                ImageFileDTO imgdto = new ImageFileDTO();
                byte[] bytes = Convert.FromBase64String(pic);
                imgdto.data = bytes;
                imgdto.FilenameWithOutExt = ImageNameWitExt;
                imgdto.fileName = ImageName;
                imgdto.ContentType = Contype;
                imgdto.UploadType = UploadType.Image;
                imageFileDTO.Add(imgdto);
                imgdto.PictureList = imageFileDTO;

                fileName = await _utilityService.UplaodBytesToDigOc(imgdto);
            }

            return fileName;
        }


        public async Task<string> InfoForImageB(string txt, byte[] bytes, string ImageNameWitExt, string ImageName, string Contype)
        {
            string fileName = "";
            List<ImageFileDTO> imageFileDTO = new List<ImageFileDTO>();
            ImageFileDTO imgdto = new ImageFileDTO();
            imgdto.data = bytes;
            imgdto.FilenameWithOutExt = DateTime.UtcNow.ToString("yymmssfff") + txt + CommonHelper.GenereateRandonAlphaNumeric().Substring(0, 3);
            //imgdto.FilenameWithOutExt = ImageNameWitExt;
            imgdto.fileName = ImageName;
            imgdto.ContentType = Contype;
            imgdto.UploadType = UploadType.Image;
            imageFileDTO.Add(imgdto);
            imgdto.PictureList = imageFileDTO;
            fileName = await _utilityService.UplaodBytesToDigOc(imgdto);
            return fileName;
        }



        public async Task RemoveVetteeList(Guid vetteeId)
        {
            var vetteeList = await _repository.GetAsync(vetteeId);

            if (vetteeList == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VETTEELIST_NOT_EXIST);
            }

            _repository.Delete(vetteeList);

            await _unitOfWork.SaveChangesAsync();
        }


        #region VettersTransactions

        public async Task<VettersTransactionDTO> GetVetTransactionById(int vetterID)
        {
            var vetter = await _repositoryVetters.GetAsync(vetterID);

            if (vetter == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VETTEELIST_EXIST);
            }

            return new VettersTransactionDTO
            {
                Id = vetter.Id,
                TenantId = vetter.TenantId,
                VetteeListID = vetter.VetteeListID,
                ReferenceCode = vetter.ReferenceCode,
                VetStOne = vetter.VetStOne,
                VetStOneBy = vetter.VetStOneBy,
                VetStOneDate = vetter.VetStOneDate,
                VetStTwo = vetter.VetStTwo,
                VetStTwoBy = vetter.VetStTwoBy,
                VetStTwoDate = vetter.VetStTwoDate,
                VetStThree = vetter.VetStThree,
                VetStThreeBy = vetter.VetStThreeBy,
                VetStThreeDate = vetter.VetStThreeDate,
                VetStfour = vetter.VetStfour,
                VetStfourBy = vetter.VetStfourBy,
                VetStfourDate = vetter.VetStfourDate,
                VetStfive = vetter.VetStfive,
                VetStfiveBy = vetter.VetStfiveBy,
                VetStfiveDate = vetter.VetStfiveDate,
                LoginDeviceType = vetter.LoginDeviceType,
                Assigned = vetter.Assigned,
                AssignedBy = vetter.AssignedBy,
                //AssignedTO = vetter.AssignedTO,
                AssignedTO = (int?)vetter.CreatorUserId,
                AssignedDate = vetter.AssignedDate,
                Verified = vetter.Verified,
                VerifiedBy = vetter.VerifiedBy,
                VerifiedDate = vetter.VerifiedDate,
                Approved = vetter.Approved,
                ApprovedBy = vetter.ApprovedBy,
                ApprovedDate = vetter.ApprovedDate,
                Posted = vetter.Posted,
                PostedBy = vetter.PostedBy,
                PostedDate = vetter.PostedDate,
                Void = vetter.Void

            };
        }

        public IQueryable<VettersTransaction> GetAllVetTransaction()
        {
            return _repositoryVetters.GetAll();
        }

        public async Task AddVettrans(VettersTransactionDTO vettrans)
        {
            if (await _repositoryVetters.ExistAsync(v => v.Id == vettrans.Id))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VETSERVICE_NOT_EXIST);
            }

            var tenant = _serviceHelper.GetTenantId();
            _repositoryVetters.Insert(new VettersTransaction
            {
                TenantId = tenant,
                VetteeListID = vettrans.VetteeListID,
                ReferenceCode = vettrans.ReferenceCode,
                VetStOne = vettrans.VetStOne,
                VetStOneBy = vettrans.VetStOneBy,
                VetStOneDate = vettrans.VetStOneDate,
                VetStTwo = vettrans.VetStTwo,
                VetStTwoBy = vettrans.VetStTwoBy,
                VetStTwoDate = vettrans.VetStTwoDate,
                VetStThree = vettrans.VetStThree,
                VetStThreeBy = vettrans.VetStThreeBy,
                VetStThreeDate = vettrans.VetStThreeDate,
                VetStfour = vettrans.VetStfour,
                VetStfourBy = vettrans.VetStfourBy,
                VetStfourDate = vettrans.VetStfourDate,
                VetStfive = vettrans.VetStfive,
                VetStfiveBy = vettrans.VetStfiveBy,
                VetStfiveDate = vettrans.VetStfiveDate,
                LoginDeviceType = vettrans.LoginDeviceType,
                Assigned = vettrans.Assigned,
                AssignedBy = vettrans.AssignedBy,
                AssignedDate = vettrans.AssignedDate,
                Verified = vettrans.Verified,
                VerifiedBy = vettrans.VerifiedBy,
                VerifiedDate = vettrans.VerifiedDate,
                Approved = vettrans.Approved,
                ApprovedBy = vettrans.ApprovedBy,
                ApprovedDate = vettrans.ApprovedDate,
                Posted = vettrans.Posted,
                PostedBy = vettrans.PostedBy,
                PostedDate = vettrans.PostedDate,
                Void = vettrans.Void,
                CreatorUserId = _serviceHelper.GetCurrentUserId()

            }) ;

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateVetTransaction(int vetterId, VettersTransactionDTO vetterDto)
        {
            var vetter = await _repositoryVetters.GetAsync(vetterId);

            if (vetter is null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VETTEELIST_NOT_EXIST);
            }
            vetter.TenantId = _serviceHelper.GetTenantId();
            vetter.VetteeListID = vetterDto.VetteeListID;
            vetter.ReferenceCode = vetterDto.ReferenceCode;
            vetter.VetStOne = vetterDto.VetStOne;
            vetter.VetStOneBy = vetterDto.VetStOneBy;
            vetter.VetStOneDate = vetterDto.VetStOneDate;
            vetter.VetStTwo = vetterDto.VetStTwo;
            vetter.VetStTwoBy = vetterDto.VetStTwoBy;
            vetter.VetStTwoDate = vetterDto.VetStTwoDate;
            vetter.VetStThree = vetterDto.VetStThree;
            vetter.VetStThreeBy = vetterDto.VetStThreeBy;
            vetter.VetStThreeDate = vetterDto.VetStThreeDate;
            vetter.VetStfour = vetterDto.VetStfour;
            vetter.VetStfourBy = vetterDto.VetStfourBy;
            vetter.VetStfourDate = vetterDto.VetStfourDate;
            vetter.VetStfive = vetterDto.VetStfive;
            vetter.VetStfiveBy = vetterDto.VetStfiveBy;
            vetter.VetStfiveDate = vetterDto.VetStfiveDate;
            vetter.LoginDeviceType = vetterDto.LoginDeviceType;
            vetter.Assigned = vetterDto.Assigned;
            vetter.AssignedBy = vetterDto.AssignedBy;
            vetter.AssignedDate = vetterDto.AssignedDate;
            vetter.Verified = vetterDto.Verified;
            vetter.VerifiedBy = vetterDto.VerifiedBy;
            vetter.VerifiedDate = vetterDto.VerifiedDate;
            vetter.Approved = vetterDto.Approved;
            vetter.ApprovedBy = vetterDto.ApprovedBy;
            vetter.ApprovedDate = vetterDto.ApprovedDate;
            vetter.Posted = vetterDto.Posted;
            vetter.PostedBy = vetterDto.PostedBy;
            vetter.PostedDate = vetterDto.PostedDate;
            vetter.Void = vetterDto.Void;

            await _unitOfWork.SaveChangesAsync();
        }

        public Task<IPagedList<VettersTransactionDTO>> GetVetTransactionList(int page, int size, string searchTerm)
        {
            var tenant = _serviceHelper.GetTenantId();
            //var CusDetail = _customerInfoRepo.GetAllIncluding(x => x.User).FirstOrDefault(x => x.UserId == clientId);
            var vetters =
               from vetter in _repositoryVetters.GetAll()

               join vetteelist in _repository.GetAll() on vetter.VetteeListID equals vetteelist.Id 
               into vetteelists from vetteelist in vetteelists.DefaultIfEmpty()

               join vetService in _vetserviceRepo.GetAll() on vetteelist.VetServiceID equals vetService.Id 
               into vetServices from vetService in vetServices.DefaultIfEmpty()

               join Customer in _customerInfoRepo.GetAllIncluding(x => x.User) on vetteelist.ClientId equals Customer.UserId
             into Customers from Customer in Customers.DefaultIfEmpty()

               where vetteelist.TenantId == tenant

               && string.IsNullOrWhiteSpace(searchTerm) || vetter.ReferenceCode.Contains(searchTerm) ||
               vetter.ReferenceCode.Contains(searchTerm) || vetter.ReferenceCode.Contains(searchTerm)
               orderby vetter.Id descending

               select new VettersTransactionDTO
               {
                   Id = vetter.Id,
                   TenantId = vetter.TenantId,
                   VetteeListID = vetter.VetteeListID,
                   ReferenceCode = vetter.ReferenceCode,
                   ClientEmail = Customer.User.Email,
                   ClientName = Customer.User.LastName + " "+ Customer.User.FirstName,
                   VetServiceID = vetteelist.VetServiceID,
                   VetService = vetService.Name,
                   ToVetDaysCount = vetService.ToVetDaysCount,
                   CreatedTime = vetService.CreationTime,
                   VetStOne = vetter.VetStOne,
                   VetStOneBy = vetter.VetStOneBy,
                   VetStOneDate = vetter.VetStOneDate,
                   VetStTwo = vetter.VetStTwo,
                   VetStTwoBy = vetter.VetStTwoBy,
                   VetStTwoDate = vetter.VetStTwoDate,
                   VetStThree = vetter.VetStThree,
                   VetStThreeBy = vetter.VetStThreeBy,
                   VetStThreeDate = vetter.VetStThreeDate,
                   VetStfour = vetter.VetStfour,
                   VetStfourBy = vetter.VetStfourBy,
                   VetStfourDate = vetter.VetStfourDate,
                   VetStfive = vetter.VetStfive,
                   VetStfiveBy = vetter.VetStfiveBy,
                   VetStfiveDate = vetter.VetStfiveDate,
                   LoginDeviceType = vetter.LoginDeviceType,
                   Assigned = vetter.Assigned,
                   AssignedBy = vetter.AssignedBy,
                   AssignedTO = (int?)vetter.CreatorUserId,
                   AssignedDate = vetter.AssignedDate,
                   Verified = vetter.Verified,
                   VerifiedBy = vetter.VerifiedBy,
                   VerifiedDate = vetter.VerifiedDate,
                   Approved = vetter.Approved,
                   ApprovedBy = vetter.ApprovedBy,
                   ApprovedDate = vetter.ApprovedDate,
                   Posted = vetter.Posted,
                   PostedBy = vetter.PostedBy,
                   PostedDate = vetter.PostedDate,
                   Void = vetter.Void,
                   Picture = vetteelist.Picture,
                   PhoneNumber = vetteelist.PhoneNumber,
                   VetCreatedTime = vetteelist.CreationTime,
                   Address = vetteelist.Address,
                   ClientId = vetteelist.ClientId,
                   VetCity = vetteelist.VetCity,
                   Landmark = vetteelist.Landmark
               };

            return vetters.AsNoTracking().ToPagedListAsync(page, size);
        }


        public Task<IPagedList<VettersTransactionDTO>> AllVetAssigned(int UserId)
        {
            var tenant = _serviceHelper.GetTenantId();

            var vetters =
               from vetter in _repositoryVetters.GetAll()

               join vetteelist in _repository.GetAll() on vetter.VetteeListID equals vetteelist.Id
               into vetteelists
               from vetteelist in vetteelists.DefaultIfEmpty()

               join vetService in _vetserviceRepo.GetAll() on vetteelist.VetServiceID equals vetService.Id
               into vetServices
               from vetService in vetServices.DefaultIfEmpty()

               join Customer in _customerInfoRepo.GetAllIncluding(x => x.User) on vetteelist.ClientId equals Customer.UserId
             into Customers
               from Customer in Customers.DefaultIfEmpty()

               where vetteelist.TenantId == tenant && vetter.AssignedTO == (int?)UserId


               orderby vetter.Id descending

               select new VettersTransactionDTO
               {
                   Id = vetter.Id,
                   TenantId = vetter.TenantId,
                   VetteeListID = vetter.VetteeListID,
                   ReferenceCode = vetter.ReferenceCode,
                   ClientEmail = Customer.User.Email,
                   ClientName = Customer.User.LastName + " " + Customer.User.FirstName,
                   VetServiceID = vetteelist.VetServiceID,
                   VetService = vetService.Name,
                   ToVetDaysCount = vetService.ToVetDaysCount,
                   CreatedTime = vetService.CreationTime,
                   VetStOne = vetter.VetStOne,
                   VetStOneBy = vetter.VetStOneBy,
                   VetStOneDate = vetter.VetStOneDate,
                   VetStTwo = vetter.VetStTwo,
                   VetStTwoBy = vetter.VetStTwoBy,
                   VetStTwoDate = vetter.VetStTwoDate,
                   VetStThree = vetter.VetStThree,
                   VetStThreeBy = vetter.VetStThreeBy,
                   VetStThreeDate = vetter.VetStThreeDate,
                   VetStfour = vetter.VetStfour,
                   VetStfourBy = vetter.VetStfourBy,
                   VetStfourDate = vetter.VetStfourDate,
                   VetStfive = vetter.VetStfive,
                   VetStfiveBy = vetter.VetStfiveBy,
                   VetStfiveDate = vetter.VetStfiveDate,
                   LoginDeviceType = vetter.LoginDeviceType,
                   Assigned = vetter.Assigned,
                   AssignedBy = vetter.AssignedBy,
                   //AssignedTO = vetter.AssignedTO,
                   AssignedTO = (int?)vetter.CreatorUserId,
                   AssignedDate = vetter.AssignedDate,
                   Verified = vetter.Verified,
                   VerifiedBy = vetter.VerifiedBy,
                   VerifiedDate = vetter.VerifiedDate,
                   Approved = vetter.Approved,
                   ApprovedBy = vetter.ApprovedBy,
                   ApprovedDate = vetter.ApprovedDate,
                   Posted = vetter.Posted,
                   PostedBy = vetter.PostedBy,
                   PostedDate = vetter.PostedDate,
                   Void = vetter.Void,
                   Picture = vetteelist.Picture,
                   PhoneNumber = vetteelist.PhoneNumber,
                   VetCreatedTime = vetteelist.CreationTime,
                   Address = vetteelist.Address,
                   ClientId = vetteelist.ClientId


               };

            return vetters.AsNoTracking().ToPagedListAsync(1, 50000);
        }

        
        public Task<List<VettersDistinctDTO>> GetDistinctVettrans()
        {
            var tenant = _serviceHelper.GetTenantId();
            var vetters =
               (from vetter in _repository.GetAll()
               join Customer in _customerInfoRepo.GetAllIncluding(x => x.User) on vetter.ClientId equals Customer.UserId
             into Customers
               from Customer in Customers.DefaultIfEmpty()

               where vetter.TenantId == tenant && vetter.PaymentStatus == PaymentStatus.Paid && vetter.VetteeStatus != VetStatus.Concluded

                orderby vetter.Id descending

               select new VettersDistinctDTO
               {
                   ClientEmail = Customer.User.Email,
                   ClientName = Customer.User.LastName + " " + Customer.User.FirstName,             
                   ClientId = vetter.ClientId,
                   ClientPhoneNumber = Customer.User.PhoneNumber,
                   ClientPicture = Customer.User.Photo,
                   TenantId = vetter.TenantId
               }).Distinct();

            return Task.FromResult(vetters.ToList());
        }

        public async Task<bool> VetterActionTo(ActionDto model)
        {

            foreach (var item in model.modellist)
            {
                var vettertrans = _repositoryVetters.Get(Convert.ToInt32(item));
                var vetList = _repository.Get(vettertrans.VetteeListID);

                if (model.ActionType == VetterStatus.Assigned)
                {               
                    var vvv = _employeeService.GetAll().Where(x => x.Id == model.EmployeeId).FirstOrDefault();
                    vettertrans.VetStOne = VetterStatus.Assigned;
                    vettertrans.VetStOneDate = DateTime.Now;
                    vettertrans.VetStOneBy = _serviceHelper.GetCurrentUserId();

                    vettertrans.Assigned = true;
                    vettertrans.AssignedDate = DateTime.Now;
                    vettertrans.AssignedBy = _serviceHelper.GetCurrentUserId();
                    //Both below are doing the same thing
                    vettertrans.AssignedTO = int.Parse(vvv.UserId.ToString());
                    vettertrans.CreatorUserId = int.Parse(vvv.UserId.ToString());
                }
                else if (model.ActionType == VetterStatus.Accepted)
                {
                    vettertrans.VetStTwo = VetterStatus.Accepted;
                    vettertrans.VetStTwoDate = DateTime.Now;
                    vettertrans.VetStTwoBy = _serviceHelper.GetCurrentUserId();
                }
                else if(model.ActionType == VetterStatus.InTransit)
                {
                    vettertrans.VetStThree = VetterStatus.InTransit;
                    vettertrans.VetStThreeDate = DateTime.Now;
                    vettertrans.VetStThreeBy = _serviceHelper.GetCurrentUserId();
                    vetList.VetteeStatus = VetStatus.InTransit;
                }
                else if(model.ActionType == VetterStatus.Available)
                {
                    vettertrans.VetStfour = VetterStatus.Available;                
                    vettertrans.VetStfourDate = DateTime.Now;
                    vettertrans.VetStfiveBy = _serviceHelper.GetCurrentUserId();
                    vetList.VetteeStatus = VetStatus.Available;

                }
                else if(model.ActionType == VetterStatus.Concluded)
                {
                    vettertrans.VetStfive = VetterStatus.Concluded;                 
                    vettertrans.VetStfiveDate = DateTime.Now;
                    vettertrans.VetStfiveBy = _serviceHelper.GetCurrentUserId();
                    vetList.VetteeStatus = VetStatus.Concluded;

                }

            }

            await _unitOfWork.SaveChangesAsync();

            return true;
        }
        #endregion
    }
}
