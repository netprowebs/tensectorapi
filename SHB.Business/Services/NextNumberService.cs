﻿using IPagedList;
using Microsoft.EntityFrameworkCore;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Business.Services
{

    public interface INextNumberService
    {
        #region NextNumberHeader
        Task AddNextNumber(NextNumberDTO model);
        Task<NextNumberDTO> GetNextNumber(int Id);

        Task<string> GetTenantNextNumber(String NextName);

        Task<List<NextNumberDTO>> GetAllNextNumbers(string Name);
        Task<bool> UpdateNextNumberById(NextNumberDTO model, int Id);
        Task DeleteNextNumber(int Id);
        #endregion
    }

    public class NextNumberService : INextNumberService
    {
        private readonly IRepository<NextNumber> _NextNumberRepo;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;

        public NextNumberService(IRepository<NextNumber> NextNumber,
            IServiceHelper serviceHelper, IUnitOfWork unitOfWork)
        {

            _NextNumberRepo = NextNumber;
            _serviceHelper = serviceHelper;
            _unitOfWork = unitOfWork;
        }

        public async Task AddNextNumber(NextNumberDTO model)
        {

            if (await _NextNumberRepo.ExistAsync(v => v.Id == model.Id))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.NextNumber_EXIST);
            }

            _NextNumberRepo.Insert(new NextNumber
            {
                Id = model.Id,
                NextNumberName = model.NextNumberName,
                NextNumberValue = model.NextNumberValue,
                NextNumberPrefix = model.NextNumberPrefix,
                NextNumberSeparator = model.NextNumberSeparator,
                CompanyId = model.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();
            //throw new NotImplementedException();
        }



        #region NextNumberHeader
 
        public async Task<List<NextNumberDTO>> GetAllNextNumbers(string Name)
        {
            try
            {
                var NextNumber = (from NextNumbers in _NextNumberRepo.GetAll()
                                where NextNumbers.CompanyId == _serviceHelper.GetTenantId() && NextNumbers.NextNumberName == Name
                                  orderby NextNumbers.NextNumberValue descending
                                select new NextNumberDTO
                                {
                                    Id = NextNumbers.Id,
                                    NextNumberName = NextNumbers.NextNumberName,
                                    NextNumberValue = NextNumbers.NextNumberValue,
                                    NextNumberPrefix = NextNumbers.NextNumberPrefix,
                                    NextNumberSeparator = NextNumbers.NextNumberSeparator,
                                      CompanyId = NextNumbers.CompanyId

                                } );

                return await NextNumber.ToListAsync();
            }
            catch (Exception ex)
            {
                return null;
            }

        }


        public async Task<NextNumberDTO> GetNextNumber(int Id)
        {
            var NextNumbers = await _NextNumberRepo.GetAsync(Id);

            if (NextNumbers == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.NextNumber_NOT_EXIST);
            }

            return new NextNumberDTO
            {
                Id = NextNumbers.Id,
                NextNumberName = NextNumbers.NextNumberName,
                NextNumberValue = NextNumbers.NextNumberValue,
                NextNumberPrefix = NextNumbers.NextNumberPrefix,
                NextNumberSeparator = NextNumbers.NextNumberSeparator,
                 CompanyId = NextNumbers.CompanyId
            };
        }

        public async Task<bool> UpdateNextNumberById(NextNumberDTO NextNumbers, int Id)
        {
            try
            {
                if (NextNumbers == null) { return false; }
                var NextNumber = await _NextNumberRepo.GetAsync(Id);
         

                NextNumber.NextNumberName = NextNumbers.NextNumberName;
                NextNumber.NextNumberValue = NextNumbers.NextNumberValue;
                NextNumber.NextNumberPrefix = NextNumbers.NextNumberPrefix;
                NextNumber.NextNumberSeparator = NextNumbers.NextNumberSeparator;

                await _NextNumberRepo.UpdateAsync(NextNumber);
                await _unitOfWork.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public async Task DeleteNextNumber(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _NextNumberRepo.DeleteAsync(_NextNumberRepo.Get(Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            //throw new NotImplementedException();
        }

        public async Task<string> GetTenantNextNumber(string NextName)
        {
            var TenantId = _serviceHelper.GetTenantId();
            var UserId = _serviceHelper.GetCurrentUserId();

            var Nextinfo = GetAllNextNumbers(NextName).Result.FirstOrDefault();
            var nextvalue = Nextinfo?.NextNumberValue ?? "0";
            var number = long.Parse(nextvalue) + 1;
            var numberStr = number.ToString("0000000");

            NextNumberDTO nextNumberDTO = new NextNumberDTO();
            nextNumberDTO.NextNumberValue = numberStr;
            nextNumberDTO.CreatorUserId = UserId;
            nextNumberDTO.CompanyId = TenantId;
            nextNumberDTO.NextNumberPrefix = Nextinfo?.NextNumberPrefix ;
            nextNumberDTO.NextNumberSeparator = Nextinfo?.NextNumberSeparator;
            nextNumberDTO.NextNumberName = Nextinfo?.NextNumberName;

            await AddNextNumber(nextNumberDTO);

            var otp =   Nextinfo.NextNumberPrefix + Nextinfo.NextNumberSeparator + numberStr;
            return otp;
        }
        #endregion NextNumber





    }
}


