﻿using IPagedList;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SHB.Core.Domain.Entities;
using System;

namespace SHB.Business.Services
{
    public interface IVehicleMakeService
    {
        Task<IPagedList<VehicleMakeDTO>> GetVehicleMakes(int page, int size, string query = null);
        Task<VehicleMakeDTO> GetVehicleMakeById(int vehicleMakeId);
        Task AddVehicleMake(VehicleMakeDTO vehicleMake);
        Task UpdateVehicleMake(int vehicleMakeId, VehicleMakeDTO vehicleMake);
        Task RemoveVehicleMake(int vehicleMakeId);
    }

    public class VehicleMakeService : IVehicleMakeService
    {
        private readonly IRepository<VehicleMake> _vehicleMakeRepo;
        private readonly IRepository<Route> _routeRepo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceHelper _serviceHelper;

        public VehicleMakeService(
            IRepository<VehicleMake> vehicleMakeRepo,
            IRepository<Route> routeRepo,
            IUnitOfWork unitOfWork,
            IServiceHelper serviceHelper)
        {
            _vehicleMakeRepo = vehicleMakeRepo;
            _routeRepo = routeRepo;
            _unitOfWork = unitOfWork;
            _serviceHelper = serviceHelper;
        }

        private async Task<bool> IsValidRoute(int routeId)
        {
            return routeId > 0 &&
                 await _routeRepo.ExistAsync(m => m.Id == routeId);
        }

        public async Task AddVehicleMake(VehicleMakeDTO vehicleMakeDto)
        {
            //if (!await IsValidRoute(vehicleMakeDto.RouteId))
            //{
            //    throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ROUTE_NOT_EXIST);
            //}


            //fareDto.Route.Name = fareDto.Route.Name.Trim();

            if (await _vehicleMakeRepo.ExistAsync(v => v.Id == vehicleMakeDto.Id))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VEHICLE_MAKE_EXIST);
            }

            _vehicleMakeRepo.Insert(new VehicleMake
            {
                Id = vehicleMakeDto.Id,
                Name = vehicleMakeDto.Name,
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<VehicleMakeDTO> GetVehicleMakeById(int vehicleMakeId)
        {
            var vehicleMake = await _vehicleMakeRepo.GetAsync(vehicleMakeId);

            if (vehicleMake == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VEHICLE_MAKE_NOT_EXIST);
            }

            return new VehicleMakeDTO
            {
                Id = vehicleMake.Id,
                Name = vehicleMake.Name
            };
        }

        public Task<IPagedList<VehicleMakeDTO>> GetVehicleMakes(int page, int size, string query = null)
        {
            var vehicleMakes =
                from vehicleMake in _vehicleMakeRepo.GetAll()
                //join terminal in _terminalRepo.GetAll() on subRoute.DepartureTerminalId equals terminal.Id
                orderby vehicleMake.Id descending
               // where string.IsNullOrWhiteSpace(query) || trip.Name.Contains(query)
                select new VehicleMakeDTO
                {
                    Id = vehicleMake.Id,
                    Name = vehicleMake.Name
                };

            return vehicleMakes.AsNoTracking().ToPagedListAsync(page, size);
        }

        public async Task RemoveVehicleMake(int vehicleMakeId)
        {
            var vehicleMake = await _vehicleMakeRepo.GetAsync(vehicleMakeId);

            if (vehicleMake == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VEHICLE_MAKE_NOT_EXIST);
            }

            _vehicleMakeRepo.Delete(vehicleMake);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateVehicleMake(int vehicleMakeId, VehicleMakeDTO vehicleMake)
        {
            var vehicleMakes = await _vehicleMakeRepo.GetAsync(vehicleMakeId);

            if (vehicleMakes == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VEHICLE_MAKE_NOT_EXIST);
            }

            await _unitOfWork.SaveChangesAsync();
        }
    }
}
