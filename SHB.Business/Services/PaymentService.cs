﻿using IPagedList;
using LME.Core.Domain.DataTransferObjects;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RestSharp;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Core.Utils;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using SHB.WebApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using PayStack.Net;
using SHB.Core.Configuration;
using Microsoft.Extensions.Options;
using SHB.Core.Entities.Enums;
using System.IO;
using SHB.Business.Messaging.Email;
using SHB.Business.Messaging.Sms;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;

namespace SHB.Business.Services
{
    public interface IPaymentService
    {

        #region ProcessPayment
        Task<VetPayResponse> ProcessVetPayment(List<string> refcode);
        Task<WalletTransactionDTO> CreditWallet(PayResponseModel model);

        #endregion

        #region PayFromWallet  
        Task<IPagedList<WalletDTO>> GetWallet(int page, int size, string search = null);
        Task<WalletDTO> GetWalletById(int walletId);
        Task AddWallet(WalletDTO wallet);
        Task UpdateWallet(int walletId, WalletDTO Wallet);
        Task RemoveWallet(int walletId);

        #endregion

        #region WalletTransaction

        Task<IPagedList<WalletTransactionDTO>> GetWalletTransaction(int page, int size, string search = null);
        Task<WalletTransactionDTO> GetWalletTransactionById(Guid WalletTransactionId);
        Task AddWalletTransaction(WalletTransactionDTO WalletTransaction);
        Task UpdateWalletTransaction(Guid WalletTransactionId, WalletTransactionDTO WalletTransaction);
        Task RemoveWalletTransaction(Guid WalletTransactionId);
        Task<IPagedList<WalletTransactionDTO>> GetWalletByUserId(int UserId, int page, int size, string query = null);

        #endregion
    }



    public class PaymentService : IPaymentService
    {
        private readonly IRepository<Wallet> _repo;
        private readonly IRepository<WalletTransaction, Guid> _repoWalletTran;
        private readonly IUserService _userService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceHelper _serviceHelper;
        private readonly AppConfig appConfig;
        private readonly PaymentConfig.Paystack payStackConfig;
        private readonly IRepository<VetteeList, Guid> _vetteerepo;
        private readonly IMailService _mailSvc;
        private readonly ISMSService _smsSvc;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IVetTransactService _vetTransactService;
        private readonly IRepository<VetBundles> _vetbundlesRepo;
        private readonly IRepository<VetService> _vetserviceRepo;
        private readonly UserManager<User> _userManager;

        public PaymentService(IRepository<Wallet> repo, IRepository<WalletTransaction, Guid> repoWalletTran,
            IUserService userService, IOptions<PaymentConfig.Paystack> _payStackConfig, IOptions<AppConfig> _appConfig,
            IUnitOfWork unitOfWork, IServiceHelper serviceHelper, IRepository<VetteeList, Guid> vetteerepo,
            IMailService mailSvc, ISMSService smsSvc, IHostingEnvironment hostingEnvironment,  UserManager<User> userManager,
            IVetTransactService vetTransactService, IRepository<VetService> vetserviceRepo, IRepository<VetBundles> vetbundlesRepo)
        {
            _repo = repo;
            _repoWalletTran = repoWalletTran;
            _userService = userService;
            _unitOfWork = unitOfWork;
            _serviceHelper = serviceHelper;
            appConfig = _appConfig.Value;
            payStackConfig = _payStackConfig.Value;
            _vetteerepo = vetteerepo;
            _mailSvc = mailSvc;
            _smsSvc = smsSvc;
            _hostingEnvironment = hostingEnvironment;
            _vetTransactService = vetTransactService;
            _vetserviceRepo = vetserviceRepo;
            _vetbundlesRepo = vetbundlesRepo;
            _userManager = userManager;
        }


        #region ProcessPayment

        public async Task<WalletTransactionDTO> CreditWallet(PayResponseModel model)
        {
            WalletTransactionDTO WalTrans = new WalletTransactionDTO();
            var transref = "";
            var CalcAmount = 0.0;
            var Result = new VetPayResponse();
            if ( model.PaymentMethod == PaymentMethod.PayStack) 
            {
                 
                Result =  await ProcessPayStack(model);
                transref = Result.PaymentGatewayReference;
                if (Result == null)
                {
                    throw await _serviceHelper.GetExceptionAsync("Payment Not Found On Paystack");
                }
            }
            else if (model.PaymentMethod == PaymentMethod.Cash || model.PaymentMethod == PaymentMethod.Pos || model.PaymentMethod == PaymentMethod.CashAndPos || model.PaymentMethod == PaymentMethod.Transfer)
            {
                transref = Guid.NewGuid().ToString().Remove(8).ToUpper();
                
            }



            try 
            {
                var GetUser = await _userService.FindFirstAsync(c => c.Email == model.email);
                if (GetUser == null)
                {
                    throw await _serviceHelper.GetExceptionAsync(ErrorConstants.USER_ACCOUNT_EXISTS);                  
                }
                else
                {
                    if (!string.IsNullOrEmpty(model.BundleType.ToString()))
                    {
                        //condition to calculate bundle type 
                        var Bundles = _vetbundlesRepo.GetAll().Where(x => x.Id == model.BundleType).FirstOrDefault();

                        if (Bundles == null)
                        {
                            throw await _serviceHelper.GetExceptionAsync("VetBundle does not exist");
                        }
                        //divide naira amount with bundle amount 
                         CalcAmount = Math.Ceiling(Convert.ToDouble(model.amount) / Convert.ToDouble(Bundles.Amount));
                    }
                    else
                    {
                        CalcAmount = model.amount;
                    }
                   
                    //get wallet unit balance of clients
                    var ClientWallet = _repo.GetAll().Where(x => x.Id == GetUser.WalletId1).FirstOrDefault();

                    //insert into wallettransaction
                    WalTrans.TransactionType = TransactionType.Credit;
                    WalTrans.TransactedBy = _serviceHelper.GetCurrentUserId().ToString();
                    WalTrans.TransactionAmount = Convert.ToDecimal(CalcAmount);
                    WalTrans.TransDescription = "Credited Amount for: " + model.email + "   ";
                    WalTrans.TransactionDate = DateTime.Now;
                   
                    WalTrans.WalletId = ClientWallet.Id;
                    WalTrans.PayTypeDiscription = PayTypeDescription.WalletUpdate;
                    WalTrans.IsSum = true;
                    WalTrans.TenantId = _serviceHelper.GetTenantId();
                    WalTrans.TransCode = transref;
                    WalTrans.AmountType = FareAdjustmentType.Value;
                    WalTrans.LineBalance = model.amount;
                    
                    var result = AddWalletTransaction(WalTrans);

                    //Update existing wallet balance with the existing bal
                    var Walbal = ClientWallet.Balance + Convert.ToDecimal(CalcAmount);
                    ClientWallet.Balance = Walbal;

                    //save all to database
                    await _unitOfWork.SaveChangesAsync();

                    //Send Mail 
                    //string emailmessage = $"Dear {model.email}, Your Acount has been Credited with  500 unit with paid amount of {model.amount}";
                    //// " New account activation code"
                    //var mail = new Mail(appConfig.seclotEmail,"Seclot Credit Alert", "shextem2008@gmail.com")
                    //{
                    //    BodyIsFile = true,
                    //    Body= emailmessage,
                    //    Sender = appConfig.seclotEmail,
                    //    SenderDisplayName = appConfig.sender_name,
                    //    //To = model.email
                    //    //BodyPath = Path.Combine(_hostingEnvironment.ContentRootPath, CoreConstants.Url.BookingSuccessEmail)
                    //};

                    //await _mailSvc.UseMailJet(mail,model.email,appConfig.MJ_APIKEY_PUBLIC, appConfig.MJ_APIKEY_PUBLIC);

                    //Send Message/*"shextem2008@gmail.com"*/
                    //string message = $"Dear {model.email}, Your Acount has been Credited with  500 unit with paid amount of {model.amount}";
                    //_smsSvc.SendSMSNow(message, recipient: user.PhoneNumber.ToNigeriaMobile());
                }

            }
            catch (Exception ex) 
            {
            }
            return WalTrans;
        }

        public async Task<VetPayResponse> ProcessVetPayment(List<string> refcode)
        {
            VetService vetserv = null;
            VetteeList VetteeDetails = null;
            VetPayResponse vetPayResponse = new VetPayResponse();
            try
            {
                //_unitOfWork.BeginTransaction();

                foreach (var item in refcode)
                {
                    VetteeDetails = _vetteerepo.GetAll().Where(x => x.ReferenceCode == item.ToString() && x.PaymentStatus != PaymentStatus.Paid).FirstOrDefault();
                    var transref = Guid.NewGuid().ToString().Remove(8).ToUpper();

                    if (VetteeDetails == null)
                    {
                        return new VetPayResponse() { Response = "vet has already been paid for" };
                        /*break*/
                    }

                    //get wallet unit balance of clients
                    var GetUser = await _userService.FindFirstAsync(c => c.Id == VetteeDetails.ClientId);
                    var ClientWallet = _repo.GetAll().Where(x => x.Id == GetUser.WalletId1).FirstOrDefault();

                    if (ClientWallet.Balance >= VetteeDetails.UnitUsed)
                    {
                        //get vetservice detail 
                        vetserv = _vetserviceRepo.GetAll().Where(x => x.Id == VetteeDetails.VetServiceID).FirstOrDefault();
                        //insert total units amount deduction into wallet transaction (genereate tranref)
                        WalletTransactionDTO WalTrans = new WalletTransactionDTO();

                        WalTrans.TransactionType = TransactionType.Debit;
                        WalTrans.TransactedBy = VetteeDetails.ClientId.ToString();
                        //WalTrans.TransactionSourceId = VetteeDetails.ClientId;
                        WalTrans.TransactionAmount = VetteeDetails.UnitUsed;
                        WalTrans.TransDescription = "Vet Unit Payment for: " + VetteeDetails.VetteeFirstName + "  " + VetteeDetails.VetteeLastName + " ";
                        WalTrans.TransactionDate = DateTime.Now;
                        WalTrans.WalletId = ClientWallet.Id;
                        WalTrans.PayTypeDiscription = PayTypeDescription.WalletDeduction;
                        WalTrans.IsSum = true;
                        WalTrans.TenantId = VetteeDetails.TenantId;
                        WalTrans.TransCode = VetteeDetails.ReferenceCode;
                        WalTrans.AmountType = FareAdjustmentType.Unit;
                        //WalTrans.TransactionSourceId = Guid.NewGuid();
                          await AddWalletTransaction(WalTrans);

                        //remove unit from Total unit from wallet balance then update wallet
                        var Walbal = ClientWallet.Balance - vetserv.CostInUnit;
                        ClientWallet.Balance = Walbal;
                        //do condition for it has couponcode ......

                        //Update for each transref with transref , the paystatus,vetstatus
                        VetteeDetails.PaymentStatus = PaymentStatus.Paid;
                        VetteeDetails.VetteeStatus = VetStatus.VetInprogress;
                        VetteeDetails.VetStartdate = DateTime.Now;
                        VetteeDetails.VetEnddate = DateTime.Now.AddDays(vetserv.ToVetDaysCount);
                        VetteeDetails.UnitUsed = vetserv.CostInUnit;

                        //Insert Into Vet transaction after Wallet payment or deduction is succesfull
                        VettersTransactionDTO vettrans = new VettersTransactionDTO();
                        vettrans.ReferenceCode = VetteeDetails.ReferenceCode;
                        vettrans.VetteeListID = VetteeDetails.Id;
                        vettrans.VetStOne = VetterStatus.Pending;
                        vettrans.LoginDeviceType = VetteeDetails.LoginDeviceType;
                        

                        await _vetTransactService.AddVettrans(vettrans);

                        
                        //Send Mail 

                        //Send Message

                        //_unitOfWork.Commit();

                        vetPayResponse.Response = "Wallet Payment is successfull";
                        vetPayResponse.Amount = vetserv.CostInUnit;
                        vetPayResponse.VetteeStatus = VetteeDetails.VetteeStatus;
                        //RefCode = VetteeDetails.ReferenceCode,
                        vetPayResponse.PaymentMethod = PaymentMethod.Wallet;
                        vetPayResponse.ClientId = VetteeDetails.ClientId;
                        vetPayResponse.PaymentStatus = PaymentStatus.Paid.ToString();


                    }


                    else
                    {
                        //_unitOfWork.Rollback();
                        throw await _serviceHelper.GetExceptionAsync("You do not have enough unit balance in your wallet");

                    }
                }

            }

            catch (Exception ex)
            {

            }

            return vetPayResponse;
        }



        public async Task<VetPayResponse> ProcessPayStack(PayResponseModel model)
        {
            PaymentResponse paystackPaymentResponse = new PaymentResponse();
            if (!string.IsNullOrEmpty(model.PaymentGatewayReference)) { model.RefCode = model.PaymentGatewayReference; }
            PaystackVerifyResponseDto paystackVerifyResponseDto = null;
            try
            {
                #region newPaystackVerifyImplementation
                var client = new RestClient("https://api.paystack.co/transaction/verify/" + model.RefCode);
                var request = new RestRequest(Method.GET);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("Authorization", "Bearer " + payStackConfig.Secret);
                IRestResponse response = await client.ExecuteTaskAsync(request);
                var payResponse = response.Content;
                paystackVerifyResponseDto = JsonConvert.DeserializeObject<PaystackVerifyResponseDto>(response.Content);
                #endregion

            }
            catch (Exception ex)
            {
                var x = 1;
                //do nothing
            }

            var responseData = paystackVerifyResponseDto?.data;
            var authorization = responseData.authorization;
             paystackPaymentResponse = new PaymentResponse
            {
                Reference = responseData.reference,
                ApprovedAmount = responseData.amount / 100,
                AuthorizationCode = authorization.authorization_code,
                CardType = authorization.card_type,
                Last4 = authorization.last4,
                Reusable = authorization.reusable,
                Bank = authorization.bank,
                ExpireMonth = authorization.exp_month,
                ExpireYear = authorization.exp_year,
                TransactionDate = responseData.transaction_date.GetValueOrDefault(),
                Channel = responseData.channel,
                Status = responseData.status

            };

            await _unitOfWork.SaveChangesAsync();

            try
            {
                //if (paystackVerifyResponseDto.data.status.ToLower() != "success"){}
                //if (paystackPaymentResponse.Status.ToLower() == "ongoing"){}
                //await CreditWallet(model);
            }
            catch (Exception)
            {

            }
            VetPayResponse VetResponseData = new VetPayResponse
            {
                Response = "Wallet Payment is successfull",
                //PaymentStatus = paystackVerifyResponseDto.data.status.ToLower()  ? PaymentStatus.Approved : PaymentStatus.Pending,
                PaymentStatus = responseData.status,
                Amount = responseData.amount,
                PaymentGatewayReference=responseData.reference
            };

            return VetResponseData;
        }


        #endregion
        #region Wallet
        public Task<IPagedList<WalletDTO>> GetWallet(int page, int size, string search)
        {
            var wallet = from wallets in _repo.GetAll()
                         where string.IsNullOrWhiteSpace(search) || wallets.WalletNumber.Contains(search)
                         orderby wallets.CreationTime descending
                         select new WalletDTO
                         {
                             Id = wallets.Id,
                             Balance = wallets.Balance,
                             WalletNumber = wallets.WalletNumber,
                             IsReset = wallets.IsReset,
                             LastResetDate = wallets.LastResetDate,
                             UserId = wallets.UserId,
                             CreationTime = wallets.CreationTime
                         };

            return wallet.AsNoTracking().ToPagedListAsync(page, size);
        }

        public async Task<WalletDTO> GetWalletById(int walletId)
        {
            var wallets = await _repo.GetAsync(walletId);

            if (wallets is null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.WALLET_NOT_EXIST);
            }

            return new WalletDTO
            {
                Id = wallets.Id,
                Balance = wallets.Balance,
                WalletNumber = wallets.WalletNumber,
                IsReset = wallets.IsReset,
                LastResetDate = wallets.LastResetDate,
                UserId = wallets.UserId,
                CreationTime = wallets.CreationTime

            };
        }

        public async Task AddWallet(WalletDTO walletDto)
        {
            walletDto.WalletNumber = walletDto.WalletNumber.Trim();

            var wallets = walletDto.WalletNumber.ToLower();

            if (await _repo.ExistAsync(v => v.WalletNumber.ToLower() == wallets))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.WALLET_NOT_EXIST);
            }

            _repo.Insert(new Wallet
            {
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                Balance = walletDto.Balance,
                WalletNumber = walletDto.WalletNumber,
                IsReset = walletDto.IsReset,
                LastResetDate = walletDto.LastResetDate,
                UserId = walletDto.UserId,
                CreationTime = walletDto.CreationTime,
                TenantId = _serviceHelper.GetTenantId()
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task RemoveWallet(int walletId)
        {
            var wallet = await _repo.GetAsync(walletId);

            if (walletId == 0)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.WALLET_NOT_EXIST);
            }

            _repo.Delete(wallet);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateWallet(int walletId, WalletDTO walletDto)
        {
            var wallet = await _repo.GetAsync(walletId);

            if (walletDto is null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.WALLET_NOT_EXIST);
            }
            wallet.WalletNumber = walletDto.WalletNumber.Trim();
            wallet.Balance = walletDto.Balance;
            wallet.WalletNumber = walletDto.WalletNumber;
            wallet.IsReset = walletDto.IsReset;
            wallet.LastResetDate = walletDto.LastResetDate;
            wallet.UserId = walletDto.UserId;
            await _unitOfWork.SaveChangesAsync();
        }


        #endregion

        #region WalletTransaction
        //public async Task<List<User>> GetUsersAsync()
        //{
        //    using (var context = new Contes())
        //    {
        //        return await context.Users.ToListAsync();
        //    }
        //}
        public Task<IPagedList<WalletTransactionDTO>> GetWalletTransaction(int page, int size, string search)
        {
       

            var walletTransactions = from service in _repoWalletTran.GetAll()
                                     where string.IsNullOrWhiteSpace(search) || service.TransCode.Contains(search)
                                     orderby service.CreationTime descending
                                     select new WalletTransactionDTO
                                     {
                                         Id = service.Id,
                                         TransCode = service.TransCode,
                                         TransDescription = service.TransDescription,
                                         TransactionAmount = service.TransactionAmount,
                                         LineBalance = service.LineBalance,
                                         TransactedBy = service.TransactedBy,
                                         TenantId = service.TenantId,                               
                                         CreatorUserId = (int)service.CreatorUserId,
                                         TransactionType = service.TransactionType,
                                         TransactionDate = service.TransactionDate,
                                         WalletId = service.WalletId,
                                         PayTypeDiscription = service.PayTypeDiscription,
                                         AmountType = service.AmountType,
                                         CreationTime = service.CreationTime,

                                     };

            return walletTransactions.AsNoTracking().ToPagedListAsync(page, size);
        }

        public async Task<WalletTransactionDTO> GetWalletTransactionById(Guid walletTransId)
        {
            var wallettrans = await _repoWalletTran.GetAsync(walletTransId);

            if (wallettrans is null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.WALLETTRANSACTION_NOT_EXIST);
            }

            return new WalletTransactionDTO
            {
                Id = wallettrans.Id,
                TransCode = wallettrans.TransCode,
                TransDescription = wallettrans.TransDescription,
                TransactionAmount = wallettrans.TransactionAmount,
                TenantId = wallettrans.TenantId,
                CreationTime = wallettrans.CreationTime,

            };
        }

        public async Task AddWalletTransaction(WalletTransactionDTO walletTransactionDto)
        {
            walletTransactionDto.TransCode = walletTransactionDto.TransCode.Trim();

            var walletTransCode = walletTransactionDto.TransCode.ToLower();

            if (await _repoWalletTran.ExistAsync(v => v.TransCode.ToLower() == walletTransCode))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.WALLETTRANSACTION_EXIST);
            }

            _repoWalletTran.Insert(new WalletTransaction
            {
                LineBalance = walletTransactionDto.LineBalance,
                TenantId =  _serviceHelper.GetTenantId(),
                TransDescription = walletTransactionDto.TransDescription,
                TransactedBy = walletTransactionDto.TransactedBy,
                TransactionAmount = walletTransactionDto.TransactionAmount,
                CreationTime = new System.DateTime(),
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                TransactionType = walletTransactionDto.TransactionType,
                TransactionDate = walletTransactionDto.TransactionDate,
                WalletId = walletTransactionDto.WalletId,
                PayTypeDiscription = walletTransactionDto.PayTypeDiscription,
                IsSum = true,
                TransCode = walletTransactionDto.TransCode,
                AmountType = walletTransactionDto.AmountType
            });

            if (walletTransactionDto.TransactionType == TransactionType.Credit)
            {
                var ClientWallet = _repo.GetAll().Where(x => x.Id == walletTransactionDto.WalletId).FirstOrDefault();
                //Add Amount to Total unit from wallet balance then update wallet
                var Walbal = ClientWallet.Balance + walletTransactionDto.TransactionAmount;
                ClientWallet.OldBalance = ClientWallet.Balance;
                ClientWallet.Balance = Walbal;
            }

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task RemoveWalletTransaction(Guid walletTransactionId)
        {
            var walletTransaction = await _repoWalletTran.GetAsync(walletTransactionId);

            if (walletTransaction is null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.WALLETTRANSACTION_NOT_EXIST);
            }

            _repoWalletTran.Delete(walletTransaction);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateWalletTransaction(Guid walletTransactionId, WalletTransactionDTO walletTransactionDto)
        {
            var walletTransaction = await _repoWalletTran.GetAsync(walletTransactionId);

            if (walletTransaction is null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.WALLETTRANSACTION_NOT_EXIST);
            }

            walletTransaction.TransCode = walletTransactionDto.TransCode.Trim();
            walletTransaction.LineBalance = walletTransactionDto.LineBalance;
            walletTransaction.TenantId = walletTransactionDto.TenantId;
            walletTransaction.TransDescription = walletTransactionDto.TransDescription;
            walletTransaction.TransactedBy = walletTransactionDto.TransactedBy;
            walletTransaction.TransactionAmount = walletTransactionDto.TransactionAmount;
            walletTransaction.TransactionType = walletTransactionDto.TransactionType;
            walletTransaction.TransactionDate = walletTransactionDto.TransactionDate;
            walletTransaction.WalletId = walletTransactionDto.WalletId;
            walletTransaction.PayTypeDiscription = walletTransactionDto.PayTypeDiscription;
            walletTransaction.IsSum = true;
            walletTransaction.TransCode = walletTransactionDto.TransCode;

            if (walletTransactionDto.TransactionType == TransactionType.Credit)
            {
                var ClientWallet = _repo.GetAll().Where(x => x.UserId == walletTransactionDto.UserId).FirstOrDefault();
                //Add Amount to Total unit from wallet balance then update wallet
                var Walbal = ClientWallet.Balance + walletTransactionDto.TransactionAmount;
                ClientWallet.OldBalance = ClientWallet.Balance;
                ClientWallet.Balance = Walbal;
            }
            else if (walletTransactionDto.TransactionType == TransactionType.Debit)
            {
                var ClientWallet = _repo.GetAll().Where(x => x.UserId == walletTransactionDto.UserId).FirstOrDefault();
                //Substract Amount from Total unit from wallet balance then update wallet
                var Walbal = ClientWallet.Balance + walletTransactionDto.TransactionAmount;
                ClientWallet.OldBalance = ClientWallet.Balance;
                ClientWallet.Balance = Walbal;
            }

                await _unitOfWork.SaveChangesAsync();
        }




        public Task<IPagedList<WalletTransactionDTO>> GetWalletByUserId(int UserId, int page, int size, string query = null)
        {
            //GEt All Users with wallet in the Identity Table
            var userWallet = _userManager.Users.Include(u => u.Wallet).ToList().Where(x => x.Id == UserId).FirstOrDefault();
            var tenant = _serviceHelper.GetTenantId();
            var walletTransactions = 
                                     from w in _repo.GetAll()
                                     join wt in _repoWalletTran.GetAll() on w.Id equals wt.WalletId
                                     where w.Id == userWallet.Wallet.Id
                                     orderby w.CreationTime descending
                                     select new WalletTransactionDTO
                                    {
                                    Id = wt.Id,
                                    UserId = UserId,
                                    TransactionAmount = wt.TransactionAmount,
                                    WalletBalance = w.Balance,
                                    TransCode = wt.TransCode,
                                    WalletId = w.Id,
                                    AmountType = wt.AmountType,
                                    CreationTime = wt.CreationTime,
                                    TransDescription = wt.TransDescription,
                                    PayTypeDiscription = wt.PayTypeDiscription,
                                    LineBalance = wt.LineBalance,
                                    TransactionType = wt.TransactionType
 
                                         //DateCreated = wt.CreationTime.ToString(CoreConstants.DateFormat)

                                     };

            return walletTransactions.AsNoTracking().ToPagedListAsync(page, size);
        }

        #endregion
    }


}
