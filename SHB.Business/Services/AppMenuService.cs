﻿using IPagedList;
using SHB.Core.Domain.Entities;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SHB.Core.Domain.DataTransferObjects;

namespace SHB.Business.Services
{
    public interface IAppMenu
    {

        #region AppMenuItems
        Task AddAppMenuItem(AppMenuItemDTO model);
        Task<AppMenuItemDTO> GetMenuItem(int Id);
        Task<IPagedList<AppMenuItemDTO>> GetAllMenuItems(int pageNumber, int pageSize, string searchTerm);
        Task<IEnumerable<AppMenuItemDTO>> GetAllMenuItems();
        Task<bool> UpdateMenuItem(AppMenuItemDTO model, int Id);
        Task DeleteMenuItem(int Id);
        #endregion

        #region AppMenuItemsAccess
        Task AddAppMenuItemAccess(AppMenuItemAccessDTO model);
        Task<AppMenuItemAccessDTO> GetMenuItemAccess(int Id);
        Task<IPagedList<AppMenuItemAccessDTO>> GetAllMenuItemsAccess(int pageNumber, int pageSize, string searchTerm);
        Task<IEnumerable<AppMenuItemAccessDTO>> GetAllMenuItemsAccess();
        Task<bool> UpdateMenuItemAccess(AppMenuItemAccessDTO model, int Id);
        Task DeleteMenuItemAccess(int Id);
        #endregion
    }
    public class AppMenuService : IAppMenu
    {
        private readonly IRepository<AppMenuItem> _menuItemRepo;
        private readonly IRepository<AppMenuItemAccess> _itemAccessRepo;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;

        public AppMenuService(
            IRepository<AppMenuItem> menuItemRepo,
            IRepository<AppMenuItemAccess> itemAccessRepo,
             IServiceHelper serviceHelper,
             IUnitOfWork unitOfWork)
        {
            _menuItemRepo = menuItemRepo;
            _itemAccessRepo = itemAccessRepo;
            _serviceHelper = serviceHelper;
            _unitOfWork = unitOfWork;
        }


        public async Task AddAppMenuItem(AppMenuItemDTO model)
        {
            _menuItemRepo.Insert(new AppMenuItem
            {
                MenuCode = model.MenuCode,
                MenuDescription = model.MenuDescription,
                DisplayOrder = model.DisplayOrder,
                DefaultPage = model.DefaultPage,
                ControllerName = model.ControllerName,
                ActionName = model.DefaultPage,
                ParentId = model.ParentId
            });

            await _unitOfWork.SaveChangesAsync();

        }

        public async Task AddAppMenuItemAccess(AppMenuItemAccessDTO model)
        {
            _itemAccessRepo.Insert(new AppMenuItemAccess
            {
                AppMenuId = model.AppMenuId,
                ClaimName = model.ClaimName,
                SetupDefault = model.SetupDefault,
                IsActive = model.IsActive
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteMenuItem(int Id)
        {
            if (Id == 0) { return; }
            await _menuItemRepo.DeleteAsync(_menuItemRepo.Get(Id));
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteMenuItemAccess(int Id)
        {
            if (Id == 0) { return; }
            await _itemAccessRepo.DeleteAsync(_itemAccessRepo.Get(Id));
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IPagedList<AppMenuItemDTO>> GetAllMenuItems(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var menuItem =
                  from AppMenuItem in _menuItemRepo.GetAll()
                  where string.IsNullOrWhiteSpace(searchTerm) ||
                  AppMenuItem.MenuCode.Contains(searchTerm)
                  orderby AppMenuItem.CreationTime descending

                  select new AppMenuItemDTO
                  {
                      Id = AppMenuItem.Id,
                      MenuCode = AppMenuItem.MenuCode,
                      MenuDescription = AppMenuItem.MenuDescription,
                      DisplayOrder = AppMenuItem.DisplayOrder,
                      DefaultPage = AppMenuItem.DefaultPage,
                      ControllerName = AppMenuItem.ControllerName,
                      ActionName = AppMenuItem.DefaultPage,
                      ParentId = AppMenuItem.ParentId
                  };

                return await menuItem.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<IEnumerable<AppMenuItemDTO>> GetAllMenuItems()
        {
            try
            {
                var menuItem =
                  from AppMenuItem in _menuItemRepo.GetAll()

                  select new AppMenuItemDTO
                  {
                      Id = AppMenuItem.Id,
                      MenuCode = AppMenuItem.MenuCode,
                      MenuDescription = AppMenuItem.MenuDescription,
                      DisplayOrder = AppMenuItem.DisplayOrder,
                      DefaultPage = AppMenuItem.DefaultPage,
                      ControllerName = AppMenuItem.ControllerName,
                      ActionName = AppMenuItem.DefaultPage,
                      ParentId = AppMenuItem.ParentId
                  };

                return menuItem;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<IPagedList<AppMenuItemAccessDTO>> GetAllMenuItemsAccess(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var menuItemAccess =
                  from AppMenuItemAccess in _itemAccessRepo.GetAll()
                  where string.IsNullOrWhiteSpace(searchTerm) ||
                  AppMenuItemAccess.ClaimName.Contains(searchTerm)
                  orderby AppMenuItemAccess.CreationTime descending

                  select new AppMenuItemAccessDTO
                  {
                      Id = AppMenuItemAccess.Id,
                      AppMenuId = AppMenuItemAccess.AppMenuId,
                      ClaimName = AppMenuItemAccess.ClaimName,
                      SetupDefault = AppMenuItemAccess.SetupDefault,
                      IsActive = AppMenuItemAccess.IsActive
                  };

                return await menuItemAccess.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<IEnumerable<AppMenuItemAccessDTO>> GetAllMenuItemsAccess()
        {
            try
            {
                var menuItemAccess =
                  from AppMenuItemAccess in _itemAccessRepo.GetAll()

                  select new AppMenuItemAccessDTO
                  {
                      Id = AppMenuItemAccess.Id,
                      AppMenuId = AppMenuItemAccess.AppMenuId,
                      ClaimName = AppMenuItemAccess.ClaimName,
                      SetupDefault = AppMenuItemAccess.SetupDefault,
                      IsActive = AppMenuItemAccess.IsActive
                  };

                return menuItemAccess;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<AppMenuItemDTO> GetMenuItem(int Id)
        {
            var menuItem = await _menuItemRepo.GetAsync(Id);

            return new AppMenuItemDTO
            {
                Id = menuItem.Id,
                MenuCode = menuItem.MenuCode,
                MenuDescription = menuItem.MenuDescription,
                DisplayOrder = menuItem.DisplayOrder,
                DefaultPage = menuItem.DefaultPage,
                ControllerName = menuItem.ControllerName,
                ActionName = menuItem.DefaultPage,
                ParentId = menuItem.ParentId
            };
        }

        public async Task<AppMenuItemAccessDTO> GetMenuItemAccess(int Id)
        {
            var menuItemAccess = await _itemAccessRepo.GetAsync(Id);

            return new AppMenuItemAccessDTO
            {
                Id = menuItemAccess.Id,
                AppMenuId = menuItemAccess.AppMenuId,
                ClaimName = menuItemAccess.ClaimName,
                SetupDefault = menuItemAccess.SetupDefault,
                IsActive = menuItemAccess.IsActive
            };
        }

        public async Task<bool> UpdateMenuItem(AppMenuItemDTO model, int Id)
        {
            var menuItem = await _menuItemRepo.GetAsync(Id);


            menuItem.MenuCode = model.MenuCode;
            menuItem.MenuDescription = model.MenuDescription;
            menuItem.DisplayOrder = model.DisplayOrder;
            menuItem.DefaultPage = model.DefaultPage;
            menuItem.ControllerName = model.ControllerName;
            menuItem.ActionName = model.DefaultPage;
            menuItem.ParentId = model.ParentId;

            await _unitOfWork.SaveChangesAsync();

            return true;

        }

        public async Task<bool> UpdateMenuItemAccess(AppMenuItemAccessDTO model, int Id)
        {
            var menuItemAccess = await _itemAccessRepo.GetAsync(Id);

            menuItemAccess.AppMenuId = menuItemAccess.AppMenuId;
            menuItemAccess.ClaimName = menuItemAccess.ClaimName;
            menuItemAccess.SetupDefault = menuItemAccess.SetupDefault;
            menuItemAccess.IsActive = menuItemAccess.IsActive;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }
    }
}
