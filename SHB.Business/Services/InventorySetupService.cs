﻿using System;
using System.Collections.Generic;
using IPagedList;
using SHB.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using SHB.Data.Repository;
using SHB.Business.Services;
using SHB.Data.UnitOfWork;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Core.Domain.DataTransferObjects;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc;

namespace SHB.Business.Services
{
    public interface IInventorySetupService
    {
        #region Next Number
        Task AddNextNumber(NextNumber model);
        Task<NextNumber> GetNextNumber(int Id);
        Task<IPagedList<NextNumber>> GetAllNextNumbers(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateNextNumber(NextNumber model, int Id);
        Task DeleteNextNumber(int Id);
        #endregion

        #region GL Chart of Account
        Task AddLedgerChartOfAccount(LedgerChartOfAccount model);
        Task<LedgerChartOfAccount> GetLedgerChartOfAccount(int Id);
        Task<IPagedList<LedgerChartOfAccount>> GetAllLedgerChartOfAccounts(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateLedgerChartOfAccount(LedgerChartOfAccount model, int Id);
        Task DeleteLedgerChartOfAccount(int Id);
        #endregion

        #region Inventory Item
        Task AddInventoryItem(InventoryItem model);
        Task<InventoryItem> GetInventoryItem(int Id);
        Task<IPagedList<InventoryItem>> GetAllInventoryItems(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateInventoryItem(InventoryItem model);
        Task<bool> UpdateInventoryItem(InventoryItem model, int Id);
        Task DeleteInventoryItem(int Id);
        #endregion

        #region Inventory Categories
        Task AddItemCategory(ItemCategory model);
        Task<ItemCategory> GetItemCategory(int Id);
        Task<IPagedList<ItemCategory>> GetAllItemCategories(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateItemCategory(ItemCategory model, int Id);
        Task DeleteItemCategory(int Id);
        #endregion

        #region Item Types
        Task AddItemType(ItemType model);
        Task<ItemType> GetItemType(int Id);
        Task<IPagedList<ItemType>> GetAllItemTypes(int pageNumber, int pageSize, string searchTerm);
        //Task<bool> UpdateItemType(ItemType model);
        Task<bool> UpdateItemType(ItemType model, int Id);
        Task DeleteItemType(int Id);
        #endregion

        #region Attributes
        Task AddAttribute(Attributes model);
        Task<Attributes> GetAttribute(int Id);
        Task<IPagedList<Attributes>> GetAllAttributes(int pageNumber, int pageSize, string searchTerm);
        //Task<bool> UpdateAttribute(Attributes model);
        Task<bool> UpdateAttribute(Attributes model, int Id);
        Task DeleteAttribute(int Id);
        #endregion

        #region Families
        Task AddItemFamily(ItemFamily model);
        Task<ItemFamily> GetItemFamily(int Id);
        Task<IPagedList<ItemFamily>> GetAllItemFamilies(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateItemFamily(ItemFamily model, int Id);
        Task DeleteItemFamily(int Id);
        #endregion

        #region Warehouses
        Task AddWarehouse(WarehouseDTO model);
        Task<WarehouseDTO> GetWarehouse(int Id);
        Task<IPagedList<WarehouseDTO>> GetAllWarehouses(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateWarehouse(WarehouseDTO model, int Id);
        Task DeleteWarehouse(int Id);

        #endregion

        #region WarehouseBins
        Task AddWarehouseBin(WarehouseBin model);
        Task<WarehouseBin> GetWarehouseBin(int Id);
        Task<IPagedList<WarehouseBin>> GetAllWarehouseBins(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateWarehouseBin(WarehouseBin model, int Id);
        Task DeleteWarehouseBin(int Id);
        IEnumerable<WarehouseBin> GetBinByWarehouseId(int id);
        #endregion

        #region Inventory Adjustment Type
        Task AddInventoryAdjustmentType(InventoryAdjustmentType model);
        Task<InventoryAdjustmentType> GetInventoryAdjustmentType(int Id);
        Task<IPagedList<InventoryAdjustmentType>> GetAllInventoryAdjustmentTypes(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateInventoryAdjustmentType(InventoryAdjustmentType model, int Id);
        Task DeleteInventoryAdjustmentType(int Id);
        #endregion

        #region Warehouse Bin Types
        Task AddWarehouseBinType(WarehouseBinType model);
        Task<WarehouseBinType> GetWarehouseBinType(int Id);
        Task<IPagedList<WarehouseBinType>> GetAllWarehouseBinTypes(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateWarehouseBinType(WarehouseBinType model);
        Task DeleteWarehouseBinType(int Id);
        #endregion

        #region Vendors
        //Task AddVendor(Vendor model);
        //Task<Vendor> GetVendor(int Id);
        //Task<IPagedList<Vendor>> GetAllVendors(int pageNumber, int pageSize, string searchTerm);
        //Task<bool> UpdateVendor(Vendor model, int Id);
        //Task DeleteVendor(int Id);
        #endregion
    }
    public class InventorySetupService : IInventorySetupService
    {
        private readonly IVendorTypesService _vendorTypesService;
        private readonly IRepository<InventoryItem> _inventoryItemRepo;
        private readonly IRepository<ItemCategory> _itemCategoryRepo;
        private readonly IRepository<ItemType> _itemTypeRepo;
        private readonly IRepository<Attributes> _attributeRepo;
        private readonly IRepository<ItemFamily> _itemFamilyRepo;
        private readonly IRepository<Warehouse> _warehouseRepo;
        private readonly IRepository<BrandType> _brandTypeRepo;
        //private readonly IRepository<Vendor> _vendorRepo;
        private readonly IRepository<WarehouseBin> _warehouseBinRepo;
        private readonly IRepository<InventoryAdjustmentType> _inventoryAdjustmentTypeRepo;
        private readonly IRepository<WarehouseBinType> _warehouseBinTypeRepo;
        private readonly IRepository<NextNumber> _nextNumberRepo;
        //private readonly IRepository<GeneralLedger> _generalLedgerRepo;
        private readonly IRepository<LedgerChartOfAccount> _ledgerChartOfAccountRepo;
        private readonly IWalletService _walletSvc;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRoleService _roleSvc;
        private readonly IRepository<WalletTransaction, Guid> _walletTransrepo;
        private readonly IRepository<GeneralTransaction, Guid> _genTransrepo;
        private readonly IRepository<Wallet> _walletRepo;
        private readonly IRepository<CompanyInfo> _companyInfoRepo;
        private readonly IUserService _userSvc;
        public InventorySetupService(
            IVendorTypesService vendorTypesService,
            IRepository<InventoryItem> inventoryItemRepo,
            IRepository<ItemCategory> itemCategoryRepo,
            IRepository<ItemType> itemTypeRepo,
            IRepository<Attributes> attributeRepo,
            IRepository<ItemFamily> itemFamilyRepo,
            IRepository<Warehouse> warehouseRepo,
            IRepository<BrandType> brandTypeRepo,
            //IRepository<Vendor> vendorRepo,
            IRepository<WarehouseBin> warehouseBinRepo,
            IRepository<InventoryAdjustmentType> inventoryAdjustmentTypeRepo,
            IRepository<WarehouseBinType> warehouseBinTypeRepo,
            IRepository<NextNumber> nextNumberRepo,
            //IRepository<GeneralLedger> generalLedgerRepo,
            IRepository<LedgerChartOfAccount> ledgerchartofAccountRepo,


            IWalletService walletSvc,
            IServiceHelper serviceHelper,
            IUnitOfWork unitOfWork,
            IRoleService roleSvc,
            IRepository<WalletTransaction, Guid> walletTransrepo,
            IRepository<GeneralTransaction, Guid> genTransrepo,
            IRepository<Wallet> walletRepo,
            IRepository<CompanyInfo> CompanyInfoRepo,
            IUserService userSvc
            )
        {
            _vendorTypesService = vendorTypesService;
            _inventoryItemRepo = inventoryItemRepo;
            _itemCategoryRepo = itemCategoryRepo;
            _itemTypeRepo = itemTypeRepo;
            _attributeRepo = attributeRepo;
            _itemFamilyRepo = itemFamilyRepo;
            _warehouseRepo = warehouseRepo;
            _brandTypeRepo = brandTypeRepo;
            //_vendorRepo = vendorRepo;
            _warehouseBinRepo = warehouseBinRepo;
            _inventoryAdjustmentTypeRepo = inventoryAdjustmentTypeRepo;
            _warehouseBinTypeRepo = warehouseBinTypeRepo;
            _nextNumberRepo = nextNumberRepo;
            //_generalLedgerRepo = generalLedgerRepo;
            _ledgerChartOfAccountRepo = ledgerchartofAccountRepo;
            _walletSvc = walletSvc;
            _serviceHelper = serviceHelper;
            _unitOfWork = unitOfWork;
            _roleSvc = roleSvc;
            _walletTransrepo = walletTransrepo;
            _genTransrepo = genTransrepo;
            _walletRepo = walletRepo;
            _companyInfoRepo = CompanyInfoRepo;
            _userSvc = userSvc;
        }

        public async Task AddAttribute(Core.Domain.Entities.Attributes model)
        {
            try
            {
                if (model == null) { return; }
                _attributeRepo.Insert(model);
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            //throw new NotImplementedException();
        }


        public async Task AddInventoryAdjustmentType(InventoryAdjustmentType model)
        {
            model.AdjustmentTypeName = model.AdjustmentTypeName?.Trim();

            if (await _inventoryAdjustmentTypeRepo.ExistAsync(a => a.AdjustmentTypeName == model.AdjustmentTypeName))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.INVENTORYADJUSTMENTTYPE_EXIST);
            }

            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);


            _inventoryAdjustmentTypeRepo.Insert(new InventoryAdjustmentType
            {
                AdjustmentTypeName = model.AdjustmentTypeName,
                AdjustmentTypeDescription = model.AdjustmentTypeDescription,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                CompanyId = (int)currentUser.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task AddInventoryItem(InventoryItem model)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);
            try
            {
                if (model == null) { return; }
                model.CompanyId = (int)currentUser.CompanyId;
                model.CreationTime = DateTime.Now;
                model.CreatorUserId = _serviceHelper.GetCurrentUserId();
                _inventoryItemRepo.Insert(model);
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            //throw new NotImplementedException();
        }

        public async Task AddItemCategory(ItemCategory model)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);
            try
            {
                if (model == null) { return; }
                model.CompanyId = (int)currentUser.CompanyId;
                _itemCategoryRepo.Insert(model);
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            //throw new NotImplementedException();
        }

        public async Task AddItemFamily(ItemFamily model)
        {
            model.ItemFamilyCode = model.ItemFamilyCode.Trim();
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            if (await _itemFamilyRepo.ExistAsync(i => i.ItemFamilyCode == model.ItemFamilyCode))
            {

                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ITEMFAMILY_EXIST);
            }

            _itemFamilyRepo.Insert(new ItemFamily
            {
                ItemFamilyCode = model.ItemFamilyCode,
                FamilyDescription = model.FamilyDescription,
                FamilyLongDescription = model.FamilyLongDescription,
                FamilyPictureURL = model.FamilyPictureURL,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                CompanyId = (int)currentUser.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();

        }

        public async Task AddItemType(ItemType model)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);
            try
            {
                if (model == null) { return; }
                model.CompanyId = (int)currentUser.CompanyId;
                _itemTypeRepo.Insert(model);
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            //throw new NotImplementedException();
        }

        public async Task AddLedgerChartOfAccount(LedgerChartOfAccount model)
        {
            model.GLAccountName = model.GLAccountName.Trim();

            if (await _ledgerChartOfAccountRepo.ExistAsync(l => l.GLAccountName == model.GLAccountName))
            {

                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.LEDGERCHARTOFACCOUNT_EXIST);
            }

            _ledgerChartOfAccountRepo.Insert(new LedgerChartOfAccount
            {
                GLAccountName = model.GLAccountName,
                CGLAccountNumber = model.CGLAccountNumber,
                GLAccountBalance = model.GLAccountBalance,
                GLAccountBeginningBalance = model.GLAccountBeginningBalance,
                GLAccountDescription = model.GLAccountDescription,
                GLAccountType = model.GLAccountType,
                GLBalanceType = model.GLBalanceType,
                GLAccountUse = model.GLAccountUse,
                GLOtherNotes = model.GLOtherNotes,
                GLReportingAccount = model.GLReportingAccount,
                CurrencyID = model.CurrencyID,
                CurrencyExchangeRate = model.CurrencyExchangeRate,
                CashFlowType = model.CashFlowType,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
            });

            await _unitOfWork.SaveChangesAsync();

        }

        public async Task AddNextNumber(NextNumber model)
        {
            model.NextNumberName = model.NextNumberName.Trim();
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            if (await _nextNumberRepo.ExistAsync(n => n.NextNumberName == model.NextNumberName))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.NEXTNUMBER_EXIST);
            }

            _nextNumberRepo.Insert(new NextNumber
            {
                NextNumberName = model.NextNumberName,
                NextNumberValue = model.NextNumberValue,
                NextNumberPrefix = model.NextNumberPrefix,
                NextNumberSeparator = model.NextNumberSeparator,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                CompanyId = (int)currentUser.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();
        }

        //public async Task AddVendor(Vendor model)
        //{
        //    model.Name = model.Name?.Trim();
        //    var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
        //    var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

        //    if (await _vendorRepo.ExistAsync(a => a.Name == model.Name))
        //    {
        //        throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VENDR_EXIST);
        //    }

        //    _vendorRepo.Insert(new Vendor
        //    {
        //        Name = model.Name,
        //        ContactName = model.ContactName,
        //        Address = model.Address,
        //        Email = model.Email,
        //        Phone = model.Phone,
        //        CompanyRegistrationNumber = model.CompanyRegistrationNumber,
        //        BankName = model.BankName,
        //        BankAccountNumber = model.BankAccountNumber,
        //        VendorType = model.VendorType,
        //        CreatorUserId = _serviceHelper.GetCurrentUserId(),
        //        CreationTime = DateTime.Now,
        //        CompanyId = (int)currentUser.CompanyId
        //    });

        //    await _unitOfWork.SaveChangesAsync();

        //}

        public async Task AddWarehouse(WarehouseDTO model)
        {
            model.WarehouseName = model.WarehouseName.Trim();
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            if (await _warehouseRepo.ExistAsync(w => w.WarehouseName == model.WarehouseName))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.WAREHOUSE_EXIST);
            }

            _warehouseRepo.Insert(new Warehouse
            {
                WarehouseName = model.WarehouseName,
                WarehouseCode = model.WarehouseCode,
                WarehouseAddress1 = model.WarehouseAddress1,
                WarehouseAddress2 = model.WarehouseAddress2,
                WarehouseEmail = model.WarehouseEmail,
                WarehousePhone = model.WarehousePhone,
                WarehouseCity = model.WarehouseCity,
                WarehouseState = model.WarehouseState,
                WarehouseFax = model.WarehouseFax,
                WarehouseZip = model.WarehouseState,
                SalesControlAccount = model.SalesControlAccount,
                StockControlAccount = model.StockControlAccount,
                COSControlAccount = model.COSControlAccount,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                CompanyId = (int)currentUser.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task AddWarehouseBin(WarehouseBin model)
        {
            model.WarehouseBinName = model.WarehouseBinName?.Trim();
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            if (await _warehouseBinRepo.ExistAsync(w => w.WarehouseBinName == model.WarehouseBinName))
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.WAREHOUSEBIN_EXIST);
            }

            try
            {
                _warehouseBinRepo.Insert(new WarehouseBin
                {
                    WarehouseBinIDCode = model.WarehouseBinIDCode,
                    WarehouseBinName = model.WarehouseBinName,
                    WarehouseBinNumber = model.WarehouseBinNumber,
                    WarehouseBinType = model.WarehouseBinType,
                    WarehouseBinHeight = model.WarehouseBinHeight,
                    WarehouseBinLength = model.WarehouseBinLength,
                    WarehouseBinWeight = model.WarehouseBinWeight,
                    WarehouseBinWidth = model.WarehouseBinWidth,
                    WarehouseBinLocation = model.WarehouseBinLocation,
                    WarehouseBinZone = model.WarehouseBinZone,
                    MaximumQuantity = model.MaximumQuantity,
                    MinimumQuantity = model.MinimumQuantity,
                    OverFlowBin = model.OverFlowBin,
                    CreatorUserId = _serviceHelper.GetCurrentUserId(),
                    CreationTime = DateTime.Now,
                    DeleterUserId = null,
                    DeletionTime = null,
                    IsActive = true,
                    IsDeleted = false,
                    LastModificationTime = DateTime.Now,
                    LastModifierUserId = _serviceHelper.GetCurrentUserId(),
                    WarehouseID = 3,
                    CompanyId = (int)currentUser.CompanyId
                    //Warehouse = model.Warehouse,
                    //Id = model.Id,
                });

                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }

        }

        public async Task AddWarehouseBinType(WarehouseBinType model)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);
            try
            {
                if (model == null) { return; }
                model.CompanyId = (int)currentUser.CompanyId;
                await _warehouseBinTypeRepo.InsertAsync(model);
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            //throw new NotImplementedException();
        }

        public async Task DeleteAttribute(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _attributeRepo.DeleteAsync(_attributeRepo.Get(Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            //throw new NotImplementedException();
        }

        public async Task DeleteInventoryAdjustmentType(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _inventoryAdjustmentTypeRepo.DeleteAsync(_inventoryAdjustmentTypeRepo.Get(Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }

        public async Task DeleteInventoryItem(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _inventoryItemRepo.DeleteAsync(_inventoryItemRepo.Get(Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            //throw new NotImplementedException();
        }

        public async Task DeleteItemCategory(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _itemCategoryRepo.DeleteAsync(_itemCategoryRepo.Get(Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            //throw new NotImplementedException();
        }

        public async Task DeleteItemFamily(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _itemFamilyRepo.DeleteAsync(_itemFamilyRepo.Get(Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            //throw new NotImplementedException();
        }

        public async Task DeleteItemType(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _itemTypeRepo.DeleteAsync(_itemTypeRepo.Get(Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            //throw new NotImplementedException();
        }

        public async Task DeleteLedgerChartOfAccount(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _ledgerChartOfAccountRepo.DeleteAsync(_ledgerChartOfAccountRepo.Get(Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            //throw new NotImplementedException();
        }

        public async Task DeleteNextNumber(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _nextNumberRepo.DeleteAsync(_nextNumberRepo.Get(Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            //throw new NotImplementedException();
        }

        //public async Task DeleteVendor(int Id)
        //{
        //    try
        //    {
        //        if (Id == 0) { return; }
        //        await _vendorRepo.DeleteAsync(_vendorRepo.Get(Id));
        //        await _unitOfWork.SaveChangesAsync();
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        public async Task DeleteWarehouse(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _warehouseRepo.DeleteAsync(_warehouseRepo.Get(Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            //throw new NotImplementedException();
        }

        public async Task DeleteWarehouseBin(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _warehouseBinRepo.DeleteAsync(_warehouseBinRepo.Get(Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            //throw new NotImplementedException();
        }

        public async Task DeleteWarehouseBinType(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _warehouseBinTypeRepo.DeleteAsync(_warehouseBinTypeRepo.Get(Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
            //throw new NotImplementedException();
        }

        public async Task<IPagedList<Attributes>> GetAllAttributes(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                IPagedList<Attributes> data = default;

                if (string.IsNullOrEmpty(searchTerm))
                {
                    data = await _attributeRepo.GetAll().Where(a => a.AttributeDescription.Contains(searchTerm) || a.AttributeName.Contains(searchTerm)).ToPagedListAsync(pageNumber, pageSize);
                    return data;
                }
                else
                {
                    data = await _attributeRepo.GetAll().ToPagedListAsync(pageNumber, pageSize);
                    return data;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            //throw new NotImplementedException();
        }

        public async Task<IPagedList<InventoryAdjustmentType>> GetAllInventoryAdjustmentTypes(int pageNumber, int pageSize, string searchTerm)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);
            try
            {
                var inventoryAdjustmentTypes =
                    from InventoryAdjustmentType in _inventoryAdjustmentTypeRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) ||
                    InventoryAdjustmentType.AdjustmentTypeName.Contains(searchTerm) && InventoryAdjustmentType.CompanyId == (int)currentUser.CompanyId
                    orderby InventoryAdjustmentType.CreationTime descending

                    select new InventoryAdjustmentType
                    {
                        Id = InventoryAdjustmentType.Id,
                        AdjustmentTypeName = InventoryAdjustmentType.AdjustmentTypeName,
                        AdjustmentTypeDescription = InventoryAdjustmentType.AdjustmentTypeDescription
                    };

                return await inventoryAdjustmentTypes.AsTracking().ToPagedListAsync(pageNumber, pageSize);

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<IPagedList<InventoryItem>> GetAllInventoryItems(int pageNumber, int pageSize, string searchTerm)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);
            try
            {
                var inventoryItems = (from InventoryItems in _inventoryItemRepo.GetAll()
                                          //join ItemCategory in _itemCategoryRepo.GetAll().DefaultIfEmpty() on InventoryItems.ItemCategoryID equals ItemCategory.Id
                                          //join ItemFam in _itemFamilyRepo.GetAll() on InventoryItems.ItemFamilyID equals ItemFam.Id
                                          //join ItemType in _itemTypeRepo.GetAll().DefaultIfEmpty() on InventoryItems.ItemTypeId equals ItemType.Id
                                          //join WareHous in _warehouseRepo.GetAll() on InventoryItems.ItemDefaultWarehouseID equals WareHous.Id
                                          //join WareBin in _warehouseBinRepo.GetAll() on InventoryItems.ItemDefaultWarehouseBinID equals WareBin.Id
                                          //join Cogs in _ledgerChartOfAccountRepo.GetAll() on InventoryItems.GLItemCOGSAccountId equals Cogs.Id
                                          //join ItemAccount in _ledgerChartOfAccountRepo.GetAll() on InventoryItems.GLItemInventoryAccountId equals ItemAccount.Id
                                          //join ItemSales in _ledgerChartOfAccountRepo.GetAll() on InventoryItems.GLItemSalesAccountId equals ItemSales.Id
                                          //join Brand in _brandTypeRepo.GetAll().DefaultIfEmpty() on InventoryItems.BrandTypeID equals Brand.Id
                                      where InventoryItems.CompanyId == currentUser.CompanyId
                                      where string.IsNullOrWhiteSpace(searchTerm) ||
                                      InventoryItems.ItemName.Contains(searchTerm) ||
                                      InventoryItems.itemDescription.Contains(searchTerm)
                                      orderby InventoryItems.ItemName
                                      select new InventoryItem
                                      {
                                          Id = InventoryItems.Id,
                                          ItemTypeName = InventoryItems.ItemTypeName,
                                          ItemCategoryID = InventoryItems.ItemCategoryID,
                                          //ItemFamilyName = ItemFam.FamilyDescription,
                                          ItemName = InventoryItems.ItemName,
                                          ItemCode = InventoryItems.ItemCode,
                                          ItemWeight = InventoryItems.ItemWeight,
                                          ItemDefaultWarehouseID = InventoryItems.ItemDefaultWarehouseID,
                                          //ItemDefaultWarehouseBinName = WareBin.WarehouseBinName,
                                          //GLItemSalesAccountName = ItemSales.CGLAccountNumber,
                                          //GLItemCOGSAccountName = Cogs.CGLAccountNumber,
                                          //GLItemInventoryAccountName = ItemAccount.CGLAccountNumber,
                                          //BrandTypeName = Brand.Name
                                          CreationTime = InventoryItems.CreationTime,
                                      }


                                      );
                return await inventoryItems.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
            //throw new NotImplementedException();
        }

        public async Task<IPagedList<ItemCategory>> GetAllItemCategories(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var itemCategories = (from ItemCategories in _itemCategoryRepo.GetAll()
                                      where string.IsNullOrWhiteSpace(searchTerm) ||
                                      ItemCategories.ItemCategoryCode.Contains(searchTerm) ||
                                      ItemCategories.CategoryName.Contains(searchTerm) ||
                                      ItemCategories.CategoryDescription.Contains(searchTerm) ||
                                      ItemCategories.CategoryLongDescription.Contains(searchTerm)
                                      orderby ItemCategories.CategoryName
                                      select ItemCategories
                                      );
                return await itemCategories.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception)
            {
                return null;
            }
            //throw new NotImplementedException();
        }

        public async Task<IPagedList<ItemFamily>> GetAllItemFamilies(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var itemFamilies =
                    from ItemFamily in _itemFamilyRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) ||
                    (ItemFamily.ItemFamilyCode.Contains(searchTerm)
                    )
                    orderby ItemFamily.CreationTime descending

                    select new ItemFamily
                    {
                        Id = ItemFamily.Id,
                        ItemFamilyCode = ItemFamily.ItemFamilyCode,
                        FamilyDescription = ItemFamily.FamilyDescription,
                        FamilyLongDescription = ItemFamily.FamilyLongDescription,
                        FamilyPictureURL = ItemFamily.FamilyPictureURL
                    };

                return await itemFamilies.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<IPagedList<ItemType>> GetAllItemTypes(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var itemTypes = (from ItemTypes in _itemTypeRepo.GetAll()
                                 where string.IsNullOrWhiteSpace(searchTerm) ||
                                 ItemTypes.ItemTypeName.Contains(searchTerm) ||
                                 ItemTypes.ItemTypeDescription.Contains(searchTerm)
                                 orderby ItemTypes.ItemTypeName
                                 select ItemTypes);
                return await itemTypes.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception)
            {
                return null;
            }
            //throw new NotImplementedException();
        }

        public async Task<IPagedList<LedgerChartOfAccount>> GetAllLedgerChartOfAccounts(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var ledgerCharts =
                    from LedgerChartOfAccount in _ledgerChartOfAccountRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) ||
                    LedgerChartOfAccount.GLAccountName.Contains(searchTerm) ||
                    LedgerChartOfAccount.CurrencyID.Contains(searchTerm)
                    orderby LedgerChartOfAccount.CreationTime descending

                    select new LedgerChartOfAccount
                    {
                        Id = LedgerChartOfAccount.Id,
                        GLAccountName = LedgerChartOfAccount.GLAccountName,
                        CGLAccountNumber = LedgerChartOfAccount.CGLAccountNumber,
                        GLAccountBalance = LedgerChartOfAccount.GLAccountBalance,
                        GLAccountBeginningBalance = LedgerChartOfAccount.GLAccountBeginningBalance,
                        GLAccountType = LedgerChartOfAccount.GLAccountType,
                        GLBalanceType = LedgerChartOfAccount.GLBalanceType,
                        GLAccountDescription = LedgerChartOfAccount.GLAccountDescription,
                        GLAccountUse = LedgerChartOfAccount.GLAccountUse,
                        GLReportingAccount = LedgerChartOfAccount.GLReportingAccount,
                        GLOtherNotes = LedgerChartOfAccount.GLOtherNotes,
                        CurrencyID = LedgerChartOfAccount.CurrencyID,
                        CurrencyExchangeRate = LedgerChartOfAccount.CurrencyExchangeRate,
                        CashFlowType = LedgerChartOfAccount.CashFlowType
                    };

                return await ledgerCharts.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public async Task<IPagedList<NextNumber>> GetAllNextNumbers(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var nexyNumbers =
                  from NextNumber in _nextNumberRepo.GetAll()
                  where string.IsNullOrWhiteSpace(searchTerm) ||
                  NextNumber.NextNumberName.Contains(searchTerm)
                  orderby NextNumber.CreationTime descending

                  select new NextNumber
                  {
                      Id = NextNumber.Id,
                      NextNumberName = NextNumber.NextNumberName,
                      NextNumberValue = NextNumber.NextNumberValue,
                      NextNumberPrefix = NextNumber.NextNumberPrefix,
                      NextNumberSeparator = NextNumber.NextNumberSeparator
                  };

                return await nexyNumbers.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        //public async Task<IPagedList<Vendor>> GetAllVendors(int pageNumber, int pageSize, string searchTerm)
        //{
        //    try
        //    {
        //        var vendors =
        //            from Vendor in _vendorRepo.GetAll()
        //            where string.IsNullOrWhiteSpace(searchTerm) ||
        //            Vendor.Name.Contains(searchTerm) ||
        //            Vendor.Name.Contains(searchTerm)
        //            orderby Vendor.CreationTime descending

        //            select new Vendor
        //            {
        //                Id = Vendor.Id,
        //                Name = Vendor.Name,
        //                ContactName = Vendor.ContactName,
        //                Address = Vendor.Address,
        //                Email = Vendor.Email,
        //                Phone = Vendor.Phone,
        //                CompanyRegistrationNumber = Vendor.CompanyRegistrationNumber,
        //                BankName = Vendor.BankName,
        //                BankAccountNumber = Vendor.BankAccountNumber,
        //            };

        //        return await vendors.AsTracking().ToPagedListAsync(pageNumber, pageSize);
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        public async Task<IPagedList<WarehouseBin>> GetAllWarehouseBins(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var warehouseBins =
                    from WarehouseBin in _warehouseBinRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) ||
                    WarehouseBin.WarehouseBinName.Contains(searchTerm) ||
                    WarehouseBin.WarehouseBinIDCode.Contains(searchTerm) ||
                    WarehouseBin.WarehouseBinType.Contains(searchTerm)
                    orderby WarehouseBin.CreationTime descending

                    select new WarehouseBin
                    {
                        Id = WarehouseBin.Id,
                        WarehouseBinIDCode = WarehouseBin.WarehouseBinIDCode,
                        WarehouseBinName = WarehouseBin.WarehouseBinName,
                        WarehouseBinNumber = WarehouseBin.WarehouseBinNumber,
                        WarehouseBinType = WarehouseBin.WarehouseBinType,
                        WarehouseBinHeight = WarehouseBin.WarehouseBinHeight,
                        WarehouseBinWidth = WarehouseBin.WarehouseBinWidth,
                        WarehouseBinLength = WarehouseBin.WarehouseBinLength,
                        WarehouseBinWeight = WarehouseBin.WarehouseBinWeight,
                        WarehouseBinLocation = WarehouseBin.WarehouseBinLocation,
                        WarehouseBinZone = WarehouseBin.WarehouseBinZone,
                        MinimumQuantity = WarehouseBin.MinimumQuantity,
                        MaximumQuantity = WarehouseBin.MaximumQuantity,
                        OverFlowBin = WarehouseBin.OverFlowBin,
                        WarehouseID = WarehouseBin.WarehouseID,
                        IsActive = WarehouseBin.IsActive,
                        WarehouseName = WarehouseBin.Warehouse.WarehouseName
                    };

                return await warehouseBins.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<IPagedList<WarehouseBinType>> GetAllWarehouseBinTypes(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var warehouseBinTypes = (from wbt in _warehouseBinTypeRepo.GetAll()
                                         where wbt.WarehouseBinTypeName.Contains(searchTerm) ||
                                         wbt.WarehouseBinTypeDescription.Contains(searchTerm)
                                         orderby wbt.WarehouseBinTypeName
                                         select wbt);
                return await warehouseBinTypes.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
            //throw new NotImplementedException();
        }

        public async Task<IPagedList<WarehouseDTO>> GetAllWarehouses(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var warehouses =
                    from Warehouse in _warehouseRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) ||
                    Warehouse.WarehouseName.Contains(searchTerm) ||
                    Warehouse.WarehouseCode.Contains(searchTerm)
                    orderby Warehouse.CreationTime descending

                    select new WarehouseDTO
                    {
                        Id = Warehouse.Id,
                        WarehouseCode = Warehouse.WarehouseCode,
                        WarehouseName = Warehouse.WarehouseName,
                        WarehouseAddress1 = Warehouse.WarehouseAddress1,
                        WarehouseAddress2 = Warehouse.WarehouseAddress2,
                        WarehouseEmail = Warehouse.WarehouseEmail,
                        WarehousePhone = Warehouse.WarehousePhone,
                        WarehouseCity = Warehouse.WarehouseCity,
                        WarehouseState = Warehouse.WarehouseState,
                        WarehouseZip = Warehouse.WarehouseZip,
                        WarehouseFax = Warehouse.WarehouseFax,
                        SalesControlAccount = Warehouse.SalesControlAccount,
                        StockControlAccount = Warehouse.StockControlAccount,
                        COSControlAccount = Warehouse.COSControlAccount,
                        IsActive = Warehouse.IsActive
                    };

                return await warehouses.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task<Attributes> GetAttribute(int Id)
        {
            try
            {
                return _ = _attributeRepo.Get(Id);
            }
            catch (Exception ex)
            {
                return null;
            }
            //throw new NotImplementedException();
        }

        public async Task<InventoryAdjustmentType> GetInventoryAdjustmentType(int Id)
        {
            var inventoryAdjustmentType = _inventoryAdjustmentTypeRepo.FirstOrDefault(x => x.Id == Id);

            if (inventoryAdjustmentType == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.INVENTORYADJUSTMENTTYPE_NOT_EXIST);
            }

            return new InventoryAdjustmentType
            {
                Id = inventoryAdjustmentType.Id,
                AdjustmentTypeName = inventoryAdjustmentType.AdjustmentTypeName,
                AdjustmentTypeDescription = inventoryAdjustmentType.AdjustmentTypeDescription
            };
        }

        public async Task<InventoryItem> GetInventoryItem(int Id)
        {
            try
            {
                return _ = _inventoryItemRepo.Get(Id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<ItemCategory> GetItemCategory(int Id)
        {
            try
            {
                return _ = _itemCategoryRepo.Get(Id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<ItemFamily> GetItemFamily(int Id)
        {
            var family = await _itemFamilyRepo.GetAsync(Id);

            if (family == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ITEMFAMILY_NOT_EXIST);
            }

            var itemFamily = new ItemFamily
            {
                Id = family.Id,
                ItemFamilyCode = family.ItemFamilyCode,
                FamilyDescription = family.FamilyDescription,
                FamilyLongDescription = family.FamilyLongDescription,
                FamilyPictureURL = family.FamilyPictureURL
            };

            return itemFamily;
        }

        public async Task<ItemType> GetItemType(int Id)
        {
            try
            {
                return _ = _itemTypeRepo.Get(Id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<LedgerChartOfAccount> GetLedgerChartOfAccount(int Id)
        {
            var ledgerChartOfAccount = await _ledgerChartOfAccountRepo.GetAsync(Id);

            if (ledgerChartOfAccount == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.LEDGERCHARTOFACCOUNT_NOT_EXIST);
            }

            return new LedgerChartOfAccount
            {
                Id = ledgerChartOfAccount.Id,
                GLAccountName = ledgerChartOfAccount.GLAccountName,
                CGLAccountNumber = ledgerChartOfAccount.CGLAccountNumber,
                GLAccountBalance = ledgerChartOfAccount.GLAccountBalance,
                GLAccountBeginningBalance = ledgerChartOfAccount.GLAccountBeginningBalance,
                GLAccountType = ledgerChartOfAccount.GLAccountType,
                GLBalanceType = ledgerChartOfAccount.GLBalanceType,
                GLAccountDescription = ledgerChartOfAccount.GLAccountDescription,
                GLAccountUse = ledgerChartOfAccount.GLAccountUse,
                GLReportingAccount = ledgerChartOfAccount.GLReportingAccount,
                GLOtherNotes = ledgerChartOfAccount.GLOtherNotes,
                CurrencyID = ledgerChartOfAccount.CurrencyID,
                CurrencyExchangeRate = ledgerChartOfAccount.CurrencyExchangeRate,
                CashFlowType = ledgerChartOfAccount.CashFlowType
            };
        }

        public async Task<NextNumber> GetNextNumber(int Id)
        {
            var nextNumber = await _nextNumberRepo.GetAsync(Id);

            if (nextNumber == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.NEXTNUMBER_NOT_EXIST);
            }

            return new NextNumber
            {
                Id = nextNumber.Id,
                NextNumberName = nextNumber.NextNumberName,
                NextNumberValue = nextNumber.NextNumberValue,
                NextNumberPrefix = nextNumber.NextNumberPrefix,
                NextNumberSeparator = nextNumber.NextNumberSeparator
            };
        }

        //public async Task<Vendor> GetVendor(int Id)
        //{
        //    var vendor = await _vendorRepo.GetAsync(Id);

        //    if (vendor == null)
        //    {
        //        throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VENDOR_NOT_EXIST);
        //    }

        //    return new Vendor
        //    {
        //        Id = vendor.Id,
        //        Name = vendor.Name,
        //        ContactName = vendor.ContactName,
        //        Address = vendor.Address,
        //        Email = vendor.Email,
        //        Phone = vendor.Phone,
        //        CompanyRegistrationNumber = vendor.CompanyRegistrationNumber,
        //        BankName = vendor.BankName,
        //        BankAccountNumber = vendor.BankAccountNumber,
        //        VendorType = vendor.VendorType,
        //    };
        //}

        public async Task<WarehouseDTO> GetWarehouse(int Id)
        {
            var warehouse = await _warehouseRepo.GetAsync(Id);

            if (warehouse == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.WAREHOUSE_NOT_EXIST);
            }

            return new WarehouseDTO
            {
                Id = warehouse.Id,
                WarehouseCode = warehouse.WarehouseCode,
                WarehouseName = warehouse.WarehouseName,
                WarehouseAddress1 = warehouse.WarehouseAddress1,
                WarehouseAddress2 = warehouse.WarehouseAddress2,
                WarehouseEmail = warehouse.WarehouseEmail,
                WarehousePhone = warehouse.WarehousePhone,
                WarehouseCity = warehouse.WarehouseCity,
                WarehouseState = warehouse.WarehouseState,
                WarehouseZip = warehouse.WarehouseZip,
                WarehouseFax = warehouse.WarehouseFax,
                SalesControlAccount = warehouse.SalesControlAccount,
                StockControlAccount = warehouse.StockControlAccount,
                COSControlAccount = warehouse.COSControlAccount,
                IsActive = warehouse.IsActive
            };
        }

        public IEnumerable<WarehouseBin> GetBinByWarehouseId(int id)
        {
            var bins = (from bin in _warehouseBinRepo.GetAll()
                        join warehouse in _warehouseRepo.GetAll() on bin.WarehouseID equals warehouse.Id
                        where bin.WarehouseID == id

                        select new WarehouseBin
                        {
                            Id = bin.Id,
                            WarehouseBinIDCode = bin.WarehouseBinIDCode,
                            WarehouseBinName = bin.WarehouseBinName,
                            WarehouseBinNumber = bin.WarehouseBinNumber,
                            WarehouseBinType = bin.WarehouseBinType,
                            WarehouseBinHeight = bin.WarehouseBinHeight,
                            WarehouseBinWidth = bin.WarehouseBinWidth,
                            WarehouseBinLength = bin.WarehouseBinLength,
                            WarehouseBinWeight = bin.WarehouseBinWeight,
                            WarehouseBinLocation = bin.WarehouseBinLocation,
                            WarehouseBinZone = bin.WarehouseBinZone,
                            MinimumQuantity = bin.MinimumQuantity,
                            MaximumQuantity = bin.MaximumQuantity,
                            OverFlowBin = bin.OverFlowBin,
                            IsActive = bin.IsActive
                        });

            return bins;
        }


        public async Task<WarehouseBin> GetWarehouseBin(int Id)
        {
            var warehouseBin = await _warehouseBinRepo.GetAsync(Id);

            if (warehouseBin == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.WAREHOUSEBIN_NOT_EXIST);
            }

            var warehouseAttached = await _warehouseRepo.GetAsync(warehouseBin.WarehouseID);

            return new WarehouseBin
            {
                Id = warehouseBin.Id,
                WarehouseBinIDCode = warehouseBin.WarehouseBinIDCode,
                WarehouseBinName = warehouseBin.WarehouseBinName,
                WarehouseBinNumber = warehouseBin.WarehouseBinNumber,
                WarehouseBinType = warehouseBin.WarehouseBinType,
                WarehouseBinHeight = warehouseBin.WarehouseBinHeight,
                WarehouseBinWidth = warehouseBin.WarehouseBinWidth,
                WarehouseBinLength = warehouseBin.WarehouseBinLength,
                WarehouseBinWeight = warehouseBin.WarehouseBinWeight,
                WarehouseBinLocation = warehouseBin.WarehouseBinLocation,
                WarehouseBinZone = warehouseBin.WarehouseBinZone,
                MinimumQuantity = warehouseBin.MinimumQuantity,
                MaximumQuantity = warehouseBin.MaximumQuantity,
                OverFlowBin = warehouseBin.OverFlowBin,
                IsActive = warehouseBin.IsActive,
                WarehouseID = warehouseBin.WarehouseID,
                WarehouseName = warehouseAttached.WarehouseName

            };
        }

        public async Task<WarehouseBinType> GetWarehouseBinType(int Id)
        {
            try
            {
                return _ = _warehouseBinTypeRepo.Get(Id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //public async Task<bool> UpdateAttribute(Attributes model, int Id)
        //{
        //    try
        //    {
        //        if(model == null) { return false; }
        //        var data = _attributeRepo.Get(model.Id);
        //        if (data == null) { return false; }
        //        data.AttributeName = model.AttributeName;
        //        data.AttributeDescription = model.AttributeDescription;
        //        data.LastModificationTime = DateTime.Now;
        //        data.LastModifierUserId = _serviceHelper.GetCurrentUserId();

        //        await _attributeRepo.UpdateAsync(data);
        //        await _unitOfWork.SaveChangesAsync();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        public async Task<bool> UpdateAttribute(Attributes model, int Id)
        {
            var attribute = await _attributeRepo.GetAsync(Id);

            if (attribute == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ATTRIBUTE_NOT_EXIST);
            }

            attribute.AttributeName = model.AttributeName;
            attribute.AttributeDescription = model.AttributeDescription;

            await _unitOfWork.SaveChangesAsync();

            return true;

        }

        public async Task<bool> UpdateInventoryAdjustmentType(InventoryAdjustmentType model, int Id)
        {
            if (model is null)
                return false;

            var inventoryAdjustmentType = await _inventoryAdjustmentTypeRepo.GetAsync(Id);

            if (inventoryAdjustmentType == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.INVENTORYADJUSTMENTTYPE_NOT_EXIST);
            }

            inventoryAdjustmentType.AdjustmentTypeName = model.AdjustmentTypeName;
            inventoryAdjustmentType.AdjustmentTypeDescription = model.AdjustmentTypeDescription;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdateInventoryItem(InventoryItem model)
        {
            try
            {
                if (model == null) { return false; }
                var data = _inventoryItemRepo.Get(model.Id);
                if (data == null) { return false; }

                data.AllowInventoryTrans = default(bool) == model.AllowInventoryTrans ? data.AllowInventoryTrans : model.AllowInventoryTrans;
                data.AllowPurchaseTrans = model.AllowPurchaseTrans;
                data.AllowSalesTrans = model.AllowSalesTrans;
                data.Average = model.Average;
                data.AverageCost = model.AverageCost;
                data.AverageValue = model.AverageValue;
                data.Expected = model.Expected;
                data.ExpectedCost = model.ExpectedCost;
                data.ExpectedValue = model.ExpectedValue;
                data.FIFOCost = model.FIFOCost;
                data.FIFOFIFOValue = model.FIFOFIFOValue;
                data.GLItemCOGSAccountId = model.GLItemCOGSAccountId;
                data.GLItemInventoryAccountId = model.GLItemInventoryAccountId;
                data.GLItemSalesAccountId = model.GLItemSalesAccountId;
                data.IsActive = model.IsActive;
                data.IsSerialLotItem = model.IsSerialLotItem;
                //data.ItemCategory = model.ItemCategory;
                data.ItemCategoryID = model.ItemCategoryID;
                data.ItemCode = model.ItemCode;
                data.ItemColor = model.ItemColor;
                data.ItemDefaultWarehouseID = model.ItemDefaultWarehouseID;
                //...
                data.ItemName = data.ItemName;
                data.itemDescription = model.itemDescription;
                //...
                await _inventoryItemRepo.UpdateAsync(data);
                await _unitOfWork.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> UpdateInventoryItem(InventoryItem model, int Id)
        {
            try
            {
                if (model == null) { return false; }
                var data = await _inventoryItemRepo.GetAsync(Id);
                if (data == null) { return false; }



                data.AllowInventoryTrans = default(bool) == model.AllowInventoryTrans ? data.AllowInventoryTrans : model.AllowInventoryTrans;
                data.AllowPurchaseTrans = model.AllowPurchaseTrans;
                data.AllowSalesTrans = model.AllowSalesTrans;
                data.Average = model.Average;
                data.AverageCost = model.AverageCost;
                data.AverageValue = model.AverageValue;
                data.Expected = model.Expected;
                data.ExpectedCost = model.ExpectedCost;
                data.ExpectedValue = model.ExpectedValue;
                data.FIFOCost = model.FIFOCost;
                data.FIFOFIFOValue = model.FIFOFIFOValue;
                data.GLItemCOGSAccountId = model.GLItemCOGSAccountId;
                data.GLItemInventoryAccountId = model.GLItemInventoryAccountId;
                data.GLItemSalesAccountId = model.GLItemSalesAccountId;
                data.IsActive = model.IsActive;
                data.IsSerialLotItem = model.IsSerialLotItem;
                //data.ItemCategory = model.ItemCategory;
                data.ItemCategoryID = model.ItemCategoryID;
                data.ItemCode = model.ItemCode;
                data.ItemColor = model.ItemColor;
                data.ItemDefaultWarehouseID = model.ItemDefaultWarehouseID;
                data.ItemWeight = model.ItemWeight;
                //...
                data.ItemName = data.ItemName;
                data.itemDescription = model.itemDescription;
                //...
                await _inventoryItemRepo.UpdateAsync(data);
                await _unitOfWork.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        //public async Task<bool> UpdateItemCategory(ItemCategory model)
        //{
        //    try
        //    {
        //        if (model == null) { return false; }
        //        var data = _itemCategoryRepo.Get(model.Id);
        //        if(data == null) { return false; }

        //        data.ItemCategoryCode = model.ItemCategoryCode;
        //        data.CategoryDescription = model.CategoryDescription;
        //        data.CategoryLongDescription = model.CategoryLongDescription;
        //        data.CategoryName = model.CategoryName;

        //        await _itemCategoryRepo.UpdateAsync(data);
        //        await _unitOfWork.SaveChangesAsync();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //    //throw new NotImplementedException();
        //}

        public async Task<bool> UpdateItemCategory(ItemCategory model, int Id)
        {
            var itemType = await _itemCategoryRepo.GetAsync(Id);

            if (itemType == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.INVENTORYCATEGORY_NOT_EXIST);
            }

            itemType.ItemCategoryCode = model.ItemCategoryCode;
            itemType.CategoryName = model.CategoryName;
            itemType.CategoryDescription = model.CategoryDescription;
            itemType.CategoryLongDescription = model.CategoryLongDescription;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdateItemFamily(ItemFamily model, int Id)
        {
            var itemFamily = await _itemFamilyRepo.GetAsync(Id);

            if (itemFamily == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ITEMFAMILY_NOT_EXIST);
            }

            itemFamily.ItemFamilyCode = model.ItemFamilyCode;
            itemFamily.FamilyDescription = model.FamilyDescription;
            itemFamily.FamilyLongDescription = model.FamilyLongDescription;
            itemFamily.FamilyPictureURL = model.FamilyPictureURL;

            await _unitOfWork.SaveChangesAsync();

            return true;

        }

        public async Task<bool> UpdateItemType(ItemType model, int Id)
        {
            var itemType = await _itemTypeRepo.GetAsync(Id);

            if (itemType == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ITEMTYPE_NOT_EXIST);
            }

            itemType.ItemTypeName = model.ItemTypeName;
            itemType.ItemTypeDescription = model.ItemTypeDescription;

            await _unitOfWork.SaveChangesAsync();

            return true;

        }

        //public async Task<bool> UpdateItemType(ItemType model)
        //{
        //    try
        //    {
        //        if(model == null) { return false; }
        //        var data = _itemTypeRepo.Get(model.Id);
        //        if(data == null) { return false; }

        //        data.ItemTypeName = model.ItemTypeName;
        //        data.ItemTypeDescription = model.ItemTypeDescription;

        //        await _itemTypeRepo.UpdateAsync(data);
        //        await _unitOfWork.SaveChangesAsync();

        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

        public async Task<bool> UpdateLedgerChartOfAccount(LedgerChartOfAccount model, int Id)
        {
            var ledgerChartOfAccount = await _ledgerChartOfAccountRepo.GetAsync(Id);

            if (ledgerChartOfAccount == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.LEDGERCHARTOFACCOUNT_NOT_EXIST);
            }

            ledgerChartOfAccount.GLAccountName = model.GLAccountName;
            ledgerChartOfAccount.CGLAccountNumber = model.CGLAccountNumber;
            ledgerChartOfAccount.GLAccountType = model.GLAccountType;
            ledgerChartOfAccount.GLAccountDescription = model.GLAccountDescription;
            ledgerChartOfAccount.GLAccountBalance = model.GLAccountBalance;
            ledgerChartOfAccount.GLAccountBeginningBalance = model.GLAccountBeginningBalance;
            ledgerChartOfAccount.GLAccountUse = model.GLAccountUse;
            ledgerChartOfAccount.GLBalanceType = model.GLBalanceType;
            ledgerChartOfAccount.GLOtherNotes = model.GLOtherNotes;
            ledgerChartOfAccount.GLReportingAccount = model.GLReportingAccount;
            ledgerChartOfAccount.CurrencyExchangeRate = model.CurrencyExchangeRate;
            ledgerChartOfAccount.CashFlowType = model.CashFlowType;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdateNextNumber(NextNumber model, int Id)
        {
            var nextNumber = await _nextNumberRepo.GetAsync(Id);

            if (nextNumber == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.NEXTNUMBER_NOT_EXIST);
            }

            nextNumber.NextNumberName = model.NextNumberName;
            nextNumber.NextNumberValue = model.NextNumberValue;
            nextNumber.NextNumberPrefix = model.NextNumberPrefix;
            nextNumber.NextNumberSeparator = model.NextNumberSeparator;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        //public async Task<bool> UpdateVendor(Vendor model, int Id)
        //{
        //    var vendor = await _vendorRepo.GetAsync(Id);

        //    if (vendor == null)
        //    {
        //        throw await _serviceHelper.GetExceptionAsync(ErrorConstants.VENDOR_NOT_EXIST);
        //    }

        //    vendor.Name = model.Name;
        //    vendor.Phone = model.Phone;
        //    vendor.ContactName = model.ContactName;
        //    vendor.Address = model.Address;
        //    vendor.Email = model.Email;
        //    vendor.CompanyRegistrationNumber = model.CompanyRegistrationNumber;
        //    vendor.BankAccountNumber = model.BankAccountNumber;
        //    vendor.BankName = model.BankName;
        //    vendor.VendorType = model.VendorType;


        //    await _unitOfWork.SaveChangesAsync();

        //    return true;
        //}

        public async Task<bool> UpdateWarehouse(WarehouseDTO model, int Id)
        {
            var warehouse = await _warehouseRepo.GetAsync(Id);

            if (warehouse == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.WAREHOUSE_NOT_EXIST);
            }

            warehouse.WarehouseCode = model.WarehouseCode;
            warehouse.WarehouseName = model.WarehouseName;
            warehouse.WarehouseAddress1 = model.WarehouseAddress1;
            warehouse.WarehouseAddress2 = model.WarehouseAddress2;
            warehouse.WarehousePhone = model.WarehousePhone;
            warehouse.WarehouseEmail = model.WarehouseEmail;
            warehouse.WarehouseCity = model.WarehouseCity;
            warehouse.WarehouseState = model.WarehouseState;
            warehouse.WarehouseZip = model.WarehouseZip;
            warehouse.WarehouseFax = model.WarehouseFax;
            warehouse.SalesControlAccount = model.SalesControlAccount;
            warehouse.StockControlAccount = model.StockControlAccount;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdateWarehouseBin(WarehouseBin model, int Id)
        {
            var warehouseBin = await _warehouseBinRepo.GetAsync(Id);

            if (warehouseBin == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.WAREHOUSEBIN_NOT_EXIST);
            }

            warehouseBin.WarehouseBinName = model.WarehouseBinName;
            warehouseBin.WarehouseBinNumber = model.WarehouseBinNumber;
            warehouseBin.WarehouseBinType = model.WarehouseBinType;
            warehouseBin.WarehouseBinWeight = model.WarehouseBinWeight;
            warehouseBin.WarehouseBinWidth = model.WarehouseBinWidth;
            warehouseBin.WarehouseBinLocation = model.WarehouseBinLocation;
            warehouseBin.WarehouseBinLength = model.WarehouseBinLength;
            warehouseBin.WarehouseBinHeight = model.WarehouseBinHeight;
            warehouseBin.WarehouseBinZone = model.WarehouseBinZone;
            warehouseBin.MaximumQuantity = model.MaximumQuantity;
            warehouseBin.MinimumQuantity = model.MinimumQuantity;
            warehouseBin.OverFlowBin = model.OverFlowBin;
            warehouseBin.WarehouseID = model.WarehouseID;


            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdateWarehouseBinType(WarehouseBinType model)
        {
            try
            {
                if (model == null || model?.Id == default) { return false; }
                var data = _warehouseBinTypeRepo.Get(model.Id);
                if (data == null) { return false; }

                data.WarehouseBinTypeName = model.WarehouseBinTypeName;
                data.WarehouseBinTypeDescription = model.WarehouseBinTypeDescription;

                await _warehouseBinTypeRepo.UpdateAsync(data);
                await _unitOfWork.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            //throw new NotImplementedException();
        }

    }
}