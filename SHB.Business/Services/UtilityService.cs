﻿using IPagedList;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Core.Utils;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using SHB.Core.Domain.Entities;
using System;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing;
using Microsoft.AspNetCore.Hosting;
using System.Collections.Generic;
using SHB.Core.Entities.Enums;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Text;
using Newtonsoft.Json.Linq;
using SHB.Core.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Amazon.S3.Model;
using Amazon;
using Amazon.S3;

namespace SHB.Business.Services
{
    public interface IUtilityService
    {
        #region Verifications

        #endregion

        #region DigitalOceanImage
        Task<string> UplaodBytesToDigOc(ImageFileDTO model);
        Task<bool> UplaodFormFileToDigOc(ImageFileDTO model);
        Task<FileStreamResult> GetUploadFiles(string ImageName);
        Task<bool> RemoveUploadImage(string imageName);
        Task<string> UploadBase64Img(ImageFileDTO model);
        #endregion

        #region WhatsAppPushMassage

        #endregion

        #region Upload Excel/Csv/Files
        //Task<bool> UploadFiles(IFormFile file);
        #endregion

    }

    public class UtilityService : IUtilityService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceHelper _serviceHelper;
        private readonly AppConfig appConfig;
        private readonly IRepository<ImageFile> _imgRepo;

        public static IAmazonS3 s3Client;

        public UtilityService(IUnitOfWork unitOfWork, IRepository<ImageFile> imgRepo,
            IServiceHelper serviceHelper, IOptions<AppConfig> _appConfig)
        {
            _unitOfWork = unitOfWork;
            _serviceHelper = serviceHelper;
            appConfig = _appConfig.Value;
            _imgRepo = imgRepo;
        }

        #region Verifications

        #endregion

        #region DigitalOceanImage

        public async Task<string> UplaodBytesToDigOc(ImageFileDTO model)
        {
            model.AccessKey = appConfig.AccessKey;
            model.SecretKey = appConfig.SecretKey;
            model.bucketName = appConfig.bucketName;
            model.HostUploadURL = appConfig.HostUploadURL;
            model.DisplayFileContentURL = appConfig.DisplayFileContentURL;

            var s3ClientConfig = new AmazonS3Config
            {
                ServiceURL = model.HostUploadURL,
            };
            s3Client = new AmazonS3Client(model.AccessKey, model.SecretKey, s3ClientConfig);
            try
            {
                if (model.PictureList.Count() != 0)
                {
                    foreach (var file in model.PictureList)
                    {
                        byte[] data = file.data;
                        ByteArrayContent bytes = new ByteArrayContent(data);
                        var stream = new MemoryStream(data);
                        IFormFile fromFile = new FormFile(stream, 0, data.Length, file.FilenameWithOutExt, file.fileName);
                        var putRequest = new PutObjectRequest()
                        {
                            BucketName = model.bucketName,
                            Key = fromFile.FileName,
                            InputStream = fromFile.OpenReadStream(),
                            ContentType = file.ContentType,
                            CannedACL = S3CannedACL.PublicReadWrite
                            //uploadRequest.AddHeader("x-amz-acl", "public-read");
                        };

                        var result = await s3Client.PutObjectAsync(putRequest);
                        model.UploadPath = model.DisplayFileContentURL + file.fileName;
                        model.FileReference = CommonHelper.RandomDigits(6);
                        _imgRepo.Insert(new ImageFile
                        {
                            FileName = file.fileName,
                            UserId = (int)_serviceHelper.GetCurrentUserId(),
                            TenantId = _serviceHelper.GetTenantId(),
                            CreatorUserId = _serviceHelper.GetCurrentUserId(),
                            FileReference = model.FileReference,
                            UploadPath = model.UploadPath,
                            UploadType = (UploadType)file.UploadFormat,
                            UploadFormat = UploadFormat.jpeg
                        });

                       await _unitOfWork.SaveChangesAsync();
                        return model.UploadPath;
                    }

                }

            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error encountered ***. Message:'{0}' when writing an object", e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
                if (e.Message.Contains("disposed"))
                    return "false";
            }
            return model.UploadPath;

        }

        public async Task<bool> UplaodFormFileToDigOc(ImageFileDTO model)
        {
            model.AccessKey = appConfig.AccessKey;
            model.SecretKey = appConfig.SecretKey;
            model.UploadPath = model.UploadPath;
            model.FolderName = model.FolderName;
            model.FileReference = model.FileReference;

            model.bucketName = appConfig.bucketName;
            model.HostUploadURL = appConfig.HostUploadURL;
            model.DisplayFileContentURL = model.DisplayFileContentURL;
            var s3ClientConfig = new AmazonS3Config
            {
                ServiceURL = model.HostUploadURL,
                //RegionEndpoint = RegionEndpoint.EUCentral1
            };

            s3Client = new AmazonS3Client(model.AccessKey, model.SecretKey, s3ClientConfig);

            try
            {
                var putRequest = new PutObjectRequest()
                {
                    BucketName = model.bucketName,
                    Key = model.file.FileName,
                    InputStream = model.file.OpenReadStream(),
                    ContentType = model.file.ContentType
                };

                var result = await s3Client.PutObjectAsync(putRequest);

                return true;
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error encountered ***. Message:'{0}' when writing an object", e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
                if (e.Message.Contains("disposed"))
                    return true;
            }
            return false;

        }

        public async Task<FileStreamResult> GetUploadFiles(string ImageName)
        {
            var bucketname = appConfig.bucketName;
            var request = new GetObjectRequest()
            {
                BucketName = bucketname,
                Key = ImageName,
            };
            using GetObjectResponse response = await s3Client.GetObjectAsync(request);
            using Stream responseStream = response.ResponseStream;
            var stream = new MemoryStream();
            await responseStream.CopyToAsync(stream);
            stream.Position = 0;

            return new FileStreamResult(stream, response.Headers["Content-Type"])
            {
                FileDownloadName = ImageName
            };
        }

        public async Task<bool> RemoveUploadImage(string imageName)
        {
            var bucketname = appConfig.bucketName;
            var request = new DeleteObjectRequest()
            {
                BucketName = bucketname,
                Key = imageName,
            };
            var response = await s3Client.DeleteObjectAsync(request);

            return true;
            //return Ok(response);
        }
        public async Task<string> UploadBase64Img(ImageFileDTO model)
        {
            model.AccessKey = appConfig.AccessKey;
            model.SecretKey = appConfig.SecretKey;
            model.UploadPath = model.UploadPath;
            model.FolderName = model.FolderName;
            model.FileReference = model.FileReference;

            // Begin :  Image logic for insertion
            var fileName = "";
            string uploads = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", CoreConstants.Url.getImages);

            if (model.PicsList != null)
            {
                foreach (var pics in model.PicsList)
                {
                    byte[] bytes = Convert.FromBase64String(pics);
                    Image image;
                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        image = Image.FromStream(ms);
                    }

                    //var fileNameb = Path.GetFileNameWithoutExtension(pics);
                    //var extension = Path.GetExtension(pics);
                    //fileNameb = DateTime.UtcNow.ToString("yymmssfff") + "_" + fileName + extension;
                    var ImageName = Guid.NewGuid().ToString().Replace("-", "") + "." + model.UploadFormat;
                    fileName = uploads + "\\" + ImageName;
                    image.Save(fileName, ImageFormat.Png);

                    _imgRepo.Insert(new ImageFile
                    {
                        FileName = ImageName.ToString(),
                        UserId = (int)_serviceHelper.GetCurrentUserId(),
                        TenantId = _serviceHelper.GetTenantId(),
                        FileReference = "",
                        UploadPath = fileName,
                        UploadType = UploadType.Image,
                        //UploadFormat =  UploadFormat.
                    });




                }
                await _unitOfWork.SaveChangesAsync();
            }
            // End:  Image logic for insertion  

            return fileName;

        }


        //public static bool UploadFile(string filePath, string fileName, string folderName)
        //     {
        //         var s3ClientConfig = new AmazonS3Config
        //         {
        //             ServiceURL = endpoingURL

        //         };
        //         s3Client = new AmazonS3Client(s3ClientConfig);

        //         try
        //         {
        //             var fileTransferUtility = new TransferUtility(s3Client);
        //             var fileTransferUtilityRequest = new TransferUtilityUploadRequest
        //             {
        //                 BucketName = bucketName + @"/" + folderName,
        //                 FilePath = filePath,                     
        //                 StorageClass = S3StorageClass.StandardInfrequentAccess,
        //                 PartSize = 6291456, // 6 MB
        //                 Key = fileName,
        //                 CannedACL = S3CannedACL.PublicRead
        //             };
        //             fileTransferUtility.Upload(fileTransferUtilityRequest);
        //             return true;
        //         }
        //         catch (AmazonS3Exception e)
        //         {
        //             Console.WriteLine("Error encountered ***. Message:'{0}' when writing an object", e.Message);
        //         }
        //         catch (Exception e)
        //         {
        //             Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
        //             if (e.Message.Contains("disposed"))
        //                 return true;
        //         }
        //         return false;
        //     }


    }
    #endregion

    #region WhatsAppPushMassage
    //public async Task<string> SendWhatsappMessageAsync(WhatsAppMessageDTO message)
    //{
    //    string result = "";
    //    if (!string.IsNullOrWhiteSpace(message.RecipientWhatsapp))
    //    {
    //        WhatsAppMessagesDTO messageDTO = new WhatsAppMessagesDTO();
    //        messageDTO.Message.Add(message);
    //        result = await ConfigSendWhatsappMessageAsync(messageDTO);
    //    }
    //    return result;
    //}

    //public async Task<string> GetConsentDetailsAsync(WhatsappNumberDTO number)
    //{
    //    string result = "";
    //    if (!string.IsNullOrWhiteSpace(number.PhoneNumber))
    //    {
    //        result = await ConfigGetConsentDetailsAsync(number);
    //    }
    //    return result;
    //}

    //public async Task<string> ManageOptInOutAsync(ManageWhatsappConsentDTO consent)
    //{
    //    string result = "";
    //    if (consent != null)
    //    {
    //        result = await ConfigManageOptInOutAsync(consent);
    //    }
    //    return result;
    //}

    //private async Task<string> ConfigSendWhatsappMessageAsync(WhatsAppMessagesDTO message)
    //{
    //    try
    //    {
    //        var whatsappToken = ConfigurationManager.AppSettings["WhatsAppToken"];
    //        var whatsappUrl = ConfigurationManager.AppSettings["WhatsAppUrl"];
    //        whatsappUrl = $"{whatsappUrl}message/";

    //        string result = "";
    //        using (var client = new HttpClient())
    //        {
    //            if (whatsappToken != null && whatsappToken.Length != 0)
    //            {
    //                if (message != null)
    //                {
    //                    System.Net.ServicePointManager.SecurityProtocol |=
    //                                                                    SecurityProtocolType.Tls12 |
    //                                                                    SecurityProtocolType.Tls11 |
    //                                                                    SecurityProtocolType.Tls;

    //                    client.DefaultRequestHeaders.Accept.Clear();
    //                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", whatsappToken);
    //                    var json = JsonConvert.SerializeObject(message);
    //                    var data = new StringContent(json, Encoding.UTF8, "application/json");
    //                    var response = await client.PostAsync(whatsappUrl, data);
    //                    result = await response.Content.ReadAsStringAsync();
    //                    var status = JObject.Parse(result)["status"].ToString();
    //                    if (status.Contains("success"))
    //                    {
    //                        var id = JObject.Parse(result)["data"]["id"].ToString();
    //                        result = $"Status : {status}, Id : {id}";
    //                    }
    //                    else
    //                    {
    //                        result = $"Status : {status}";
    //                    }
    //                }
    //                else
    //                {
    //                    result = $"Message cannot be null. Please provide the appropriate values";
    //                }
    //            }
    //        }
    //        return result;
    //    }
    //    catch (Exception)
    //    {
    //        throw;
    //    }
    //}

    //private async Task<string> ConfigGetConsentDetailsAsync(WhatsappNumberDTO number)
    //{
    //    try
    //    {
    //        var whatsappToken = ConfigurationManager.AppSettings["WhatsAppToken"];
    //        var whatsappUrl = ConfigurationManager.AppSettings["WhatsAppUrl"];
    //        whatsappUrl = $"{whatsappUrl}consent/";
    //        string result = "";
    //        using (var client = new HttpClient())
    //        {
    //            if (whatsappToken != null && whatsappToken.Length != 0)
    //            {
    //                System.Net.ServicePointManager.SecurityProtocol |=
    //                                                                    SecurityProtocolType.Tls12 |
    //                                                                    SecurityProtocolType.Tls11 |
    //                                                                    SecurityProtocolType.Tls;
    //                client.DefaultRequestHeaders.Accept.Clear();
    //                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", whatsappToken);
    //                var json = JsonConvert.SerializeObject(number);
    //                var data = new StringContent(json, Encoding.UTF8, "application/json");
    //                var response = await client.PostAsync(whatsappUrl, data);
    //                result = await response.Content.ReadAsStringAsync();
    //                result = JObject.Parse(result)["status"].ToString();
    //            }
    //        }
    //        return result;
    //    }
    //    catch (Exception)
    //    {
    //        throw;
    //    }
    //}

    //private async Task<string> ConfigManageOptInOutAsync(ManageWhatsappConsentDTO consent)
    //{
    //    try
    //    {
    //        var whatsappToken = ConfigurationManager.AppSettings["WhatsAppToken"];
    //        var whatsappUrl = ConfigurationManager.AppSettings["WhatsAppUrl"];
    //        whatsappUrl = $"{whatsappUrl}consent/manage/";
    //        string result = "";
    //        using (var client = new HttpClient())
    //        {
    //            if (whatsappToken != null && whatsappToken.Length != 0)
    //            {
    //                System.Net.ServicePointManager.SecurityProtocol |=
    //                                                                    SecurityProtocolType.Tls12 |
    //                                                                    SecurityProtocolType.Tls11 |
    //                                                                    SecurityProtocolType.Tls;
    //                client.DefaultRequestHeaders.Accept.Clear();
    //                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", whatsappToken);
    //                var json = JsonConvert.SerializeObject(consent);
    //                var data = new StringContent(json, Encoding.UTF8, "application/json");
    //                var response = await client.PostAsync(whatsappUrl, data);
    //                result = await response.Content.ReadAsStringAsync();
    //                result = JObject.Parse(result)["status"].ToString();
    //            }
    //        }
    //        return result;
    //    }
    //    catch (Exception)
    //    {
    //        throw;
    //    }
    //}
    #endregion

    #region Upload Excel/Csv/Files
    //public async Task<bool> UploadFiles(IFormFile file)
    //{
    //    if (file?.Length > 0)
    //    {
    //        // convert to a stream
    //        var stream =  file.OpenReadStream();

    //        List<User> users = new List<User>();

    //        try
    //        {
    //            using (var package = new ExcelPackage(stream))
    //            {
    //                var worksheet = package.Workbook.Worksheets.First();
    //                var rowCount = worksheet.Dimension.Rows;

    //                for (var row = 2; row <= rowCount; row++)
    //                {
    //                    try
    //                    {
    //                        var name = worksheet.Cells[row, 1].Value?.ToString();
    //                        var email = worksheet.Cells[row, 2].Value?.ToString();
    //                        var phone = worksheet.Cells[row, 3].Value?.ToString();

    //                        var user = new User()
    //                        {
    //                            Email = email,
    //                            Name = name,
    //                            Phone = phone
    //                        };

    //                        users.Add(user);
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        Console.WriteLine(ex.Message);
    //                    }
    //                }
    //            }

    //            return true;
    //            //return View("Index", users);
    //        }
    //        catch (Exception ex)
    //        {
    //            Console.WriteLine(ex.Message);
    //        }
    //    }
    //    return await false;

    //}

    #endregion


}


