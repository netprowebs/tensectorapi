﻿using System;
using System.Collections.Generic;
using IPagedList;
using SHB.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using SHB.Data.Repository;
using SHB.Business.Services;
using SHB.Data.UnitOfWork;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Core.Domain.DataTransferObjects;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc;
using LME.Core.Domain.DataTransferObjects;

namespace SHB.Business.Services
{
    //public interface ITransportSetupService
    //{
    //    #region Route
    //    Task<IPagedList<RouteDTO>> GetRoutes(int pageNumber, int pageSize, string query = null);
    //    Task<RouteDTO> GetRouteById(int routeId);
    //    Task<List<TerminalDTO>> GetDestinationTerminals(int departureTerminalId);
    //    Task<List<RouteDTO>> GetTerminalRoutes(int terminalId);
    //    Task AddRoute(RouteDTO route);
    //    Task UpdateRoute(int routeId, RouteDTO route);
    //    Task RemoveRoute(int routeId);
    //    IQueryable<Route> GetAll();
    //    //Task<Route> SingleOrDefaultAsync(Expression<Func<Route, bool>> predicate);
    //    Task<string> GetDepartureterminalFromRoute(int? RouteId);
    //    Task<int> GetDepartureterminalIdFromRoute(int? RouteId);
    //    Task<string> GetDestinationterminalFromRoute(int? RouteId);
    //    Task<List<TerminalDTO>> GetRouteIdByDestinationAndDepartureId(int departureTerminalId, int destinationTerminalId);
    //    Task<List<TerminalDTO>> GetDepartureOnlineDiscountFromTerminalId(int? departureTerminalId);
    //    #endregion

    //}
    //public class TransportSetupService : IInventorySetupService
    //{
    //    private readonly IRepository<SeatManagement, long> _seatMgtRepo;
    //    private readonly IRepository<VehicleTripRegistration, Guid> _vehicleTripRegRepo;
    //    private readonly IRepository<VehicleModel> _vehicleModelRepo;
    //    private readonly IRepository<Fare> _fareRepo;
    //    private readonly IRepository<Trip, Guid> _tripRepo;
    //    private readonly IRepository<ExcludedSeat> _excludedSeatRepo;
    //    private readonly IRepository<Route> _repo;
    //    private readonly IRepository<State> _stateRepo;
    //    private readonly IRepository<Terminal> _terminalRepo;
    //    private readonly IRepository<Employee> _employeeRepo;
    //    //private readonly IRepository<JourneyManagement, Guid> _journeyManagementRepo;
    //    private readonly IServiceHelper _serviceHelper;
    //    private readonly IUnitOfWork _unitOfWork;
    //    private readonly IEmployeeService _employeeSvc;
    //    private readonly IUserService _userSvc;
    //    public TransportSetupService(
    //        IRepository<SeatManagement, long> seatMgtRepo, IUserService userSvc,
    //        IRepository<VehicleTripRegistration, Guid> vehicleTripRegRepo,
    //        IRepository<VehicleModel> vehicleModelRepo,

    //        IRepository<Trip, Guid> tripRepo, IRepository<ExcludedSeat> excludedSeatRepo,
    //        IRepository<Route> routeRepo, IRepository<State> stateRepo,
    //        IRepository<Terminal> terminalRepo,
    //        IRepository<Employee> employeeRepo, IRepository<Fare> fareRepo,
    //        IServiceHelper serviceHelper, IUnitOfWork unitOfWork, IEmployeeService employeeSvc
    //        )
    //    {
    //        _seatMgtRepo = seatMgtRepo;
    //        _userSvc = userSvc;
    //        _employeeSvc = employeeSvc;
    //        _vehicleTripRegRepo = vehicleTripRegRepo;
    //        _vehicleModelRepo = vehicleModelRepo;
    //        _fareRepo = fareRepo;
    //        _tripRepo = tripRepo;
    //        _excludedSeatRepo = excludedSeatRepo;
    //        _repo = routeRepo;
    //        _stateRepo = stateRepo;
    //        _terminalRepo = terminalRepo;
    //        _serviceHelper = serviceHelper;
    //        _unitOfWork = unitOfWork;
    //        _employeeRepo = employeeRepo;
    //    }

    //    public async Task UpdateRoute(int routeId, RouteDTO dto)
    //    {
    //        var route = await _repo.GetAsync(routeId);

    //        if (route is null)
    //        {
    //            throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ROUTE_NOT_EXIST);
    //        }

    //        if (route.DepartureTerminalId != dto.DepartureTerminalId || route.DestinationTerminalId != dto.DestinationTerminalId)
    //        {
    //            if (await _repo.ExistAsync(v => v.DepartureTerminalId == dto.DepartureTerminalId && v.DestinationTerminalId == dto.DestinationTerminalId))
    //            {
    //                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ROUTE_EXIST);
    //            }
    //        }

    //        route.Name = await GetRouteNameAsync(dto.DepartureTerminalId, dto.DestinationTerminalId);
    //        route.Type = dto.RouteType;
    //        route.DepartureTerminalId = dto.DepartureTerminalId;
    //        route.DestinationTerminalId = dto.DestinationTerminalId;
    //        route.DriverFee = dto.DriverFee;
    //        route.DispatchFee = dto.DispatchFee;
    //        route.LoaderFee = dto.LoaderFee;
    //        route.ParentRouteId = dto.ParentRouteId;
    //        route.AvailableAtTerminal = dto.AvailableAtTerminal;
    //        route.AvailableOnline = dto.AvailableOnline;

    //        if (dto.ParentRouteId != null)
    //        {
    //            route.ParentRoute = await GetParentRouteNameAsync(dto.ParentRouteId);
    //        }

    //        await _unitOfWork.SaveChangesAsync();
    //    }

    //    public async Task AddRoute(RouteDTO routeDto)
    //    {
    //        if (await _repo.ExistAsync(v => v.DepartureTerminalId == routeDto.DepartureTerminalId && v.DestinationTerminalId == routeDto.DestinationTerminalId))
    //        {
    //            throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ROUTE_EXIST);
    //        }

    //        routeDto.Name = await GetRouteNameAsync(routeDto.DepartureTerminalId, routeDto.DestinationTerminalId);

    //        if (routeDto.ParentRouteId != null)
    //        {
    //            routeDto.ParentRouteName = await GetParentRouteNameAsync(routeDto.ParentRouteId);
    //        }

    //        _repo.Insert(new Route
    //        {
    //            Name = routeDto.Name,
    //            ParentRoute = routeDto.ParentRouteName,
    //            ParentRouteId = routeDto.ParentRouteId,
    //            Type = routeDto.RouteType,
    //            DepartureTerminalId = routeDto.DepartureTerminalId,
    //            DestinationTerminalId = routeDto.DestinationTerminalId,
    //            DriverFee = routeDto.DriverFee,
    //            DispatchFee = routeDto.DispatchFee,
    //            LoaderFee = routeDto.LoaderFee,
    //            AvailableAtTerminal = routeDto.AvailableAtTerminal,
    //            AvailableOnline = routeDto.AvailableOnline
    //        });
    //        await _unitOfWork.SaveChangesAsync();
    //    }

    //    public Task<List<RouteDTO>> GetTerminalRoutes(int terminalId)
    //    {
    //        var routes =
    //            from route in GetAll()
    //            where route.DepartureTerminalId == terminalId && route.AvailableAtTerminal

    //            select new RouteDTO
    //            {
    //                Id = route.Id,
    //                Name = route.Name,
    //                RouteType = route.Type,
    //                AvailableOnline = route.AvailableOnline,
    //                AvailableAtTerminal = route.AvailableAtTerminal
    //            };

    //        return routes.AsNoTracking().ToListAsync();
    //    }
    //    public Task<List<TerminalDTO>> GetDestinationTerminals(int departureTerminalId)
    //    {
    //        var routes =
    //            from route in GetAll()
    //            join destination in _terminalRepo.GetAll() on route.DestinationTerminalId equals destination.Id
    //            join state in _stateRepo.GetAll() on destination.StateId equals state.Id
    //            where route.DepartureTerminalId == departureTerminalId && route.AvailableOnline

    //            select new TerminalDTO
    //            {
    //                Id = destination.Id,
    //                Name = state.Name + "-" + destination.Name
    //            };

    //        return routes.AsNoTracking().ToListAsync();
    //    }
    //    public IQueryable<Route> GetAll()
    //    {
    //        return _repo.GetAll();
    //    }

    //    public Task<IPagedList<RouteDTO>> GetRoutes(int pageNumber, int pageSize, string query)
    //    {
    //        var routes = from r in GetAll()
    //                     join depature in _terminalRepo.GetAll() on r.DepartureTerminalId equals depature.Id
    //                     join destination in _terminalRepo.GetAll() on r.DestinationTerminalId equals destination.Id
    //                     where r.IsDeleted == false
    //                     where string.IsNullOrWhiteSpace(query) || r.Name.Contains(query)
    //                     orderby r.Name

    //                     select new RouteDTO
    //                     {
    //                         Id = r.Id,
    //                         Name = r.Name,
    //                         RouteType = r.Type,
    //                         DepartureTerminalId = depature.Id,
    //                         DepartureTerminalName = depature.Name,
    //                         DestinationTerminalId = destination.Id,
    //                         DestinationTerminalName = destination.Name,
    //                         DriverFee = r.DriverFee,
    //                         DispatchFee = r.DispatchFee,
    //                         LoaderFee = r.LoaderFee,
    //                         AvailableOnline = r.AvailableOnline,
    //                         AvailableAtTerminal = r.AvailableAtTerminal,
    //                         ParentRouteName = r.ParentRoute
    //                     };

    //        return routes.ToPagedListAsync(pageNumber, pageSize);
    //    }

    //    public async Task<RouteDTO> GetRouteById(int routeId)
    //    {
    //        var route = await _repo.GetAsync(routeId);

    //        if (route is null)
    //        {
    //            throw await _serviceHelper.GetExceptionAsync(ErrorConstants.ROUTE_NOT_EXIST);
    //        }

    //        return new RouteDTO
    //        {
    //            Id = route.Id,
    //            Name = route.Name,
    //            RouteType = route.Type,
    //            DriverFee = route.DriverFee,
    //            DispatchFee = route.DispatchFee,
    //            LoaderFee = route.LoaderFee,
    //            AvailableOnline = route.AvailableOnline,
    //            AvailableAtTerminal = route.AvailableAtTerminal,
    //            ParentRouteId = route.ParentRouteId,
    //            DestinationTerminalId = route.DestinationTerminalId,
    //            DepartureTerminalId = route.DepartureTerminalId,
    //            ParentRouteName = route.ParentRoute
    //        };
    //    }

    //}
}