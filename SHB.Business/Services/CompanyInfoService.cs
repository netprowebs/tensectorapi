﻿using IPagedList;
using SHB.Core.Configuration;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.Core.Entities;
using SHB.Core.Exceptions;
using SHB.Core.Utils;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using SHB.Business.Messaging.Sms;
using SHB.Business.Messaging.Email;
using SHB.Core.Entities.Enums;

namespace SHB.Business.Services
{
    public interface ICompanyInfo
    {
        Task AddCompanyInfo(CompanyInfoDTO companyInfo);
        Task UpdateCompanyInfo(int id, CompanyInfoDTO companyInfo);
        Task<IPagedList<CompanyInfoDTO>> GetcompanyInfo(int page, int size, string query = null);
        Task<CompanyInfoDTO> GetcompanyInfoById(int id);
        Task<CompanyInfoDTO> GetcompanyInfoByName(string name);
        Task RemoveCompanyInfo(int id);

    }

    public class CompanyInfoService : ICompanyInfo
    {
        private readonly IRepository<CompanyInfo> _companyInforepo;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRoleService _roleService;
        private readonly RoleManager<Role> _roleManager;
        private readonly IEmployeeService _employeeService;
        private readonly UserManager<User> _userManager;
        private readonly IUserService _userSvc;
        private readonly IRepository<Employee> _repo;
        private readonly IRoleService _roleSvc;
        private readonly IAppMenu _appMenu;
        private readonly IMailService _mailSvc;
        private readonly ISMSService _smsSvc;
        private readonly AppConfig _appConfig;
        public CompanyInfoService(IRepository<CompanyInfo> CompanyInforepo, IServiceHelper serviceHelper, IUnitOfWork unitOfWork, IRoleService roleService,
            RoleManager<Role> roleManager, IEmployeeService employeeService, UserManager<User> userManager, IUserService userSvc, IRepository<Employee> employeeRepo, IMailService mailSvc,
            ISMSService smsSvc,
            IRoleService roleSvc, IAppMenu appMenu, IOptions<AppConfig> appConfig
            )
        {
            _companyInforepo = CompanyInforepo;
            _serviceHelper = serviceHelper;
            _unitOfWork = unitOfWork;
            _roleService = roleService;
            _roleManager = roleManager;
            _employeeService = employeeService;
            _userManager = userManager;
            _userSvc = userSvc;
            _repo = employeeRepo;
            _roleSvc = roleSvc;
            _appMenu = appMenu;
            _mailSvc = mailSvc;
            _smsSvc = smsSvc;
            _appConfig = appConfig.Value;
        }


        public async Task AddCompanyInfo(CompanyInfoDTO companyInfo)
        {
            companyInfo.Name = companyInfo.Name.Trim();

            if (await _companyInforepo.ExistAsync(c => c.Name == companyInfo.Name))
            {

                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.COMPANYINFO_EXIST);
            }

            _companyInforepo.Insert(new CompanyInfo
            {
                Name = companyInfo.Name,
                LastName = companyInfo.LastName,
                Address = companyInfo.Address,
                Email = companyInfo.Email,
                ContactPerson = companyInfo.ContactPerson,
                ContactPhoneNo = companyInfo.ContactPhoneNo,
                PilotPayrollDate = companyInfo.PilotPayrollDate,
                BookingDaysRange = companyInfo.BookingDaysRange,

                IsActive = companyInfo.IsActive,
                IsTranspRecieved = companyInfo.IsTranspRecieved,
                SmsUrl = companyInfo.SmsUrl,
                HostingUrl = companyInfo.HostingUrl,
                TenantUrl = companyInfo.TenantUrl,
                CountryId = companyInfo.CountryId,
                CityId = companyInfo.CityId,
                IsParentCompany = companyInfo.IsParentCompany,
                IsParentDate = companyInfo.IsParentDate,
                ImageUrlPath = companyInfo.ImageUrlPath,

                MJAPIKEYPRIVATE = companyInfo.MJAPIKEYPRIVATE,
                MJAPIKEYPUBLIC = companyInfo.MJAPIKEYPUBLIC,
                MJSenderEmail = companyInfo.MJSenderEmail,
                MJSenderName = companyInfo.MJSenderName,
                DOAccessKey = companyInfo.DOAccessKey,
                DOSecretKey = companyInfo.DOSecretKey,
                bucketName = companyInfo.bucketName,
                DOHostUploadURL = companyInfo.DOHostUploadURL,
                PYPublicKey = companyInfo.PYPublicKey,
                PYSecretKey = companyInfo.PYSecretKey,
                FlPublicKey = companyInfo.FlPublicKey,
                FlSecretKey = companyInfo.FlSecretKey,

                SmsEnableSSl = companyInfo.SmsEnableSSl,
                SmsPort = companyInfo.SmsPort,
                SmsServer = companyInfo.SmsServer,
                SmsPassword = companyInfo.SmsPassword,
                SmsUserName = companyInfo.SmsUserName,
                SmsUseDefaultCred = companyInfo.SmsUseDefaultCred,

               
            });
            await _unitOfWork.SaveChangesAsync();

            try
            {
                var newAdded = await GetcompanyInfoByName(companyInfo.Name);

                _unitOfWork.BeginTransaction();
                var dbrol = new Role
                {
                    IsActive = true,
                    Name = "Admin" + '-' + newAdded.Id,
                    RolesDescription = "Admin",
                    CompanyInfoId = newAdded.Id
                };

                await _roleManager.CreateAsync(dbrol);

                var user = new User
                {
                    FirstName = companyInfo.Name,
                    LastName = "Administrator",
                    MiddleName = companyInfo.LastName,
                    Gender = 0,
                    Email = companyInfo.Email,
                    PhoneNumber = companyInfo.ContactPhoneNo,
                    Address = companyInfo.Address,
                    NextOfKinName = null,
                    NextOfKinPhone = null,
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true,
                    UserName = companyInfo.Email,
                    ReferralCode = CommonHelper.GenereateRandonAlphaNumeric(),
                    UserType = UserType.Employee,
                    CreationTime = DateTime.Now,
                    IsFirstTimeLogin = false,
                    CompanyId = newAdded.Id
                    
                };

                //_userManager.CreateAsync(user, "123456");
                var creationStatus = await _userSvc.CreateAsync(user, "123456");

                var createdRole = "Admin" + '-' + newAdded.Id;

                if (creationStatus.Succeeded)
                {
                    //var dbRole = await _roleSvc.FindByIdAsync(0);

                    var dbRole = await _roleSvc.FindByName(createdRole);

                    if (dbRole != null)
                    {
                        await _userSvc.AddToRoleAsync(user, dbRole.Name);
                    }

                    var newGuid = Guid.NewGuid().ToString("N").Substring(1, 6);
                    var newDate = DateTime.Now;

                    _repo.Insert(new Employee
                    {
                        UserId = user.Id,
                        EmployeeCode = newGuid,
                        DateOfEmployment = newDate,
                        DepartmentId = 1,
                        TerminalId = companyInfo.CityId,
                        CreatorUserId = _serviceHelper.GetCurrentUserId(),
                        OtpIsUsed = false,
                        TicketRemovalOtpIsUsed = false,
                        IsDeleted = false,
                        CreationTime = DateTime.UtcNow
                    });


                    string smsMessage = $"Hello your login email and password are {user.Email} and 123456 respectively";
                    _smsSvc.SendSMSNow(smsMessage, recipient: newAdded.ContactPhoneNo.ToNigeriaMobile());

                    if (newAdded.Email != null)
                    {
                        var mail = new Mail(_appConfig.AppEmail, "New Company Login Details!", newAdded.Email)
                        {
                            Body = smsMessage
                        };
                        _mailSvc.SendMail(mail);
                    }


                    //creating claim for roles
                    //get all from appmenuitemaccess where defaultsetup is true
                    var claimNames = await _appMenu.GetAllMenuItemsAccess();
                    var menuItem = await _appMenu.GetAllMenuItems();
                    var defaultSetup = claimNames.Where(x => x.SetupDefault == true);

                    //var defaultSetups = (Claim)claimNames.Where(x => x.SetupDefault == true);
                    var mapToMenuItem = defaultSetup.Where(x => x.AppMenuId == 1);

                    // insert claims into asproleclaim with for each
                    foreach (var item in defaultSetup)
                    {
                        //await _roleManager.AddClaimAsync(dbrol, (Claim)defaultSetup.Select(x => x.ClaimName);
                        //await _roleManager.AddClaimAsync(dbrol, new Claim(ClaimTypes.Role as string, "Permission" as string));
                        var vpermision = item.IsActive ? Permission.Permission : Permission.NotPermitted;
                        await _roleManager.AddClaimAsync(dbrol, new Claim(vpermision.ToString() as string, item.ClaimName.ToString() as string));
                    }
                    //await SendAccountEmail(user);
                    _unitOfWork.Commit();
                }
                else
                {
                    _unitOfWork.Rollback();

                    throw await _serviceHelper
                        .GetExceptionAsync(creationStatus.Errors.FirstOrDefault()?.Description);
                }
                //_unitOfWork.Commit();

            }
            catch (Exception ex)
            {
                var eee = ex.Message;
                _unitOfWork.Rollback();
                throw;

            }

        }

        public async Task UpdateCompanyInfo(int id, CompanyInfoDTO companyInfo)
        {
            var model = await _companyInforepo.GetAsync(id);

            if (model == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.COMPANYINFO_NOT_EXIST);
            }
            model.Name = companyInfo.Name;
            model.LastName = companyInfo.LastName;
            model.Address = companyInfo.Address;
            model.Email = companyInfo.Email;
            model.ContactPerson = companyInfo.ContactPerson;
            model.ContactPhoneNo = companyInfo.ContactPhoneNo;
            model.PilotPayrollDate = companyInfo.PilotPayrollDate;
            model.BranchedFrom = companyInfo.BranchedFrom;

            model.IsActive = companyInfo.IsActive;
            model.IsTranspRecieved = companyInfo.IsTranspRecieved;
            model.SmsUrl = companyInfo.SmsUrl;
            model.HostingUrl = companyInfo.HostingUrl;
            model.TenantUrl = companyInfo.TenantUrl;
            model.CountryId = companyInfo.CountryId;
            model.CityId = companyInfo.CityId;
            model.IsParentCompany = companyInfo.IsParentCompany;
            model.IsParentDate = companyInfo.IsParentDate;
            model.ImageUrlPath = companyInfo.ImageUrlPath;

            model.MJAPIKEYPRIVATE = companyInfo.MJAPIKEYPRIVATE;
            model.MJAPIKEYPUBLIC = companyInfo.MJAPIKEYPUBLIC;
            model.MJSenderEmail = companyInfo.MJSenderEmail;
            model.MJSenderName = companyInfo.MJSenderName;
            model.DOAccessKey = companyInfo.DOAccessKey;
            model.DOSecretKey = companyInfo.DOSecretKey;
            model.bucketName = companyInfo.bucketName;
            model.DOHostUploadURL = companyInfo.DOHostUploadURL;
            model.PYPublicKey = companyInfo.PYPublicKey;
            model.PYSecretKey = companyInfo.PYSecretKey;
            model.FlPublicKey = companyInfo.FlPublicKey;
            model.FlSecretKey = companyInfo.FlSecretKey;

            model.SmsEnableSSl = companyInfo.SmsEnableSSl;
            model.SmsPort = companyInfo.SmsPort;
            model.SmsServer = companyInfo.SmsServer;
            model.SmsPassword = companyInfo.SmsPassword;
            model.SmsUserName = companyInfo.SmsUserName;
            model.SmsUseDefaultCred = companyInfo.SmsUseDefaultCred;

            await _companyInforepo.UpdateAsync(model);

            await _unitOfWork.SaveChangesAsync();
        }

        public Task<IPagedList<CompanyInfoDTO>> GetcompanyInfo(int page, int size, string query = null)
        {
            var companyInfos =
                from CompanyInfo in _companyInforepo.GetAll()
                where query == null || CompanyInfo.Name.Contains(query)
                select new CompanyInfoDTO
                {
                    Id = CompanyInfo.Id,
                    Name = CompanyInfo.Name,
                    LastName = CompanyInfo.LastName,
                    Address = CompanyInfo.Address,
                    ContactPerson = CompanyInfo.ContactPerson,
                    ContactPhoneNo = CompanyInfo.ContactPhoneNo,
                    PilotPayrollDate = CompanyInfo.PilotPayrollDate,
                    BranchedFrom = CompanyInfo.BranchedFrom,
                    IsActive = CompanyInfo.IsActive,
                    IsTranspRecieved = CompanyInfo.IsTranspRecieved,
                    SmsUrl = CompanyInfo.SmsUrl,
                    HostingUrl = CompanyInfo.HostingUrl,
                    TenantUrl = CompanyInfo.TenantUrl,
                    CountryId = CompanyInfo.CountryId,
                    CityId = CompanyInfo.CityId,
                    Email = CompanyInfo.Email,
                    IsParentCompany = CompanyInfo.IsParentCompany,
                    IsParentDate = CompanyInfo.IsParentDate,
                    ImageUrlPath = CompanyInfo.ImageUrlPath,

                    MJAPIKEYPRIVATE = CompanyInfo.MJAPIKEYPRIVATE,
                    MJAPIKEYPUBLIC = CompanyInfo.MJAPIKEYPUBLIC,
                    MJSenderEmail = CompanyInfo.MJSenderEmail,
                    MJSenderName = CompanyInfo.MJSenderName,
                    DOAccessKey = CompanyInfo.DOAccessKey,
                    DOSecretKey = CompanyInfo.DOSecretKey,
                    bucketName = CompanyInfo.bucketName,
                    DOHostUploadURL = CompanyInfo.DOHostUploadURL,
                    PYPublicKey = CompanyInfo.PYPublicKey,
                    PYSecretKey = CompanyInfo.PYSecretKey,
                    FlPublicKey = CompanyInfo.FlPublicKey,
                    FlSecretKey = CompanyInfo.FlSecretKey,

                    SmsEnableSSl = CompanyInfo.SmsEnableSSl,
                    SmsPort = CompanyInfo.SmsPort,
                    SmsServer = CompanyInfo.SmsServer,
                    SmsPassword = CompanyInfo.SmsPassword,
                    SmsUserName = CompanyInfo.SmsUserName,
                    SmsUseDefaultCred = CompanyInfo.SmsUseDefaultCred,
                };
            return companyInfos.AsTracking().ToPagedListAsync(page, size);
        }

        public async Task<CompanyInfoDTO> GetcompanyInfoById(int id)
        {

            var companyInfo = await _companyInforepo.GetAsync(id);

            if (companyInfo == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.COMPANYINFO_NOT_EXIST);
            }

            return new CompanyInfoDTO
            {
                Id = companyInfo.Id,
                Name = companyInfo.Name,
                LastName = companyInfo.LastName,
                Address = companyInfo.Address,
                ContactPerson = companyInfo.ContactPerson,
                ContactPhoneNo = companyInfo.ContactPhoneNo,
                PilotPayrollDate = companyInfo.PilotPayrollDate,
                BranchedFrom = companyInfo.BranchedFrom,
                IsActive = companyInfo.IsActive,
                IsTranspRecieved = companyInfo.IsTranspRecieved,
                SmsUrl = companyInfo.SmsUrl,
                HostingUrl = companyInfo.HostingUrl,
                TenantUrl = companyInfo.TenantUrl,
                CountryId = companyInfo.CountryId,
                CityId = companyInfo.CityId,
                Email = companyInfo.Email,
                IsParentCompany = companyInfo.IsParentCompany,
                IsParentDate = companyInfo.IsParentDate,
                ImageUrlPath = companyInfo.ImageUrlPath,

                MJAPIKEYPRIVATE = companyInfo.MJAPIKEYPRIVATE,
                MJAPIKEYPUBLIC = companyInfo.MJAPIKEYPUBLIC,
                MJSenderEmail = companyInfo.MJSenderEmail,
                MJSenderName = companyInfo.MJSenderName,
                DOAccessKey = companyInfo.DOAccessKey,
                DOSecretKey = companyInfo.DOSecretKey,
                bucketName = companyInfo.bucketName,
                DOHostUploadURL = companyInfo.DOHostUploadURL,
                PYPublicKey = companyInfo.PYPublicKey,
                PYSecretKey = companyInfo.PYSecretKey,
                FlPublicKey = companyInfo.FlPublicKey,
                FlSecretKey = companyInfo.FlSecretKey,

                SmsEnableSSl = companyInfo.SmsEnableSSl,
                SmsPort = companyInfo.SmsPort,
                SmsServer = companyInfo.SmsServer,
                SmsPassword = companyInfo.SmsPassword,
                SmsUserName = companyInfo.SmsUserName,
                SmsUseDefaultCred = companyInfo.SmsUseDefaultCred,


            };
        }

        public async Task<CompanyInfoDTO> GetcompanyInfoByName(string name)
        {

            var companyInfo = await _companyInforepo.FirstOrDefaultAsync(x => x.Name == name);

            if (companyInfo == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.COMPANYINFO_NOT_EXIST);
            }

            return new CompanyInfoDTO
            {
                Id = companyInfo.Id,
                Name = companyInfo.Name,
                LastName = companyInfo.LastName,
                Address = companyInfo.Address,
                ContactPerson = companyInfo.ContactPerson,
                ContactPhoneNo = companyInfo.ContactPhoneNo,
                PilotPayrollDate = companyInfo.PilotPayrollDate,
                BranchedFrom = companyInfo.BranchedFrom,
                IsActive = companyInfo.IsActive,
                IsTranspRecieved = companyInfo.IsTranspRecieved,
                SmsUrl = companyInfo.SmsUrl,
                HostingUrl = companyInfo.HostingUrl,
                TenantUrl = companyInfo.TenantUrl,
                CountryId = companyInfo.CountryId,
                CityId = companyInfo.CityId,
                Email = companyInfo.Email,
                IsParentCompany = companyInfo.IsParentCompany,
                IsParentDate = companyInfo.IsParentDate,
                ImageUrlPath = companyInfo.ImageUrlPath,

                MJAPIKEYPRIVATE = companyInfo.MJAPIKEYPRIVATE,
                MJAPIKEYPUBLIC = companyInfo.MJAPIKEYPUBLIC,
                MJSenderEmail = companyInfo.MJSenderEmail,
                MJSenderName = companyInfo.MJSenderName,
                DOAccessKey = companyInfo.DOAccessKey,
                DOSecretKey = companyInfo.DOSecretKey,
                bucketName = companyInfo.bucketName,
                DOHostUploadURL = companyInfo.DOHostUploadURL,
                PYPublicKey = companyInfo.PYPublicKey,
                PYSecretKey = companyInfo.PYSecretKey,
                FlPublicKey = companyInfo.FlPublicKey,
                FlSecretKey = companyInfo.FlSecretKey,

                SmsEnableSSl = companyInfo.SmsEnableSSl,
                SmsPort = companyInfo.SmsPort,
                SmsServer = companyInfo.SmsServer,
                SmsPassword = companyInfo.SmsPassword,
                SmsUserName = companyInfo.SmsUserName,
                SmsUseDefaultCred = companyInfo.SmsUseDefaultCred,

            };
        }

        public async Task RemoveCompanyInfo(int id)
        {
            var model = await _companyInforepo.GetAsync(id);

            if (model == null)
            {
                throw await _serviceHelper.GetExceptionAsync(ErrorConstants.COMPANYINFO_NOT_EXIST);
            }

            _companyInforepo.Delete(model);

            await _unitOfWork.SaveChangesAsync();
        }

    }
}
