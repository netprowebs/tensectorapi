﻿using System;
using System.Collections.Generic;
using System.Text;
using IPagedList;
using System.Threading.Tasks;
using SHB.Core.Domain.Entities;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using SHB.Core.Domain.DataTransferObjects;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace SHB.Business.Services
{
    public interface IVendorInformationService
    {

        #region Vendor Information Types
        Task AddVendorInformation(VendorInformationDTO model);
        Task<VendorInformationDTO> GetVendorInformation(int Id);
        Task<IPagedList<VendorInformation>> GetAllVendorInformation(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateVendorInformation(VendorInformationDTO model, int Id);
        Task DeleteVendorInformation(int Id);

        #endregion
    }
    public class VendorInformationService : IVendorInformationService
    {
        private readonly IRepository<VendorInformation> _VendorInformationRepo;

        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;

        private readonly IUserService _userSvc;
        public VendorInformationService(

             IRepository<VendorInformation> VendorInformationRepo,

            IServiceHelper serviceHelper,
            IUnitOfWork unitOfWork,

            IUserService userSvc
            )
        {

            _VendorInformationRepo = VendorInformationRepo;
            _serviceHelper = serviceHelper;
            _unitOfWork = unitOfWork;
            _userSvc = userSvc;
        }

        #region Vendor Types
        public async Task AddVendorInformation(VendorInformationDTO model)
        {
            model.VendorName = model.VendorName.Trim();
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);



            _VendorInformationRepo.Insert(new VendorInformation
            {
                VendorName = model.VendorName,
                VendorPhone = model.VendorPhone,
                VendorTypeID = model.VendorTypeID,
                IsActive = model.IsActive,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                CompanyID = (int)currentUser.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteVendorInformation(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _VendorInformationRepo.DeleteAsync(_VendorInformationRepo.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
            }
        }

        public async Task<IPagedList<VendorInformation>> GetAllVendorInformation(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var vendorInfo =
                    from model in _VendorInformationRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) ||
                    model.VendorName.Contains(searchTerm)

                    select new VendorInformation
                    {
                        CompanyID = model.CompanyID,
                        AccountStatus = model.AccountStatus,
                        VendorName = model.VendorName,
                        VendorAddress1 = model.VendorAddress1,
                        VendorAddress2 = model.VendorAddress2,
                        VendorAddress3 = model.VendorAddress3,
                        VendorCity = model.VendorCity,
                        VendorState = model.VendorState,
                        VendorZip = model.VendorZip,
                        VendorCountry = model.VendorCountry,
                        VendorPhone = model.VendorPhone,
                        VendorFax = model.VendorFax,
                        VendorWebPage = model.VendorWebPage,
                        VendorTypeID = model.VendorTypeID,
                        AccountNumber = model.AccountNumber,
                        ContactID = model.ContactID,
                        WarehouseID = model.WarehouseID,
                        CurrencyID = model.CurrencyID,
                        TermsID = model.TermsID,
                        TermsStart = model.TermsStart,
                        GLPurchaseAccount = model.GLPurchaseAccount,
                        TaxIDNo = model.TaxIDNo,
                        VATTaxIDNumber = model.VATTaxIDNumber,
                        VatTaxOtherNumber = model.VatTaxOtherNumber,
                        TaxGroupID = model.TaxGroupID,
                        CreditLimit = model.CreditLimit,
                        AvailibleCredit = model.AvailibleCredit,
                        CreditComments = model.CreditComments,
                        CreditRating = model.CreditRating,
                        ApprovalDate = model.ApprovalDate,
                        CustomerSince = model.CustomerSince,
                        FreightPayment = model.FreightPayment,
                        CustomerSpecialInstructions = model.CustomerSpecialInstructions,
                        SpecialTerms = model.SpecialTerms,
                        ConvertedFromCustomer = model.ConvertedFromCustomer,
                        VendorRegionID = model.VendorRegionID,
                        VendorSourceID = model.VendorSourceID,
                        VendorIndustryID = model.VendorIndustryID,
                        Confirmed = model.Confirmed,
                        ReferedBy = model.ReferedBy,
                        ReferedDate = model.ReferedDate,
                        ReferalURL = model.ReferalURL,
                        AccountBalance = model.AccountBalance,
                        IsActive = model.IsActive,
                    };
                return await vendorInfo.AsTracking().ToPagedListAsync(pageNumber,pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task<VendorInformationDTO> GetVendorInformation(int Id)
        {
            var model = _VendorInformationRepo.FirstOrDefault(x => x.Id == Id);

            //For Vendor Form
            //VendorName, VendorType , FirstName, lastname,CustomerPhone, DateCreated, Email, State ,Address
            return new VendorInformationDTO
            {
                ID = model.Id,
                VendorName = model.VendorName,
                VendorTypeID = model.VendorTypeID,
                VendorPhone = model.VendorPhone,
                VendorState = model.VendorState,
                VendorAddress1 = model.VendorAddress1,
            };
        }


        public async Task<bool> UpdateVendorInformation(VendorInformationDTO model, int Id)
        {
            var vendorType = _VendorInformationRepo.FirstOrDefault(x => x.Id == Id);

            //vendorType.Id = model.Id;
            vendorType.VendorName = model.VendorName;
            vendorType.VendorTypeID = model.VendorTypeID;
            vendorType.VendorPhone = model.VendorPhone;
            vendorType.VendorState = model.VendorState;
            vendorType.VendorAddress1 = model.VendorAddress1;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }
        #endregion
    }
}
