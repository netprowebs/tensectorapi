﻿using System;
using System.Collections.Generic;
using System.Text;
using IPagedList;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.Core.Domain.Entities.Enums;
using SHB.Core.Entities;
using SHB.Core.Entities.Enums;
using SHB.Core.Exceptions;
using SHB.Core.Utils;
using SHB.Data.efCore.Context;
using SHB.Data.Repository;
using SHB.Data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SHB.Business.Messaging.Email;
using SHB.Business.Messaging.Sms;

namespace SHB.Business.Services
{
    public interface IInvoiceService
    {

        #region Invoice Header
        Task AddInvoiceHeader(InvoiceHeaderDTO model);
        Task<InvoiceHeaderDTO> GetInvoiceHeader(Guid Id);
        Task<IPagedList<InvoiceHeaderDTO>> GetAllInvoiceHeader(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateInvoiceHeader(InvoiceHeaderDTO model, Guid Id);
        Task DeleteInvoiceHeader(Guid Id);

        #endregion

        #region Invoice Detail
        Task AddInvoiceDetail(InvoiceDetailDTO model);
        Task<List<InvoiceDetailDTO>> GetInvoiceDetail(Guid Id);
        Task<IPagedList<InvoiceDetailDTO>> GetAllInvoiceDetail(int pageNumber, int pageSize, string searchTerm);
        Task<IPagedList<InvoiceDetailDTO>> GetAllInvoiceDetailByUser(int pageNumber, int pageSize, string searchTerm);
        Task<bool> UpdateInvoiceDetail(InvoiceDetailDTO model, int Id);
        Task DeleteInvoiceDetail(int Id);
        #endregion
    }

    public class InvoiceService : IInvoiceService
    {
        private readonly IRepository<InvoiceHeader, Guid> _InvoiceHeaderRepo;
        private readonly IRepository<InvoiceDetail> _InvoiceDetailRepo;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUnitOfWork _unitOfWork;

        private readonly IUserService _userSvc;
        public InvoiceService(

             IRepository<InvoiceHeader, Guid> InvoiceHeaderRepo,
            IRepository<InvoiceDetail> InvoiceDetailRepo,
        IServiceHelper serviceHelper,
            IUnitOfWork unitOfWork,
            IUserService userSvc
            )
        {
            _InvoiceDetailRepo = InvoiceDetailRepo;
            _InvoiceHeaderRepo = InvoiceHeaderRepo;
            _serviceHelper = serviceHelper;
            _unitOfWork = unitOfWork;
            _userSvc = userSvc;
        }


        #region Invoice Header
        public async Task AddInvoiceHeader(InvoiceHeaderDTO model)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            _InvoiceHeaderRepo.Insert(new InvoiceHeader
            {
                Id = model.Id,
                InvoiceNumber = model.InvoiceNumber,
                Void = model.Void,
                Captured = model.Captured,
                Verified = model.Verified,
                VerifiedBy = model.VerifiedBy,
                VerifiedDate = model.VerifiedDate,
                Approved = model.Approved,
                ApprovedBy = model.ApprovedBy,
                ApprovedDate = model.ApprovedDate,
                Posted = model.Posted,
                PostedBy = model.PostedBy,
                PostedDate = model.PostedDate,
                Reference = model.Reference,
                UserId = model.UserId,
                AccTranType = model.AccTranType,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                TenantId = (int)currentUser.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteInvoiceHeader(Guid Id)
        {
            try
            {
                if (Id == null) { return; }
                await _InvoiceHeaderRepo.DeleteAsync(_InvoiceHeaderRepo.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<IPagedList<InvoiceHeaderDTO>> GetAllInvoiceHeader(int pageNumber, int pageSize, string searchTerm)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            try
            {
                var InvoiceHeader =
                    from model in _InvoiceHeaderRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) ||
                    model.InvoiceNumber.Contains(searchTerm)
                    orderby model.CreationTime descending

                    select new InvoiceHeaderDTO
                    {
                        Id = model.Id,
                        Void = model.Void,
                        Captured = model.Captured,
                        Verified = model.Verified,
                        VerifiedBy = model.VerifiedBy,
                        VerifiedDate = model.VerifiedDate,
                        Approved = model.Approved,
                        ApprovedBy = model.ApprovedBy,
                        ApprovedDate = model.ApprovedDate,
                        Posted = model.Posted,
                        PostedBy = model.PostedBy,
                        PostedDate = model.PostedDate,
                        Reference = model.Reference,
                    };

                return await InvoiceHeader.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task<InvoiceHeaderDTO> GetInvoiceHeader(Guid Id)
        {
            var model = _InvoiceHeaderRepo.FirstOrDefault(x => x.Id == Id);

            return new InvoiceHeaderDTO
            {
                Id = model.Id,
                Void = model.Void,
                Captured = model.Captured,
                Verified = model.Verified,
                VerifiedBy = model.VerifiedBy,
                VerifiedDate = model.VerifiedDate,
                Approved = model.Approved,
                ApprovedBy = model.ApprovedBy,
                ApprovedDate = model.ApprovedDate,
                Posted = model.Posted,
                PostedBy = model.PostedBy,
                PostedDate = model.PostedDate,
                Reference = model.Reference,
            };
        }


        public async Task<bool> UpdateInvoiceHeader(InvoiceHeaderDTO model, Guid Id)
        {
            var InvoiceHeader = _InvoiceHeaderRepo.FirstOrDefault(x => x.Id == model.Id);
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            if (model.Captured == true)
            {
                model.CreatorUserId = currentUser.Id;
                model.PostedDate = DateTime.Now;
            }


            //InvoiceHeader.Void = model.Void;
            //InvoiceHeader.Captured = model.Captured;
            //InvoiceHeader.Verified = model.Verified;
            //InvoiceHeader.VerifiedBy = model.VerifiedBy;
            //InvoiceHeader.VerifiedDate = model.VerifiedDate;
            //InvoiceHeader.Approved = model.Approved;
            //InvoiceHeader.ApprovedBy = model.ApprovedBy;
            //InvoiceHeader.ApprovedDate = model.ApprovedDate;
            //InvoiceHeader.Posted = model.Posted;
            //InvoiceHeader.PostedBy = model.PostedBy
            //InvoiceHeader.PostedDate = model.PostedDate;
            InvoiceHeader.PostedDate = model.PostedDate;
            InvoiceHeader.TenantId = (int)currentUser.CompanyId;
            InvoiceHeader.AccTranType = model.AccTranType;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        #endregion

        #region Invoice Detail
        public async Task AddInvoiceDetail(InvoiceDetailDTO model)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);

            _InvoiceDetailRepo.Insert(new InvoiceDetail
            {
                InvoiceId = model.InvoiceHeaderId,
                
                ItemID = model.ItemID,
                Description = model.Description,
                WarehouseID = model.WarehouseID,
                WarehouseBinID = model.WarehouseBinID,
                ItemUPCCode = model.ItemUPCCode,
                OrderQty = model.OrderQty,
                ItemUnitPrice = model.ItemUnitPrice,
                TaxAmount = model.TaxAmount,
                SerialNumber = model.SerialNumber,
                CreatorUserId = _serviceHelper.GetCurrentUserId(),
                CreationTime = DateTime.Now,
                TenantId = (int)currentUser.CompanyId
            });

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteInvoiceDetail(int Id)
        {
            try
            {
                if (Id == 0) { return; }
                await _InvoiceDetailRepo.DeleteAsync(_InvoiceDetailRepo.FirstOrDefault(x => x.Id == Id));
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<IPagedList<InvoiceDetailDTO>> GetAllInvoiceDetail(int pageNumber, int pageSize, string searchTerm)
        {
            try
            {
                var InvoiceDetail =
                    from model in _InvoiceDetailRepo.GetAll()
                    where string.IsNullOrWhiteSpace(searchTerm) ||
                    model.Description.Contains(searchTerm)// ||
                    orderby model.CreationTime descending

                    select new InvoiceDetailDTO
                    {
                        Id = model.Id,
                        InvoiceHeaderId = model.InvoiceId,
                        ItemID = model.ItemID,
                        Description = model.Description,
                        WarehouseID = model.WarehouseID,
                        WarehouseBinID = model.WarehouseBinID,
                    };

                return await InvoiceDetail.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<IPagedList<InvoiceDetailDTO>> GetAllInvoiceDetailByUser(int pageNumber, int pageSize, string searchTerm)
        {
            var currentUserEmail = _serviceHelper.GetCurrentUserEmail();
            var currentUser = await _userSvc.FindByEmailAsync(currentUserEmail);
            try
            {
                var InvoiceDetail =
                    from model in _InvoiceDetailRepo.GetAll()
                    where model.TenantId == currentUser.CompanyId &&string.IsNullOrWhiteSpace(searchTerm) ||
                    model.Description.Contains(searchTerm)// ||
                    orderby model.CreationTime descending

                    select new InvoiceDetailDTO
                    {
                        Id = model.Id,
                        InvoiceHeaderId = model.InvoiceId,
                        ItemID = model.ItemID,
                        Description = model.Description,
                        WarehouseID = model.WarehouseID,
                        WarehouseBinID = model.WarehouseBinID,
                    };

                return await InvoiceDetail.AsTracking().ToPagedListAsync(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Task<List<InvoiceDetailDTO>> GetInvoiceDetail(Guid Id)
        {
            var invrecdetails = from model in _InvoiceDetailRepo.GetAll()
                                where model.InvoiceId == Id

                                select new InvoiceDetailDTO
                                {
                                    Id = model.Id,
                                    InvoiceHeaderId = model.InvoiceId,
                                    ItemID = model.ItemID,
                                    Description = model.Description,
                                    WarehouseID = model.WarehouseID,
                                    WarehouseBinID = model.WarehouseBinID,
                                    ItemUPCCode = model.ItemUPCCode,
                                    OrderQty = model.OrderQty,
                                    ItemUnitPrice = model.ItemUnitPrice,
                                    TaxAmount = model.TaxAmount,
                                    SerialNumber = model.SerialNumber
                                };
            return invrecdetails.ToListAsync();
        }


        public async Task<bool> UpdateInvoiceDetail(InvoiceDetailDTO model, int Id)
        {
            var InvoiceDetail = _InvoiceDetailRepo.FirstOrDefault(x => x.Id == Id);

            InvoiceDetail.ItemID = model.ItemID;
            InvoiceDetail.Description = model.Description;
            InvoiceDetail.WarehouseID = model.WarehouseID;
            InvoiceDetail.WarehouseBinID = model.WarehouseBinID;
            InvoiceDetail.ItemUPCCode = model.ItemUPCCode;
            InvoiceDetail.OrderQty = model.OrderQty;
            InvoiceDetail.ItemUnitPrice = model.ItemUnitPrice;
            InvoiceDetail.TaxAmount = model.TaxAmount;
            InvoiceDetail.SerialNumber = model.SerialNumber;

            await _unitOfWork.SaveChangesAsync();

            return true;
        }

        #endregion
    }
}