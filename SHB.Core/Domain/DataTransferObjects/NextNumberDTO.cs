﻿using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class NextNumberDTO : FullAuditedEntity
    {
        //public int Id { get; set; }
        public string NextNumberName { get; set; }
        public string NextNumberValue { get; set; }
        public string NextNumberPrefix { get; set; }
        public string NextNumberSeparator { get; set; }

        public int CompanyId { get; set; }
    }
}
