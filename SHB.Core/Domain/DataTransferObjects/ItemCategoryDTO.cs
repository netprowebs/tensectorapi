﻿using SHB.Core.Domain.Entities;
using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class ItemCategoryDTO : FullAuditedEntity
    {
        public string ItemCategoryCode { get; set; }
        public int ItemFamilyID { get; set; }
        //public ItemFamily ItemFamily { get; set; }
        public string CategoryName { get; set; }
        public string CategoryDescription { get; set; }
        public string CategoryLongDescription { get; set; }
        public string CategoryPictureURL { get; set; }
        public ICollection<InventoryItem> InventoryItems { get; set; }

    }
}
