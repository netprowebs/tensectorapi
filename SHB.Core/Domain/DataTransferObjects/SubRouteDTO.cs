﻿using SHB.Core.Entities;
using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class SubRouteDTO
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public int NameId { get; set; }

        public int RouteId { get; set; }
        public Route Route { get; set; }
    }
}
