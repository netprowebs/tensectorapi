﻿using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class VettersDistinctDTO
    {
        public int ClientId { get; set; }
        public int TenantId { get; set; }
        public string ClientEmail { get; set; }
        public string ClientName { get; set; }        
        public string ClientPicture { get; set; }
        public string ClientPhoneNumber { get; set; }

    }
}
