﻿using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class ReceiptHeaderDTO
    {
        public Guid Id { get; set; }
        public int TenantId { get; set; }
        public string ReceiptNumber { get; set; }
        public AccTransType ReceiptType { get; set; }
        public DateTime DueDate { get; set; }
        public string Reference { get; set; }
        public string ChequeNumber { get; set; }
        public int CustomerID { get; set; }
        public DateTime LodgementDate { get; set; }
        public int BankID { get; set; }
        public decimal TotalAmount { get; set; }
        public int LocationId { get; set; }
        public int UserId { get; set; }
        public bool Captured { get; set; }
        public DateTime TransactionDate { get; set; }
        public bool Verified { get; set; }
        public string VerifiedBy { get; set; }
        public DateTime VerifiedDate { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime ApprovedDate { get; set; }
        public bool Posted { get; set; }
        public string PostedBy { get; set; }
        public DateTime PostedDate { get; set; }
        public bool IsReverse { get; set; }
        public bool Void { get; set; }
    }
}
