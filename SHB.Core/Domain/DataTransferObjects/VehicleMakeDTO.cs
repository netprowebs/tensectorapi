﻿using SHB.Core.Entities;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class VehicleMakeDTO 
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
