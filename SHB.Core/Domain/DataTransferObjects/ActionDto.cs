﻿using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class ActionDto
    {
        public List<string> modellist { get; set; }
        public VetterStatus ActionType { get; set; }
        public int EmployeeId { get; set; }

    }
}
