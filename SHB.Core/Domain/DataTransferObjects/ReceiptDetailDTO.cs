﻿using SHB.Core.Entities;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class ReceiptDetailDTO
    {
        public int Id { get; set; }
        public Guid ReceiptHeaderId { get; set; }
        public AccTransType TransType { get; set; }
        public int AccountID { get; set; }
        public string Narratives { get; set; }
        public string DocumentNo { get; set; }
        public decimal Amount { get; set; }
        public int CurrencyID { get; set; }
        public decimal ExchangeRate { get; set; }
        public int ProjectId { get; set; }
        public int AssetId { get; set; }
        public int TenantId { get; set; }
    }
}
