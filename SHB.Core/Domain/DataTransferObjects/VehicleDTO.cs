﻿using SHB.Core.Domain.Entities;
using SHB.Core.Entities;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class VehicleDTO 
    {
        public int Id { get; set; }
        public string RegistrationNumber { get; set; }
        public string ChasisNumber { get; set; }
        public string EngineNumber { get; set; }
        public string IMEINumber { get; set; }
        public string Type { get; set; }
        public virtual VehicleStatus Status { get; set; }

        public int VehicleModelId { get; set; }
        public  VehicleModel VehicleModel { get; set; }

        public int? LocationId { get; set; }
        public  Terminal Location { get; set; }

        public bool IsOperational { get; set; }

        public Employee Employee { get; set; }
        public int? EmployeeId { get; set; }

        public DateTime? PurchaseDate { get; set; }
        public DateTime? LicenseDate { get; set; }
        public DateTime? InsuranceDate { get; set; }
        public string Description { get; set; }
        public DateTime? LicenseExpiration { get; set; }
        public DateTime? InsuranceExpiration { get; set; }
        public DateTime? RoadWorthinessDate { get; set; }
        public DateTime? RoadWorthinessExpiration { get; set; }
    }
}
