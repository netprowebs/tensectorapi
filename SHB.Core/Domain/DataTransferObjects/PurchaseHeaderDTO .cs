﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class PurchaseHeaderDTO : FullAuditedEntity<Guid>
    {
        public string TenantId { get; set; }
        public string DocNumber { get; set; }
        public AccTransType DocType { get; set; }

        public DateTime PaymentDate { get; set; }

        public string ContractNumber { get; set; }
        public string VendorInvoiceNumber { get; set; }
        public int VendorID { get; set; }
        public string Reference { get; set; }
        public decimal HDiscountAmount { get; set; }

        public decimal TaxableSubTotal { get; set; }
        public decimal TotalAddCost { get; set; }
        public decimal TotalSubAmount { get; set; }
        public decimal TotalWeight { get; set; }


        public string TrackingNumber { get; set; }
        public int VehicleId { get; set; }
        public int BankID { get; set; }
        public int LocationId { get; set; }
        public int UserId { get; set; }
        public int WarehouseID { get; set; }

        public bool Captured { get; set; }
        public DateTime PurchaseDate { get; set; }

        public bool Verified { get; set; }
        public string VerifiedBy { get; set; }
        public DateTime VerifiedDate { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime ApprovedDate { get; set; }
        public bool Posted { get; set; }
        public string PostedBy { get; set; }
        public DateTime PostedDate { get; set; }
        public bool IsReverse { get; set; }
        public bool Void { get; set; }
    }
}