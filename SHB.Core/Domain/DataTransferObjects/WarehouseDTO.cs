﻿using SHB.Core.Domain.Entities;
using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class WarehouseDTO : FullAuditedEntity
    {
        public string WarehouseCode { get; set; }
        public string WarehouseName { get; set; }
        public string WarehouseAddress1 { get; set; }
        public string WarehouseAddress2 { get; set; }
        public string WarehouseCity { get; set; }
        public string WarehouseState { get; set; }
        public string WarehouseZip { get; set; }
        public string WarehousePhone { get; set; }
        public string WarehouseFax { get; set; }
        public string WarehouseEmail { get; set; }
        public string StockControlAccount { get; set; }
        public string SalesControlAccount { get; set; }
        public string COSControlAccount { get; set; }
        public bool IsActive { get; set; }

        public ICollection<WarehouseBinDTO> WarehouseBins { get; set; }
    }
}
