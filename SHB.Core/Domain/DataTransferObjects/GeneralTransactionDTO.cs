﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class GeneralTransactionDTO: FullAuditedEntity<Guid>
    {
        public TransactionType TransactionType { get; set; }
        public string TransType { get; set; }
        public PayTypeDescription PayTypeDiscription { get; set; }
        public string PayType { get; set; }
        public decimal TransactionAmount { get; set; }
        public DateTime TransactionDate { get; set; }
        public string TransactedBy { get; set; }
        public bool IsActive { get; set; }
        public string TransDescription { get; set; }
    }
}
