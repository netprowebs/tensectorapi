﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
   

    public class PasswordResetDTO
    {
        public string UserNameOrEmail { get; set; }
        public string Code { get; set; }
        public string NewPassword { get; set; }
    }


    public class ChangePasswordDTO
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
