﻿using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class WarehouseBinTypeDTO : FullAuditedEntity
    {
        public string WarehouseBinTypeName { get; set; }
        public string WarehouseBinTypeDescription { get; set; }
    }
}
