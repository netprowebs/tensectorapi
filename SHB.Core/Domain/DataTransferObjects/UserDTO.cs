﻿using SHB.Core.Domain.Entities;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class UserDTO
    {
        public long UserId { get; set; }

        public bool IsActive { get; set; }

        public UserType UserType { get; set; }

        [StringLength(50, MinimumLength = 1, ErrorMessage = "{0} must be between {1} and {2} characters long.")]
        [Display(Name = "First Name")]
        [Required]
        public string FirstName { get; set; }

        [StringLength(50, MinimumLength = 1, ErrorMessage = "{0} must be between {1} and {2} characters long.")]
        [Display(Name = "Last Name")]
        [Required]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please specify a valid email.")]
        [EmailAddress(ErrorMessage = "Please specify a valid email.")]
        public string Email { get; set; }

        [StringLength(20, MinimumLength = 8, ErrorMessage = "{0} must be between {1} and {2} characters long.")]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        public string Image { get; set; }
        public List<string> Roles { get; set; }
        public bool AccountIsDeleted { get; set; }
        public Gender Gender { get; set; }
        public string ReferralCode { get; set; }
        public int CompanyId { get; set; }
        public DeviceType deviceType { get; set; }
        public string IdentificationCode { get; set; }
        public IdentificationType IdentificationType { get; set; }
        public DateTime IDIssueDate { get; set; }
        public DateTime IDExpireDate { get; set; }
        public int? WalletId1 { get; set; }
        public int? LocationId { get; set; }
        public DateTime CreationTime { get; set; }
        public string IdentityPhoto { get; set; }
    }

    public class UserProfileDTO
    {
        public int UserId { get; set; }
        public string NextOfKin { get; set; }

        public string NextOfKinPhone { get; set; }
        public string FirstName { get; set; }
        public int? WalletId { get; set; }
  
        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }
        public Gender Gender { get; set; }
        public string ReferralCode { get; set; }
        public string Address { get; set; }
        public string MiddleName { get; set; }
        public string DateJoined { get; set; }
        public string DateOfBirth { get; set; }
        public string Title { get; set; }
        public string Referrer { get; set; }
        public UserType userType { get; set; }
        public int? CompanyId { get; set; }
        public DeviceType deviceType { get; set; }
        public string IdentificationCode { get; set; }
        public IdentificationType IdentificationType { get; set; }
        public DateTime IDIssueDate { get; set; }
        public DateTime IDExpireDate { get; set; }
        public int? LocationId { get; set; }
        public DateTime CreationTime { get; set; }
        public string IdentityPhoto { get; set; }
        public string Photo { get; set; }
        public bool IsActive { get; set; }
        public string PhotoContent { get; set; }
        public bool IsFirstTimeLogin { get; set; }
        public List<Claim> UserClaimlist { get; set; }

    }
}
