﻿using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class ItemTypeDTO : FullAuditedEntity
    {
        public string ItemTypeName { get; set; }
        public string ItemTypeDescription { get; set; }
        //public ICollection<InventoryItem> InventoryItems { get; set; }
    }
}
