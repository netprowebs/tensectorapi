﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class AppMenuItemDTO
    {
        public int Id { get; set; }
        public string MenuCode { get; set; }
        public string MenuDescription { get; set; }
        public int DisplayOrder { get; set; }
        public string DefaultPage { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public int? ParentId { get; set; }
    }
}
