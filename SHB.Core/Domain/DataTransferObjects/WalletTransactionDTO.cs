﻿using SHB.Core.Entities;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class WalletTransactionDTO
    {
        public Guid Id { get; set; }
        public TransactionType TransactionType { get; set; }
        public int? UserId { get; set; }
        public string UserEmail { get; set; }
        public decimal TransactionAmount { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal LineBalance { get; set; }
        public int WalletId { get; set; }
        public string TransDescription { get; set; }
        public PayTypeDescription PayTypeDiscription { get; set; }
        public string TransactedBy { get; set; }
        public bool IsSum { get; set; }
        public bool IsCaptured { get; set; }
        public bool IsVerified { get; set; }
        public string IsVerifiedBy { get; set; }
        public DateTime? IsVerifiedDate { get; set; }
        public bool IsApproved { get; set; }
        public string IsApprovedBy { get; set; }
        public DateTime? IsApprovedDate { get; set; }
        public int TenantId { get; set; }
        public string TransCode { get; set; }
        public decimal WalletBalance { get; set; }
        public DateTime CreationTime  { get; set; }
        public int CreatorUserId { get; set; }
        public FareAdjustmentType AmountType { get; set; }

    }
}
