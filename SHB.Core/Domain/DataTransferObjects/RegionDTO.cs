﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class RegionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DateCreated { get; set; }
    }

}
