﻿using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class AttributesDTO : FullAuditedEntity
    {
        public string AttributeName { get; set; }
        public string AttributeDescription { get; set; }

    }
}
