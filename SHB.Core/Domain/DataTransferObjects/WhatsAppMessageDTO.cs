﻿using Newtonsoft.Json;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class TypeTextDTO
    {
        [JsonProperty("preview_url")]
        public bool PreviewUrl { get; set; }
        [JsonProperty("content")]
        public string Content { get; set; }
    }

    public class WhatsAppMessageDTO
    {
        public WhatsAppMessageDTO()
        {
            TypeText = new List<TypeTextDTO>();
        }
        [JsonProperty("recipient_whatsapp")]
        public string RecipientWhatsapp { get; set; }
        [JsonProperty("recipient_type")]
        public string RecipientType { get; set; }
        [JsonProperty("message_type")]
        public string MessageType { get; set; }
        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("x-apiheader")]
        public string XApiheader { get; set; }
        [JsonProperty("type_text")]
        public List<TypeTextDTO> TypeText { get; set; }
        public string Waybill { get; set; }
        [JsonProperty("type_template")]
        public List<TypeTemplateDTO> TypeTemplate { get; set; }
    }

    public class WhatsAppMessagesDTO
    {
        public WhatsAppMessagesDTO()
        {
            Message = new List<WhatsAppMessageDTO>();
        }
        [JsonProperty("message")]
        public List<WhatsAppMessageDTO> Message { get; set; }
    }

    public class WhatsappNumberDTO
    {
        [JsonProperty("phone_number")]
        public string PhoneNumber { get; set; }
    }

    public class RecipientDTO
    {
        [JsonProperty("recipient")]
        public long Recipient { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("user_agent")]
        public string UserAgent { get; set; }

        [JsonProperty("ip")]
        public string Ip { get; set; }
    }

    public class ManageWhatsappConsentDTO
    {
        public ManageWhatsappConsentDTO()
        {
            Recipients = new List<RecipientDTO>();
        }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("recipients")]
        public List<RecipientDTO> Recipients { get; set; }
    }

    public class TypeTemplateDTO
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("attributes")]
        public List<string> Attributes { get; set; }

        [JsonProperty("language")]
        public LanguageDTO Language { get; set; }
    }

    public class LanguageDTO
    {
        [JsonProperty("locale")]
        public string Locale { get; set; }

        [JsonProperty("policy")]
        public string Policy { get; set; }
    }
}
