﻿using SHB.Core.Domain.Entities;
using SHB.Core.Domain.Entities.Enums;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class RequisitionDetailDTO
    { 
        //Repushing for Mr. Segun
        public int Id { get; set; }
        public Guid RequisitionID { get; set; }
        public int TenantId { get; set; }
        public string ItemID { get; set; }
        public string Description { get; set; }
        public string RequestedQty { get; set; }
        public int? WarehouseID { get; set; }
        public int? WarehouseBinID { get; set; }
        public int? ToWarehouseID { get; set; }
        public int? ToWarehouseBinID { get; set; }
        public string GLExpenseAccount { get; set; }
        public decimal ItemValue { get; set; }
        public decimal ItemCost { get; set; }
        public CostMethod CostMethod { get; set; }
        public string PONumber { get; set; }
        public string ItemUPCCode { get; set; }
        public string OrderQty { get; set; }
        public string IssuedQty { get; set; }
        public string ProjectID { get; set; }
        public string GLAnalysisType1 { get; set; }
        public string GLAnalysisType2 { get; set; }
        public string AssetID { get; set; }
        public string TaxGroupID { get; set; }
        public string Vehicle { get; set; }
        public string Staff { get; set; }
        public string ItemName { get; set; }
    }
}
