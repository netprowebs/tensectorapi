﻿using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class InvoiceHeaderDTO
    {
        public Guid Id { get; set; }
        public int TenantId { get; set; }
        public string InvoiceNumber { get; set; }
        public AccTransType AccTranType { get; set; }

        public DateTime DueDate { get; set; }
        public string Reference { get; set; }
        public decimal TotalAmount { get; set; }
        public string PurchaseOrderNumber { get; set; }
        public int CustomerID { get; set; }
        public decimal TaxableSubTotal { get; set; }

        public int ShipMethodID { get; set; }
        public decimal BalanceDue { get; set; }
        public string OrderNumber { get; set; }

        public string TrackingNumber { get; set; }
        public int VehicleId { get; set; }
        public int BankID { get; set; }
        public int LocationId { get; set; }
        public int UserId { get; set; }
        public int WarehouseID { get; set; }

        public bool Captured { get; set; }
        public DateTime InvoiceDate { get; set; }

        public bool Verified { get; set; }
        public string VerifiedBy { get; set; }
        public DateTime VerifiedDate { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime ApprovedDate { get; set; }
        public bool Posted { get; set; }
        public string PostedBy { get; set; }
        public DateTime PostedDate { get; set; }
        public bool IsReverse { get; set; }
        public bool Void { get; set; }

        public int CreatorUserId { get; set; }
      

    }
}
