﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class VendorTypesDTO
    {
        public int ID { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string VendorTypeDescription { get; set; }
        public string CreditorsControlAccount { get; set; }
        public string CurrencyExchange { get; set; }
        public string GainOrLossAccount { get; set; }
        public string DiscountsAccount { get; set; }
        public string DiscountRate { get; set; }
    }
}
