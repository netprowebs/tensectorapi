﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class InventoryAdjustmentsHeaderDTO 
    {
        public Guid Id { get; set; }
        public string TenantId { get; set; }
        public string Code { get; set; }
        public DateTime? AdjustmentDate { get; set; }
        public string Reason { get; set; }
        public string Notes { get; set; }
        public string AdjustmentPostToGL { get; set; }
        public string SupervisorSignature { get; set; }
        public string Total { get; set; }
        public bool? IsCountAdjust { get; set; }
        public string CurrencyID { get; set; }
        public string CurrencyExchangeRate { get; set; }
        public bool Void { get; set; }
        public bool Verified { get; set; }
        public string VerifiedBy { get; set; }
        public DateTime? VerifiedDate { get; set; }
        public bool Issued { get; set; }
        public string IssuedBy { get; set; }
        public DateTime? IssuedDate { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime ApprovedDate { get; set; }
        public bool Posted { get; set; }
        public string PostedBy { get; set; }
        public DateTime PostedDate { get; set; }
        public bool Cancelled { get; set; }
        public DateTime CreationTime { get; set; }
        public string SiteNumber { get; set; }
	    public string Status { get; set; }
        public AdjustmentType AdjustmentType { get; set; }
        public string Reference { get; set; }
        public string Narratives { get; set; }
    }
}
