﻿using SHB.Core.Domain.Entities;
using SHB.Core.Entities;
using System;
using System.Collections.Generic;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class EmailSetting
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string EmailFrom { get; set; }
        public string EmailTo { get; set; }
        public string SenderName { get; set; }
        public bool IsSent { get; set; }
        public int SenderType { get; set; }
        public string ToPhoneContact { get; set; }

        //for Emaill setting
        public string SmtpServer { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool enableSSL { get; set; }
        public string seclotEmail { get; set; }
        public string MJ_APIKEY_PRIVATE { get; set; }
        public string MJ_APIKEY_PUBLIC { get; set; }
        public string sender_name { get; set; }
        public string policyUrlLink { get; set; }
        public string termUrlLink { get; set; }

        // for EmailBodyProperties
        public string body_variable { get; set; }
        public string user_name { get; set; }
        public string payment_link { get; set; }
        public string seclot_account_number { get; set; }
        public string vettes_phone_number { get; set; }
        public string profile_url { get; set; }
        public string vetter_phone_number { get; set; }
        public string user_seclot_id { get; set; }
    }
}
