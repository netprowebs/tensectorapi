﻿using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class InventoryAdjustmentsDetailDTO 
    {
        public int Id { get; set; }
        public Guid AdjustmentID { get; set; }
        public string ItemID { get; set; }
        public string WarehouseID { get; set; }
        public string WarehouseBinID { get; set; }
        public string Description { get; set; }
        public string OriginalQuantity { get; set; }
        public string AdjustedQuantity { get; set; }
        public string CurrentID { get; set; }
        public string CurrentExchangeRate  { get; set; }
        public string Cost  { get; set; }
        public string GLAdjustmentPostingAccount  { get; set; }
        public string ProjectID  { get; set; }
        public string UnitCost  { get; set; }
        public string GLAnalysisType1  { get; set; }
        public string AssetID  { get; set; }
    }
}
