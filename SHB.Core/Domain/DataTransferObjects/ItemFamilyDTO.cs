﻿using SHB.Core.Domain.Entities;
using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class ItemFamilyDTO : FullAuditedEntity
    {
        public string ItemFamilyCode { get; set; }
        public string FamilyDescription { get; set; }
        public string FamilyLongDescription { get; set; }
        public string FamilyPictureURL { get; set; }

        //public ICollection<InventoryItem> InventoryItems { get; set; }
        //public ICollection<ItemCategory> ItemCategories { get; set; }
    }
}
