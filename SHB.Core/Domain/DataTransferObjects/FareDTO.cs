﻿using SHB.Core.Domain.Entities;
using SHB.Core.Entities;
using System;
using System.Collections.Generic;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class FareDTO 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public float? ChildrenDiscountPercentage { get; set; }
        public int RouteId { get; set; }
        public virtual Route Route { get; set; }
        public int VehicleModelId { get; set; }
        public virtual VehicleModel VehicleModel { get; set; }
        public decimal NonIdAmount { get; set; }
    }
}
