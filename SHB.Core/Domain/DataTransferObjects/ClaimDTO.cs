﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class ClaimDTO
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public int ClaimType { get; set; }
        public int ClaimValue { get; set; }
    }
}
