﻿using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class WalletDTO
    {
        public int Id { get; set; }
        public string WalletNumber { get; set; }
        public decimal Balance { get; set; }
        public UserType UserType { get; set; }
        public int? UserId { get; set; }
        public string WalletOwnerName { get; set; }
        public bool IsReset { get; set; }
        public DateTime? LastResetDate { get; set; }
        public DateTime CreationTime { get; set; }
        public string PartnerName { get; set; }
        public int TenantId { get; set; }

    }
}
