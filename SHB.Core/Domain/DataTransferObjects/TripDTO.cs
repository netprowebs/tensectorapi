﻿using SHB.Core.Domain.Entities;
using SHB.Core.Entities;
using System;
using System.Collections.Generic;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class TripDTO
    {
        public int Id { get; set; }
        public string DepartureTime { get; set; }
        public string TripCode { get; set; }
        public bool AvailableOnline { get; set; }
        public string ParentRouteDepartureTime { get; set; }
        public int RouteId { get; set; }
        public  Route Route { get; set; }

        public int? VehicleModelId { get; set; }
        public  VehicleModel VehicleModel { get; set; }
        public int? ParentRouteId { get; set; }
        public Guid? ParentTripId { get; set; }
        public bool IsActive { get; set; }
    }
}
