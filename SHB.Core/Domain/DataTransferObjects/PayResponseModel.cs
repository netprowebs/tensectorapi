﻿using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SHB.WebApi.ViewModels
{
    public class PayResponseModel
    {
        public string RefCode { get; set; }
        public string email { get; set; }
        public int amount { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public string PaymentGatewayReference { get; set; }
        public int BundleType { get; set; }
    }
}
