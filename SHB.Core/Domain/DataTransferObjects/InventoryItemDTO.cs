﻿using SHB.Core.Entities.Common;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class InventoryItemDTO : FullAuditedEntity
    {
        public string ItemCode { get; set; }
        public bool IsActive { get; set; }
        public int ItemTypeId { get; set; }
        public string ItemTypeName { get; set; }
        //public ItemType ItemType { get; set; }
        public string ItemName { get; set; }
        public string itemDescription { get; set; }
        public int ItemCategoryID { get; set; }
        public string ItemCategoryName { get; set; }
        //public ItemCategory ItemCategory { get; set; }
        public int ItemFamilyID { get; set; }
        public string ItemFamilyName { get; set; }
        //public ItemFamily ItemFamily { get; set; }
        public string PictureURL { get; set; }
        public string ItemWeight { get; set; }
        public string ItemUPCCode { get; set; }
        public string ItemColor { get; set; }
        public int ItemDefaultWarehouseID { get; set; }
        public string ItemDefaultWarehouseName { get; set; }
        public int ItemDefaultWarehouseBinID { get; set; }
        public string ItemDefaultWarehouseBinName { get; set; }
        public string ItemUOM { get; set; }
        public int GLItemSalesAccountId { get; set; }
        public string GLItemSalesAccountName { get; set; }
        public int GLItemCOGSAccountId { get; set; }
        public string GLItemCOGSAccountName { get; set; }
        public int GLItemInventoryAccountId { get; set; }
        public string GLItemInventoryAccountName { get; set; }
        public decimal Price { get; set; }
        public string ItemPricingCode { get; set; }
        public string VendorID { get; set; }
        public float ReOrderLevel { get; set; }
        public float ReOrderQty { get; set; }
        public decimal LIFO { get; set; }
        public decimal LIFOValue { get; set; }
        public decimal LIFOCost { get; set; }
        public decimal Average { get; set; }
        public decimal AverageValue { get; set; }
        public decimal AverageCost { get; set; }
        public decimal FIFOFIFOValue { get; set; }
        public decimal FIFOCost { get; set; }
        public decimal Expected { get; set; }
        public decimal ExpectedValue { get; set; }
        public decimal ExpectedCost { get; set; }
        public bool IsSerialLotItem { get; set; }
        public bool AllowPurchaseTrans { get; set; }
        public bool AllowSalesTrans { get; set; }
        public bool AllowInventoryTrans { get; set; }
    }
}
