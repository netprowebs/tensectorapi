﻿
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using static System.Net.Mime.MediaTypeNames;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class ImageFileDTO 
    {
        public int Id { get; set; }
        public string ImgContent { get; set; }
        public string FolderName { get; set; }
        public byte[] data { get; set; }
        public string ContentType { get; set; }
        public string fileName { get; set; }
        public string FilenameWithOutExt { get; set; }
        public string Description { get; set; }     
        public string FileReference { get; set; }       
        public string UploadPath { get; set; }       
        public UploadType UploadType { get; set; }
        public UploadFormat UploadFormat { get; set; }
        public IFormFile file { get; set; }
        public List<ImageFileDTO> PictureList { get; set; }
        public List<string> PicsList { get; set; }
        public DateTime DeletionTime { get; set; }
        public int DeleterUserId { get; set; }
        public bool IsDeleted { get; set; }
        public int LastModifierUserId { get; set; }
        public DateTime LastModificationTime { get; set; }
        public DateTime CreationTime { get; set; } 
        public Guid RequisitionID { get; set; }

        //Begin:: Upload Settings
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public string bucketName { get; set; }
        public string HostUploadURL { get; set; }
        public string DisplayFileContentURL { get; set; }
        public int CreatorUserId { get; set; }
        public int TenantId { get; set; }
        //End:: Upload Settings

 
   
    }
}
