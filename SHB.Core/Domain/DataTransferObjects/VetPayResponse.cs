﻿using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class VetPayResponse
    {
        public string Response { get; set; }
        public decimal? Amount { get; set; }
        public string RefCode { get; set; }
        public string email { get; set; }
        //public int amount { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public string PaymentGatewayReference { get; set; }
        public int UnitUsed { get; set; }
        public VetStatus VetteeStatus { get; set; }
        public string PaymentStatus { get; set; }
        public int ClientId { get; set; }

    }

}
