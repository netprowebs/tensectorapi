﻿using SHB.Core.Domain.Entities;
using SHB.Core.Entities;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class VehicleModelDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NumberOfSeats { get; set; }
        public string VehicleModelTypeCode { get; set; }
        public int VehicleMakeId { get; set; }
        public VehicleMake VehicleMake { get; set; }
    }
}
