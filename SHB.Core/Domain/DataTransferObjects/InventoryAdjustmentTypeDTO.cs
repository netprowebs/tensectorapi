﻿using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class InventoryAdjustmentTypeDTO : FullAuditedEntity
    {
        public string AdjustmentTypeName { get; set; }
        public string AdjustmentTypeDescription { get; set; }

    }
}
