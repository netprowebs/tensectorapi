﻿using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;

namespace LME.Core.Domain.DataTransferObjects
{
    public class EmployeeDTO
    {
        public string Email { get; set; }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmployeeCode { get; set; }
        public DateTime? DateOfEmployment { get; set; }

        public string FullName => FirstName + " " + LastName;
        public string MiddleName { get; set; }
        public Gender Gender { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string EmployeePhoto { get; set; }
        public string NextOfKin { get; set; }
        public string NextOfKinPhone { get; set; }

        //public int? WalletId { get; set; }
        //public string WalletNumber { get; set; }

        public int? DepartmentId { get; set; }
        public string DepartmentName { get; set; }

        public int? TerminalId { get; set; }
        public string TerminalName { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string UserId { get; set; }
        public string Otp { get; set; }
        public bool OtpIsUsed { get; set; }
        public string TicketRemovalOtp { get; set; }
        public bool TicketRemovalOtpIsUsed { get; set; }
        public DateTime? OTPLastUsedDate { get; set; }
        public int? OtpNoOfTimeUsed { get; set; }
        public bool LockoutEnabled { get; set; }

        public string Company { get; set; }
        public bool IsActive { get; set; }
        public string Desgination { get; set; }
        public string ReportTo { get; set; }
        public string IdentificationCode { get; set; }
        public IdentificationType IdentificationType { get; set; }
        public DateTime IDIssueDate { get; set; }
        public DateTime IDExpireDate { get; set; }
        public DeviceType DeviceType { get; set; }

        public List<ImageFileDTO> PictureList { get; set; }


    }

    public class AllUserDTO
    {
        public string Email { get; set; }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Code { get; set; }
        public DateTime? DateOfEmployment { get; set; }

        public string FullName => FirstName + " " + LastName;
        public string MiddleName { get; set; }
        public Gender Gender { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string EmployeePhoto { get; set; }
        public string NextOfKin { get; set; }
        public string NextOfKinPhone { get; set; }

        //public int? WalletId { get; set; }
        //public string WalletNumber { get; set; }

        public int? DepartmentId { get; set; }
        public string DepartmentName { get; set; }

        public int? TerminalId { get; set; }
        public string TerminalName { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string UserId { get; set; }
        public string Otp { get; set; }
        public bool OtpIsUsed { get; set; }
        public string TicketRemovalOtp { get; set; }
        public bool TicketRemovalOtpIsUsed { get; set; }
        public DateTime? OTPLastUsedDate { get; set; }
        public int? OtpNoOfTimeUsed { get; set; }
        public bool LockoutEnabled { get; set; }

        public string Company { get; set; }
        public bool IsActive { get; set; }
        public string Desgination { get; set; }
        public string ReportTo { get; set; }
        public string IdentificationCode { get; set; }
        public IdentificationType IdentificationType { get; set; }
        public DateTime IDIssueDate { get; set; }
        public DateTime IDExpireDate { get; set; }
        public DeviceType DeviceType { get; set; }
    }
}
