﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.DataTransferObjects
{
    public class AppMenuItemAccessDTO
    {
        public int Id { get; set; }
        public int AppMenuId { get; set; }
        public string ClaimName { get; set; }
        public bool SetupDefault { get; set; }
        public bool IsActive { get; set; }
    }
}
