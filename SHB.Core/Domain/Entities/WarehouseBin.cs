﻿using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class WarehouseBin : FullAuditedEntity
    {
        public string WarehouseBinIDCode { get; set; }
        public int WarehouseID { get; set; }
        [NotMapped]
        public string WarehouseName { get; set; }
        public Warehouse Warehouse { get; set; }
        public string WarehouseBinName { get; set; }
        public string WarehouseBinNumber { get; set; }
        public string WarehouseBinZone { get; set; }
        public string WarehouseBinType { get; set; }
        public string WarehouseBinLocation { get; set; }
        public string WarehouseBinLength { get; set; }
        public string WarehouseBinWidth { get; set; }
        public string WarehouseBinHeight { get; set; }
        public long WarehouseBinWeight { get; set; }
        public float MinimumQuantity { get; set; }
        public float MaximumQuantity { get; set; }
        public string OverFlowBin { get; set; }
        public bool IsActive { get; set; }
        public int CompanyId { get; set; }

    }
}
