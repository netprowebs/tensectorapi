﻿using SHB.Core.Entities.Common;

namespace SHB.Core.Entities
{
    public class Region : FullAuditedEntity
    {
        public string Name { get; set; }
    }
}
