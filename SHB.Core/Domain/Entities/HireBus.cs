﻿using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class HireBus : FullAuditedEntity<Guid>
    {
        public string DepartureAddress { get; set; }
        public string DestinationAddress { get; set; }
        public decimal Amount { get; set; }
        public int VehicleId { get; set; }
        public virtual Vehicle Vehicle { get; set; }
        public string DriverCode { get; set; }
        public DateTime DepartureDate { get; set; }
        public Guid VehicleTripRegistrationId { get; set; }
        public int UserLocationId { get; set; }
        public int DepartureTerminalId { get; set; }
        public bool? IsManifestPrinted { get; set; } = false;
        public DateTime? ArrivalDate { get; set; }
        public int? ReceivedLocationId { get; set; }
    }
}
