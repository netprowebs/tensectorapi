﻿using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class CustomerTypes : FullAuditedEntity
    {
        public string CompanyID { get; set; }
        public string CustomerTypeID { get; set; }
        public string CustomerTypeDescription { get; set; }
        public string SalesControlAccount { get; set; }
        public string COSControlAccount { get; set; }
        public string DebtorsControlAccount { get; set; }
        public string CurrencyExchange { get; set; }
        public string GainOrLossAccount { get; set; }
        public string DiscountsAccount { get; set; }
        public string DiscountRate { get; set; }
        public string BillingType { get; set; }
    }
}
