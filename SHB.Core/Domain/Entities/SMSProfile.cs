﻿using SHB.Core.Entities;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class SMSProfile : Entity
    {
        [Required]
        [MaxLength(50)]
        public string AppName { get; set; }
        public double? SMSMinQty { get; set; }
        public bool? ConfirmEmail { get; set; }
        [MaxLength(50)]
        public string SmtpAddress { get; set; }
        [MaxLength(50)]
        public string Port { get; set; }
        [MaxLength(50)]
        public string Username { get; set; }
        [MaxLength(50)]
        public string Password { get; set; }
        [MaxLength(50)]
        public string Profile { get; set; }
        [MaxLength(50)]
        public string SMSUserName { get; set; }
        [MaxLength(50)]
        public string SMSPassword { get; set; }
        [MaxLength(50)]
        public string SMSsubUserName { get; set; }
        public bool? IsActive { get; set; }

    }
}
