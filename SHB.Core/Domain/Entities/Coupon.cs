﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;

namespace SHB.Core.Entities
{
    public class Coupon : AuditedEntity<Guid>
    {
        public string CouponCode { get; set; }
        public decimal CouponValue { get; set; }
        public CouponType CouponType { get; set; }

        public bool Validity { get; set; }
        public DurationType DurationType { get; set; }
        public int Duration { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsUsed { get; set; }
        public DateTime? DateUsed { get; set; }
        public bool IsActive { get; set; }
    }
}
