﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Entities
{
    public class Workshop : FullAuditedEntity
    {
        public Workshop()
        {

        }
        public VehicleStatus VehicleStatus { get; set; }
        public int VehicleId { get; set; }
        public int VehicleLocationId { get; set; }
        public int WorkshopLocationId { get; set; }
        public bool WorkshopStatus { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public int? ReleaseUserId { get; set; }
        public string WorkshopNote { get; set; }
        public string ReleaseNote { get; set; }
        public int? ReleaseLocationId { get; set; }
    }
}
