﻿using SHB.Core.Domain.Entities;
using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;

namespace SHB.Core.Entities
{
    public class Fare : FullAuditedEntity
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public float? ChildrenDiscountPercentage { get; set; }
        public int RouteId { get; set; }
        public virtual Route Route { get; set; }
        public int VehicleModelId { get; set; }
        public virtual VehicleModel VehicleModel { get; set; }
        public decimal NonIdAmount { get; set; }
    }
}
