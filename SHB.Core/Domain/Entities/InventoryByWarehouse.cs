﻿using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Core.Domain.Entities
{
    public class InventoryByWarehouse : FullAuditedEntity<Guid>
    {
        //[Key, Column(Order = 0)]
        //public Guid Id { get; set; }
        [Key, Column(Order = 1)]
        public int CompanyID { get; set; }
        public int ItemID { get; set; }
        public string ItemName { get; set; }
        public int WarehouseID { get; set; }
        public int WarehouseBinID { get; set; }
        public string QtyOnHand { get; set; }
        public string QtyCommitted { get; set; }
        public string QtyOnOrder { get; set; }
        public string QtyOnBackorder { get; set; }
        public string CycleCode { get; set; }
        public DateTime LastCountDate { get; set; }
        public float ItemCost { get; set; }
    }
}
