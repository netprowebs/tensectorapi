﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class InventoryReceivedHeader : FullAuditedEntity<Guid>
    {
        public int CompanyId { get; set; }
        public int? AdjustmentTypeID { get; set; }
        public DateTime TransactionDate { get; set; }
        public int? WarehouseID { get; set; }
        public int? WarehouseBinID { get; set; }
        public string Notes { get; set; }
        public bool? Void { get; set; }
        public bool Captured { get; set; }
        public bool Verified { get; set; }
        public string VerifiedBy { get; set; }
        public DateTime? VerifiedDate { get; set; }
        public bool Issued { get; set; }
        public string IssuedBy { get; set; }
        public DateTime? IssuedDate { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime ApprovedDate { get; set; }
        public bool Posted { get; set; }
        public string PostedBy { get; set; }
        public DateTime PostedDate { get; set; }
        public string BatchControlNumber { get; set; }
        public string BatchControlTotal { get; set; }
        public string Signature { get; set; }
        public string SignaturePassword { get; set; }
        public string SupervisorSignature { get; set; }
        public string ManagerSignature { get; set; }
        public int? Currency { get; set; }
        public decimal CurrencyExchangeRate { get; set; }
        public string Reference { get; set; }
        public int? WarehouseCustomerID { get; set; }
        public string DeliveryNote { get; set; }
        public string SiteNumber { get; set; }
        public string VehicleRegistration { get; set; }
        public int? DriversId { get; set; }
        public int? FromCompanyID { get; set; }
        public int? FromWarehouseID { get; set; }
        public int? FromWarehouseBinID { get; set; }
        public int? InventoryIssueTransferID { get; set; }

        public DateTime? RecieveDate { get; set; }
        public PlatformType PlatformType { get; set; }
        public DateTime? ReturnDate { get; set; }
        public string ReturnNotes { get; set; }



    }
}
