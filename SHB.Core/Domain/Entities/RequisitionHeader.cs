﻿using SHB.Core.Domain.Entities.Enums;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Core.Domain.Entities
{
    public class RequisitionHeader : FullAuditedEntity<Guid>
    {

        //public Guid RequisitionID { get; set; }
        public int TenantId { get; set; }
        public AdjustmentTypeID AdjustmentTypeID { get; set; }
        public DateTime TransactionDate { get; set; }
        public int? WarehouseID { get; set; }
        public int? WarehouseBinID { get; set; }
        public string Notes { get; set; }
        public bool Captured { get; set; }
        public bool Verified { get; set; }
        public string VerifiedBy { get; set; }
        public DateTime VerifiedDate { get; set; }
        public bool Issued { get; set; }
        public string IssuedBy { get; set; }
        public DateTime IssuedDate { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime ApprovedDate { get; set; }
        public bool Posted { get; set; }
        public string PostedBy { get; set; }
        public DateTime? PostedDate { get; set; }
        public string BatchControlNumber { get; set; }
        public string BatchControlTotal { get; set; }
        public string Signature { get; set; }
        public string SignaturePassword { get; set; }
        public string SupervisorSignature { get; set; }
        public string CustomerName { get; set; }
        public PlatformType PlatformType { get; set; }
        public bool? Void { get; set; }
        public DateTime? ReturnDate { get; set; }
        public string ReturnNotes { get; set; }
        public RequestTypes RequestType { get; set; }
        public string Location { get; set; }
        public string Department { get; set; }
        public int TotalQty { get; set; }
        public string RetType { get; set; }
        public string Code { get; set; }
        public string Amount { get; set; }
        public string Picture { get; set; }
        public string ItemName { get; set; }



    }
}
