﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SHB.Core.Entities
{
    public class InsurancePolicyClaim : FullAuditedEntity<Guid>
    {
       
        [MaxLength(450)]
        public string BrokerClaimCode { get; set; }
        public Guid InsurancePolicyId { get; set; }
        public InsurancePolicy InsurancePolicy { get; set; }
        public string PolicyBrokerCode { get; set; }

        public DateTime AccidentDate { get; set; }
        public string AccidentDetails { get; set; }
        public DateTime CustomerReportDate { get; set; }
        public DateTime BrokerUnderwriterDate { get; set; }
        public decimal CustomerEstimate { get; set; }
        public decimal AdjusterEstimate { get; set; }
        public bool  DocSumbitCount { get; set; }

        //Approval Settings
        public bool IsCaptured { get; set; }
        public bool Verified { get; set; }
        public int? VerifiedBy { get; set; }
        public DateTime VerifiedDate { get; set; }
        public bool Approved { get; set; }
        public int? ApprovedBy { get; set; }
        public DateTime ApprovedDate { get; set; }
        public bool Posted { get; set; }
        public int? PostedBy { get; set; }
        public DateTime PostedDate { get; set; }
        public bool? Void { get; set; }

    }
}
