﻿using SHB.Core.Entities;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class VetService : FullAuditedEntity
    {
        public int TenantId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ToVetDaysCount { get; set; }
        public int CostInUnit { get; set; }
        public decimal CostInAmount { get; set; }

    }
}
