﻿using SHB.Core.Entities.Common;

namespace SHB.Core.Entities
{
    public class WalletNumber : FullAuditedEntity
    {
        public string WalletPan { get; set; }
        public bool IsActive { get; set; }
    }
}
