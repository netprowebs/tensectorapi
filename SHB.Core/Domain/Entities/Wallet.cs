﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;

namespace SHB.Core.Entities
{
    public class Wallet : FullAuditedEntity
    {
        public string WalletNumber { get; set; }
        public decimal Balance { get; set; }
        public UserType UserType { get; set; }
        public int? UserId { get; set; }
        public bool IsReset { get; set; }
        public string UpdatedBy { get; set; }
        public decimal OldBalance { get; set; }
        public DateTime? WalletLastUpdated { get; set; }
        public DateTime? LastResetDate { get; set; }
        public int TenantId { get; set; }
    }
}
