﻿using SHB.Core.Entities.Common;
using System;

namespace SHB.Core.Entities
{
    public class HirePassenger : FullAuditedEntity
    {
        public string Name { get; set; }
        public string PhoneNo { get; set; }
        public string NokName { get; set; }
        public string NokPhone { get; set; }
        public Guid HireBusId { get; set; }
    }
}
