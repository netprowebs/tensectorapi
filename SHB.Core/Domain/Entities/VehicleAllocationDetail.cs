﻿using SHB.Core.Entities.Common;
using System;

namespace SHB.Core.Entities
{
    public class VehicleAllocationDetail : FullAuditedEntity
    {
        public int? DriverId { get; set; }
        public DateTime TransactionDate { get; set; }
        public int VehicleId { get; set; }
        public string VehicleName { get; set; }
        public int? DestinationTerminal { get; set; }
        public string UserEmail { get; set; }
    }
}
