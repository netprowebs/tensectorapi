﻿using SHB.Core.Domain.Entities;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;

namespace SHB.Core.Entities
{
    public class ReceiptDetail : FullAuditedEntity
    {
        public Guid ReceiptId { get; set; }
        public  ReceiptHeader ReceiptHeader { get; set; }
        public AccTransType TransType { get; set; }
        public int AccountID { get; set; }
        public string Narratives { get; set; }
        public string DocumentNo { get; set; }
        public decimal Amount { get; set; }
        public int CurrencyID { get; set; }
        public decimal ExchangeRate { get; set; }
        public int ProjectId { get; set; }
        public int AssetId { get; set; }
        public int TenantId { get; set; }

    }
}
