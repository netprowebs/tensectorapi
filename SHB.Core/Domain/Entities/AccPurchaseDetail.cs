﻿using SHB.Core.Entities;
using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class AccPurchaseDetail : FullAuditedEntity
    {
        public Guid PurchaseHeaderId { get; set; }
        public virtual AccPurchase PurchaseHeader { get; set; }
        public int ItemID { get; set; }
        public string ItemUPCCode { get; set; }
        public int WarehouseID { get; set; }
        public decimal OrderQty { get; set; }
        public decimal ItemUnitPrice { get; set; }
        public decimal ItemUnitCost { get; set; }
        public int WarehouseBinID { get; set; }
        public string Description { get; set; }
        public string SerialNumber { get; set; }      
        public int ItemUOM { get; set; }
        public int ItemWeight { get; set; }
        public int CurrencyID { get; set; }
        public decimal CurrencyExcRate { get; set; }
        public int TaxGroupID { get; set; }
        public decimal TaxAmount { get; set; }
        public bool TaxInclusive { get; set; }
        public int GLSalesAccount { get; set; }
        public int GLCOGAccount  { get; set; }
        public int DiscountAmount { get; set; }
        public int TenantId { get; set; }
        public string TExt { get; set; }


    }
}
