﻿using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SHB.Core.Entities
{
    public class Email : FullAuditedEntity
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public string EmailFrom { get; set; }
        public string EmailTo { get; set; }
        public string SenderName { get; set; }
        public bool IsSent { get; set; }
        public string SmtpServer { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool enableSSL { get; set; }
        public string TenantEmail { get; set; }
        public string APIKEY_PRIVATE { get; set; }
        public string APIKEY_PUBLIC { get; set; }
        public int TenantId { get; set; }
        //public EmailSetting setting { get; set; }
        //public EmailBodyProperties EmailBodyProperties { get; set; }
    }
}
