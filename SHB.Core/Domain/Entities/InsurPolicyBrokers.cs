﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SHB.Core.Entities
{
    public class InsurPolicyBrokers : FullAuditedEntity
    {
        public Guid InsurancePolicyId { get; set; }
        public InsurancePolicy InsurancePolicy { get; set; }
        [MaxLength(450)]
        public string BrokerCode { get; set; }
        public int BrokersCompID { get; set; }
        [MaxLength(450)]
        public string UnderWriterPolicyNo { get; set; }
        public decimal Apportionment { get; set; }
        public decimal BrokerCommission { get; set; }
        public decimal BrokerCommissionRate { get; set; }
        public decimal VATRate { get; set; }
        public decimal InsuranceRate { get; set; }
        public decimal VATDue { get; set; }
        public decimal NetDue { get; set; }

        [MaxLength(1000)]
        public string Description { get; set; }
        public bool  ActualPolicyBroker { get; set; }


    }
}
