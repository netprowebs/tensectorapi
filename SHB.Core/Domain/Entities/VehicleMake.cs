﻿using SHB.Core.Entities;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class VehicleMake : FullAuditedEntity
    {
        public string Name { get; set; }
    }
}
