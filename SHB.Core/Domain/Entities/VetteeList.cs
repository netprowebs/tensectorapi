﻿using SHB.Core.Entities;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class VetteeList : FullAuditedEntity<Guid>
    {
        public int TenantId { get; set; }
        public int ClientId { get; set; }
        public string ReferenceCode { get; set; }
        public string VetteeLastName { get; set; }
        public string VetteeFirstName { get; set; }
        public string VetteeOtherName { get; set; }
        public int VetServiceID { get; set; }
        public VetService VetService { get; set; }       
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public int VetCity { get; set; }
        public DateTime VetStartdate { get; set; }
        public DateTime VetEnddate { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string Picture { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public decimal Vat { get; set; }
        public int UnitUsed { get; set; }
        public VetStatus VetteeStatus { get; set; }
        public DeviceType LoginDeviceType { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
        public string VetAddress { get; set; }
        public float VetLatitude { get; set; }
        public float VetLongitude { get; set; }
        public string CouponCode { get; set; }
        public string VetGeoLink { get; set; }
        public string VetOwnerStatus { get; set; }
        public string VetPeriodOfstay { get; set; }
        public string VetGuardians { get; set; }
        public string VettersRemarks { get; set; }
        public string VetVideoUpload { get; set; }
        public string VetPhotoUpload { get; set; }

        //public int LGA { get; set; }
        public string Landmark { get; set; }


    }
}
