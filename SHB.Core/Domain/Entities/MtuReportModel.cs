﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class MtuReportModel : FullAuditedEntity
    {
        public string Email { get; set; }
        public string Notes { get; set; }
        public string FullName { get; set; }
        public string VehicleId { get; set; }
        public string RegistrationNumber { get; set; }
        public string DriverId { get; set; }
        public VehicleStatus Status { get; set; }
        public string Picture { get; set; }
    }
}
