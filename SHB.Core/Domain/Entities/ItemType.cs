﻿using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class ItemType : FullAuditedEntity
    {
        public string ItemTypeName { get; set; }
        public string ItemTypeDescription { get; set; }
        public int CompanyId { get; set; }
        //public ICollection<InventoryItem> InventoryItems { get; set; }
    }
}
