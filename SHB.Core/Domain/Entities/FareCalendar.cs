﻿using SHB.Core.Entities;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class FareCalendar : FullAuditedEntity
    {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public FareType FareType { get; set; }
        public FareAdjustmentType FareAdjustmentType { get; set; }
        public FareParameterType FareParameterType { get; set; }
        public decimal FareValue { get; set; }

        public int? RouteId { get; set; }
        public virtual Route Route { get; set; }
        public BookingTypes? BookingTypes { get; set; }
        public int? TerminalId { get; set; }
        public virtual Terminal Terminal { get; set; }
        public int? VehicleModelId { get; set; }
        public virtual VehicleModel VehicleModel { get; set; }
        public bool? Monday { get; set; }
        public bool? Tuesday { get; set; }
        public bool? Wednesday { get; set; }
        public bool? Thursday { get; set; }
        public bool? Friday { get; set; }
        public bool? Saturday { get; set; }
        public bool? Sunday { get; set; }
    }
}
