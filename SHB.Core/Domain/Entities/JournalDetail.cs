﻿using SHB.Core.Domain.Entities;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;

namespace SHB.Core.Entities
{
    public class JournalDetail : FullAuditedEntity
    {
        public Guid JournalId { get; set; }
        public JournalHeader JournalHeader { get; set; }    
        public AccTransType TransType { get; set; }
        public string GlAccount { get; set; }
        public string GLDescription { get; set; }
        public int CurrencyID { get; set; }
        public decimal ExchangeRate { get; set; }
        public decimal GLDebitAmount { get; set; }
        public decimal GLCreditAmount { get; set; }
        public int ProjectId { get; set; }
        public int AssetId { get; set; }
        public int TenantId { get; set; }

    }
}
