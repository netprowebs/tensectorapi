﻿using SHB.Core.Entities;
using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class SubRoute : FullAuditedEntity
    {
        public string Name { get; set; }
        public int NameId { get; set; }

        public int RouteId { get; set; }
        public virtual Route Route { get; set; }
    }
}
