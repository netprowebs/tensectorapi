﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;

namespace SHB.Core.Entities
{
    public class Complaint : FullAuditedEntity
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public ComplaintTypes ComplaintType { get; set; }
        public PriorityLevel PriorityLevel { get; set; }
        public string BookingReference { get; set; }
        public string Message { get; set; }
        public DateTime TransDate { get; set; }
        public bool Responded { get; set; }
        public string RepliedMessage { get; set; }
        public int TenantId { get; set; }
    }
}
