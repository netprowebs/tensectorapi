﻿using SHB.Core.Domain.Entities;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;

namespace SHB.Core.Entities
{
    public class InvoiceDetail : FullAuditedEntity
    {
        public Guid InvoiceId { get; set; }
        public virtual InvoiceHeader InvoiceHeader { get; set; }
        public int ItemID { get; set; }
        public string ItemUPCCode { get; set; }
        public int WarehouseID { get; set; }
        public decimal OrderQty { get; set; }
        public decimal ItemUnitPrice { get; set; }
        public decimal ItemUnitCost { get; set; }
        public int WarehouseBinID { get; set; }
        public string Description { get; set; }
        public string SerialNumber { get; set; }
        
        public int ItemUOM { get; set; }
        public int CurrencyID { get; set; }
        public decimal CurrencyExcRate { get; set; }
      
        public int TaxGroupID { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal ItemWeight { get; set; }
        public bool TaxInclusive { get; set; }

       
        public int GLSalesAccount { get; set; }
        public int GLCOGAccount  { get; set; }
        public int GLDebtorsAccount { get; set; }
        public int GLCommissionAccount { get; set; }
        public int GLVATAccount { get; set; }


        public int DiscountAmount { get; set; }
        public int TenantId { get; set; }

    }
}
