﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class Role : IdentityRole<int>
    {
        public bool IsActive { get; set; }
        public DateTime? CreationTime { get; set; }
        public bool IsDefaultRole { get; set; }
        public string RolesDescription { get; set; }
        public int CompanyInfoId { get; set; }

        public CompanyInfo CompanyInfo { get; set; }


        // public ICollection<UserRole> UserRoles { get; set; }
    }
}
