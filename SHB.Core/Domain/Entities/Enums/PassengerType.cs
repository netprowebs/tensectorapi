﻿namespace SHB.Core.Entities.Enums
{
    public enum PassengerType
    {
        Adult = 0,
        Children = 1
    }
}
