﻿namespace SHB.Core.Entities.Enums
{
    public enum PlatformType
    {
        AdminWeb,
        Android,
        iOS,
        Windows,
        Symbian,
        Tizen,
        Website,
    }
}
