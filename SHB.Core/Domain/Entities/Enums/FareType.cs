﻿namespace SHB.Core.Entities.Enums
{
    public enum FareType
    {
        Discount = 0,
        Increase = 1
    }

    public enum FareAdjustmentType
    {
        Value = 0,
        Percentage =1,
        Unit = 2
    }

    public enum FareParameterType
    {
        Route = 0,
        Terminal = 1
    }
}
