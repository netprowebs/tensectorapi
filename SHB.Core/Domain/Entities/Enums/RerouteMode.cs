﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;

namespace SHB.Core.Entities
{
    public enum RerouteMode
    {
        Admin = 0,
        Android = 1,
        IOS = 2,
        OnlineWebsite = 3,
        OnlineMobile = 4
    }
}
