﻿namespace SHB.Core.Entities.Enums
{
    public enum UploadFormat
    {
        png,
        jpeg,
        gif,
        bmp,
        tiff,
        unknown
    }
}
