﻿using System;

namespace SHB.Core.Entities.Enums
{
    [Flags]
    public enum BookingTypes
    {
        Terminal = 0,
        Advanced = 1,
        Online = 2,
        All = 3,
        BookOnHold = 4,
        Agent = 5,
        PlentyWaka = 6
    }
}
