﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;

namespace SHB.Core.Entities
{
    public enum UpgradeType
    {
        Downgrade = 0,
        Upgrade = 1
    }
}
