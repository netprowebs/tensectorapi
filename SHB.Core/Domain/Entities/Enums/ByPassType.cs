﻿namespace SHB.Core.Entities.Enums
{
    public enum ByPassType
    {
        ByPassVerify = 1,
        ByPassApprove = 2,
        ByPassAll = 3
    }
}
