﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;

namespace SHB.Core.Entities
{
    public enum RescheduleStatus
    {
        NotReshcdule = 0,
        Pending = 1,
        PayAtTerminal = 2,
        Accepted = 3,
        Declined = 4

    }
}
