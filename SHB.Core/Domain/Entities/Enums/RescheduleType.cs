﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;

namespace SHB.Core.Entities
{
    public enum RescheduleType
    {
        NotRescheduled = 0,
        DroppedFromManifest = 1,
        RescheduledIntoBus = 2
    }
}
