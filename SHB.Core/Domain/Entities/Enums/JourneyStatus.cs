﻿namespace SHB.Core.Entities.Enums
{
    public enum JourneyStatus
    {
        Pending = 0,
        Approved = 1,
        InTransit = 2,
        Received = 3,
        Transloaded = 4,
        Denied = 5,
    }
}
