﻿namespace SHB.Core.Entities.Enums
{
    public enum NotificationLevel
    {
        None = 0,
        FirstAlert = 1,
        SecondAlert = 2
    }
}
