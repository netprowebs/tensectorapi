﻿namespace SHB.Core.Entities.Enums
{
    public enum AdjustmentType
    {
        Cost = 0,
        Acquired = 1,
        Assembly = 2,
        Beginning = 3,
        Breakage = 4,
        Environment = 5,
        Shrink = 6,
        Spoilage = 7
    }
}
