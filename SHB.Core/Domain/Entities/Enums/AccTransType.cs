﻿namespace SHB.Core.Entities.Enums
{
    public enum AccTransType
    {
        Deposit ,
        PayVoucher,
        DirectLodgement,
        Renewal,
        Debit,
        Credit,
        Invoice,
        Receipt,
        Purchase,
        Adjustment,
        DebitMemo,
        CreditMemo,
        Beginning,
        GLEntry,
        JVEntry,
        ServiceCharge,
        InvReceived,
        IssueStock,
        TransferTo,
        TransferFrom,
        Manufacture,
   
    }

    
}
