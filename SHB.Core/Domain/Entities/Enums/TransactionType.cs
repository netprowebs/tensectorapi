﻿namespace SHB.Core.Entities.Enums
{
    public enum TransactionType
    {
        Debit = 0,
        Credit = 1,
        WalletUpdate = 2,
    }
}
