﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Core.Domain.Entities.Enums
{
    public enum AdjustmentTypeID
    {
        Issue = 1, Transfer = 2, Request = 3, Received = 4
    }
}
