﻿using System;

namespace SHB.Core.Entities.Enums
{
    public enum ServiceLevelType
    {
        Level_A = 0,
        Level_B = 1,
        Level_C = 2,
        Level_D = 3,
        NONE = 100
    }
}
