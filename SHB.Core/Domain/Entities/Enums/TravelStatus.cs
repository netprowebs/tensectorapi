﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;

namespace SHB.Core.Entities
{
    public enum TravelStatus
    {
        Pending = 0,
        Travelled = 1,
        NoShow = 2,
        Rescheduled = 3
    }
}
