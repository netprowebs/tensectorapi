﻿using System;

namespace SHB.Core.Entities.Enums
{
    [Flags]
    public enum RequestTypes
    {
        Stock = 0,
        Payment = 1,
        Maintenance = 2
    }
}
