﻿namespace SHB.Core.Entities.Enums
{
    public enum PickupStatus
    {
        NotForPickUp = 0,
        PendingPickup = 1,
        Pickedup = 2,
        NoShow = 3
    }
}
