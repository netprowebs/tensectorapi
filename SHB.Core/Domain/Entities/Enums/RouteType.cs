﻿namespace SHB.Core.Entities.Enums
{
    public enum RouteType
    {
        Short = 0,
        Medium = 1,
        Long = 2,
        BackwardPickup = 3
    }
}
