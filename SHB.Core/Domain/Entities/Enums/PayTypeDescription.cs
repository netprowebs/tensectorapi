﻿namespace SHB.Core.Entities.Enums
{
    public enum PayTypeDescription
    {
        Cash = 1,
        CashAndPos = 2,
        Pos = 3,
        Transfer = 4,
        Bonus = 5,
        Fine = 6,
        Loan = 7,
        WalletUpdate = 8,
        WalletDeduction = 9
    }
}
