﻿namespace SHB.Core.Entities.Enums
{
    public enum SectorsName
    {
         InventoryAndPOS = 1,
         CargoAndLogistics = 2,
         Transport = 3,
         RealEstate = 4,
         Insurance = 5,
         Farm = 6,
         AccountingAndPayroll = 7,
         Education = 8,
         OilAndGas = 9,
         Security = 10,
         Medical = 11
    }

}
