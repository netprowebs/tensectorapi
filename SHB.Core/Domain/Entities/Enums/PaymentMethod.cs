﻿namespace SHB.Core.Entities.Enums
{
    public enum PaymentMethod
    {
       
        Pos = 1,
        PayStack = 2,
        FlutterWave = 3,
        FlutterWaveUssd = 4,
        CashAndPos = 5,
        Transfer = 6,
        Wallet = 7,
         Cash = 8
    }
}
