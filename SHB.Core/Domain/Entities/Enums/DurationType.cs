﻿namespace SHB.Core.Entities.Enums
{
    public enum DurationType
    {
        Second = 0,
        Minute = 1,
        Hour = 2,
        Day = 3,
        Month = 4,
        Year = 5,
        Week = 6
    }
}
