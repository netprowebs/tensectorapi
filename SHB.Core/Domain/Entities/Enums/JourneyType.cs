﻿namespace SHB.Core.Entities.Enums
{
    public enum JourneyType
    {
        Loaded = 0,
        Blown = 1,
        Pickup = 2,
        Rescue = 3,
        Transload = 4,
        Hire = 5
    }
}
