﻿using SHB.Core.Entities;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class VettersTransaction : FullAuditedEntity
    {
        public int TenantId { get; set; }      
        public Guid VetteeListID { get; set; }
        public VetteeList VetteeList { get; set; }
        public string ReferenceCode { get; set; }
        // this part is for Vetters Status
        public VetterStatus VetStOne { get; set; }
        public int? VetStOneBy { get; set; }
        public DateTime VetStOneDate { get; set; }
        public VetterStatus VetStTwo { get; set; }
        public int? VetStTwoBy { get; set; }
        public DateTime VetStTwoDate { get; set; }
        public VetterStatus VetStThree { get; set; }
        public int? VetStThreeBy { get; set; }
        public DateTime VetStThreeDate { get; set; }
        public VetterStatus VetStfour { get; set; }
        public int? VetStfourBy { get; set; }
        public DateTime VetStfourDate { get; set; }
        public VetterStatus VetStfive { get; set; }
        public int? VetStfiveBy { get; set; }
        public DateTime VetStfiveDate { get; set; }
        public DeviceType LoginDeviceType { get; set; }
        // this part is for 3 level Approval
        public bool Assigned { get; set; }
        public int? AssignedBy { get; set; }
        public int? AssignedTO { get; set; }
        public DateTime AssignedDate { get; set; }
        public bool Verified { get; set; }
        public int? VerifiedBy { get; set; }
        public DateTime VerifiedDate { get; set; }
        public bool Approved { get; set; }
        public int? ApprovedBy { get; set; }
        public DateTime ApprovedDate { get; set; }
        public bool Posted { get; set; }
        public int? PostedBy { get; set; }
        public DateTime PostedDate { get; set; }
        public bool? Void { get; set; }

    }
}
