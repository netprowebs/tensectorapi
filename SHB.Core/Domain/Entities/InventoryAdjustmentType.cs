﻿using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class InventoryAdjustmentType : FullAuditedEntity
    {
        public string AdjustmentTypeName { get; set; }
        public string AdjustmentTypeDescription { get; set; }
        public int CompanyId { get; set; }

    }
}
