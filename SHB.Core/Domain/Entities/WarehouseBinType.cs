﻿using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class WarehouseBinType : FullAuditedEntity
    {
        public string WarehouseBinTypeName { get; set; }
        public string WarehouseBinTypeDescription { get; set; }
        public int CompanyId { get; set; }

    }
}
