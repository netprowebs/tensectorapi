﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;

namespace SHB.Core.Entities
{
    public class VehicleMileage : FullAuditedEntity<Guid>
    {
        public string VehicleRegistrationNumber { get; set; }
        public ServiceLevelType ServiceLevel { get; set; }

        public int CurrentMileage { get; set; }
        public DateTime? LastServiceDate { get; set; }
        public DateTime? DateDue { get; set; }
        public bool IsDue { get; set; }
        public bool IsDeactivated { get; set; }

        public NotificationLevel NotificationLevel { get; set; }
    }
}
