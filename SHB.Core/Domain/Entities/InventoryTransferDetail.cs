﻿using SHB.Core.Domain.Entities.Enums;
using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Core.Domain.Entities
{
    public class InventoryTransferDetail : FullAuditedEntity
    {

        public Guid InventoryTransferID { get; set; }
        public virtual InventoryTransferHeader InventoryTransferHeader { get; set; }
        public int TenantId { get; set; }
        public int? ItemID { get; set; }
        public string Description { get; set; }
        public int? RequestedQty { get; set; }
        public int? WarehouseID { get; set; }
        public int? WarehouseBinID { get; set; }
        public int? ToWarehouseID { get; set; }
        public int? ToWarehouseBinID { get; set; }
        public int? GLExpenseAccount { get; set; }
        public decimal ItemValue { get; set; }
        public decimal ItemCost { get; set; }
        public CostMethod CostMethod { get; set; }
        public int? ProjectID { get; set; }
        public string GLAnalysisType { get; set; }
        public int? AssetID { get; set; }
        public string ItemUPCCode { get; set; }
        public string ItemName { get; set; }
        public int? ToCompanyID { get; set; }
        public string TruckNumber { get; set; }
        public string QtyReceived { get; set; }
        public DateTime DateReceived { get; set; }
        public string TruckAdvance { get; set; }
        public string TruckAdvBalance { get; set; }
        public string LoadingTicket { get; set; }


    }
}
