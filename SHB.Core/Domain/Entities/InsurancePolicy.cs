﻿using SHB.Core.Domain.Entities;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SHB.Core.Entities
{
    public class InsurancePolicy : FullAuditedEntity<Guid>
    {
        [MaxLength(450)]
        public string PolicyBrokerCode { get; set; }   
        public int CustomerID { get; set; }
        public int InsuranceBizClassId { get; set; }
        public int InsuranceRiskTypeId { get; set; }
        public InsuranceRiskType InsuranceRiskType { get; set; }
        [MaxLength(1000)]
        public string PolicyDescription { get; set; }
        public DateTime PolicyStartDate { get; set; }
        public DateTime PolicyEndDate { get; set; }
        public int AcctHandlerId { get; set; }
        public bool IsMotorFleet { get; set; }
        public bool IsMarineOpen { get; set; }
        public bool IsGroupLife { get; set; }
        public int DocSumbitCount { get; set; }
        public int ProjectID { get; set; }
        public int LocationId { get; set; }
        public bool IsCoIsured { get; set; }
        public bool IsCoBroked { get; set; }

        //Payment Settings
        public InsurPaytype InsurPaytype { get; set; }
        public decimal SumInsured { get; set; }
        public decimal PremiumAmount { get; set; }     
        public bool Commissionable { get; set; }


        //Approval Settings
        public bool IsCaptured { get; set; }
        public bool Verified { get; set; }
        public int? VerifiedBy { get; set; }
        public DateTime VerifiedDate { get; set; }
        public bool Approved { get; set; }
        public int? ApprovedBy { get; set; }
        public DateTime ApprovedDate { get; set; }
        public bool Posted { get; set; }
        public int? PostedBy { get; set; }
        public DateTime PostedDate { get; set; }
        public bool? Void { get; set; }

        //Renewal Settings
        public Guid? RenewPolicyId { get; set; }
        public string RenewPolicyCode { get; set; }
        public DateTime RenewalDate { get; set; }
      

    }
}
