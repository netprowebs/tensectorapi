﻿using SHB.Core.Entities;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class VehicleTripRegistration : FullAuditedEntity<Guid>
    {
        public string PhysicalBusRegistrationNumber { get; set; }
        public DateTime DepartureDate { get; set; }

        public bool IsVirtualBus { get; set; }
        public bool IsBusFull { get; set; }
        public bool IsBlownBus { get; set; }
        public bool ManifestPrinted { get; set; }
        public string DriverCode { get; set; }
        public string OriginalDriverCode { get; set; }

        public virtual JourneyType JourneyType { get; set; }

        public Guid TripId { get; set; }
        public virtual Trip Trip { get; set; }

        public int? VehicleModelId { get; set; }
        public virtual VehicleModel VehicleModel { get; set; }
    }

}
