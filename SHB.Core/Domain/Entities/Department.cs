﻿using SHB.Core.Entities.Common;


namespace SHB.Core.Entities
{
    public class Department : FullAuditedEntity
    {
        public string Name { get; set; }
        public int CompanyId { get; set; }
        public string Description { get; set; }

    }
}
