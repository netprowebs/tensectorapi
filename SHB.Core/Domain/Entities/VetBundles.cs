﻿using SHB.Core.Entities;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class VetBundles : FullAuditedEntity
    {
        public int TenantId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public Decimal UnitCounts { get; set; }
        public Decimal AdsPrice { get; set; }
        public Decimal Amount { get; set; }     
        public FareAdjustmentType CalcType { get; set; }
        public bool IsMax { get; set; }

    }
}
