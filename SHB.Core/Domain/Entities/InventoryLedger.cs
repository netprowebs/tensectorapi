﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Core.Domain.Entities
{
    public class InventoryLedger : FullAuditedEntity
    {
        public int CompanyID { get; set; }
        public DateTime TransDate { get; set; }
        public AccTransType TransType { get; set; }
        public int ItemID { get; set; }
        public string TransNumber { get; set; }
        public string ILLineNumber { get; set; }
        public int TransactionType { get; set; }
        public int WarehouseID { get; set; }
        public int WarehouseBinID { get; set; }
        public string Quantity { get; set; }
        public float CostPerUnit { get; set; }
        public float TotalCost { get; set; }
        public float LIFOCost { get; set; }
        public float AverageCost { get; set; }
        public float FIFOCost { get; set; }
        public float ExpectedCost { get; set; }
        public float OtherCost { get; set; }
        public float StandardCost { get; set; }
        public string BalanceQty { get; set; }
        public string BatchNumber { get; set; }
        public string AccountName { get; set; }
        public string OriginalDocumentNumber { get; set; }
        public string ReferenceNumber { get; set; }
        public int CurrencyID { get; set; }
        public float CurrencyExchangeRate { get; set; }
        public string AdjustedBalanceQty { get; set; }
        public DateTime? RecieveDate { get; set; }
        public PlatformType PlatformType { get; set; }
    }
}
