﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SHB.Core.Entities
{
    public class InsuranceBizClass : FullAuditedEntity
    {
        [MaxLength(450)]
        public string Code { get; set; }
        [MaxLength(1000)]
        public string Bizname { get; set; }
        [MaxLength(450)]
        public string Description { get; set; }
        public decimal BrokersComm { get; set; }
    }
}
