﻿using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class Manifest : FullAuditedEntity<Guid>
    {
        public int NumberOfSeats { get; set; }
        public bool IsPrinted { get; set; }

        public decimal? Amount { get; set; }
        public decimal? Dispatch { get; set; }
        public DateTime? ManifestPrintedTime { get; set; }
        public Guid VehicleTripRegistrationId { get; set; }
        public virtual VehicleTripRegistration VehicleTripRegistration { get; set; }

        public int? VehicleModelId { get; set; }
        public virtual VehicleModel VehicleModel { get; set; }

        public string Employee { get; set; }
        public string DispatchSource { get; set; }

        public decimal? Commision { get; set; }
        public decimal? DriverFee { get; set; }
        public decimal? MTU { get; set; }
        public decimal? Transload { get; set; }
        public decimal? VAT { get; set; }
    }
}
