﻿using SHB.Core.Entities;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class CustomerInformation : FullAuditedEntity
    {
        public string CustomerCode { get; set; }
        public string CustomerAddress2 { get; set; }
        public string CustomerAddress3 { get; set; }
        public int CustomerCity { get; set; }
        public int CustomerState { get; set; }
        public string CustomerZip { get; set; }
        public int CustomerCountry { get; set; }
        public string CustomerFax { get; set; }
        public string CustomerWebPage { get; set; }
        public string CustomerSalutation { get; set; }
        public int? CustomerTypeID { get; set; }
        public string TaxIDNo { get; set; }
        public string VATTaxIDNumber { get; set; }
        public string VatTaxOtherNumber { get; set; }
        public int CurrencyID { get; set; }
        public int? GLSalesAccount { get; set; }
        public string TermsID { get; set; }
        public string TermsStart { get; set; }
        public int? TaxGroupID { get; set; }
        public decimal CreditLimit { get; set; }
        public string CreditComments { get; set; }
        public string PaymentDay { get; set; }
        public DateTime CustomerSince { get; set; }
        public string SendCreditMemos { get; set; }
        public string SendDebitMemos { get; set; }
        public string Statements { get; set; }
        public int WarehouseID { get; set; }
        public string WarehouseGLAccount { get; set; }
        public decimal AccountBalance { get; set; }
        public bool IsActive { get; set; }
        //public int WalletId { get; set; }
        public int TenantId { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public string Otp { get; set; }
        public bool OtpIsUsed { get; set; }
        public DateTime? OTPLastUsedDate { get; set; }
        public int? OtpNoOfTimeUsed { get; set; }
        public DeviceType DeviceType { get; set; }



    }
}
