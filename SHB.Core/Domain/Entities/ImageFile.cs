﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Core.Domain.Entities
{
    public class ImageFile : FullAuditedEntity
    {
        public string FileName { get; set; }
        public Guid RequisitionID { get; set; }
        public string FileReference { get; set; }
        public UploadType UploadType { get; set; }
        public UploadFormat UploadFormat { get; set; }
        public string UploadPath { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public int TenantId { get; set; }
    }
}
