﻿using SHB.Core.Domain.Entities.Enums;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Core.Domain.Entities
{
    public class InventoryTransferHeader : FullAuditedEntity<Guid>
    {
        //public Guid InventoryTransferID { get; set; }
        public int TenantID { get; set; }
        public AdjustmentTypeID AdjustmentTypeID { get; set; }
        public DateTime TransactionDate { get; set; }
        public int? WarehouseID { get; set; }
        public int? WarehouseBinID { get; set; }
        public string Notes { get; set; }
        public bool Cleared { get; set; }
        public string EnteredBy { get; set; }
        public bool? Void { get; set; }
        public bool Captured { get; set; }
        public bool Verified { get; set; }
        public string VerifiedBy { get; set; }
        public DateTime VerifiedDate { get; set; }
        public bool Issued { get; set; }
        public string IssuedBy { get; set; }
        public DateTime? IssuedDate { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime ApprovedDate { get; set; }
        public bool Posted { get; set; }
        public string PostedBy { get; set; }
        public DateTime PostedDate { get; set; }
        public string BatchControlNumber { get; set; }
        public string BatchControlTotal { get; set; }
        public string Signature { get; set; }
        public string SignaturePassword { get; set; }
        public string SupervisorSignature { get; set; }
        public string SupervisorPassword { get; set; }
        public string ManagerSignature { get; set; }
        public string ManagerPassword { get; set; }
        public string CurrencyID { get; set; }
        public string CurrencyExchangeRate { get; set; }
        public bool InterTransfer { get; set; }
        public int? ToCompanyID { get; set; }
        public string LoadingOfficer { get; set; }
        public string Rate { get; set; }

        public DateTime? TransferDate { get; set; }
        public PlatformType PlatformType { get; set; }

        public DateTime? ReturnDate { get; set; }
        public string ReturnNotes { get; set; }
        public string Code { get; set; }
        public string Vehicle { get; set; }
        public string Staff { get; set; }
        public string TotalQty { get; set; }

    }
}
