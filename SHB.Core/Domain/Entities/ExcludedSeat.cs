﻿using SHB.Core.Entities;
using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class ExcludedSeat : Entity
    {
        public int SeatNumber { get; set; }
        public bool IsActive { get; set; }
    }
}
