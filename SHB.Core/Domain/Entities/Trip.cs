﻿using SHB.Core.Domain.Entities;
using System;
using System.Collections.Generic;

namespace SHB.Core.Entities
{
    public class Trip : Entity
    {
        public string DepartureTime { get; set; }
        public string TripCode { get; set; }
        public bool AvailableOnline { get; set; }
        public string ParentRouteDepartureTime { get; set; }
        public int RouteId { get; set; }
        public virtual Route Route { get; set; }

        public int? VehicleModelId { get; set; }
        public virtual VehicleModel VehicleModel { get; set; }
        public int? ParentRouteId { get; set; }
        public Guid? ParentTripId { get; set; }
        public bool IsActive { get; set; }
    }
}
