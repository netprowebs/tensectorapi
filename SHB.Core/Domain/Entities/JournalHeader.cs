﻿using SHB.Core.Domain.Entities;
using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;

namespace SHB.Core.Entities
{
    public class JournalHeader : FullAuditedEntity<Guid>
    {
        public int TenantId { get; set; }
        public string GLTransactionNo { get; set; }
        public AccTransType GlTransType { get; set; }
        public DateTime GlTransactionDate { get; set; }
        public string GLReference { get; set; }
        public decimal GLTransAmount { get; set; }
        public bool Captured { get; set; }
        public bool Verified { get; set; }
        public string VerifiedBy { get; set; }
        public DateTime VerifiedDate { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime ApprovedDate { get; set; }
        public bool Posted { get; set; }
        public string PostedBy { get; set; }
        public DateTime PostedDate { get; set; }
        public bool Reversal { get; set; }
        public bool Void { get; set; }
    }
}
