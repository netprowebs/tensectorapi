﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;

namespace SHB.Core.Entities
{
    public class FeedbackResponse : FullAuditedEntity
    {
        public int ComplaintID { get; set; }
        public Complaint Complaint { get; set;}           
        public string EmployeeMessage { get; set; }
        public string ClientMessage { get; set; }
        public DateTime EmployeeDate { get; set; }
        public DateTime ClientDate { get; set; }
        public bool Responded { get; set; }
        public int UserId { get; set; }
        public int TenantId { get; set; }


    }
}
