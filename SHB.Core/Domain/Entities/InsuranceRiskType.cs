﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SHB.Core.Entities
{
    public class InsuranceRiskType : FullAuditedEntity
    {

        public int InsuranceBizClassId { get; set; }
        public InsuranceBizClass InsuranceBizClass { get; set; }
        [MaxLength(450)]
        public string Code { get; set; }       
        [MaxLength(450)]
        public string RiskName { get; set; }
        [MaxLength(1000)]
        public string Description { get; set; }
        public decimal BrokersComm { get; set; }
    }
}
