﻿using SHB.Core.Entities.Enums;
using SHB.Core.Entities.Common;
using System;

namespace SHB.Core.Entities
{
    public class WalletTransaction : FullAuditedEntity<Guid>
    {
        public TransactionType TransactionType { get; set; }
        public int UserId { get; set; }
        public decimal TransactionAmount { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal LineBalance { get; set; }
        public int WalletId { get; set; }
        public virtual Wallet Wallet { get; set; }
        public string TransDescription { get; set; }
        public PayTypeDescription PayTypeDiscription { get; set; }
        public string TransactedBy { get; set; }
        public bool IsSum { get; set; }
        public bool IsCaptured { get; set; }
        public bool IsVerified { get; set; }
        public string IsVerifiedBy { get; set; }
        public DateTime? IsVerifiedDate { get; set; }
        public bool IsApproved { get; set; }
        public string IsApprovedBy { get; set; }
        public DateTime? IsApprovedDate { get; set; }
        public int TenantId { get; set; }
        public string TransCode { get; set; }
        public FareAdjustmentType AmountType { get; set; }
    }
}
