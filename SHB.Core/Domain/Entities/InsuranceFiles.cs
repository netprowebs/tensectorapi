﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.Core.Domain.Entities
{
    public class InsuranceFiles : FullAuditedEntity
    {
        public string FileName { get; set; }
        public string FileReference { get; set; }
        public UploadType UploadType { get; set; }
        public UploadFormat UploadFormat { get; set; }
        public string UploadPath { get; set; }
        public string Description { get; set; }
        public int TenantId { get; set; }
    }
}
