﻿using SHB.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class AppMenuItemAccess : FullAuditedEntity
    {
        public int Id { get; set; }
        public int AppMenuId { get; set; }
        [ForeignKey("AppMenuId")]
        public virtual AppMenuItem AppMenuItem { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string ClaimName { get; set; }
        public bool SetupDefault { get; set; }
        public bool IsActive { get; set; }
    }
}
