﻿using SHB.Core.Entities.Common;
using SHB.Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SHB.Core.Domain.Entities
{
    public class AppMenuItem : FullAuditedEntity
    {
        //public int Id { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string MenuCode { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string MenuDescription { get; set; }
        public int DisplayOrder { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string DefaultPage { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public int? ParentId { get; set; }
        public RoleActiveType RoleActiveType { get; set; }
    }
}
