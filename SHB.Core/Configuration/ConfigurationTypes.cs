﻿namespace SHB.Core.Configuration
{
    public class AppConfig : ISettings
    {
        public string AppEmail { get; set; }
        public string HiredBookingEmail { get; set; }
        public string OTPMaxperday { get; set; }
        public string MtuSms { get; set; }
        public int ExpiryDays { get; set; }
        public decimal OnlineDiscount { get; set; }

        //for mailjet settings
        public string MJ_APIKEY_PRIVATE { get; set; }
        public string MJ_APIKEY_PUBLIC { get; set; }
        public string seclotEmail { get; set; }
        public string sender_name { get; set; }

        //for Digital Ocean Setting
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public string bucketName { get; set; }
        public string HostUploadURL { get; set; }
        public string DisplayFileContentURL { get; set; }

        //for Seclot CustomerRoleID
        public string SecRoleId { get; set; }


        

    }

    public class BookingConfig : ISettings
    {
        public string CampTripEndDate { get; set; }
        public string TerminalKey { get; set; }
        public string BookingCountReciever { get; set; }
        public string HHEx { get; set; }
        public string HHSEx { get; set; }
        public string HExHSEx { get; set; }
    }

    public class PaymentConfig : ISettings
    {
        public class Paystack
        {
            public string Secret { get; set; }
        }
    }
}