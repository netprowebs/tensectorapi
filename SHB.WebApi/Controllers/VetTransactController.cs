﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.WebApi.Utils;

namespace SHB.WebApi.Controllers
{
    // [Authorize]
    public class VetTransactController : BaseController
    {
        private readonly IVetTransactService _vetteeSvc;  

        public VetTransactController(IVetTransactService vetteeSvc)
        {
            _vetteeSvc = vetteeSvc;
        }

        #region VetteeLists
        [HttpPost]
        [Route("AddVettees")]
        public async Task<IServiceResponse<bool>> AddVetteeList(VetteeListDTO vetteeList)
        {
            return await HandleApiOperationAsync(async () => {
                await _vetteeSvc.AddVetteeList(vetteeList);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetVettee")]
        [Route("GetVettee/{pageNumber}/{pageSize}")]
        [Route("GetVettee/{pageNumber}/{pageSize}/{query}")]
        public async Task<IServiceResponse<IPagedList<VetteeListDTO>>> GetVettee(int pageNumber = 1,
           int pageSize = WebConstants.DefaultPageSize, string query = null)
        {
            return await HandleApiOperationAsync(async () => {
                var vettee = await _vetteeSvc.GetVetteeList(pageNumber, pageSize);

                return new ServiceResponse<IPagedList<VetteeListDTO>>
                {
                    Object = vettee
                };
            });
        }




        [HttpPut]
        [Route("UpdateVettee/{id}")]
        public async Task<IServiceResponse<bool>> UpdateVetteeList(Guid id, VetteeListDTO vetteeList)
        {
            return await HandleApiOperationAsync(async () => {
                await _vetteeSvc.UpdateVetteeList(id, vetteeList);

                return new ServiceResponse<bool>(true);
            });
        }



        [HttpPost]
        [Route("UpdateVettersForm")]
        public async Task<IServiceResponse<bool>> AddVettrans(VetteeListDTO vetteeList)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _vetteeSvc.UpdateVettersForm(vetteeList.Id, vetteeList);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetVettee/{id}")]
        public async Task<IServiceResponse<VetteeListDTO>> GetVetteeListById(Guid id)
        {
            return await HandleApiOperationAsync(async () => {
                var vetteeList = await _vetteeSvc.GetVetteeListById(id);

                return new ServiceResponse<VetteeListDTO>
                {
                    Object = vetteeList
                };
            });
        }

  
        [HttpGet]
        [Route("GetVetByClient/{id}")]
        [Route("GetVetByClient/{id}/{pageNumber}/{pageSize}")]
        [Route("GetVetByClient/{id}/{pageNumber}/{pageSize}/{query}")]
        public async Task<IServiceResponse<IPagedList<VetteeListDTO>>> GetVetByClient(int id, int pageNumber = 1,
          int pageSize = WebConstants.DefaultPageSize, string query = null )
        {
            return await HandleApiOperationAsync(async () => {
                var vettee = await _vetteeSvc.GetVetByClient(id, pageNumber, pageSize);

                return new ServiceResponse<IPagedList<VetteeListDTO>>
                {
                    Object = vettee
                };
            });
        }


        [HttpDelete]
        [Route("DeleteVettees/{id}")]
        public async Task<IServiceResponse<bool>> DeleteVetteeList(Guid id)
        {
            return await HandleApiOperationAsync(async () => {
                await _vetteeSvc.RemoveVetteeList(id);

                return new ServiceResponse<bool>(true);
            });
        }

        //[AllowAnonymous]
        [HttpPost]
        [Route("UploadVetteeFiles")]
        public async Task<IServiceResponse<bool>> UploadVetteeFiles(IFormFile file)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _vetteeSvc.UploadVetteeFiles(file);

                return new ServiceResponse<bool>(true);
            });
        }


        #endregion

        #region VettersTransactions
        [HttpGet]
        [Route("GetVettrans")]
        [Route("GetVettrans/{pageNumber}/{pageSize}")]
        [Route("GetVettrans/{pageNumber}/{pageSize}/{query}")]
        public async Task<IServiceResponse<IPagedList<VettersTransactionDTO>>> GetVettersTransaction(int pageNumber = 1,
         int pageSize = WebConstants.DefaultPageSize, string query = null)
        {
            return await HandleApiOperationAsync(async () => {
                var vetter = await _vetteeSvc.GetVetTransactionList(pageNumber, pageSize, query);

                return new ServiceResponse<IPagedList<VettersTransactionDTO>>
                {
                    Object = vetter
                };
            });
        }

        [HttpGet]
        [Route("AllVetAssigned/{UserId}")]
        public async Task<IServiceResponse<IPagedList<VettersTransactionDTO>>> AllVetAssigned(int UserId)
        {
            return await HandleApiOperationAsync(async () => {
                var vetter = await _vetteeSvc.AllVetAssigned(UserId);

                return new ServiceResponse<IPagedList<VettersTransactionDTO>>
                {
                    Object = vetter
                };
            });
        }

        [HttpGet]
        [Route("GetDistinctVettrans")]
        public async Task<IServiceResponse<List<VettersDistinctDTO>>> GetDistinctVettrans()
        {
            return await HandleApiOperationAsync(async () => {
                var vetter = await _vetteeSvc.GetDistinctVettrans();
                return new ServiceResponse<List<VettersDistinctDTO>>
                {
                    Object = vetter
                };
            });
        }


        [HttpPost]
        [Route("AddVettrans")]
        public async Task<IServiceResponse<bool>> AddVettrans(VettersTransactionDTO vetter)
        {
            return await HandleApiOperationAsync(async () => {
                await _vetteeSvc.AddVettrans(vetter);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("UpdateVettrans/{id}")]
        public async Task<IServiceResponse<bool>> UpdateVettersTransaction(int id, VettersTransactionDTO vetter)
        {
            return await HandleApiOperationAsync(async () => {
                await _vetteeSvc.UpdateVetTransaction(id, vetter);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetVettrans/{id}")]
        public async Task<IServiceResponse<VettersTransactionDTO>> GetVettersTransactionById(int id)
        {
            return await HandleApiOperationAsync(async () => {
                var vetter = await _vetteeSvc.GetVetTransactionById(id);

                return new ServiceResponse<VettersTransactionDTO>
                {
                    Object = vetter
                };
            });
        }

        [HttpPut]
        [Route("VetterActionTo")]
        public async Task<IServiceResponse<bool>> VetterActionTo(ActionDto model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _vetteeSvc.VetterActionTo(model);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion

    }
}
