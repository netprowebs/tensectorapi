﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.WebApi.Utils;

namespace SHB.WebApi.Controllers
{
    [Authorize]
    public class VehicleController : BaseController
    {
        private readonly IVehicleService _vehicleService;

        public VehicleController(IVehicleService vehicleService)
        {
            _vehicleService = vehicleService;
        }

        [HttpGet]
        [Route("Get")]
        [Route("Get/{pageNumber}/{pageSize}")] 
        [Route("Get/{pageNumber}/{pageSize}/{query}")]
        public async Task<IServiceResponse<IPagedList<VehicleDTO>>> GetVehicles(int pageNumber = 1,
            int pageSize = WebConstants.DefaultPageSize, string query = null)
        {
            return await HandleApiOperationAsync(async () => {
                var vehicles = await _vehicleService.GetVehicles(pageNumber, pageSize);

                return new ServiceResponse<IPagedList<VehicleDTO>>
                {
                    Object = vehicles
                };
            });
        }

        [HttpGet]
        [Route("Get/{id}")]
        public async Task<IServiceResponse<VehicleDTO>> GetVehicleById(int id)
        {
            return await HandleApiOperationAsync(async () => {
                var vehicle = await _vehicleService.GetVehicleById(id);

                return new ServiceResponse<VehicleDTO>
                {
                    Object = vehicle
                };
            });
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IServiceResponse<bool>> AddVehicle(VehicleDTO vehicle)
        {
            return await HandleApiOperationAsync(async () => {
                await _vehicleService.AddVehicle(vehicle);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IServiceResponse<bool>> UpdateVehicle(int id, VehicleDTO vehicle)
        {
            return await HandleApiOperationAsync(async () => {
                await _vehicleService.UpdateVehicle(id, vehicle);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IServiceResponse<bool>> DeleteVehicle(int id)
        {
            return await HandleApiOperationAsync(async () => {
                await _vehicleService.RemoveVehicle(id);

                return new ServiceResponse<bool>(true);
            });
        }
    }
}
