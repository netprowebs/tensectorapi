﻿using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SHB.WebApi.Controllers
{
    [Authorize]
    public class CustomerTypesController : BaseController
    {
        private readonly ICustomerTypesService _customerTypes;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUserService _userManagerSvc;

        public CustomerTypesController(ICustomerTypesService customerTypes, IServiceHelper serviceHelper, IUserService userManagerSvc)
        {
            _customerTypes = customerTypes;
            _serviceHelper = serviceHelper;
            _userManagerSvc = userManagerSvc;
        }

        #region Customer Types
        [HttpPost]
        [Route("AddCustomerType")]
        public async Task<IServiceResponse<bool>> AddCustomerType(CustomerTypesDTO model)
        {
            return await HandleApiOperationAsync(async () => {
                await _customerTypes.AddCustomerTypes(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllCustomerTypes")]
        [Route("GetAllCustomerTypes/{pageNumber}/{pageSize}")]
        [Route("GetAllCustomerTypes/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<CustomerTypes>>> GetAllCustomerTypes(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () => {

                var CustomerType = await _customerTypes.GetAllCustomerTypes(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<CustomerTypes>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("GetCustomerType/{id}")]
        public async Task<IServiceResponse<CustomerTypesDTO>> GetCustomerType(int id)
        {
            return await HandleApiOperationAsync(async () => {
                var CustomerType = await _customerTypes.GetCustomerTypes(id);

                return new ServiceResponse<CustomerTypesDTO>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpPut]
        [Route("UpdateCustomerType/{id}")]
        public async Task<IServiceResponse<bool>> UpdateCustomerType(CustomerTypesDTO model, int Id)
        {
            return await HandleApiOperationAsync(async () => {
                await _customerTypes.UpdateCustomerTypes(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteCustomerType/{id}")]
        public async Task<IServiceResponse<bool>> DeleteCustomerType(int id)
        {
            return await HandleApiOperationAsync(async () => {
                await _customerTypes.DeleteCustomerTypes(id);

                return new ServiceResponse<bool>(true);
            });
        }

        #endregion
    }
}
