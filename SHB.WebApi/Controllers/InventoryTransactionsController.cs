﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPagedList;
using SHB.Business.Services;
using SHB.Core.Domain.Entities;
using SHB.Core.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SHB.WebApi.Controllers;
using SHB.WebApi.Utils;
using SHB.Core.Domain.DataTransferObjects;

namespace SHB.WebAPI.Controllers
{
    [Authorize]
    public class InventoryTransactionsController : BaseController
    {
        private readonly IInventoryTransactionsService _inventoryTransactions;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUserService _userManagerSvc;

        public InventoryTransactionsController(IInventoryTransactionsService inventoryTransactions, IServiceHelper serviceHelper, IUserService userManagerSvc)
        {
            _inventoryTransactions = inventoryTransactions;
            _serviceHelper = serviceHelper;
            _userManagerSvc = userManagerSvc;
        }

        #region Inventory Received Header
        [HttpPost]
        [Route("AddHeader")]
        public async Task<IServiceResponse<bool>> AddInventoryReceivedHeader(InventoryReceivedHeader model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryTransactions.AddInventoryReceivedHeader(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllHeader")]
        [Route("GetAllHeader/{pageNumber}/{pageSize}")]
        [Route("GetAllHeader/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<InventoryReceivedHeader>>> GetAllInventoryReceivedHeader(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var CustomerType = await _inventoryTransactions.GetAllInventoryReceivedHeader(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<InventoryReceivedHeader>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("GetHeader/{id}")]
        public async Task<IServiceResponse<InventoryReceivedHeader>> GetInventoryReceivedHeader(Guid id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var CustomerType = await _inventoryTransactions.GetInventoryReceivedHeader(id);

                return new ServiceResponse<InventoryReceivedHeader>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpPut]
        [Route("UpdateHeader/{id}")]
        public async Task<IServiceResponse<bool>> UpdateHeader(InventoryReceivedHeader model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryTransactions.UpdateInventoryReceivedHeader(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteHeader/{id}")]
        public async Task<IServiceResponse<bool>> DeleteInventoryReceivedHeader(Guid id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryTransactions.DeleteInventoryReceivedHeader(id);

                return new ServiceResponse<bool>(true);

            });
        }


        [HttpPut]
        [Route("InvRecVerify/{id}")]
        public async Task<IServiceResponse<bool>> VerifyRequest(InventoryReceivedHeader model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryTransactions.VerifyRequest(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }


        [HttpPut]
        [Route("InvReceive/{id}")]
        public async Task<IServiceResponse<bool>> ReceiveRequest(InventoryReceivedHeader model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryTransactions.ReceiveRequest(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("InvRecApprove/{id}")]
        public async Task<IServiceResponse<bool>> ApproveRequest(InventoryReceivedHeader model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryTransactions.ApproveRequest(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("InvRecReturn/{id}")]
        public async Task<IServiceResponse<bool>> ReturnRequest(InventoryReceivedHeader model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryTransactions.ReturnRequest(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }


        #endregion

        #region Inventory Received Detail
        [HttpPost]
        [Route("AddDetail")]
        public async Task<IServiceResponse<bool>> AddInventoryReceivedDetail(InventoryReceivedDetail model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryTransactions.AddInventoryReceivedDetail(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllDetail")]
        [Route("GetAllDetail/{pageNumber}/{pageSize}")]
        [Route("GetAllDetail/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<InventoryReceivedDetail>>> GetAllInventoryReceivedDetail(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var CustomerType = await _inventoryTransactions.GetAllInventoryReceivedDetail(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<InventoryReceivedDetail>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("GetDetail/{id}")]
        public async Task<IServiceResponse<List<InventoryReceivedDetailDTO>>> GetInventoryReceivedDetail(Guid id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var CustomerType = await _inventoryTransactions.GetInventoryReceivedDetail(id);

                return new ServiceResponse<List<InventoryReceivedDetailDTO>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpPut]
        [Route("UpdateDetail/{id}")]
        public async Task<IServiceResponse<bool>> UpdateInventoryReceivedDetail(InventoryReceivedDetail model, int Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryTransactions.UpdateInventoryReceivedDetail(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteDetail/{id}")]
        public async Task<IServiceResponse<bool>> DeleteInventoryReceivedDetail(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryTransactions.DeleteInventoryReceivedDetail(id);

                return new ServiceResponse<bool>(true);

            });
        }
        #endregion

    }
}