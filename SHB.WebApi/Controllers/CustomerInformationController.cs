﻿using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SHB.WebApi.Controllers
{
    [Authorize]
    public class CustomerInformationController : BaseController
    {
        private readonly ICustomerInformationService _customerInformation;
        public CustomerInformationController(ICustomerInformationService customerInformation)
        {
            _customerInformation = customerInformation;
        }

        #region Customer Information
        [AllowAnonymous]
        [HttpPost]
        [Route("Add")]
        public async Task<IServiceResponse<bool>> AddCustomerInfo(CustomerInformationDTO model)
        {
            return await HandleApiOperationAsync(async () => {
                await _customerInformation.AddCustomerInformation(model);

                return new ServiceResponse<bool>(true);
            });
        }
        //[AllowAnonymous]
        [HttpGet]
        [Route("Get")]
        [Route("Get/{pageNumber}/{pageSize}")]
        [Route("Get/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<CustomerInformationDTO>>> GetAllCustomerInfos(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {
                var CustomerType = await _customerInformation.GetAllCustomerInformation(pageNumber, pageSize, searchTerm);
                return new ServiceResponse<IPagedList<CustomerInformationDTO>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("GetCustomer/{id}")]
        public async Task<IServiceResponse<CustomerInformationDTO>> GetCustomerInfo(int id)
        {
            return await HandleApiOperationAsync(async () => {
                var CustomerType = await _customerInformation.GetCustomerInformation(id);

                return new ServiceResponse<CustomerInformationDTO>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("GetCustomerByUserId/{idorEmail}")]
        public async Task<IServiceResponse<CustomerInformationDTO>> GetCustomerByUser(string idorEmail)
        {
            return await HandleApiOperationAsync(async () => {
                var Customer = await _customerInformation.GetCustomerByUserId(idorEmail);

                return new ServiceResponse<CustomerInformationDTO>
                {
                    Object = Customer
                };
            });
        }
        [HttpPut]
        [Route("UpdateCustomer/{id}")]
        public async Task<IServiceResponse<bool>> UpdateCustomerInfo(CustomerInformationDTO model, int Id)
        {
            return await HandleApiOperationAsync(async () => {
                await _customerInformation.UpdateCustomerInformation(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteCustomer/{id}")]
        public async Task<IServiceResponse<bool>> DeleteCustomerInfo(int id)
        {
            return await HandleApiOperationAsync(async () => {
                await _customerInformation.DeleteCustomerInformation(id);

                return new ServiceResponse<bool>(true);

            });
        }
        #endregion
    }
}
