﻿using IPagedList;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SHB.WebApi.Controllers
{
    public class ReceiptController : BaseController
    {
        private readonly IReceiptService receiptService;

        public ReceiptController(IReceiptService receiptService)
        {
            this.receiptService = receiptService;
        }

        #region ReceiptHeader

        [HttpPost]
        [Route("AddReceiptHeader")]
        public async Task<IServiceResponse<bool>> AddReceiptHeader(ReceiptHeaderDTO receiptHeader)
        {
            return await HandleApiOperationAsync(async () => {
                await this.receiptService.AddReceiptHeader(receiptHeader);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetReceiptHeader")]
        [Route("GetReceiptHeader/{pageNumber}/{pageSize}")]
        [Route("GetReceiptHeader/{pageNumber}/{pageSize}/{query}")]
        public async Task<IServiceResponse<IPagedList<ReceiptHeaderDTO>>> GetReceiptHeader(int pageNumber = 1,
           int pageSize = WebConstants.DefaultPageSize, string query = null)
        {
            return await HandleApiOperationAsync(async () => {
                var receipt = await this.receiptService.GetReceiptHeader(pageNumber, pageSize, query);

                return new ServiceResponse<IPagedList<ReceiptHeaderDTO>>
                {
                    Object = receipt
                };
            });
        }


        [HttpPut]
        [Route("UpdateReceiptHeader/{id}")]
        public async Task<IServiceResponse<bool>> UpdateReceiptHeader(Guid id, ReceiptHeaderDTO receiptHeader)
        {
            return await HandleApiOperationAsync(async () => {
                await this.receiptService.UpdateReceiptHeader(id, receiptHeader);

                return new ServiceResponse<bool>(true);
            });
        }


        [HttpGet]
        [Route("GetReceiptHeader/{id}")]
        public async Task<IServiceResponse<ReceiptHeaderDTO>> GetReceiptHeader(Guid id)
        {
            return await HandleApiOperationAsync(async () => {
                var receipt = await this.receiptService.GetReceiptHeaderById(id);

                return new ServiceResponse<ReceiptHeaderDTO>
                {
                    Object = receipt
                };
            });
        }



        [HttpDelete]
        [Route("DeleteReceiptHeader/{id}")]
        public async Task<IServiceResponse<bool>> DeleteReceiptHeader(Guid id)
        {
            return await HandleApiOperationAsync(async () => {
                await this.receiptService.RemoveReceiptHeader(id);

                return new ServiceResponse<bool>(true);
            });
        }

        #endregion


        #region ReceiptDetail

        [HttpGet]
        [Route("GetReceiptDetail")]
        [Route("GetReceiptDetail/{pageNumber}/{pageSize}")]
        [Route("GetReceiptDetail/{pageNumber}/{pageSize}/{query}")]
        public async Task<IServiceResponse<IPagedList<ReceiptDetailDTO>>> GetReceiptDetail(int pageNumber = 1,
        int pageSize = WebConstants.DefaultPageSize, string query = null)
        {
            return await HandleApiOperationAsync(async () => {
                var receipt = await this.receiptService.GetReceiptDetail(pageNumber, pageSize, query);

                return new ServiceResponse<IPagedList<ReceiptDetailDTO>>
                {
                    Object = receipt
                };
            });
        }


        [HttpPost]
        [Route("AddReceiptDetail")]
        public async Task<IServiceResponse<bool>> AddReceiptDetail(ReceiptDetailDTO receipt)
        {
            return await HandleApiOperationAsync(async () => {
                await this.receiptService.AddReceiptDetail(receipt);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("UpdateReceiptDetail/{id}")]
        public async Task<IServiceResponse<bool>> UpdateReceiptDetail(int id, ReceiptDetailDTO receiptDto)
        {
            return await HandleApiOperationAsync(async () => {
                await this.receiptService.UpdateReceiptDetail(id, receiptDto);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetReceiptDetail/{id}")]
        public async Task<IServiceResponse<ReceiptDetailDTO>> GetReceiptDetailById(int id)
        {
            return await HandleApiOperationAsync(async () => {
                var receipt = await this.receiptService.GetReceiptDetailById(id);

                return new ServiceResponse<ReceiptDetailDTO>
                {
                    Object = receipt
                };
            });
        }
        #endregion
    }
}
