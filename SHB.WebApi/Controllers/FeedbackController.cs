﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPagedList;
using SHB.Business.Services;
using SHB.Core.Domain.Entities;
using SHB.Core.Entities;
using SHB.WebAPI.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SHB.WebApi.Controllers;
using SHB.WebApi.Utils;
using SHB.Core.Domain.DataTransferObjects;
using System.Text.Json;

namespace LME.WebAPI.Controllers
{
    public class FeedbackController : BaseController//: ControllerBase
    {
        private readonly IFeedbackService _feedbackService;

        public FeedbackController(IFeedbackService feedbackService)
        {
            _feedbackService = feedbackService;
        }

        #region Feedback

        [AllowAnonymous]
        [HttpPost]
        [Route("AddFeedBack")]
        public async Task<IServiceResponse<bool>> AddFeedBack(ComplaintDTO model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _feedbackService.AddFeedback(model);

                return new ServiceResponse<bool>(true);
            });
        }


        //[HttpGet]
        //[Route("GetAllFeedBacks")]
        //[Route("GetAllFeedBacks/{pageNumber}/{pageSize}")]
        //[Route("GetAllFeedBacks/{pageNumber}/{pageSize}/{searchTerm}")]
        //public async Task<IServiceResponse<IPagedList<ComplaintDTO>>> GetAllFeedBack(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        //{
        //    return await HandleApiOperationAsync(async () =>
        //    {

        //        var response = await _feedbackService.GetAllFeedbacks(pageNumber, pageSize, searchTerm);

        //        return new ServiceResponse<IPagedList<ComplaintDTO>>
        //        {
        //            Object = response
        //        };
        //    });
        //}

        [HttpGet]
        [Route("GetAllFeedBacks")]
        public async Task<IServiceResponse<List<ComplaintDTO>>> GetAllFeedBack()
        {
            return await HandleApiOperationAsync(async () =>
            {

                var response = await _feedbackService.GetAllFeedbacks();

                return new ServiceResponse<List<ComplaintDTO>>
                {
                    Object = response
                };
            });
        }

        [HttpGet]
        [Route("GetFeedBack/{id}")]
        public async Task<IServiceResponse<ComplaintDTO>> GetFeedBack(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var resp = await _feedbackService.GetFeedback(id);

                return new ServiceResponse<ComplaintDTO>
                {
                    Object = resp
                };
            });
        }



        [HttpPut]
        [Route("UpdateFeedBackById/{Id}")]
        public async Task<IServiceResponse<bool>> UpdateFeedBackById(ComplaintDTO model, int Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _feedbackService.UpdateFeedbackById(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteFeedBack/{id}")]
        public async Task<IServiceResponse<bool>> DeleteFeedback(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _feedbackService.DeleteFeedback(id);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion


        #region FeedbackResponse

        [HttpPost]
        [Route("AddFeedBackResp")]
        public async Task<IServiceResponse<bool>> AddFeedBackResponse(FeedBackResponseDTO model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _feedbackService.AddFeedbackResponse(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]

        [Route("GetAllFeedBackResp/{id}")]
        public async Task<IServiceResponse<List<FeedBackResponseDTO>>> GetAllFeedBackResponse(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var response = await _feedbackService.GetAllFeedbackResponses(id);

                return new ServiceResponse<List<FeedBackResponseDTO>>
                {
                    Object = response
                };


            });
        }

        [HttpGet]
        [Route("GetFeedBackResp/{id}")]
        public async Task<IServiceResponse<FeedBackResponseDTO>> GetFeedBackResponse(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var resp = await _feedbackService.GetFeedbackResponse(id);

                return new ServiceResponse<FeedBackResponseDTO>
                {
                    Object = resp
                };
            });
        }

        [HttpPut]
        [Route("UpdateFeedBackResp/{Id}")]
        public async Task<IServiceResponse<bool>> UpdateFeedBackResponseById(FeedBackResponseDTO model, int Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _feedbackService.UpdateFeedbackResponseById(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteFeedBackResp/{id}")]
        public async Task<IServiceResponse<bool>> DeleteFeedbackResponse(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _feedbackService.DeleteFeedbackResponse(id);

                return new ServiceResponse<bool>(true);
            });
        }

        #endregion


    }
}






