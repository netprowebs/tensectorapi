﻿using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SHB.WebApi.Controllers
{
    [Authorize]
    public class VendorTypesController : BaseController
    {

        private readonly IVendorTypesService _vendorTypes;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUserService _userManagerSvc;
        public VendorTypesController(IVendorTypesService vendorTypes, IServiceHelper serviceHelper, IUserService userManagerSvc)
        {
            _vendorTypes = vendorTypes;
            _serviceHelper = serviceHelper;
            _userManagerSvc = userManagerSvc;
        }

        #region Vendor Types
        [HttpPost]
        [Route("AddVendorType")]
        public async Task<IServiceResponse<bool>> AddVendorType(VendorTypesDTO model)
        {
            return await HandleApiOperationAsync(async () => {
                await _vendorTypes.AddVendorTypes(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllVendorTypes")]
        [Route("GetAllVendorTypes/{pageNumber}/{pageSize}")]
        [Route("GetAllVendorTypes/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<VendorTypes>>> GetAllVendorTypes(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var CustomerType = await _vendorTypes.GetAllVendorTypes(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<VendorTypes>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("GetVendorType/{id}")]
        public async Task<IServiceResponse<VendorTypesDTO>> GetVendorType(int id)
        {
            return await HandleApiOperationAsync(async () => {
                var CustomerType = await _vendorTypes.GetVendorType(id);

                return new ServiceResponse<VendorTypesDTO>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpPut]
        [Route("UpdateVendorType/{id}")]
        public async Task<IServiceResponse<bool>> UpdateVendorType(VendorTypesDTO model, int Id)
        {
            return await HandleApiOperationAsync(async () => {
                await _vendorTypes.UpdateVendorType(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteVendorType/{id}")]
        public async Task<IServiceResponse<bool>> DeleteVendorType(int id)
        {
            return await HandleApiOperationAsync(async () => {
                await _vendorTypes.DeleteVendorType(id);

                return new ServiceResponse<bool>(true);

            });
        }
        #endregion
    }
}
