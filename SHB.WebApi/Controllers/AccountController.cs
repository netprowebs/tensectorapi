﻿using IdentityModel;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Entities;
using SHB.WebAPI.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using LME.Core.Domain.DataTransferObjects;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;

namespace SHB.WebApi.Controllers
{
    //[Authorize]
    public class AccountController : BaseController
    {
        private readonly IUserService _userSvc;
        private readonly IRoleService _roleSvc;
        private readonly IEmployeeService _employeeService;
     

        public AccountController(IUserService userSvc, IRoleService roleSvc, IEmployeeService employeeService)
        {
            _userSvc = userSvc;
            _roleSvc = roleSvc;
            _employeeService = employeeService;
            
        }

        private async Task<List<Claim>> GetUserIdentityClaims(User user)
        {
            var userClaims = user.UserToClaims();
            var roles = await _userSvc.GetUserRoles(user);

            foreach (var item in roles)
            {
                userClaims.Add(new Claim(JwtClaimTypes.Role, item));
                var roleClaims = await _roleSvc.GetClaimsAsync(item);
                userClaims.AddRange(roleClaims);
            }
            var employee = await _employeeService.GetEmployeesByemailAsync(user.Email);
            if (employee != null) 
            {
                userClaims.Add(new Claim("location", employee?.TerminalId?.ToString()));
                userClaims.Add(new Claim("company", employee.Company?.ToString()));
            }
            return userClaims;
        }

        //[AllowAnonymous]
        [HttpGet]
        [Route("GetCurrentUserClaims")]
        public async Task<IServiceResponse<IEnumerable<Claim>>> GetCurrentUserClaims()
        {
            return await HandleApiOperationAsync(async () => {

                var response = new ServiceResponse<IEnumerable<Claim>>();

                var user = await _userSvc.FindByNameAsync(User?.FindFirst(JwtClaimTypes.Name)?.Value);

                var claims = await GetUserIdentityClaims(user);

                response.Object = claims;

                return response;
            });
        }

        [HttpGet]
        [Route("GetProfile")] 
        public async Task<IServiceResponse<UserProfileDTO>> GetCurrentUserProfile()
        {
            return await HandleApiOperationAsync(async () => {

                var response = new ServiceResponse<UserProfileDTO>();

                var profile = await _userSvc.GetProfile(User.FindFirst(JwtClaimTypes.Name)?.Value);
                response.Object = profile;
                return response;
            });
        }

        [HttpPost]
        [Route("UpdateProfile")]
        public async Task<IServiceResponse<bool>> UpdatetUserProfile(UserProfileDTO model)
        {
            return await HandleApiOperationAsync(async () => {

                var result = await _userSvc.UpdateProfile(User.FindFirst(JwtClaimTypes.Name)?.Value, model);

                return new ServiceResponse<bool>(result);
            });
        }

        [AllowAnonymous]
        [HttpPut]
        [Route("Activate")]
        public async Task<ServiceResponse<UserDTO>> Activate(UserAccountDTO model)
        {
            return await HandleApiOperationAsync(async () => {
                var result = await _userSvc.ActivateAccount(model);
                return new ServiceResponse<UserDTO>(result);
            });
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("ForgotPassword/{usernameOrEmail}")]
        public async Task<ServiceResponse<bool>> ForgotPassword(string usernameOrEmail)
        {
            return await HandleApiOperationAsync(async () => {
                var result = await _userSvc.ForgotPassword(usernameOrEmail);
                return new ServiceResponse<bool>(result);
            });
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("ResetPassword")]
        public async Task<ServiceResponse<bool>> ResetPassword(PassordResetDTO model)
        {
            return await HandleApiOperationAsync(async () => {
                var result = await _userSvc.ResetPassword(model);
                return new ServiceResponse<bool>(result);
            });
        }


        [HttpPost]
        [Route("ChangePassword")]
        public async Task<ServiceResponse<bool>> ChangePassword(ChangePassordDTO model)
        {
            return await HandleApiOperationAsync(async () => {
                var result = await _userSvc.ChangePassword(User.FindFirst(JwtClaimTypes.Name)?.Value, model);
                return new ServiceResponse<bool>(result);
            });
        }

  
        //[HttpGet]
        //[Route("GetUserInfo/{Email}")]
        //public async Task<IServiceResponse<User>> GetUserInfo(string email)
        //{
        //    return await HandleApiOperationAsync(async () =>
        //    {
        //        var result = await _userSvc.FindByEmailAsync(email);
        //        return new ServiceResponse<User>(result);
        //    });
        //}

        [HttpGet]
        [Route("GetUserInfo/{Email}")]
        public async Task<IServiceResponse<User>> GetUserInfo(string email)
        {
            return await HandleApiOperationAsync(async () => {
                var employee = await _employeeService.GetUserInfo(email);

                return new ServiceResponse<User>
                {
                    Object = employee
                };
            });
        }


        //[HttpGet]
        //[Route("Logout")]
        //public async Task<bool> Logout()
        //{
        //    _IdUserMgr.Dispose();
        //    //_IdUserMgr.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme); 
        //    return true;
        //}
    }
}