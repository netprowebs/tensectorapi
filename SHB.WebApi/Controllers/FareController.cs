﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.WebApi.Utils;

namespace SHB.WebApi.Controllers
{
    [Authorize]
    public class FareController : BaseController
    {
        private readonly IFareService _fareService;

        public FareController(IFareService fareService)
        {
            _fareService = fareService;
        }

        [HttpGet]
        [Route("Get")]
        [Route("Get/{pageNumber}/{pageSize}")]
        [Route("Get/{pageNumber}/{pageSize}/{query}")]
        public async Task<IServiceResponse<IPagedList<FareDTO>>> GetFares(int pageNumber = 1,
            int pageSize = WebConstants.DefaultPageSize, string query = null)
        {
            return await HandleApiOperationAsync(async () => {
                var fares = await _fareService.GetFares(pageNumber, pageSize);

                return new ServiceResponse<IPagedList<FareDTO>>
                {
                    Object = fares
                };
            });
        }

        [HttpGet]
        [Route("Get/{id}")]
        public async Task<IServiceResponse<FareDTO>> GetFareById(int id)
        {
            return await HandleApiOperationAsync(async () => {
                var fare = await _fareService.GetFareById(id);

                return new ServiceResponse<FareDTO>
                {
                    Object = fare
                };
            });
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IServiceResponse<bool>> AddFare(FareDTO fare)
        {
            return await HandleApiOperationAsync(async () => {
                await _fareService.AddFare(fare);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IServiceResponse<bool>> UpdateFare(int id, FareDTO fare)
        {
            return await HandleApiOperationAsync(async () => {
                await _fareService.UpdateFare(id, fare);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IServiceResponse<bool>> DeleteFare(int id)
        {
            return await HandleApiOperationAsync(async () => {
                await _fareService.RemoveFare(id);

                return new ServiceResponse<bool>(true);
            });
        }
    }
}
