﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.WebApi.Utils;

namespace SHB.WebApi.Controllers
{
    // [Authorize]
    public class UtilityController : BaseController
    {
        private readonly IUtilityService _utilitySvc;  

        public UtilityController(IUtilityService utilitySvc)
        {
            _utilitySvc = utilitySvc;
        }
        #region Verifications

        #endregion

        #region DigitalOceanImage

        [AllowAnonymous]
        [HttpPost]
        [Route("UplaodToDigOc")]
        public async Task<IServiceResponse<bool>> UplaodToDigOc(ImageFileDTO model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _utilitySvc.UplaodBytesToDigOc(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetUploadDigOcFiles/{ImageName}")]
        public async Task<IServiceResponse<FileStreamResult>> GetUploadFiles(string ImageName)
        {
            return await HandleApiOperationAsync(async () => {
                var uploadContent = await _utilitySvc.GetUploadFiles(ImageName);

                return new ServiceResponse<FileStreamResult>
                {
                    Object = uploadContent
                };
            });
        }

        [HttpDelete]
        [Route("RemoveDigOcImage/{imageName}")]
        public async Task<IServiceResponse<bool>> RemoveUploadImage(string imageName)
        {
            return await HandleApiOperationAsync(async () => {
                await _utilitySvc.RemoveUploadImage(imageName);

                return new ServiceResponse<bool>(true);

            });
        }
        #endregion

        #region WhatsApp

        #endregion

        #region Upload Excel/Csv/Files
        //[AllowAnonymous]
        //[HttpPost]
        //[Route("UplaodFiles")]
        //public async Task<IServiceResponse<bool>> UploadFiles(IFormFile file)
        //{
        //    return await HandleApiOperationAsync(async () =>
        //    {
        //        await _utilitySvc.UploadFiles(file);

        //        return new ServiceResponse<bool>(true);
        //    });
        //}
        #endregion

    }
}
