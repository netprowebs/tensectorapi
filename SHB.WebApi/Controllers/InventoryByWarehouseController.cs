﻿using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.WebApi.Controllers
{
    [Authorize]
    public class InventoryByWarehouseController:BaseController
    {
        private readonly IInventoryByWarehouseService _inventoryByWarehouse;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUserService _userManagerSvc;

        public InventoryByWarehouseController(IInventoryByWarehouseService inventoryByWarehouse, IServiceHelper serviceHelper, IUserService userManagerSvc)
        {
            _inventoryByWarehouse = inventoryByWarehouse;
            _serviceHelper = serviceHelper;
            _userManagerSvc = userManagerSvc;
        }

        #region Inventory By Warehouse
        [HttpPost]
        [Route("Add")]
        public async Task<IServiceResponse<bool>> AddInventoryByWarehouse(InventoryByWarehouseDTO model)
        {
            return await HandleApiOperationAsync(async () => {
                await _inventoryByWarehouse.AddInventoryByWarehouse(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAll")]
        [Route("GetAll/{pageNumber}/{pageSize}")]
        [Route("GetAll/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<InventoryByWarehouseDTO>>> GetAllInventoryByWarehouse(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () => {

                var CustomerType = await _inventoryByWarehouse.GetAllInventoryByWarehouse(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<InventoryByWarehouseDTO>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("Get/{id}")]
        public async Task<IServiceResponse<InventoryByWarehouseDTO>> GetInventoryByWarehouse(Guid id)
        {
            return await HandleApiOperationAsync(async () => {
                var CustomerType = await _inventoryByWarehouse.GetInventoryByWarehouse(id);

                return new ServiceResponse<InventoryByWarehouseDTO>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IServiceResponse<bool>> UpdateInventoryByWarehouse(InventoryByWarehouseDTO model, Guid Id)
        {
            return await HandleApiOperationAsync(async () => {
                await _inventoryByWarehouse.UpdateInventoryByWarehouse(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IServiceResponse<bool>> DeleteInventoryByWarehouse(Guid id)
        {
            return await HandleApiOperationAsync(async () => {
                await _inventoryByWarehouse.DeleteInventoryByWarehouse(id);

                return new ServiceResponse<bool>(true);

            });
        }
        #endregion
    }
}
