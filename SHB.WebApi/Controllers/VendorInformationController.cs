﻿using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SHB.WebApi.Controllers
{
    [Authorize]
    public class VendorInformationController : BaseController
    {

        private readonly IVendorInformationService _vendorInformation;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUserService _userManagerSvc;
        public VendorInformationController(IVendorInformationService vendorInformation, IServiceHelper serviceHelper, IUserService userManagerSvc)
        {
            _vendorInformation = vendorInformation;
            _serviceHelper = serviceHelper;
            _userManagerSvc = userManagerSvc;
        }

        #region Vendor Information
        [HttpPost]
        [Route("AddVendorInfo")]
        public async Task<IServiceResponse<bool>> AddVendorInfo(VendorInformationDTO model)
        {
            return await HandleApiOperationAsync(async () => {
                await _vendorInformation.AddVendorInformation(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllVendorInfos")]
        [Route("GetAllVendorInfos/{pageNumber}/{pageSize}")]
        [Route("GetAllVendorInfos/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<VendorInformation>>> GetAllVendorInfos(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var CustomerType = await _vendorInformation.GetAllVendorInformation(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<VendorInformation>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("GetVendorInfo/{id}")]
        public async Task<IServiceResponse<VendorInformationDTO>> GetVendorInfo(int id)
        {
            return await HandleApiOperationAsync(async () => {
                var CustomerType = await _vendorInformation.GetVendorInformation(id);

                return new ServiceResponse<VendorInformationDTO>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpPut]
        [Route("UpdateVendorInfo/{id}")]
        public async Task<IServiceResponse<bool>> UpdateVendorInfo(VendorInformationDTO model, int Id)
        {
            return await HandleApiOperationAsync(async () => {
                await _vendorInformation.UpdateVendorInformation(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteVendorInfo/{id}")]
        public async Task<IServiceResponse<bool>> DeleteVendorInfo(int id)
        {
            return await HandleApiOperationAsync(async () => {
                await _vendorInformation.DeleteVendorInformation(id);

                return new ServiceResponse<bool>(true);

            });
        }
        #endregion
    }
}
