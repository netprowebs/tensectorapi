﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.WebApi.Utils;

namespace SHB.WebApi.Controllers
{
    [Authorize]
    public class RouteController : BaseController
    {
        private readonly IRouteService _routeService;

        public RouteController(IRouteService routeService)
        {
            _routeService = routeService;
        }

        [HttpGet]
        [Route("Get")]
        [Route("Get/{pageNumber}/{pageSize}")]
        [Route("Get/{pageNumber}/{pageSize}/{query}")]
        public async Task<IServiceResponse<IPagedList<RouteDTO>>> GetRoutes(int pageNumber = 1,
            int pageSize = WebConstants.DefaultPageSize, string query = null)
        {
            return await HandleApiOperationAsync(async () => {
                var routes = await _routeService.GetRoutes(pageNumber, pageSize);

                return new ServiceResponse<IPagedList<RouteDTO>>
                {
                    Object = routes
                };
            });
        }

        [HttpGet]
        [Route("Get/{id}")]
        public async Task<IServiceResponse<RouteDTO>> GetRouteById(int id)
        {
            return await HandleApiOperationAsync(async () => {
                var route = await _routeService.GetRouteById(id);

                return new ServiceResponse<RouteDTO>
                {
                    Object = route
                };
            });
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IServiceResponse<bool>> AddRoute(RouteDTO route)
        {
            return await HandleApiOperationAsync(async () => {
                await _routeService.AddRoute(route);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IServiceResponse<bool>> UpdateRoute(int id, RouteDTO route)
        {
            return await HandleApiOperationAsync(async () => {
                await _routeService.UpdateRoute(id, route);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IServiceResponse<bool>> DeleteRoute(int id)
        {
            return await HandleApiOperationAsync(async () => {
                await _routeService.RemoveRoute(id);

                return new ServiceResponse<bool>(true);
            });
        }
    }
}
