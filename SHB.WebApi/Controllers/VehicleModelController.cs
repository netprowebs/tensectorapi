﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.WebApi.Utils;

namespace SHB.WebApi.Controllers
{
    [Authorize]
    public class VehicleModelController : BaseController
    {
        private readonly IVehicleModelService _vehicleModelService;

        public VehicleModelController(IVehicleModelService vehicleModelService)
        {
            _vehicleModelService = vehicleModelService;
        }

        [HttpGet]
        [Route("Get")]
        [Route("Get/{pageNumber}/{pageSize}")] 
        [Route("Get/{pageNumber}/{pageSize}/{query}")]
        public async Task<IServiceResponse<IPagedList<VehicleModelDTO>>> GetVehicleModels(int pageNumber = 1,
            int pageSize = WebConstants.DefaultPageSize, string query = null)
        {
            return await HandleApiOperationAsync(async () => {
                var vehicleModels = await _vehicleModelService.GetVehicleModels(pageNumber, pageSize);

                return new ServiceResponse<IPagedList<VehicleModelDTO>>
                {
                    Object = vehicleModels
                };
            });
        }

        [HttpGet]
        [Route("Get/{id}")]
        public async Task<IServiceResponse<VehicleModelDTO>> GetVehicleModelById(int id)
        {
            return await HandleApiOperationAsync(async () => {
                var vehicleModel = await _vehicleModelService.GetVehicleModelById(id);

                return new ServiceResponse<VehicleModelDTO>
                {
                    Object = vehicleModel
                };
            });
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IServiceResponse<bool>> AddVehicleModel(VehicleModelDTO vehicleModel)
        {
            return await HandleApiOperationAsync(async () => {
                await _vehicleModelService.AddVehicleModel(vehicleModel);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IServiceResponse<bool>> UpdateVehicleModel(int id, VehicleModelDTO vehicleModel)
        {
            return await HandleApiOperationAsync(async () => {
                await _vehicleModelService.UpdateVehicleModel(id, vehicleModel);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IServiceResponse<bool>> DeleteVehicleModel(int id)
        {
            return await HandleApiOperationAsync(async () => {
                await _vehicleModelService.RemoveVehicleModel(id);

                return new ServiceResponse<bool>(true);
            });
        }
    }
}
