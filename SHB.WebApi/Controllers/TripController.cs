﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.WebApi.Utils;

namespace SHB.WebApi.Controllers
{
    [Authorize]
    public class TripController : BaseController
    {
        private readonly ITripService _tripService;

        public TripController(ITripService tripService)
        {
            _tripService = tripService;
        }

        [HttpGet]
        [Route("Get")]
        [Route("Get/{pageNumber}/{pageSize}")] 
        [Route("Get/{pageNumber}/{pageSize}/{query}")]
        public async Task<IServiceResponse<IPagedList<TripDTO>>> GetTrips(int pageNumber = 1,
            int pageSize = WebConstants.DefaultPageSize, string query = null)
        {
            return await HandleApiOperationAsync(async () => {
                var trips = await _tripService.GetTrips(pageNumber, pageSize);

                return new ServiceResponse<IPagedList<TripDTO>>
                {
                    Object = trips
                };
            });
        }

        [HttpGet]
        [Route("Get/{id}")]
        public async Task<IServiceResponse<TripDTO>> GetTripById(int id)
        {
            return await HandleApiOperationAsync(async () => {
                var trip = await _tripService.GetTripById(id);

                return new ServiceResponse<TripDTO>
                {
                    Object = trip
                };
            });
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IServiceResponse<bool>> AddTrip(TripDTO trip)
        {
            return await HandleApiOperationAsync(async () => {
                await _tripService.AddTrip(trip);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IServiceResponse<bool>> UpdateTrip(int id, TripDTO trip)
        {
            return await HandleApiOperationAsync(async () => {
                await _tripService.UpdateTrip(id, trip);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IServiceResponse<bool>> DeleteTrip(int id)
        {
            return await HandleApiOperationAsync(async () => {
                await _tripService.RemoveTrip(id);

                return new ServiceResponse<bool>(true);
            });
        }
    }
}
