﻿
using IPagedList;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.Core.Entities;
using SHB.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SHB.WebApi.Controllers
{
    public class PurchaseController : BaseController
    {
        private readonly IPurchaseService _purchaseService;
        public PurchaseController(IPurchaseService purchaseService)
        {
            _purchaseService = purchaseService;
        }
        #region PurchaseHeader
        [HttpPost]
        [Route("AddHeader")]
        public async Task<IServiceResponse<bool>> AddPurchaseHeader(AccPurchase model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _purchaseService.AddPurchaseHeader(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetHeader/{id}")]
        public async Task<IServiceResponse<AccPurchase>> GetPurchaseHeaderById(Guid id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var CustomerType = await _purchaseService.GetPurchaseHeaderById(id);

                return new ServiceResponse<AccPurchase>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpPut]
        [Route("UpdateHeader/{id}")]
        public async Task<IServiceResponse<bool>> UpdateHeader(Guid Id, PurchaseHeaderDTO model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _purchaseService.UpdatePurchaseHeader(Id, model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteHeader/{id}")]
        public async Task<IServiceResponse<bool>> RemovePurchaseHeader(Guid id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _purchaseService.RemovePurchaseHeader(id);

                return new ServiceResponse<bool>(true);

            });
        }

        [HttpGet]
        [Route("GetAllHeader")]
        [Route("GetAllHeader/{pageNumber}/{pageSize}")]
        [Route("GetAllHeader/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<AccPurchase>>> GetAllPurchaseHeader(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var CustomerType = await _purchaseService.GetAllPurchaseHeader(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<AccPurchase>>
                {
                    Object = CustomerType
                };
            });
        }
        #endregion

        #region PurchaseDetail

        [HttpPost]
        [Route("AddDetail")]
        public async Task<IServiceResponse<bool>> AddPurchaseDetail(PurchaseDetailDTO model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _purchaseService.AddPurchaseDetail(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetDetail/{id}")]
        public async Task<IServiceResponse<PurchaseDetailDTO>> GetPurchaseDetail(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {  
                var CustomerType = await _purchaseService.GetPurchaseDetail(id);

                return new ServiceResponse<PurchaseDetailDTO>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("GetDetail")]
        [Route("GetDetail/{pageNumber}/{pageSize}")]
        [Route("GetDetail/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<PurchaseDetailDTO>>> GetAllPurchaseDetail(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var CustomerType = await _purchaseService.GetAllPurchaseDetail(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<PurchaseDetailDTO>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("GetDetailUser")]
        [Route("GetDetailUser/{pageNumber}/{pageSize}")]
        [Route("GetDetailUser/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<AccPurchaseDetail>>> GetAllPurchaseDetailByUser(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var CustomerType = await _purchaseService.GetAllPurchaseDetailByUser(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<AccPurchaseDetail>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpPut]
        [Route("UpdateDetail/{id}")]
        public async Task<IServiceResponse<bool>> UpdatePurchaseDetail(PurchaseDetailDTO model, int Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _purchaseService.UpdatePurchaseDetail(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteDetail/{id}")]
        public async Task<IServiceResponse<bool>> DeletePurchaseDetail(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _purchaseService.DeletePurchaseDetail(id);

                return new ServiceResponse<bool>(true);

            });
        }

        #endregion
    }
}
