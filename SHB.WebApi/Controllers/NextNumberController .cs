﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPagedList;
using SHB.Business.Services;
using SHB.Core.Domain.Entities;
using SHB.Core.Entities;
using SHB.WebAPI.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SHB.WebApi.Controllers;
using SHB.WebApi.Utils;
using SHB.Core.Domain.DataTransferObjects;
using System.Text.Json;

namespace LME.WebAPI.Controllers
{
    public class NextNumberController : BaseController//: ControllerBase
    {
        private readonly INextNumberService _NextNumberService;

        public NextNumberController(INextNumberService NextNumberService)
        {
            _NextNumberService = NextNumberService;
        }

        #region NextNumber

        [AllowAnonymous]
        [HttpPost]
        [Route("AddNextNumber")]
        public async Task<IServiceResponse<bool>> AddNextNumber(NextNumberDTO model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _NextNumberService.AddNextNumber(model);

                return new ServiceResponse<bool>(true);
            });
        }


   

        [HttpGet]
        [Route("GetAllNextNumbers")]
        public async Task<IServiceResponse<List<NextNumberDTO>>> GetAllNextNumber(string Name)
        {
            return await HandleApiOperationAsync(async () =>
            {

                var response = await _NextNumberService.GetAllNextNumbers(Name);

                return new ServiceResponse<List<NextNumberDTO>>
                {
                    Object = response
                };
            });
        }

        [HttpGet]
        [Route("GetNextNumber/{id}")]
        public async Task<IServiceResponse<NextNumberDTO>> GetNextNumber(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var resp = await _NextNumberService.GetNextNumber(id);

                return new ServiceResponse<NextNumberDTO>
                {
                    Object = resp
                };
            });
        }



        [HttpPut]
        [Route("UpdateNextNumberById/{Id}")]
        public async Task<IServiceResponse<bool>> UpdateNextNumberById(NextNumberDTO model, int Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _NextNumberService.UpdateNextNumberById(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteNextNumber/{id}")]
        public async Task<IServiceResponse<bool>> DeleteNextNumber(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _NextNumberService.DeleteNextNumber(id);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion





    }
}






