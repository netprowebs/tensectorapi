﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Entities;
using SHB.WebApi.Utils;

namespace SHB.WebApi.Controllers
{
    [Authorize]
    public class DepartmentsController : BaseController
    {
        private readonly IDepartmentsService _Departments;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUserService _userManagerSvc;

        public DepartmentsController(IDepartmentsService Departments, IServiceHelper serviceHelper, IUserService userManagerSvc)
        {
            _Departments = Departments;
            _serviceHelper = serviceHelper;
            _userManagerSvc = userManagerSvc;
        }

        #region Departments
        [HttpPost]
        [Route("AddDepartment")]
        public async Task<IServiceResponse<bool>> AddDepartment(Department model)
        {
            return await HandleApiOperationAsync(async () => {
                await _Departments.AddDepartment(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllDepartments")]
        [Route("GetAllDepartments/{pageNumber}/{pageSize}")]
        [Route("GetAllDepartments/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<Department>>> GetAllDepartments(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () => {

                var Departments = await _Departments.GetAllDepartments(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<Department>>
                {
                    Object = Departments
                };
            });
        }

        [HttpGet]
        [Route("GetDepartment/{id}")]
        public async Task<IServiceResponse<Department>> GetDepartment(int id)
        {
            return await HandleApiOperationAsync(async () => {
                var Departments = await _Departments.GetDepartment(id);

                return new ServiceResponse<Department>
                {
                    Object = Departments
                };
            });
        }

        [HttpPut]
        [Route("UpdateDepartment/{id}")]
        public async Task<IServiceResponse<bool>> UpdateDepartment(Department model, int Id)
        {
            return await HandleApiOperationAsync(async () => {
                await _Departments.UpdateDepartment(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteDepartment/{id}")]
        public async Task<IServiceResponse<bool>> DeleteDepartment(int id)
        {
            return await HandleApiOperationAsync(async () => {
                await _Departments.DeleteDepartment(id);

                return new ServiceResponse<bool>(true);

            });
        }
        #endregion


    }
}
