﻿using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.WebApi.Controllers
{
    [Authorize]
    public class InventoryIssueController : BaseController
    {
        private readonly IInventoryIssueService _inventoryIssue;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUserService _userManagerSvc;

        public InventoryIssueController(IInventoryIssueService inventoryissue, IServiceHelper serviceHelper, IUserService userManagerSvc)
        {
            _inventoryIssue = inventoryissue;
            _serviceHelper = serviceHelper;
            _userManagerSvc = userManagerSvc;
        }

        //Repushing for Mr. Segun
        #region Inventory Requisition Header
        [HttpPost]
        [Route("AddHeader")]
        public async Task<IServiceResponse<bool>> AddRequisitionHeader(RequisitionHeaderDTO model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryIssue.AddRequisitionHeader(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllRequestHeader")]
        [Route("GetAllRequestHeader/{pageNumber}/{pageSize}")]
        [Route("GetAllRequestHeader/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<RequisitionHeaderDTO>>> GetAllRequestHeader(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var CustomerType = await _inventoryIssue.GetAllRequestHeader(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<RequisitionHeaderDTO>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("GetAllHeader")]
        [Route("GetAllHeader/{pageNumber}/{pageSize}")]
        [Route("GetAllHeader/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<RequisitionHeaderDTO>>> GetAllRequisitionHeader(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var CustomerType = await _inventoryIssue.GetAllRequisitionHeader(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<RequisitionHeaderDTO>>
                {
                    Object = CustomerType
                };
            });
        }



        [HttpGet]
        [Route("GetHeader/{id}")]
        public async Task<IServiceResponse<RequisitionHeaderDTO>> GetRequisitionHeader(Guid id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var CustomerType = await _inventoryIssue.GetRequisitionHeader(id);

                return new ServiceResponse<RequisitionHeaderDTO>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpPut]
        [Route("UpdateHeader/{id}")]
        public async Task<IServiceResponse<bool>> UpdateHeader(RequisitionHeaderDTO model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryIssue.UpdateRequisitionHeader(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteHeader/{id}")]
        public async Task<IServiceResponse<bool>> DeleteRequisitionHeader(Guid id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryIssue.DeleteRequisitionHeader(id);

                return new ServiceResponse<bool>(true);

            });
        }


        [HttpPut]
        [Route("InvReqVerify/{id}")]
        public async Task<IServiceResponse<bool>> VerifyRequisition(RequisitionHeaderDTO model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryIssue.VerifyRequisition(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }


        [HttpPut]
        [Route("InvRequisition/{id}")]
        public async Task<IServiceResponse<bool>> ReceiveRequisition(RequisitionHeaderDTO model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryIssue.ReceiveRequisition(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("InvReqApprove/{id}")]
        public async Task<IServiceResponse<bool>> ApproveRequisition(RequisitionHeaderDTO model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryIssue.ApproveRequisition(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("InvReqReturn/{id}")]
        public async Task<IServiceResponse<bool>> ReturnRequisition(RequisitionHeaderDTO model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryIssue.ReturnRequisition(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion

        #region Inventory Requisition Detail
        [HttpPost]
        [Route("AddDetail")]
        public async Task<IServiceResponse<bool>> AddRequisitionDetail(RequisitionDetailDTO model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryIssue.AddRequisitionDetail(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllDetail")]
        [Route("GetAllDetail/{pageNumber}/{pageSize}")]
        [Route("GetAllDetail/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<RequisitionDetailDTO>>> GetAllRequisitionDetail(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var CustomerType = await _inventoryIssue.GetAllRequisitionDetail(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<RequisitionDetailDTO>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("GetDetail/{id}")]
        public async Task<IServiceResponse<List<RequisitionDetailDTO>>> GetRequisitionDetail(Guid id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var CustomerType = await _inventoryIssue.GetRequisitionDetail(id);

                return new ServiceResponse<List<RequisitionDetailDTO>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpPut]
        [Route("UpdateDetail/{id}")]
        public async Task<IServiceResponse<bool>> UpdateRequisitionDetail(RequisitionDetailDTO model, int Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryIssue.UpdateRequisitionDetail(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteDetail/{id}")]
        public async Task<IServiceResponse<bool>> DeleteRequisitionDetail(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryIssue.DeleteRequisitionDetail(id);

                return new ServiceResponse<bool>(true);

            });
        }
        #endregion
    }
}
