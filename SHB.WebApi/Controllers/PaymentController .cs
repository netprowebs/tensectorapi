﻿using IPagedList;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.WebApi.Utils;
using SHB.WebApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SHB.WebApi.Controllers
{
    public class PaymentController : BaseController
    {
        private readonly IPaymentService _paymentSvc;

        public PaymentController(IPaymentService paymentSvc)
        {
            _paymentSvc = paymentSvc;
        }


        #region ProcessPayment
        [HttpPost]
        [Route("ProcessVetPayment")]
        public async Task<IServiceResponse<VetPayResponse>> ProcessVetPayment(List<string> refcode)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var PayDetails = await _paymentSvc.ProcessVetPayment(refcode);
                return new ServiceResponse<VetPayResponse>(PayDetails);
            });
        }


        [HttpPost]
        [Route("CreditWallet")]
        public async Task<IServiceResponse<WalletTransactionDTO>> CreditWallet(PayResponseModel model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var WalletDetails = await _paymentSvc.CreditWallet(model);
                return new ServiceResponse<WalletTransactionDTO>(WalletDetails);
            });
        }

        #endregion

        #region Wallet
        [HttpGet]
        [Route("GetWallet")]
        [Route("GetWallet/{pageNumber}/{pageSize}")]
        [Route("GetWallet/{pageNumber}/{pageSize}/{query}")]
        public async Task<IServiceResponse<IPagedList<WalletDTO>>> GetWallet(int pageNumber = 1,
           int pageSize = WebConstants.DefaultPageSize, string query = null)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var wallet = await _paymentSvc.GetWallet(pageNumber, pageSize);

                return new ServiceResponse<IPagedList<WalletDTO>>
                {
                    Object = wallet
                };
            });
        }

        [HttpGet]
        [Route("GetWallet/{id}")]
        public async Task<IServiceResponse<WalletDTO>> GetWalletById(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var vetBundles = await _paymentSvc.GetWalletById(id);

                return new ServiceResponse<WalletDTO>
                {
                    Object = vetBundles
                };
            });
        }

        [HttpPut]
        [Route("UpdateWallet/{id}")]
        public async Task<IServiceResponse<bool>> UpdateWalletList(int id, WalletDTO walletList)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _paymentSvc.UpdateWallet(id, walletList);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPost]
        [Route("AddWallet")]
        public async Task<IServiceResponse<bool>> AddWalletList(WalletDTO Wallet)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _paymentSvc.AddWallet(Wallet);

                return new ServiceResponse<bool>(true);
            });
        }
        [HttpDelete]
        [Route("DeleteWallet/{id}")]
        public async Task<IServiceResponse<bool>> DeleteWalletList(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _paymentSvc.RemoveWallet(id);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion

        #region WalletTransaction

        [HttpGet]
        [Route("GetAllWalletTrans")]
        [Route("GetAllWalletTrans/{pageNumber}/{pageSize}")]
        [Route("GetAllWalletTrans/{pageNumber}/{pageSize}/{query}")]
        public async Task<IServiceResponse<IPagedList<WalletTransactionDTO>>> GetWalletTransaction(int pageNumber = 1,
                int pageSize = WebConstants.DefaultPageSize, string query = null)
        {
            return await HandleApiOperationAsync(async () => {
                var walletTransaction = await _paymentSvc.GetWalletTransaction(pageNumber, pageSize);

                return new ServiceResponse<IPagedList<WalletTransactionDTO>>
                {
                    Object = walletTransaction
                };
            });
        }

        [HttpGet]
        [Route("GetAllWalletTrans/{id}")]
        public async Task<IServiceResponse<WalletTransactionDTO>> GetWalletTransactionById(Guid id)
        {
            return await HandleApiOperationAsync(async () => {
                var walletTransaction = await _paymentSvc.GetWalletTransactionById(id);

                return new ServiceResponse<WalletTransactionDTO>
                {
                    Object = walletTransaction
                };
            });
        }

 

        [HttpGet]
        [Route("GetUserWalletTrans/{id}")]
        [Route("GetUserWalletTrans/{id}/{pageNumber}/{pageSize}")]
        [Route("GetUserWalletTrans/{id}/{pageNumber}/{pageSize}/{query}")]
        public async Task<IServiceResponse<IPagedList<WalletTransactionDTO>>> GetVetByClient(int id, int pageNumber = 1,
              int pageSize = WebConstants.DefaultPageSize, string query = null)
        {
            return await HandleApiOperationAsync(async () => {
                var walletTransaction = await _paymentSvc.GetWalletByUserId(id, pageNumber, pageSize);

                return new ServiceResponse<IPagedList<WalletTransactionDTO>>
                {
                    Object = walletTransaction
                };
            });
        }


        [HttpPost]
        [Route("AddWalletTrans")]
        public async Task<IServiceResponse<bool>> AddWalletTransaction(WalletTransactionDTO walletTransaction)
        {
            return await HandleApiOperationAsync(async () => {
                await _paymentSvc.AddWalletTransaction(walletTransaction);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("UpdateWalletTrans/{id}")]
        public async Task<IServiceResponse<bool>> UpdateWalletTransaction(Guid id, WalletTransactionDTO walletTransaction)
        {
            return await HandleApiOperationAsync(async () => {
                await _paymentSvc.UpdateWalletTransaction(id, walletTransaction);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteWalletTrans/{id}")]
        public async Task<IServiceResponse<bool>> DeleteWalletTransaction(Guid id)
        {
            return await HandleApiOperationAsync(async () => {
                await _paymentSvc.RemoveWalletTransaction(id);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion
    }
}
