﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPagedList;
using SHB.Business.Services;
using SHB.Core.Domain.Entities;
using SHB.Core.Entities;
using SHB.WebAPI.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SHB.WebApi.Controllers;
using SHB.WebApi.Utils;
using SHB.Core.Domain.DataTransferObjects;
using System.Text.Json;

namespace LME.WebAPI.Controllers
{
    [Authorize]
    public class InventorySetupController : BaseController//: ControllerBase
    {
        private readonly IInventorySetupService _inventorySetupService;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUserService _userManagerSvc;

        public InventorySetupController(IInventorySetupService inventorySetupService, IServiceHelper serviceHelper, IUserService userManagerSvc)
        {
            _inventorySetupService = inventorySetupService;
            _serviceHelper = serviceHelper;
            _userManagerSvc = userManagerSvc;
        }

        #region InventoryAdjustmentType
        //InventoryAdjustmentType
        [HttpPost]
        [Route("AddInventoryAdjustmentType")]
        public async Task<IServiceResponse<bool>> AddInventoryAdjustmentType(InventoryAdjustmentType model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.AddInventoryAdjustmentType(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllInventoryAdjustmentTypes")]
        [Route("GetAllInventoryAdjustmentTypes/{pageNumber}/{pageSize}")]
        [Route("GetAllInventoryAdjustmentTypes/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<InventoryAdjustmentType>>> GetAllInventoryAdjustmentTypes(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var inventoryAdjustmentType = await _inventorySetupService.GetAllInventoryAdjustmentTypes(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<InventoryAdjustmentType>>
                {
                    Object = inventoryAdjustmentType
                };
            });
        }


        [HttpGet]
        [Route("GetInventoryAdjustmentType/{id}")]
        public async Task<IServiceResponse<InventoryAdjustmentType>> GetInventoryAdjustmentType(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var inventoryAdjustmentType = await _inventorySetupService.GetInventoryAdjustmentType(id);

                return new ServiceResponse<InventoryAdjustmentType>
                {
                    Object = inventoryAdjustmentType
                };
            });
        }

        [HttpPut]
        [Route("UpdateInventoryAdjustmentType/{id}")]
        public async Task<IServiceResponse<bool>> UpdateInventoryAdjustmentType(InventoryAdjustmentType model, int Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.UpdateInventoryAdjustmentType(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }


        [HttpDelete]
        [Route("DeleteInventoryAdjustmentType/{id}")]
        public async Task<IServiceResponse<bool>> DeleteInventoryAdjustmentType(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.DeleteInventoryAdjustmentType(id);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion

        #region ItemFamily
        //ItemFamily
        [HttpPost]
        [Route("AddItemFamily")]
        public async Task<IServiceResponse<bool>> AddItemFamily(ItemFamily model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.AddItemFamily(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllItemFamilies")]
        [Route("GetAllItemFamilies/{pageNumber}/{pageSize}")]
        [Route("GetAllItemFamilies/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<ItemFamily>>> GetAllItemFamilies(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var itemFamily = await _inventorySetupService.GetAllItemFamilies(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<ItemFamily>>
                {
                    Object = itemFamily
                };
            });
        }

        [HttpGet]
        //[AllowAnonymous]
        [Route("GetItemFamily/{id}")]
        public async Task<IServiceResponse<ItemFamily>> GetItemFamily(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var itemFamily = await _inventorySetupService.GetItemFamily(id);

                return new ServiceResponse<ItemFamily>
                {
                    Object = itemFamily
                };
            });
        }

        [HttpPut]
        [Route("UpdateItemFamily/{id}")]
        public async Task<IServiceResponse<bool>> UpdateItemFamily(ItemFamily model, int Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.UpdateItemFamily(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteItemFamily/{id}")]
        public async Task<IServiceResponse<bool>> DeleteItemFamily(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.DeleteItemFamily(id);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion

        #region LedgerChartOfAccount
        //LedgerChartOfAccount
        [HttpPost]
        [Route("AddLedgerChartOfAccount")]
        public async Task<IServiceResponse<bool>> AddLedgerChartOfAccount(LedgerChartOfAccount model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.AddLedgerChartOfAccount(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllLedgerChartOfAccounts")]
        [Route("GetAllLedgerChartOfAccounts/{pageNumber}/{pageSize}")]
        [Route("GetAllLedgerChartOfAccounts/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<LedgerChartOfAccount>>> GetAllLedgerChartOfAccounts(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var ledgerChartOfAccount = await _inventorySetupService.GetAllLedgerChartOfAccounts(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<LedgerChartOfAccount>>
                {
                    Object = ledgerChartOfAccount
                };
            });
        }

        [HttpGet]
        [Route("GetLedgerChartOfAccount/{id}")]
        public async Task<IServiceResponse<LedgerChartOfAccount>> GetLedgerChartOfAccount(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var ledgerChartOfAccount = await _inventorySetupService.GetLedgerChartOfAccount(id);

                return new ServiceResponse<LedgerChartOfAccount>
                {
                    Object = ledgerChartOfAccount
                };
            });
        }

        [HttpPut]
        [Route("UpdateLedgerChartOfAccount/{id}")]
        public async Task<IServiceResponse<bool>> UpdateLedgerChartOfAccount(LedgerChartOfAccount model, int Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.UpdateLedgerChartOfAccount(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteLedgerChartOfAccount/{id}")]
        public async Task<IServiceResponse<bool>> DeleteLedgerChartOfAccount(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.DeleteLedgerChartOfAccount(id);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion

        #region next number
        //NextNumber
        [HttpPost]
        [Route("AddNextNumber")]
        public async Task<IServiceResponse<bool>> AddNextNumber(NextNumber model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.AddNextNumber(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllNextNumbers")]
        [Route("GetAllNextNumbers/{pageNumber}/{pageSize}")]
        [Route("GetAllNextNumbers/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<NextNumber>>> GetAllNextNumbers(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var nextNumber = await _inventorySetupService.GetAllNextNumbers(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<NextNumber>>
                {
                    Object = nextNumber
                };
            });
        }

        [HttpGet]
        [Route("GetNextNumber/{id}")]
        public async Task<IServiceResponse<NextNumber>> GetNextNumber(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var nextNumber = await _inventorySetupService.GetNextNumber(id);

                return new ServiceResponse<NextNumber>
                {
                    Object = nextNumber
                };
            });
        }

        [HttpPut]
        [Route("UpdateNextNumber/{id}")]
        public async Task<IServiceResponse<bool>> UpdateNextNumber(NextNumber model, int Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.UpdateNextNumber(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteNextNumber/{id}")]
        public async Task<IServiceResponse<bool>> DeleteNextNumber(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.DeleteNextNumber(id);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion

        #region warehouse bin
        //WarehouseBin
        [HttpPost]
        [Route("AddWarehouseBin")]
        public async Task<IServiceResponse<bool>> AddWarehouseBin(WarehouseBin model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.AddWarehouseBin(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllWarehouseBins")]
        [Route("GetAllWarehouseBins/{pageNumber}/{pageSize}")]
        [Route("GetAllWarehouseBins/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<WarehouseBin>>> GetAllWarehouseBins(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var warehouseBin = await _inventorySetupService.GetAllWarehouseBins(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<WarehouseBin>>
                {
                    Object = warehouseBin
                };
            });
        }

        [HttpGet]
        [Route("GetWarehouseBin/{id}")]
        public async Task<IServiceResponse<WarehouseBin>> GetWarehouseBin(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var warehouseBin = await _inventorySetupService.GetWarehouseBin(id);

                return new ServiceResponse<WarehouseBin>
                {
                    Object = warehouseBin
                };
            });
        }

        [HttpPut]
        [Route("UpdateWarehouseBin/{id}")]
        public async Task<IServiceResponse<bool>> UpdateWarehouseBin(WarehouseBin model, int Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.UpdateWarehouseBin(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteWarehouseBin/{id}")]
        public async Task<IServiceResponse<bool>> DeleteWarehouseBin(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.DeleteWarehouseBin(id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetBinByWarehouseId/{id}")]
        public ServiceResponse<IEnumerable<WarehouseBin>> GetBinByWarehouseId(int id)
        {

            var bins = _inventorySetupService.GetBinByWarehouseId(id);

            return new ServiceResponse<IEnumerable<WarehouseBin>>
            {
                Object = bins
            };

        }
        #endregion

        #region warehouse endpoints
        //Warehouse
        [HttpPost]
        [Route("AddWarehouse")]
        public async Task<IServiceResponse<bool>> AddWarehouse(WarehouseDTO model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.AddWarehouse(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllWarehouses")]
        [Route("GetAllWarehouses/{pageNumber}/{pageSize}")]
        [Route("GetAllWarehouses/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<WarehouseDTO>>> GetAllWarehouses(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var warehouse = await _inventorySetupService.GetAllWarehouses(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<WarehouseDTO>>
                {
                    Object = warehouse
                };
            });
        }

        [HttpGet]
        [Route("GetWarehouse/{id}")]
        public async Task<IServiceResponse<WarehouseDTO>> GetWarehouse(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var warehouse = await _inventorySetupService.GetWarehouse(id);

                return new ServiceResponse<WarehouseDTO>
                {
                    Object = warehouse
                };
            });
        }

        [HttpPut]
        [Route("UpdateWarehouse/{id}")]
        public async Task<IServiceResponse<bool>> UpdateWarehouse(WarehouseDTO model, int Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.UpdateWarehouse(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteWarehouse/{id}")]
        public async Task<IServiceResponse<bool>> DeleteWarehouse(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.DeleteWarehouse(id);

                return new ServiceResponse<bool>(true);

            });
        }


        #endregion

        #region Inventory item
        //InventoryItem
        [HttpPost]
        [Route("AddInventoryItem")]
        public async Task<IServiceResponse<bool>> AddInventoryItem(InventoryItem model)
        {//InventoryItem
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.AddInventoryItem(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllInventoryItems")]
        [Route("GetAllInventoryItems/{pageNumber}/{pageSize}")]
        [Route("GetAllInventoryItems/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<InventoryItem>>> GetAllInventoryItems(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var response = await _inventorySetupService.GetAllInventoryItems(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<InventoryItem>>
                {
                    Object = response
                };
            });
        }

        [HttpGet]
        [Route("GetInventoryItem/{id}")]
        public async Task<IServiceResponse<InventoryItem>> GetInventoryItem(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var resp = await _inventorySetupService.GetInventoryItem(id);

                return new ServiceResponse<InventoryItem>
                {
                    Object = resp
                };
            });
        }

        [HttpPut]
        [Route("UpdateInventoryItem")]
        public async Task<IServiceResponse<bool>> UpdateInventoryItem(InventoryItem model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.UpdateInventoryItem(model);

                return new ServiceResponse<bool>(true);
            });
        }


        [HttpPut]
        [Route("UpdateInventoryItem/{id}")]
        public async Task<IServiceResponse<bool>> UpdateInventoryItem(InventoryItem model, int Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.UpdateInventoryItem(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteInventoryItem/{id}")]
        public async Task<IServiceResponse<bool>> DeleteInventoryItem(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.DeleteInventoryItem(id);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion

        #region Item Categories
        //ItemCategory
        [HttpPost]
        [Route("AddItemCategory")]
        public async Task<IServiceResponse<bool>> AddItemCategory(ItemCategory model)
        {//ItemCategory
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.AddItemCategory(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllItemCategories")]
        [Route("GetAllItemCategories/{pageNumber}/{pageSize}")]
        [Route("GetAllItemCategories/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<ItemCategory>>> GetAllItemCategories(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var resp = await _inventorySetupService.GetAllItemCategories(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<ItemCategory>>
                {
                    Object = resp
                };
            });
        }

        [HttpGet]
        [Route("GetItemCategory/{id}")]
        public async Task<IServiceResponse<ItemCategory>> GetItemCategory(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var resp = await _inventorySetupService.GetItemCategory(id);

                return new ServiceResponse<ItemCategory>
                {
                    Object = resp
                };
            });
        }

        //[HttpPut]
        //[Route("UpdateItemCategory")]
        //public async Task<IServiceResponse<bool>> UpdateItemCategory(ItemCategory model)
        //{
        //    return await HandleApiOperationAsync(async () => {
        //        await _inventorySetupService.UpdateItemCategory(model);

        //        return new ServiceResponse<bool>(true);
        //    });
        //}

        [HttpPut]
        [Route("UpdateItemCategory/{id}")]
        public async Task<IServiceResponse<bool>> UpdateItemCategory(ItemCategory model, int Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.UpdateItemCategory(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteItemCategory/{id}")]
        public async Task<IServiceResponse<bool>> DeleteItemCategory(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.DeleteItemCategory(id);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion

        #region Item Types
        //ItemType
        [HttpPost]
        [Route("AddItemType")]
        public async Task<IServiceResponse<bool>> AddItemType(ItemType model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.AddItemType(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllItemTypes")]
        [Route("GetAllItemTypes/{pageNumber}/{pageSize}")]
        [Route("GetAllItemTypes/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<ItemType>>> GetAllItemTypes(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var resp = await _inventorySetupService.GetAllItemTypes(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<ItemType>>
                {
                    Object = resp
                };
            });
        }

        [HttpGet]
        [Route("GetItemType/{id}")]
        public async Task<IServiceResponse<ItemType>> GetItemType(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var resp = await _inventorySetupService.GetItemType(id);

                return new ServiceResponse<ItemType>
                {
                    Object = resp
                };
            });
        }

        //[HttpPut]
        //[Route("UpdateItemType")]
        //public async Task<IServiceResponse<bool>> UpdateItemType(ItemType model)
        //{
        //    return await HandleApiOperationAsync(async () => {
        //        await _inventorySetupService.UpdateItemType(model);

        //        return new ServiceResponse<bool>(true);
        //    });
        //}

        [HttpPut]
        [Route("UpdateItemType/{id}")]
        public async Task<IServiceResponse<bool>> UpdateItemType(ItemType model, int Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.UpdateItemType(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteItemType/{id}")]
        public async Task<IServiceResponse<bool>> DeleteItemType(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.DeleteItemType(id);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion

        #region Attributes
        //Attributes
        [HttpPost]
        [Route("AddAttribute")]
        public async Task<IServiceResponse<bool>> AddAttribute(Attributes model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.AddAttribute(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllAttributes")]
        [Route("GetAllAttributes/{pageNumber}/{pageSize}")]
        [Route("GetAllAttributes/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<Attributes>>> GetAllAttributes(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var resp = await _inventorySetupService.GetAllAttributes(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<Attributes>>
                {
                    Object = resp
                };
            });
        }

        [HttpGet]
        [Route("GetAttribute/{id}")]
        public async Task<IServiceResponse<Attributes>> GetAttribute(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var resp = await _inventorySetupService.GetAttribute(id);

                return new ServiceResponse<Attributes>
                {
                    Object = resp
                };
            });
        }

        [HttpPut]
        [Route("UpdateAttribute/{id}")]
        public async Task<IServiceResponse<bool>> UpdateAttribute(Attributes model, int Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.UpdateAttribute(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteAttribute/{id}")]
        public async Task<IServiceResponse<bool>> DeleteAttribute(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.DeleteAttribute(id);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion


        #region warehouse bin Type
        //WarehouseBin
        [HttpPost]
        [Route("AddWarehouseBinType")]
        public async Task<IServiceResponse<bool>> AddWarehouseBinType(WarehouseBinType model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.AddWarehouseBinType(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllWarehouseBinTypes")]
        [Route("GetAllWarehouseBinTypes/{pageNumber}/{pageSize}")]
        [Route("GetAllWarehouseBinTypes/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<WarehouseBinType>>> GetAllWarehouseBinTypes(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var warehouseBinType = await _inventorySetupService.GetAllWarehouseBinTypes(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<WarehouseBinType>>
                {
                    Object = warehouseBinType
                };
            });
        }

        [HttpGet]
        [Route("GetWarehouseBinType/{id}")]
        public async Task<IServiceResponse<WarehouseBinType>> GetWarehouseBinType(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var warehouseBinType = await _inventorySetupService.GetWarehouseBinType(id);

                return new ServiceResponse<WarehouseBinType>
                {
                    Object = warehouseBinType
                };
            });
        }

        [HttpPut]
        [Route("UpdateWarehouseBinType/{id}")]
        public async Task<IServiceResponse<bool>> UpdateWarehouseBinType(WarehouseBinType model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.UpdateWarehouseBinType(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteWarehouseBinType/{id}")]
        public async Task<IServiceResponse<bool>> DeleteWarehouseBinType(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventorySetupService.DeleteWarehouseBinType(id);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion


        #region vendors
        ////Vendor
        //[HttpPost]
        //[Route("AddVendor")]
        //public async Task<IServiceResponse<bool>> AddVendor(Vendor model)
        //{
        //    return await HandleApiOperationAsync(async () => {
        //        await _inventorySetupService.AddVendor(model);

        //        return new ServiceResponse<bool>(true);
        //    });
        //}

        //[HttpGet]
        //[Route("GetAllVendors")]
        //[Route("GetAllVendors/{pageNumber}/{pageSize}")]
        //[Route("GetAllVendors/{pageNumber}/{pageSize}/{searchTerm}")]
        //public async Task<IServiceResponse<IPagedList<Vendor>>> GetAllVendors(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        //{
        //    return await HandleApiOperationAsync(async () =>
        //    {

        //        var vendors = await _inventorySetupService.GetAllVendors(pageNumber, pageSize, searchTerm);

        //        return new ServiceResponse<IPagedList<Vendor>>
        //        {
        //            Object = vendors
        //        };
        //    });
        //}

        //[HttpGet]
        //[Route("GetVendor/{id}")]
        //public async Task<IServiceResponse<Vendor>> GetVendor(int id)
        //{
        //    return await HandleApiOperationAsync(async () => {
        //        var vendor = await _inventorySetupService.GetVendor(id);

        //        return new ServiceResponse<Vendor>
        //        {
        //            Object = vendor
        //        };
        //    });
        //}

        //[HttpPut]
        //[Route("UpdateVendor/{id}")]
        //public async Task<IServiceResponse<bool>> UpdateVendor(Vendor model, int Id)
        //{
        //    return await HandleApiOperationAsync(async () => {
        //        await _inventorySetupService.UpdateVendor(model, Id);

        //        return new ServiceResponse<bool>(true);
        //    });
        //}

        //[HttpDelete]
        //[Route("DeleteVendor/{id}")]
        //public async Task<IServiceResponse<bool>> DeleteVendor(int id)
        //{
        //    return await HandleApiOperationAsync(async () => {
        //        await _inventorySetupService.DeleteVendor(id);

        //        return new ServiceResponse<bool>(true);

        //    });
        //}
        #endregion


    }
}