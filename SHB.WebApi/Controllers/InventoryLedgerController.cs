﻿using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.Entities;
using SHB.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.WebApi.Controllers
{
    [Authorize]
    public class InventoryLedgerController:BaseController
    {
        private readonly IInventoryLedgerService _inventoryLedger;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUserService _userManagerSvc;

        public InventoryLedgerController(IInventoryLedgerService inventoryLedger, IServiceHelper serviceHelper, IUserService userManagerSvc)
        {
            _inventoryLedger = inventoryLedger;
            _serviceHelper = serviceHelper;
            _userManagerSvc = userManagerSvc;
        }

        #region Inventory Ledger
        [HttpPost]
        [Route("Add")]
        public async Task<IServiceResponse<bool>> AddInventoryLedger(InventoryLedger model)
        {
            return await HandleApiOperationAsync(async () => {
                await _inventoryLedger.AddInventoryLedger(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAll")]
        [Route("GetAll/{pageNumber}/{pageSize}")]
        [Route("GetAll/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<InventoryLedger>>> GetAllInventoryLedger(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () => {

                var CustomerType = await _inventoryLedger.GetAllInventoryLedger(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<InventoryLedger>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("Get/{id}")]
        public async Task<IServiceResponse<InventoryLedger>> GetInventoryLedger(int id)
        {
            return await HandleApiOperationAsync(async () => {
                var CustomerType = await _inventoryLedger.GetInventoryLedger(id);

                return new ServiceResponse<InventoryLedger>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IServiceResponse<bool>> UpdateInventoryLedger(InventoryLedger model, int Id)
        {
            return await HandleApiOperationAsync(async () => {
                await _inventoryLedger.UpdateInventoryLedger(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IServiceResponse<bool>> DeleteInventoryLedger(int id)
        {
            return await HandleApiOperationAsync(async () => {
                await _inventoryLedger.DeleteInventoryLedger(id);

                return new ServiceResponse<bool>(true);

            });
        }
        #endregion
    }
}
