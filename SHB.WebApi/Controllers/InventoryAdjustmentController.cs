﻿using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.WebApi.Controllers
{
    [Authorize]
    public class InventoryAdjustmentController:BaseController
    {
        private readonly IInventoryAdjustmentService _inventoryAdjustment;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUserService _userManagerSvc;

        public InventoryAdjustmentController(IInventoryAdjustmentService inventoryAdjustment, IServiceHelper serviceHelper, IUserService userManagerSvc)
        {
            _inventoryAdjustment = inventoryAdjustment;
            _serviceHelper = serviceHelper;
            _userManagerSvc = userManagerSvc;
        }

        #region Inventory Adjustment Header
        [HttpPost]
        [Route("AddHeader")]
        public async Task<IServiceResponse<bool>> AddAdjustmentHeader(InventoryAdjustmentsHeaderDTO model)
        {
            return await HandleApiOperationAsync(async () => {
                await _inventoryAdjustment.AddAdjustmentHeader(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllHeader")]
        [Route("GetAllHeader/{pageNumber}/{pageSize}")]
        [Route("GetAllHeader/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<InventoryAdjustmentsHeaderDTO>>> GetAllAdjustmentHeader(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () => {

                var CustomerType = await _inventoryAdjustment.GetAllAdjustmentHeader(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<InventoryAdjustmentsHeaderDTO>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("GetHeader/{id}")]
        public async Task<IServiceResponse<InventoryAdjustmentsHeaderDTO>> GetAdjustmentHeader(Guid id)
        {
            return await HandleApiOperationAsync(async () => {
                var CustomerType = await _inventoryAdjustment.GetAdjustmentHeader(id);

                return new ServiceResponse<InventoryAdjustmentsHeaderDTO>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpPut]
        [Route("UpdateHeader/{id}")]
        public async Task<IServiceResponse<bool>> UpdateHeader(InventoryAdjustmentsHeaderDTO model, Guid Id)
        {
            return await HandleApiOperationAsync(async () => {
                await _inventoryAdjustment.UpdateAdjustmentHeader(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteHeader/{id}")]
        public async Task<IServiceResponse<bool>> DeleteAdjustmentHeader(Guid id)
        {
            return await HandleApiOperationAsync(async () => {
                await _inventoryAdjustment.DeleteAdjustmentHeader(id);

                return new ServiceResponse<bool>(true);

            });
        }


        [HttpPut]
        [Route("InvAdjVerify/{id}")]
        public async Task<IServiceResponse<bool>> VerifyAdjustment(InventoryAdjustmentsHeaderDTO model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryAdjustment.VerifyAdjustment(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }


        [HttpPut]
        [Route("InvAdjustment/{id}")]
        public async Task<IServiceResponse<bool>> ReceiveAdjustment(InventoryAdjustmentsHeaderDTO model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryAdjustment.ReceiveAdjustment(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("InvAdjApprove/{id}")]
        public async Task<IServiceResponse<bool>> ApproveAdjustment(InventoryAdjustmentsHeaderDTO model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryAdjustment.ApproveAdjustment(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("InvAdjReturn/{id}")]
        public async Task<IServiceResponse<bool>> ReturnAdjustment(InventoryAdjustmentsHeaderDTO model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryAdjustment.ReturnAdjustment(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion

        #region Inventory Adjustment Detail
        [HttpPost]
        [Route("AddDetail")]
        public async Task<IServiceResponse<bool>> AddAdjustmentDetail(InventoryAdjustmentsDetailDTO model)
        {
            return await HandleApiOperationAsync(async () => {
                await _inventoryAdjustment.AddAdjustmentDetail(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllDetail")]
        [Route("GetAllDetail/{pageNumber}/{pageSize}")]
        [Route("GetAllDetail/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<InventoryAdjustmentsDetailDTO>>> GetAllAdjustmentDetail(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () => {

                var CustomerType = await _inventoryAdjustment.GetAllAdjustmentDetail(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<InventoryAdjustmentsDetailDTO>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("GetDetail/{id}")]
        public async Task<IServiceResponse<List<InventoryAdjustmentsDetailDTO>>> GetAdjustmentDetail(Guid id)
        {
            return await HandleApiOperationAsync(async () => {
                var CustomerType = await _inventoryAdjustment.GetAdjustmentDetail(id);

                return new ServiceResponse<List<InventoryAdjustmentsDetailDTO>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpPut]
        [Route("UpdateDetail/{id}")]
        public async Task<IServiceResponse<bool>> UpdateAdjustmentDetail(InventoryAdjustmentsDetailDTO model, int Id)
        {
            return await HandleApiOperationAsync(async () => {
                await _inventoryAdjustment.UpdateAdjustmentDetail(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteDetail/{id}")]
        public async Task<IServiceResponse<bool>> DeleteAdjustmentDetail(int id)
        {
            return await HandleApiOperationAsync(async () => {
                await _inventoryAdjustment.DeleteAdjustmentDetail(id);

                return new ServiceResponse<bool>(true);

            });
        }
        #endregion
    }
}
