﻿using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.WebApi.Controllers
{
    [Authorize]
    public class InsuranceSetupsController : BaseController
    {

        private readonly IServiceHelper _serviceHelper;
        private readonly IUserService _userManagerSvc;

        public InsuranceSetupsController(IServiceHelper serviceHelper, IUserService userManagerSvc)
        {
            _serviceHelper = serviceHelper;
            _userManagerSvc = userManagerSvc;
        }

        #region InsuranceBizClass

        #endregion

        #region InsuranceRiskType

        #endregion
    }
}
