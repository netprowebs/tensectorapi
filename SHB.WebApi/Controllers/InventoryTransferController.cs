﻿using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Domain.Entities;
using SHB.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHB.WebApi.Controllers
{
    [Authorize]
    public class InventoryTransferController:BaseController
    {
        private readonly IInventoryTransferService _inventoryTransfer;
        private readonly IServiceHelper _serviceHelper;
        private readonly IUserService _userManagerSvc;

        public InventoryTransferController(IInventoryTransferService inventoryTransfer, IServiceHelper serviceHelper, IUserService userManagerSvc)
        {
            _inventoryTransfer = inventoryTransfer;
            _serviceHelper = serviceHelper;
            _userManagerSvc = userManagerSvc;
        }

        #region Inventory Transfer Header
        [HttpPost]
        [Route("AddHeader")]
        public async Task<IServiceResponse<bool>> AddInventoryTransferHeader(InventoryTransferHeaderDTO model)
        {
            return await HandleApiOperationAsync(async () => {
                await _inventoryTransfer.AddInventoryTransferHeader(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllHeader")]
        [Route("GetAllHeader/{pageNumber}/{pageSize}")]
        [Route("GetAllHeader/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<InventoryTransferHeaderDTO>>> GetAllInventoryTransferHeader(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () => {

                var CustomerType = await _inventoryTransfer.GetAllInventoryTransferHeader(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<InventoryTransferHeaderDTO>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("GetHeader/{id}")]
        public async Task<IServiceResponse<InventoryTransferHeaderDTO>> GetInventoryTransferHeader(Guid id)
        {
            return await HandleApiOperationAsync(async () => {
                var CustomerType = await _inventoryTransfer.GetInventoryTransferHeader(id);

                return new ServiceResponse<InventoryTransferHeaderDTO>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpPut]
        [Route("UpdateHeader/{id}")]
        public async Task<IServiceResponse<bool>> UpdateHeader(InventoryTransferHeaderDTO model, Guid Id)
        {
            return await HandleApiOperationAsync(async () => {
                await _inventoryTransfer.UpdateInventoryTransferHeader(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteHeader/{id}")]
        public async Task<IServiceResponse<bool>> DeleteInventoryTransferHeader(Guid id)
        {
            return await HandleApiOperationAsync(async () => {
                await _inventoryTransfer.DeleteInventoryTransferHeader(id);

                return new ServiceResponse<bool>(true);

            });
        }

        [HttpPut]
        [Route("InvTransVerify/{id}")]
        public async Task<IServiceResponse<bool>> VerifyTransfer(InventoryTransferHeaderDTO model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryTransfer.VerifyTransfer(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }


        [HttpPut]
        [Route("InvTransfer/{id}")]
        public async Task<IServiceResponse<bool>> ReceiveTransfer(InventoryTransferHeaderDTO model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryTransfer.ReceiveTransfer(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("InvTransApprove/{id}")]
        public async Task<IServiceResponse<bool>> ApproveTransfer(InventoryTransferHeaderDTO model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryTransfer.ApproveTransfer(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("InvTransReturn/{id}")]
        public async Task<IServiceResponse<bool>> ReturnTransfer(InventoryTransferHeaderDTO model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _inventoryTransfer.ReturnTransfer(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion

        #region Inventory Transfer Detail
        [HttpPost]
        [Route("AddDetail")]
        public async Task<IServiceResponse<bool>> AddInventoryTransferDetail(InventoryTransferDetailDTO model)
        {
            return await HandleApiOperationAsync(async () => {
                await _inventoryTransfer.AddInventoryTransferDetail(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllDetail")]
        [Route("GetAllDetail/{pageNumber}/{pageSize}")]
        [Route("GetAllDetail/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<InventoryTransferDetailDTO>>> GetAllInventoryTransferDetail(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () => {

                var CustomerType = await _inventoryTransfer.GetAllInventoryTransferDetail(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<InventoryTransferDetailDTO>>
                {
                    Object = CustomerType
                };
            });
        }


        [HttpGet]
        [Route("GetDetail/{id}")]
        public async Task<IServiceResponse<List<InventoryTransferDetailDTO>>> GetInventoryTransferDetail(Guid id)
        {
            return await HandleApiOperationAsync(async () => {
                var CustomerType = await _inventoryTransfer.GetInventoryTransferDetail(id);

                return new ServiceResponse<List<InventoryTransferDetailDTO>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpPut]
        [Route("UpdateDetail/{id}")]
        public async Task<IServiceResponse<bool>> UpdateInventoryTransferDetail(InventoryTransferDetailDTO model, int Id)
        {
            return await HandleApiOperationAsync(async () => {
                await _inventoryTransfer.UpdateInventoryTransferDetail(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteDetail/{id}")]
        public async Task<IServiceResponse<bool>> DeleteInventoryTransferDetail(int id)
        {
            return await HandleApiOperationAsync(async () => {
                await _inventoryTransfer.DeleteInventoryTransferDetail(id);

                return new ServiceResponse<bool>(true);

            });
        }
        #endregion
    }
}
