﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.WebApi.Utils;

namespace SHB.WebApi.Controllers
{
    [Authorize]
    public class VetSetupsController : BaseController
    {
        private readonly IVetSetupsService _vetsetupsService;
        

        public VetSetupsController(IVetSetupsService vetsetupsService)
        {
            _vetsetupsService = vetsetupsService;
         
        }


        #region VetBundles
        [AllowAnonymous]
        [HttpGet]
        [Route("Get")]
        [Route("Get/{pageNumber}/{pageSize}")]
        [Route("Get/{pageNumber}/{pageSize}/{query}")]
        public async Task<IServiceResponse<IPagedList<VetBundlesDTO>>> GetVetBundles(int pageNumber = 1,
                int pageSize = WebConstants.DefaultPageSize, string query = null)
        {
            return await HandleApiOperationAsync(async () => {
                var vetbundles = await _vetsetupsService.GetVetBundles(pageNumber, pageSize);

                return new ServiceResponse<IPagedList<VetBundlesDTO>>
                {
                    Object = vetbundles
                };
            });
        }

        [HttpGet]
        [Route("Get/{id}")]
        public async Task<IServiceResponse<VetBundlesDTO>> GetVetBundlesById(int id)
        {
            return await HandleApiOperationAsync(async () => {
                var vetBundles = await _vetsetupsService.GetVetBundlesById(id);

                return new ServiceResponse<VetBundlesDTO>
                {
                    Object = vetBundles
                };
            });
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IServiceResponse<bool>> AddVetBundles(VetBundlesDTO vetBundles)
        {
            return await HandleApiOperationAsync(async () => {
                await _vetsetupsService.AddVetBundles(vetBundles);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IServiceResponse<bool>> UpdateVetBundles(int id, VetBundlesDTO vetBundles)
        {
            return await HandleApiOperationAsync(async () => {
                await _vetsetupsService.UpdateVetBundles(id, vetBundles);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IServiceResponse<bool>> DeleteVetBundles(int id)
        {
            return await HandleApiOperationAsync(async () => {
                await _vetsetupsService.RemoveVetBundles(id);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion

        #region VetServicesType
        [HttpGet]
        [Route("GetAllVetService")]
        [Route("GetAllVetService/{pageNumber}/{pageSize}")]
        [Route("GetAllService/{pageNumber}/{pageSize}/{query}")]
        public async Task<IServiceResponse<IPagedList<VetServiceDTO>>> GetVetService(int pageNumber = 1,
                int pageSize = WebConstants.DefaultPageSize, string query = null)
        {
            return await HandleApiOperationAsync(async () => {
                var vetService = await _vetsetupsService.GetVetService(pageNumber, pageSize);

                return new ServiceResponse<IPagedList<VetServiceDTO>>
                {
                    Object = vetService
                };
            });
        }

        [HttpGet]
        [Route("GetVetService/{id}")]
        public async Task<IServiceResponse<VetServiceDTO>> GetVetServiceById(int id)
        {
            return await HandleApiOperationAsync(async () => {
                var vetService = await _vetsetupsService.GetVetServiceById(id);

                return new ServiceResponse<VetServiceDTO>
                {
                    Object = vetService
                };
            });
        }

        [HttpPost]
        [Route("AddVetService")]
        public async Task<IServiceResponse<bool>> AddVetService(VetServiceDTO vetService)
        {
            return await HandleApiOperationAsync(async () => {
                await _vetsetupsService.AddVetService(vetService);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("UpdateVetService/{id}")]
        public async Task<IServiceResponse<bool>> UpdateVetService(int id, VetServiceDTO vetService)
        {
            return await HandleApiOperationAsync(async () => {
                await _vetsetupsService.UpdateVetService(id, vetService);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteVetService/{id}")]
        public async Task<IServiceResponse<bool>> DeleteVetService(int id)
        {
            return await HandleApiOperationAsync(async () => {
                await _vetsetupsService.RemoveVetService(id);

                return new ServiceResponse<bool>(true);
            });
        }
        #endregion


        //#region Vet
        //#endregion




    }
}
