﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.WebApi.Utils;

namespace SHB.WebApi.Controllers
{
    [Authorize]
    public class SubRouteController : BaseController
    {
        private readonly ISubRouteService _subRouteService;

        public SubRouteController(ISubRouteService subRouteService)
        {
            _subRouteService = subRouteService;
        }

        [HttpGet]
        [Route("Get")]
        [Route("Get/{pageNumber}/{pageSize}")]
        [Route("Get/{pageNumber}/{pageSize}/{query}")]
        public async Task<IServiceResponse<IPagedList<SubRouteDTO>>> GetSubRoutes(int pageNumber = 1,
            int pageSize = WebConstants.DefaultPageSize, string query = null)
        {
            return await HandleApiOperationAsync(async () => {
                var subRoutes = await _subRouteService.GetSubRoutes(pageNumber, pageSize);

                return new ServiceResponse<IPagedList<SubRouteDTO>>
                {
                    Object = subRoutes
                };
            });
        }

        [HttpGet]
        [Route("Get/{id}")]
        public async Task<IServiceResponse<SubRouteDTO>> GetSubRouteById(int id)
        {
            return await HandleApiOperationAsync(async () => {
                var subRoute = await _subRouteService.GetSubRouteById(id);

                return new ServiceResponse<SubRouteDTO>
                {
                    Object = subRoute
                };
            });
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IServiceResponse<bool>> AddSubRoute(SubRouteDTO subRoute)
        {
            return await HandleApiOperationAsync(async () => {
                await _subRouteService.AddSubRoute(subRoute);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpPut]
        [Route("Update/{id}")]
        public async Task<IServiceResponse<bool>> UpdateSubRoute(int id, SubRouteDTO subRoute)
        {
            return await HandleApiOperationAsync(async () => {
                await _subRouteService.UpdateSubRoute(id, subRoute);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IServiceResponse<bool>> DeleteSubRoute(int id)
        {
            return await HandleApiOperationAsync(async () => {
                await _subRouteService.RemoveSubRoute(id);

                return new ServiceResponse<bool>(true);
            });
        }
    }
}
