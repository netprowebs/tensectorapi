﻿using IPagedList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SHB.Business.Services;
using SHB.Core.Domain.DataTransferObjects;
using SHB.Core.Entities;
using SHB.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SHB.WebApi.Controllers
{
    //[Authorize]
    public class InvoiceController : BaseController
    {
        private readonly IInvoiceService _invoiceService;
        public InvoiceController(IInvoiceService invoiceService)
        {
            _invoiceService = invoiceService;
        }

        #region Invoice Header
        [HttpPost]
        [Route("AddHeader")]
        public async Task<IServiceResponse<bool>> AddInvoiceHeader(InvoiceHeaderDTO model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _invoiceService.AddInvoiceHeader(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllHeader")]
        [Route("GetAllHeader/{pageNumber}/{pageSize}")]
        [Route("GetAllHeader/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<InvoiceHeaderDTO>>> GetAllInvoiceHeader(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = null)
        {
            return await HandleApiOperationAsync(async () =>
            {

                var CustomerType = await _invoiceService.GetAllInvoiceHeader(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<InvoiceHeaderDTO>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("GetHeader/{id}")]
        public async Task<IServiceResponse<InvoiceHeaderDTO>> GetInvoiceHeader(Guid id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var CustomerType = await _invoiceService.GetInvoiceHeader(id);

                return new ServiceResponse<InvoiceHeaderDTO>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpPut]
        [Route("UpdateHeader/{id}")]
        public async Task<IServiceResponse<bool>> UpdateHeader(InvoiceHeaderDTO model, Guid Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _invoiceService.UpdateInvoiceHeader(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteHeader/{id}")]
        public async Task<IServiceResponse<bool>> DeleteInvoiceHeader(Guid id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _invoiceService.DeleteInvoiceHeader(id);

                return new ServiceResponse<bool>(true);

            });
        }

        #endregion

        #region Invoice Detail
        [HttpPost]
        [Route("AddDetail")]
        public async Task<IServiceResponse<bool>> AddInvoiceDetail(InvoiceDetailDTO model)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _invoiceService.AddInvoiceDetail(model);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpGet]
        [Route("GetAllDetail")]
        [Route("GetAllDetail/{pageNumber}/{pageSize}")]
        [Route("GetAllDetail/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<InvoiceDetailDTO>>> GetAllInvoiceDetail(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var CustomerType = await _invoiceService.GetAllInvoiceDetail(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<InvoiceDetailDTO>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("GetDetailByUser")]
        [Route("GetDetailByUser/{pageNumber}/{pageSize}")]
        [Route("GetDetailByUser/{pageNumber}/{pageSize}/{searchTerm}")]
        public async Task<IServiceResponse<IPagedList<InvoiceDetailDTO>>> GetAllInvoiceDetailByUser(int pageNumber = 1, int pageSize = WebConstants.DefaultPageSize, string searchTerm = "")
        {
            return await HandleApiOperationAsync(async () =>
            {

                var CustomerType = await _invoiceService.GetAllInvoiceDetailByUser(pageNumber, pageSize, searchTerm);

                return new ServiceResponse<IPagedList<InvoiceDetailDTO>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpGet]
        [Route("GetDetail/{id}")]
        public async Task<IServiceResponse<List<InvoiceDetailDTO>>> GetInvoiceDetail(Guid id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                var CustomerType = await _invoiceService.GetInvoiceDetail(id);

                return new ServiceResponse<List<InvoiceDetailDTO>>
                {
                    Object = CustomerType
                };
            });
        }

        [HttpPut]
        [Route("UpdateDetail/{id}")]
        public async Task<IServiceResponse<bool>> UpdateInvoiceDetail(InvoiceDetailDTO model, int Id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _invoiceService.UpdateInvoiceDetail(model, Id);

                return new ServiceResponse<bool>(true);
            });
        }

        [HttpDelete]
        [Route("DeleteDetail/{id}")]
        public async Task<IServiceResponse<bool>> DeleteInvoiceDetail(int id)
        {
            return await HandleApiOperationAsync(async () =>
            {
                await _invoiceService.DeleteInvoiceDetail(id);

                return new ServiceResponse<bool>(true);

            });
        }
        #endregion
    }
}
