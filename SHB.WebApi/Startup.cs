using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SHB.Business.Messaging;
using SHB.Business.Messaging.Email;
using SHB.Business.Messaging.Sms;
using SHB.Business.Services;
using SHB.Core.Caching;
using SHB.Core.Configuration;
using SHB.Core.Domain.Entities;
using SHB.Core.Extensions;
using SHB.Core.Utilities;
using SHB.Data.efCore;
using SHB.Data.efCore.Context;
using SHB.Data.UnitOfWork;
using SHB.WebApi.Infrastructure.Services;
using SHB.WebApi.Utils;
using SHB.WebApi.ViewModels;
using SHB.WebAPI.Utils;
using SHB.WebAPI.Utils.Extentions;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using SHB.Core.Entities;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Hosting;

namespace SHB.WebApi
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            #region Identity

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString(WebConstants.ConnectionStringName))
                , ServiceLifetime.Transient);

            services.AddIdentity<User, Role>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Lockout.AllowedForNewUsers = false;
                options.SignIn.RequireConfirmedEmail = true;
                options.User.RequireUniqueEmail = true;
            })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();
            #endregion

            #region Services
            services.AddTransient<DbContext>((_) =>
            {
                var connStr = Configuration.GetConnectionString(WebConstants.ConnectionStringName);
                return new ApplicationDbContext(new DbContextOptionsBuilder<ApplicationDbContext>()
                                         .UseSqlServer(connStr)
                                         .Options);
            });

            services.AddScoped<IUnitOfWork, EfCoreUnitOfWork>();
            services.AddScoped(typeof(IDbContextProvider<>), typeof(UnitOfWorkDbContextProvider<>));
            services.RegisterGenericRepos(typeof(ApplicationDbContext));
                 
            //services.AddScoped<IRouteService, RouteService>();
            //services.AddScoped<IBookingService, BookingService>();
            //services.AddScoped<IManifestService, ManifestService>();
            //services.AddScoped<ISeatManagementService, SeatManagementService>();
            //services.AddScoped<IDiscountService, DiscountService>();
            //services.AddScoped<IDriverService, DriverService>();
            //services.AddScoped<IMtuReports, MtuReportService>();
            //services.AddScoped<ITripAvailabilityService, TripAvailabilityService>();

            //services.AddScoped<IAccountTransactionService, AccountTransactionService>();
            //services.AddScoped<IFareService, FareService>();
            //services.AddScoped<IFareCalendarService, FareCalendarService>();
            //services.AddScoped<IVehicleTripRegistrationService, VehicleTripRegistrationService>();
            //services.AddScoped<IAccountSummaryService, AccountSummaryService>();
            //services.AddScoped<IHireRequestService, HireRequestService>();
            //services.AddScoped<IBookingReportService, BookingReportService>();
            //services.AddScoped<IFeedbackService, FeedbackService>();
            //services.AddScoped<ISubRouteService, SubRouteService>();
            //services.AddScoped<IJourneyManagementService, JourneyManagementService>();
            //services.AddScoped<IManifestService, ManifestService>();


            //services.AddScoped<IHireBusService, HireBusService>();
            //services.AddScoped<IHirePassengerService, HirePassengerService>();
            //services.AddScoped<IWorkshopService, WorkshopService>();
            //services.AddScoped<IGeneralTransactionService, GeneralTransactionService>();

            #region Admin Scope
            services.AddScoped<ICustomerInformationService, CustomerInformationService>();
            services.AddScoped<IWalletService, WalletService>();
            services.AddScoped<IWalletNumberService, WalletNumberService>();
            services.AddScoped<ICustomerTypesService, CustomerTypesService>();
            services.AddScoped<IVendorInformationService, VendorInformationService>();
            services.AddScoped<IVendorTypesService, VendorTypesService>();
            services.AddScoped<ICompanyInfo, CompanyInfoService>();
            services.AddScoped<IVendorTypesService, VendorTypesService>();
            services.AddScoped<IVehicleModelService, VehicleModelService>();
            services.AddScoped<IVehicleMakeService, VehicleMakeService>();
            services.AddScoped<IVehicleService, VehicleService>();
            services.AddScoped<IRegionService, RegionService>();
            services.AddScoped<IStateService, StateService>();
            services.AddScoped<ITerminalService, TerminalService>();
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<ICouponService, CouponService>();
            services.AddScoped<IReferralService, ReferralService>();
            services.AddScoped<ICouponService, CouponService>();
            services.AddScoped<IErrorCodeService, ErrorCodeService>(); 
            services.AddScoped<IDepartmentsService, DepartmentsService>();
            services.AddScoped<IPaymentService, PaymentService>();
            services.AddScoped<IFeedbackService, FeedBackService>();
            services.AddScoped<IUtilityService, UtilityService>();
            services.AddScoped<INextNumberService, NextNumberService>();
            #endregion
            #region Inventory Scope
            services.AddScoped<IInventoryTransactionsService, InventoryTransactionsService>();
            services.AddScoped<IInventoryIssueService, InventoryIssueService>();
            services.AddScoped<IInventoryByWarehouseService, InventoryByWarehouseService>();
            services.AddScoped<IInventoryLedgerService, InventoryLedgerService>();
            services.AddScoped<IInventoryTransferService, InventoryTransferService>();
            services.AddScoped<IInventoryAdjustmentService, InventoryAdjustmentService>();
            services.AddScoped<IInventorySetupService, InventorySetupService>();
            #endregion
            #region Security Scope
            services.AddScoped<IVetSetupsService, VetSetupsService>();
            services.AddScoped<IVetTransactService, VetTransactService>();
            #endregion
            #region Transport Scope
            services.AddScoped<IRouteService, RouteService>();
            services.AddScoped<ITripService, TripService>();
            services.AddScoped<ISubRouteService, SubRouteService>();
            services.AddScoped<IFareService, FareService>();
            #endregion
            #region Acounting Scope
            services.AddScoped<IPurchaseService, PurchaseService>();
            services.AddScoped<IInvoiceService, InvoiceService>();
            services.AddScoped<IReceiptService, ReceiptService>();
            #endregion
            #region Cargo Scope
            #endregion
            #region Farm Scope
            #endregion
            #region Health Scope
            #endregion
            #region Insurance Scope
            #endregion
            #region OilNGas Scope
            #endregion
            #region Real Estate Scope
            #endregion


            #endregion

            services.Configure<JwtConfig>(options =>
                        Configuration.GetSection(WebConstants.Sections.AuthJwtBearer).Bind(options));

            //services.Configure<BookingConfig>(options =>
            //           Configuration.GetSection(WebConstants.Sections.Booking).Bind(options));

            services.Configure<AppConfig>(options =>
                     Configuration.GetSection(WebConstants.Sections.App).Bind(options));

            services.Configure<SmtpConfig>(options =>
                     Configuration.GetSection(WebConstants.Sections.Smtp).Bind(options));

            services.Configure<PaymentConfig.Paystack>(options =>
                     Configuration.GetSection(WebConstants.Sections.Paystack).Bind(options));

            services.Configure<DataProtectionTokenProviderOptions>(o =>
                     o.TokenLifespan = TimeSpan.FromHours(3));

            #region Auth
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                var jwtConfig = new JwtConfig();

                Configuration.Bind(WebConstants.Sections.AuthJwtBearer, jwtConfig);

                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ClockSkew = TimeSpan.FromMinutes(3),
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtConfig.SecurityKey)),
                    ValidateIssuer = true,
                    ValidIssuer = jwtConfig.Issuer,
                    ValidateLifetime = true,
                    ValidateAudience = false
                };
                x.Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = context =>
                    {
                        if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                        {
                            context.Response.Headers.Add("Token-Expired", "true");
                        }
                        return Task.CompletedTask;
                    }
                };
            });

            //#endregion

            //#region Auth
            services.AddAuthorization(options =>
            {
                SetupPolicies(options);
            });

            services.AddControllers();
            services.AddCors();
            services.AddDistributedMemoryCache();

            #endregion


            services.AddHttpContextAccessor();
            services.AddTransient<IServiceHelper, ServiceHelper>();
            services.AddSingleton<ITokenService, TokenService>();
            services.AddSingleton<ICacheManager, MemoryCacheManager>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<IMailService, SmtpEmailService>();
            services.AddTransient<ISMSService, SMSService>();
            services.AddTransient<IWebClient, WebClient>();
            services.AddSingleton<IGuidGenerator>((s) => SequentialGuidGenerator.Instance);
            services.AddTransient<IAppMenu, AppMenuService>();


            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
                .AddNewtonsoftJson(o =>
                {
                    o.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    o.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                });

            //Environment.SetEnvironmentVariable("AWS_ACCESS_KEY_ID", Configuration["AWS:AccessKey"]);
            //Environment.SetEnvironmentVariable("AWS_SECRET_ACCESS_KEY", Configuration["AWS:SecretKey"]);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Shared API(sMART View)",

                });
                c.AddSecurityDefinition(JwtBearerDefaults.AuthenticationScheme, new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Token Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement {
                       {
                         new OpenApiSecurityScheme
                         {
                           Reference = new OpenApiReference
                           {
                             Type = ReferenceType.SecurityScheme,
                             Id = "Bearer"
                           }
                          },
                          new string[] { }
                        }
                      });
            });


            //services.AddHangfire(configuration => configuration
            //    .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
            //    .UseSimpleAssemblyNameTypeSerializer()
            //    .UseRecommendedSerializerSettings()
            //    .UseSqlServerStorage(Configuration.GetConnectionString("Database"), new SqlServerStorageOptions
            //    {
            //        CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
            //        SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
            //        QueuePollInterval = TimeSpan.Zero,
            //        UseRecommendedIsolationLevel = true,
            //        DisableGlobalLocks = true
            //    }));

            //services.AddHangfireServer();
            //services.AddScoped<IRecurringJob, RecurringJob>();
        }

        private static void SetupPolicies(Microsoft.AspNetCore.Authorization.AuthorizationOptions options)
        {
            options.AddPolicy("Manage Customer", policy =>
                 policy.RequireClaim("Permission", PermissionClaimsProvider.ManageCustomer.Value));

            options.AddPolicy("Manage Employee", policy =>
                 policy.RequireClaim("Permission", PermissionClaimsProvider.ManageEmployee.Value));

            options.AddPolicy("Manage Report", policy =>
                policy.RequireClaim("Permission", PermissionClaimsProvider.ManageReport.Value));

            options.AddPolicy("Manage State", policy =>
                policy.RequireClaim("Permission", PermissionClaimsProvider.ManageState.Value));

            options.AddPolicy("Manage Region", policy =>
                policy.RequireClaim("Permission", PermissionClaimsProvider.ManageRegion.Value));

            options.AddPolicy("Manage HireBooking", policy =>
                policy.RequireClaim("Permission", PermissionClaimsProvider.ManageHireBooking.Value));

            options.AddPolicy("Manage Vehicle", policy =>
                policy.RequireClaim("Permission", PermissionClaimsProvider.ManageVehicle.Value));

            options.AddPolicy("Manage Terminal", policy =>
              policy.RequireClaim("Permission", PermissionClaimsProvider.ManageTerminal.Value));

            options.AddPolicy("Manage Route", policy =>
              policy.RequireClaim("Permission", PermissionClaimsProvider.ManageRoute.Value));

            options.AddPolicy("Manage Trip", policy =>
              policy.RequireClaim("Permission", PermissionClaimsProvider.ManageTrip.Value));
        }

        public void Configure(IApplicationBuilder app,
            // IRecurringJobManager recurringJobManager,
            IServiceProvider serviceProvider/*, ILoggerFactory loggerFactory*/)
        {
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            if (Environment.IsDevelopment())
            {
                UserSeed.SeedDatabase(app);
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseCors(x =>
            {
                //x.WithOrigins(Configuration["App:CorsOrigins"]
                //  .Split(",", StringSplitOptions.RemoveEmptyEntries)
                //  .Select(o => o.RemovePostFix("/"))
                //  .ToArray())
                //x.WithOrigins("http://localhost:4200", "http://localhost:5001", "https://one.wo.vet.seclot.com",
                //"https://dev.vet.seclot.com", "http://vet.surge.sh", "https://vet.seclot.com","https://app.mailjet.com","https://api.mailjet.com")
                x.AllowAnyOrigin()
            .AllowAnyMethod()
             .AllowAnyHeader();
            });



            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                });

            app.UseSwagger(
            c =>
            {
                c.RouteTemplate = "docs/{documentName}/docs.json";
            });
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(Configuration["app:serverrootaddress"].EnsureEndsWith('/') + "docs/v1/docs.json", "Shared Data Vet");
                c.RoutePrefix = "";

            });

            //app.UseHangfireDashboard();
            //app.UseHangfireServer();
            //recurringJobManager.AddOrUpdate(
            //    "Run every minute",
            //    //() => serviceProvider.GetService<IRecurringJob>().BackgroundJob(),
            //    () => serviceProvider.GetService<IBookingService>().VerifyPaystack(),
            //    "*/5 * * * *");
            //app.UseFileServer();

        }
    }
}
